#!/usr/bin/python
import os, re, sys, copy
from cpblUtilities.configtools import readConfigFile, read_hierarchy_of_config_files

"""
This module provides dicts paths and defaults, which contain any parameters needed by multiple other modules.
These parameters are generally unchanging over time, but may vary from one installation/environment to another.


 - Specify structure of file.
 - Load cascaded values from config files.
 - Then rearrange as we need to put them into the dict arrays paths and defaults.
"""
config_file_structure={
    'paths': [
        'usintersections'
        'usparcels',
        'working',
        'otherinput',
        'parcel',
        'groundtruth',
        'download',
        'input',
        'paper2014',
        'paper2017',
        'graphics',
        'outputdata',
        'output',
        'tex',
        'scratch',
        'scratch_history',
        'bin',
        'python_utils_path',
        ],
    'defaults': [
        ('rdc',bool),
        'mode',
        ],
    'server': [
        'postgres_host',
        'postgres_role',
        'postgres_db',
        ('postgres_prompt_for_password',bool),
        ('parallel',bool),
        ('graphics_ok',bool),
        'stataVersion',
    ],
    'OSM data': [
        'osmSchema',
        'processingSchema',
        'seg_table',
        'junc_table',
        'nodebuffer_tables',
        'buffer_tables',
        ('continents','commasep'),
        ],
    'OSM history': [],
    'OSM analysis': [
        'analysisSchema',
        'cluster_tables',
        'edge_tables',
        ('includeExtraWays', 'commasep'),
#        ('includeNoCarWays',bool),
#        ('includeTrackService',bool),
        ('clusterRadii','commasep'),
#        ('neighbourRadius1',int),
#        ('neighbourRadius2',int),
        'boundarySchema',
        'citiesSchema',
        ('roadgraphChunkSize', int),
        ('max_country_serial_nodes', int),
        ('GADM_neighbourRadius',int), 
        ('neighbourRadiusRes',int), #=500
        ('neighbourRadiusMax',int), #=3000
        ('sampling_floor',int),
        ('max_nodes',int),
        ('srid_equalarea', int),
        ('srid_equaldistance', int),
        ('srid_viewing', int),        
        ('srid_compromise', int),
        ('urban_pctile',int),
        'PCA_coefficients_from',
        ],
    'rasters': [
        'rasterSchema',
        ('ghsl',bool),
        ('landscan',bool),
        ],
    'tests': [
        ('verbose',bool),
        ('test_continents','commasep'),
        ('sample_size',int),
        ('ignore','commasep'),
        ],
    'gadm': [
        'GADM_VERSION',
        #'GADM_TABLE',
        #'GADM_FILE'
        ],
    }


def update_paths_to_be_specific_to_database(defaults, psqldb=None):
    # This is a separate function because it may be called from the outside. e.g. osm_master may change the default database, in which case these need to be updated.
    # Do NOT mkdir folders if they include a NULL database setting.
    #
    # Update: this can now be called for other defaults. E.g. if you import pystata or cpblUtilities, you can run this on them.
    # So there are now two calling methods:
    # update_paths_to_be_specific_to_database(defaults, psqldb) # For external modules
    # update_paths_to_be_specific_to_database(defaults)   # For osm modules
    
    if psqldb is None:
        psqldb = defaults['server']['postgres_db']
    createFolders = not (  psqldb in ['None',None] or '_NULL_' in psqldb )
    if not createFolders:
        print('  -- (Not creating db-specific folders until db is updated using the command line..) -- ')
    for folder in ['scratch','working','output','tex']:
        paths=defaults['paths']
        assert paths['base'+folder].endswith('/')
        baseExists=os.path.exists(paths['base'+folder])
        paths[folder]=paths['base'+folder]+psqldb+'/'
        if createFolders and baseExists and not os.path.exists(paths[folder]): os.mkdir(paths[folder])
        print((folder+' [MODFIED:] = '+paths[folder]))
    if 1: #paths['graphics'] == paths['baseoutput']+'graphics/':
        paths['graphics']=paths['baseoutput']+psqldb+'/graphics/'
        if createFolders and not os.path.exists(paths['graphics']): os.mkdir(paths['graphics'])
        print(('graphics'+' [MODFIED:] = '+paths['graphics']))

    return(defaults)

    
# The file config-template.cfg contains an example of a file which should be renamed config.cfg

def main():
    """
    """
    VERBOSE=True
    localConfigFile=os.getcwd()+'/config.cfg'
    #localConfigTemplateFile=os.getcwd()+'/config-template.cfg' # Why is this one not included in osm_config, here?
    repoPath=os.path.abspath(os.path.dirname(__file__ if __file__ is not None else '.'))
    
    # Change directory to the osm folder, ie location of this module. That way, we always have the osm config.cfg file as local, which means other utlilities using config.cfg will find the right one.
    path = os.path.abspath(__file__)
    dir_path = os.path.dirname(path)
    if 'bin/osm' not in os.getcwd():
        os.chdir(dir_path)

    repoFile=(repoPath if repoPath else '.')+'/config.cfg'
    repoTemplateFile=(repoPath if repoPath else '.')+'/config-template.cfg'
    if 0:
      # Is there a config file in the local directory?
      if os.path.exists(localConfigFile):
          configDict=readConfigFile(localConfigFile)
      # Is there a config file in the local directory?    
      elif os.path.exists(repoFile):
          configDict=readConfigFile(repoFile)
      else:
          raise Exception("Cannot find config.cfg file in local path ("+localConfigFile+") nor in OSM repo ("+repoFile+").  Copy "+repoTemplateFile+" to config.cfg and edit it for your environment"  )

      #    templateDict=readConfigFile(repoTemplateFile)

    print('\nosm_config setting defaults:')
    merged_dictionary=read_hierarchy_of_config_files([
        repoTemplateFile,
        repoFile,
        localConfigFile,
    ], config_file_structure)

    # Now impose our structure
    defaults=dict([[kk,vv] for kk,vv in list(merged_dictionary.items()) if kk in ['rdc','mode']])
    paths= merged_dictionary['paths']
    paths.update(dict(basescratch=paths['scratch'],
                      baseworking=paths['working'],
                      basetex=paths['tex'],
                      baseoutput=paths['output']))
    defaults.update(dict(paths=merged_dictionary['paths'],
                         osm=merged_dictionary['OSM data'],
                        server=merged_dictionary['server'],
                        tests=merged_dictionary['tests'],
                        gadm= GADM_params( merged_dictionary['gadm']['GADM_VERSION'] ),
                  ))
    defaults['osm'].update(merged_dictionary['OSM analysis'])
    defaults['osm'].update(merged_dictionary['rasters'])
    if defaults['osm']['PCA_coefficients_from'] not in ['hardcoded', 'estimate']:
        assert os.path.exists(defaults['osm']['PCA_coefficients_from'])

    #dsetset(defaults,('osm','schemas','osm'),dgetget(merged_dictionary,('OSM data','osmSchema'),None))
    #dsetset(defaults,('osm','schemas','analysis'),dgetget(merged_dictionary,('OSM data','osmSchema'),None))
    # Fiddle with scratch and working paths so that they are specific to the DB being used (but shared across users working with that DB):
    defaults=update_paths_to_be_specific_to_database(defaults)
    
    defaults['osm']['clusterRadius'] = None if not len(defaults['osm']['clusterRadii'])==1 else  defaults['osm']['clusterRadii'][0] # Issue#275: Offer scalar version since we so often want it.
  
    return(defaults)



def GADM_params(gadmVersion):
    "Return dict of GADM defaults based on a GADM version"
    assert gadmVersion in ['2.8','3.6', 'fake']
    gvn = gadmVersion.replace('.','')
    return  dict(GADM_VERSION=gadmVersion,
                 VERSION=gadmVersion,
                 # Name of full GADM table. Can be changed to create a separate, customized
                 # version of the GADM data.
                 GADM_TABLE='gadm'+gvn,
                 TABLE='gadm'+gvn,
                 # Name of the shapefile used to load GADM data. Relative to the input path.
                 GADM_FILE='other/GADM-global-boundaries/gadm'+gvn+'.shp',
                 FILE='other/GADM-global-boundaries/gadm'+gvn+'.shp',
                 GADM_ID_LEVELS = ['iso', 'id_1', 'id_2', 'id_3', 'id_4', 'id_5'],
                 ID_LEVELS = ['iso', 'id_1', 'id_2', 'id_3', 'id_4', 'id_5']
                 )


def parse_common_docopt_options(defs, arguments):
        if arguments['--cluster-radius']:
            defs['osm']['clusterRadii'] = [arguments['--cluster-radius']]
        if arguments['--db']:
            defs['server']['postgres_db'] = arguments['--db']
            defs = update_paths_to_be_specific_to_database(defs) # But this also needs to be done fr pystata, cpblUtiltiies, etc.
        #skipConflictChecks = arguments['--skip-conflict-checks']
        if '--verbose' in arguments:
            defs['verbose'] = arguments['--verbose']
        if arguments.get('--excludeExtraWays', False) == True:
            defs['osm']['includeExtraWays'] = []
            print("  Setting includeExtraWays to  []")
        #if '--includeExtraWays' in arguments:
        #    assert arguments['--includeExtraWays'] in ['bike,foot,service','bike,foot,service,track',False]
        #    if arguments['--includeExtraWays'] != False:
        #        not_impmlemented_yet
        #        defs['osm']['includeExtraWays'] = ['bike','foot','service']
        #        print("  Setting includeExtraWays to  ['bike','foot','service']")
        if arguments['--serial']:
            defs['server']['parallel'] = False
        if arguments['--continent']:
            knownContinents = 'australiaoceania,centralamerica,africa,southamerica,northamerica,asia,europe,hell,medieval,grid,monaco,macedonia,planet,fakeplanet'.split(
                ',')
            if arguments['--continent'].strip() not in knownContinents:
                print(('\n !!!! That continent is unknown. Probably you want to try one of the following: \n  ' +
                      '\n  '.join(knownContinents)))
            defs['osm']['continents'] = [arguments['--continent'].strip()]

        if '--gadm' in arguments and arguments['--gadm']:
            og = str(defs['gadm'])
            defs['gadm'].update(GADM_params(arguments['--gadm']))
            print((' GADM defaults changed from {} to {}'.format(og, defs['gadm'])))

            
        forceUpdate = arguments['--force-update'] == True
        return forceUpdate



defaults=main()
paths=defaults['paths']
if 'python_utils_path' in paths:
    sys.path.append(paths['python_utils_path'])

