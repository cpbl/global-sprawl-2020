#!/usr/bin/python


"""Series of tools to work with raster data
Specifically Landscan and GHSL"""
import os, math
import numpy as np
import pandas as pd
from osm_config import paths,defaults
import postgres_tools as psqlt
import osmTools as osmt
from osgeo import gdal
from cpblUtilities import shelfSave,shelfLoad

rasterSchema = defaults['osm']['rasterSchema'] # May not change after config read
TILESIZE = 30   # each tile is 30x30 arcseconds ("pixels"?). Consistent across all rasters, so hardcode here

class rasterCon(psqlt.dbConnection):
    """  Inherit postgres stuff from dbConnection and add raster-specific methods.  
    Allow for null logger, since this is likely being called in parallel, without a logger specified."""
    def __init__(self, name=None, schema=rasterSchema, verbose=False, host=None, curType='DictCursor', logger=None, command=None):
        self.logger=logger
        psqlt.dbConnection.__init__(self,schema=schema,verbose=verbose,host=host,curType=curType,logger=logger, command=command)
        if name not in self.list_tables(schema='rasterdata'):
            self.logprint('Warning: raster %s does not currently exist' %name)
        self.name = name.lower()
        self.schema = schema.lower()
        self.alignments = {}
    
    def get_noDataValues(self):
        """Returns no data value, or a list if there is more than one band"""
        if not(hasattr(self, 'noDataValues')):
            bands = list(range(1, self.get_nBands()+1))
            cmd = 'SELECT '+', '.join(['ST_BandNoDataValue(rast, '+str(bb)+')' for bb in bands])+' FROM '+self.name+' LIMIT 1;'
            result = self.execfetch(cmd)[0]
            self.noDataValues = result[0] if len(result)==1 else dict([(ii,result[ii-1]) for ii in bands])
        return self.noDataValues
            
    def get_bbox(self):
        """Returns  ((MINX, MINY), (MINX, MAXY), (MAXX, MAXY), (MAXX, MINY), (MINX, MINY))"""
        if not(hasattr(self, 'bbox')):
            self.get_tileSizeDegrees()
            assert self.tileSizeDegrees[1]>0 # not set up to deal with tiles that go from right to left
            cmd = 'SELECT MIN(ST_UpperLeftX(rast)), MAX(ST_UpperLeftX(rast)), MIN(ST_UpperLeftY(rast)), MAX(ST_UpperLeftY(rast))  FROM '+self.name+';'
            xmin,xmax,ymin,ymax = self.execfetch(cmd)[0]
            self.xmin = xmin
            self.xmax = xmax+self.tileSizeDegrees[1]
            self.ymin = ymin if self.tileSizeDegrees[0] > 0 else ymin+self.tileSizeDegrees[0]
            self.ymax = ymax if self.tileSizeDegrees[0] < 0 else ymax+self.tileSizeDegrees[0]
            self.bbox = ((self.xmin, self.ymin), (self.xmin, self.ymax), (self.xmax, self.ymax), (self.xmax, self.ymin), (self.xmin, self.ymin))
        return self.bbox
        
    def get_scale(self):
        """Returns tuple of scaleY and scaleX"""
        if not(hasattr(self, 'scale')): 
            self.scale = tuple(self.execfetch('SELECT ST_ScaleY(rast), ST_ScaleX(rast) FROM '+self.name+' LIMIT 1')[0])
        assert self.scale[0]<0 and self.scale[1]>0   # They don't have to be, but we need to check these functions work with inverted scales
        return self.scale
        
    def get_tileSizeDegrees(self):
        """Returns height first, width second as tuple"""
        if self.getSRID()!=4326: raise Exception('rasterCon().get_tileSizeDegrees() only deals with SRID 4326 for now')
        if not(hasattr(self, 'tileSizeDegrees')):
            # avoid floating point precision issues by doing this in PostGIS
            cmd = 'SELECT ST_Height(rast)*ST_ScaleY(rast), ST_Width(rast)*ST_ScaleX(rast) FROM '+self.name+' LIMIT 1;'
            self.tileSizeDegrees = tuple(self.execfetch(cmd)[0])  
        return self.tileSizeDegrees
                
    def get_tileSizePixels(self):
        if not(hasattr(self, 'tileSizePixels')):
            cmd = 'SELECT ST_Height(rast), ST_Width(rast) FROM '+self.name+' LIMIT 1;'
            self.tileSizePixels = tuple(self.execfetch(cmd)[0])
        return self.tileSizePixels
    
    def get_nTiles(self):
        if not(hasattr(self, 'nTiles')):
            self.nTiles = self.execfetch('SELECT COUNT(*) FROM '+self.name+';')[0][0]
        return self.nTiles
            
    def get_nTilesYX(self):
        """Returns tuple of rows and columns"""
        th, tw = self.get_tileSizeDegrees()
        bbox = self.get_bbox()
        nRows = abs(int((bbox[1][1]-bbox[0][1])/th))
        nCols = abs(int((bbox[2][0]-bbox[0][0])/tw))
        self.nTilesXY = (nRows, nCols)
        return self.nTilesXY

    def get_nBands(self):
        if not(hasattr(self, 'nBands')): self.nBands = self.execfetch('SELECT ST_NumBands(rast)  FROM '+self.name+' LIMIT 1')[0][0]
        return self.nBands    
    
    def get_lonLat(self, lonlat=None, band=1):
        if self.getSRID()!=4326: raise Exception('rasterCon().get_lonLat() only deals with SRID 4326 for now')
        if isinstance(lonlat, list): lonlat = tuple(lonlat)
        assert isinstance(lonlat, tuple)   
        cmd = """WITH p AS (SELECT ST_SetSRID(ST_MakePoint%(lonlat)s, 4326) pt)                  
                 SELECT ST_Value(rast,%(band)s,pt) FROM %(name)s, p 
                 WHERE ST_Intersects(rast,pt);""" % dict(name=self.name, band=band, lonlat=str(lonlat))
        result = [rr[0] for rr in self.execfetch(cmd) if rr[0] is not None]
        return np.nan if result==[] else result[0] if len(result)==1 else result
    
    def sameAlignnment(self, raster2):
        if not(raster2 in self.alignments):
            cmd ='SELECT DISTINCT ST_SameAlignment(r1.rast,r2.rast) FROM '+self.name+' r1, '+raster2+' r2 WHERE r1.rid=r2.rid;'
            result = self.execfetch(cmd)[0]
            self.alignments.update({raster2: (result[0] is True and len(result)==1)})
        return self.alignments[raster2]
     
    def getSRID(self):
        if not(hasattr(self, 'srid')):
            srid = self.execfetch("SELECT srid from raster_columns where r_table_name='"+self.name+"';")
            if srid:
                self.srid = srid[0][0]
            else: # table not yet defined
                self.srid = None
        return self.srid
        
    def addSpatialIndex(self):
        self.execute('DROP INDEX IF EXISTS '+self.name+'_spat_idx;')
        self.execute('CREATE INDEX '+self.name+'_spat_idx ON '+self.name+' USING gist (ST_ConvexHull(rast));')
        return
        
    def addConstraints(self, constraints=None, setall=None, setAllExceptExtent=None):
        # Raster constraints make sure all tiles have the same srid, scale, etc. See http://postgis.net/docs/manual-2.2/RT_AddRasterConstraints.html
        assert (constraints is not None) ^ (setall is not None) ^ (setAllExceptExtent is not None)
        if isinstance(constraints, str): constraints = [constraints]
        allConstraints = ['srid','scale_x','scale_y','blocksize_x','blocksize_y','same_alignment','regular_blocking','num_bands','pixel_types','nodata_values','out_db','extent']
        if setall:
            constraints = allConstraints
        if setAllExceptExtent:
            constraints = allConstraints[:-1]
        cmd = "SELECT AddRasterConstraints('%(schema)s'::name, '%(name)s'::name, 'rast'::name, "%dict(schema=self.schema, name=self.name)
        cmd+=", ".join([cc+":=True" if cc in constraints else cc+":=False" for cc in allConstraints ])+");"
        self.execute(cmd)
        return
    
    def dropConstraints(self, constraints=None, dropall=None):
        assert (dropall is None) ^ (constraints is None)
        if dropall:
            cmd = "SELECT DropRasterConstraints('%(schema)s'::name, '%(name)s'::name, 'rast'::name)"%dict(schema=self.schema, name=self.name)
        else:
            if isinstance(constraints, str): constraints = [constraints]
            allConstraints = ['srid','scale_x','scale_y','blocksize_x','blocksize_y','same_alignment','regular_blocking','num_bands','pixel_types','nodata_values','out_db','extent']
            cmd = "SELECT DropRasterConstraints('%(schema)s'::name, '%(name)s'::name, 'rast'::name, "%dict(schema=self.schema, name=self.name)
            cmd+=", ".join([cc+":=True" if cc in constraints else cc+":=False" for cc in allConstraints ])+");"
        self.execute(cmd)
        return        
    
    def get_gadmArray(self,GADM_ids,bands=1):
        """Returns an array of the given raster, only for the area of the given gadm id
        GADM_ids is an array of [iso, id_1, id_2, id_3, id_4, id_5] up to the desired depth
        The first id can also be an integer, in which case it is interpreted as id_0"""
        if self.getSRID()!=4326: raise Exception('rasterCon().get_gadmArray() only deals with SRID 4326 for now')
        assert self.sameAlignnment('gadmraster')
        if isinstance(GADM_ids,str) or isinstance(GADM_ids,int):
            GADM_ids=[GADM_ids]
        else:
            GADM_ids=list(GADM_ids)

        
        if isinstance(GADM_ids[0], str):  # replace iso with id_0
            GADM_ids[0] = self.execfetch("SELECT DISTINCT id_0::int FROM "+defaults['gadm']['TABLE']+" WHERE iso='"+GADM_ids[0]+"';")[0][0]
        whereClause = ' AND '.join(['id_'+str(ii)+'='+str(gadmId) for ii, gadmId in enumerate(GADM_ids)])
       
        # Get the tiles that we need
        cmd = """SELECT DISTINCT rid FROM %(rasterName)s, 
                   (SELECT the_geom AS geom FROM %(gadmName)s WHERE %(whereClause)s) t1
                    WHERE ST_Intersects(rast, geom)""" %dict(rasterName=self.name, gadmName=defaults['gadm']['TABLE'], whereClause=whereClause)
        rids = [rr[0] for rr in self.execfetch(cmd)]
        if rids is None or rids==[]: return np.array([])
        array = self.asarray(bands=bands, rids=rids)  # get the array that we are interested in 
        rcGadm = rasterCon('gadmraster') # get the gadm array for the relevant tiles, so we can mask 
        gadmBands = list(range(1, len(GADM_ids)+1))
        arrayGadm = rcGadm.asarray(bands=gadmBands, rids=rids)
        assert arrayGadm.ndim==3 # asarray should have a 3rd dimension, even if there is only 1 gadmLevel (i.e. band) passed
        assert array.shape[:2]==arrayGadm.shape[:2]   
        for ii, gadmId in enumerate(GADM_ids):
            if array.ndim==2:
                np.putmask(array,arrayGadm[:,:,ii]!=gadmId,np.nan)  # set array to np.nan outside the specified gadmId
            else:  # need to tile the mask over the third dimension
                assert array.ndim==3 
                np.putmask(array,np.tile(np.expand_dims(arrayGadm[:,:,ii]!=gadmId,2),(1,1,len(bands))),np.nan)  # set array to np.nan outside the specified gadmId
                
        return array   

    def get_gadmAgg(self,GADM_ids,bands=1, areaWgted=True):
        """Returns mean and sum of values in a given gadm
        Area weighted if areaWgted=True"""
        if self.getSRID()!=4326: raise Exception('rasterCon().get_gadmAgg() only deals with SRID 4326 for now')
        if isinstance(bands, int): bands = [bands]
        array = self.get_gadmArray(GADM_ids,bands)
        if areaWgted:
            areaRc = rasterCon('landscan')
            areaArray = areaRc.get_gadmArray(GADM_ids,bands=2)
        else:
            areaArray = np.ones(array.shape)[:,:,0]
        arraySum = [np.nansum(array[:,:,bb-1]) for bb in bands]
        arrayMean = [np.nansum(array[:,:,bb-1]*areaArray)/np.nansum(areaArray) for bb in bands]
        
        if len(bands)==1:
            arraySum=arraySum[0]
            arrayMean = arrayMean[0]
            
        return {'sum': arraySum, 'mean': arrayMean}

    def saveImage(self,filename,format='GTiff', forceUpdate=True):
        assert format in ['JPEG','GTiff'] # others not tested
        if os.path.exists(filename) and not forceUpdate: 
            self.logprint( '%s already exists.' % filename)
        else:  
            cmd = '''gdal_translate -of %s PG:"%s table='%s' mode='2'" "%s"''' % (format, self.gdal_command_line_flags(), self.name, filename)
            self.logprint( 'Executing '+cmd)
            os.system(cmd)
        return
    
    def logprint(self,logmessage):
        if self.logger:
            self.logger.write(logmessage)
        else:
            print((' [No logger present:] '+logmessage))



    def asarray(self, bands=1, rids=None, padEmptyRows=False, noDataToNan=True):
        """Based on http://gis.stackexchange.com/questions/130139/downloading-raster-data-into-python-from-postgis-using-psycopg2
        padEmptyRows will expand the number of rows so the whole world is captured (i.e. 21600 x 432600 for a 30-arc-second raster
        empty columns are always padded"""
        if bands=='all': bands = list(range(1,self.get_nBands()+1))
        assert isinstance(bands,int) or isinstance(bands, list)
        
        # Get subset of rids if necessary
        if isinstance(rids, int): rids=[rids]
        assert isinstance(rids, list) or rids is None
        if rids is None:
            fromClause = 'FROM '+self.name        
        elif len(rids)>1: 
            fromClause = 'FROM (SELECT rid, rast FROM '+self.name+' WHERE rid IN ' +str(tuple(rids))+') t1 '
        else:
            assert len(rids)==1
            fromClause = 'FROM (SELECT rid, rast FROM '+self.name+' WHERE rid =' +str(rids[0])+') t1 '

        tileDict = {}
        useGdal = False  # would be better, but there seems to be a bug in ST_AsGDALRaster. http://gis.stackexchange.com/questions/198787/getting-st-asgdalraster-to-recognize-pixel-type
        if useGdal:
            self.execute("""SELECT rid, ST_AsGDALRaster(rast, 'JPEG') """+fromClause+""" 
                           ORDER BY round(ST_UpperLeftY(rast)::numeric, 9) DESC, round(ST_UpperLeftX(rast)::numeric, 9);""")
        else: 
            bandText = 'ARRAY'+str([bands]) if isinstance(bands,int) else 'ARRAY'+str(bands)
            self.execute("""SELECT rid, (ST_DumpValues(rast,"""+bandText+""")).* """+fromClause+""" 
                           ORDER BY round(ST_UpperLeftY(rast)::numeric, 9) DESC, round(ST_UpperLeftX(rast)::numeric, 9), nband;""")
        for ii, tile in enumerate(self.cursor): # iterate over tiles, pull each into a dict of numpy arrays
            if ii>0: assert tile[0]>=rid  # confirm that rids are in correct order, i.e. this rid is greater than last rid
            rid = tile[0]
            if useGdal: 
                vsipath = '/vsimem/from_postgis'
                gdal.FileFromMemBuffer(vsipath, bytes(tile[1]))
                ds = gdal.Open(vsipath)
                if isinstance(bands,int):
                    rbs = ds.GetRasterBand(bands)  # we have to do this in 2 steps...not sure why
                    tileDict[rid] = rbs.ReadAsArray()
                else:
                    rbs = [ds.GetRasterBand(bb) for bb in bands]
                    tileDict[rid] = np.dstack([rb.ReadAsArray() for rb in rbs])
                gdal.Unlink(vsipath)
           
            else:
                if rid in tileDict:  # we've already got at least one bann
                    tileDict[rid] = np.dstack([tileDict[rid], np.array(tile[2], dtype=float)]) # dype float  will convert None values to float as well 
                else: 
                    tileDict[rid] = np.array(tile[2], dtype=float)
                    
        # negative y scale indicates top to bottom for gdal tiles, so for a positive we need to flip each tile
        yscale = self.get_scale()[0]
        if yscale>0:  
            for rid in tileDict: tileDict[rid] = np.flipud(tileDict[rid]) 
                    
        # assemble into a single array
        # create empty array where rid does not exist
        emptyArray = np.full(self.get_tileSizePixels(), np.nan)
        if isinstance(bands, list) and len(bands)>1: emptyArray = np.dstack([emptyArray for jj in range(len(bands))]) # 3D array
        if rids is None:
            if padEmptyRows:
                minRid = 1  
                nRows, nCols = (int(abs(180/self.get_tileSizeDegrees()[0])), int(abs(360/self.get_tileSizeDegrees()[1])))
            else:
                minRid = self.execfetch('SELECT MIN(rid) FROM '+self.name)[0][0]
                minRid = 1+int(minRid/self.get_nTilesYX()[1])*self.get_nTilesYX()[1]  # make sure it starts at -180 degrees
                nRows, nCols = self.get_nTilesYX()
            assert self.get_nTiles() == (ii+1.)/len(bands) if isinstance(bands, list) else ii+1
            nMissingTiles = sum([row*nCols+col+minRid not in tileDict for col in range(nCols) for row in range(nRows)])
            if nMissingTiles>0: print('Typically due to uninhabited regions at high latitude, etc: %d of %d tiles are missing. Padding with empty array.' % (nMissingTiles, len(list(tileDict.keys()))))
            array = np.vstack([np.hstack([tileDict[row*nCols+col+minRid] if row*nCols+col+minRid in tileDict else emptyArray for col in range(nCols)]) for row in range(nRows)])
        else: # rids may not be contiguous or rectangular, so let's fill in
            nRows, nCols = abs(int(180./self.get_tileSizeDegrees()[0])), abs(int(360./self.get_tileSizeDegrees()[1]))
            ridArray = np.array(list(range(1, nRows*nCols+1))).reshape((nRows, nCols))
            ridMask = np.ma.MaskedArray(ridArray, np.logical_not(np.in1d(ridArray, rids)))
            # extract subarray of non-null values
            nonZeroRows = np.nonzero(np.ma.count(ridMask, 1))
            nonZeroCols = np.nonzero(np.ma.count(ridMask, 0))
            # now get subset of the rid array
            ridArray = ridArray[np.min(nonZeroRows):np.max(nonZeroRows)+1, np.min(nonZeroCols):np.max(nonZeroCols)+1]
            nRows, nCols = ridArray.shape
            
            array = np.vstack([np.hstack([tileDict[ridArray[row, col]] if ridArray[row,col] in rids else emptyArray for col in range(nCols)]) for row in range(nRows)])
        
        # convert nodata values to nan. If we do it without gdal, they are nan already
        array = array.astype(float) 
        if useGdal:         
            if isinstance(bands, list):
                for bb in bands: array[:,:,bb-1][array[:,:,bb-1]==self.get_noDataValues()[bb]] = np.nan 
            else:
                ndValue = self.get_noDataValues() if self.get_nBands()==1 else self.get_noDataValues()[bands]
                array[array==ndValue] = np.nan 
        if isinstance(bands,list) and len(bands)==1: # promote to 3D array
            array = np.expand_dims(array,2)
        return array

def latlon_to_urldict(lat, lon):
    return dict(
        streeturl = r'https://www.google.com/maps/@?api=1\&map_action=map\&center={},{}\&zoom=16\&basemap=roadmap'.format(lat, lon),
    osmurl =    r'http://www.openstreetmap.org/?mlat={}\&mlon={}\&zoom=16'.format(lat, lon),
        saturl = r'https://www.google.com/maps/@?api=1\&map_action=map\&center={},{}\&zoom=16\&basemap=satellite'.format(lat, lon),
                      )
    
def latlon_from_rowcol(row,col=None, loc='NW', prefix=''):
    """ Take a dataframe with "row", "col" columns, or else take a row and col value, and return the grid cell centroid lat, lon
    N.B.: Cells are "defined" by psql code in aggregate....() 
    which is simply tagging GIS features with a row/col. Only here do we try to do the construction.  
    The simple formula below gives the upper left locations
    """
    if isinstance(row, pd.DataFrame):
        lat,lon = latlon_from_rowcol(row['row'].values,row['col'].values, loc=loc)
        row[prefix+'lat'] = lat
        row[prefix+'lon'] = lon
        return row # Return the dataframe that was passed
    addrow = {'center':+0.5, 'NW':0}[loc] # Rows increase to the south
    addcol = {'center':0.5, 'NW':0}[loc]
    lat = ((row+addrow) / 21600. * -180) + 90
    lon = ((col+addcol) / 43200. * 360) - 180
    return lat,lon # Matrix (y,x) order, not (x,y) order
    
def latLonToCell(lat, lon, missingHeaderRows=0): # This function not used anywhere. 201806.
    """Lookup from latLong to cell for the full Landscan array, or another array that spans the whole world
    Warning: only works for 30-arc second spaced cells (for now)
    Where it is on a cell boundary, gives the lower right cell"""
    assert lat>=-90 and lat<=90
    assert lon>=-180 and lon<=180
    row = int((lat-90)/-180.*21600)-missingHeaderRows
    col = int((lon+180)/360.*43200)
    if row==21600: 
        assert lat==-90
        row=21599
    if col==43200:
        assert lon==180
        col=43199
    return (row, col)

def add_safe_st_clip():   
    db = psqlt.dbConnection(defaults['osm']['processingSchema'])
    if not('safe_st_clip' in db.list_functions()):
        db.execute('''CREATE FUNCTION safe_ST_Clip(rast raster, geom geometry)
                        RETURNS raster AS
                        $$ BEGIN
                            RETURN ST_Clip($1, $2);
                            EXCEPTION WHEN others THEN
                                RAISE NOTICE 'TopologyException';
                                RETURN ST_MakeEmptyRaster(rast);
                        END; $$ LANGUAGE plpgsql;''') 
    
def loadLandScan(logger=None, forceUpdate=False):
    """ 
    Uploads the LandScan population and density data to postgres
    Note: units are people per sq km
    
    The challenges are (i) that population counts and areas are in different rasters, and 
      (ii) the area raster values are actually a lookup (values are rownumbers to the lookup table)
      (ArcGIS does the lookup automatically, but GDAL doesn't seem to handle this yet)
      
    The landscan raster will have a single 3 band raster:
        Band 1: population (number of people)
        Band 2: area (sq km)
        Band 3: density (persons km-2)
    """

    lsFullPath = paths['input'] + 'other/Landscan_2013/'
    lsPath = lsFullPath + 'scratch/'
    pgLogin = psqlt.getPgLogin()
    
    ls = rasterCon('landscan',verbose=True,logger=logger) 
    if 'landscan' in ls.list_tables(schema='rasterdata') and not forceUpdate: return
    
    # unzip files
    if not os.path.exists(lsFullPath+'scratch/'): os.mkdir(lsFullPath+'scratch/')
    os.system('unzip %sLandScan2013.zip -d %s' % (lsFullPath, lsPath))
    
    # upload rasters. Note that we don't have an extent constraint (which is very slow, so use -x). 
    # Use 30x30 tile size (i.e, tile raster into 1 degree squares)
    logger.write('Uploading population raster')
    cmd = 'raster2pgsql -I -b 1 -t "%sx%s" -s 4326 %s -d -C -x %s.landscan | psql -q %s' % (TILESIZE, TILESIZE, lsPath + 'ArcGIS/Population/lspop2013', rasterSchema, ls.psql_command_line_flags())
    logger.write(cmd)
    os.system(cmd)
    os.system('psql -c "ALTER TABLE %s.landscan OWNER to osmusers" %s' % (rasterSchema, ls.psql_command_line_flags()))

    # The only reason we load this in is to confirm that the alignment is OK. We don't use this raster, the values are actually lookups, not areas
    logger.write('Uploading area raster')
    cmd = 'raster2pgsql -I -b 1 -t "%sx%s" -s 4326 %s -d -C -x %s.landscanarea | psql -q %s' % (TILESIZE, TILESIZE, lsPath + 'ArcGIS/AreaGrid/areagrd', rasterSchema, ls.psql_command_line_flags())
    logger.write(cmd)
    os.system(cmd)
    cmd = 'psql -c "ALTER TABLE %s.landscanarea OWNER to osmusers" %s' % (rasterSchema, ls.psql_command_line_flags())
    logger.write(cmd)
    os.system(cmd)

    # The tricky bit is how to figure out how to match the row number of the population raster with the row number of the area raster
    # If we calculate it, it avoids problematic alignment issues due to floating point precision
    tilesPerRow = 360 * 60 * 2 / TILESIZE  # 30 arc-second tiles, 30 pixels per tile
    for rid in [1, 1+tilesPerRow]: # check that our tile calculation is right
        ls.execute('SELECT ST_UpperLeftx(p.rast) AS popy, ST_UpperLeftx(a.rast) AS areay FROM landscan as p, landscanarea a WHERE p.rid=%d AND a.rid=%d' % (rid, rid))
        assert ls.fetchall()[0] == [-180, -180]

    # how many extra rows (pixels, not tiles) do we have on the population raster?
    popUpperLeftY, areaUpperLeftY = ls.execfetch('SELECT ST_UpperLeftY(p.rast) AS popy, ST_UpperLeftY(a.rast) AS areay FROM landscan as p, landscanarea a WHERE p.rid=1 AND a.rid=1')[0]
    extraHeaderPixels = int(np.round((popUpperLeftY-areaUpperLeftY) * 60 * 2, 5))   # round is to deal with floating point precision issues
    assert extraHeaderPixels==np.round((popUpperLeftY-areaUpperLeftY) * 60 * 2, 5)
 
    # how many landscanarea rows do we have?
    nAreaRows = ls.execfetch('SELECT MAX(rid) FROM landscanarea')[0][0]*1.0/tilesPerRow
 
    # another check on our math
    result = ls.execfetch('''SELECT pmax, amax, ST_UpperLeftY(p.rast) AS popy, ST_UpperLeftY(a.rast) AS areay FROM landscan AS p, landscanarea AS a,
                    (SELECT max(rid) AS pmax FROM landscan) t1, (SELECT max(rid) AS amax FROM landscanarea) t2
                    WHERE p.rid=pmax AND a.rid=amax''')[0]
    assert result[0] - result[1] == extraHeaderPixels / TILESIZE * tilesPerRow
    assert np.round(result[2], 10) == np.round(result[3], 10)  # bottom row should be the same
    
    # Load the lookup table. Manually exported from ArcGIS. 
    # Note that row1 is at the BOTTOM, so this confuses things
    areaLookup = pd.read_csv(lsFullPath+'areaLookup.csv', usecols=['VALUE', 'AREA'], thousands=',')
    areaLookup.rename(columns={'VALUE':'areaRow'}, inplace=True)
    areaLookup['areaRid'] = areaLookup.areaRow.apply(lambda x: (math.floor((x-1)/TILESIZE)*-1+nAreaRows-1)*tilesPerRow+1)
    areaLookup['popRid']  =  areaLookup.areaRid.apply(lambda x: x+extraHeaderPixels/30*tilesPerRow)
    areaLookup['pixelY']  = areaLookup.areaRow.apply(lambda x: TILESIZE-(x-1)%TILESIZE) # within each raster image, what's the pixel?
    areaLookup.set_index('pixelY', inplace=True)

    # Now we have to use a loop, because otherwise the length of the lookup slows things down crazily
    # The loop allows us to use a smaller lookup
    # For use of reclass, see http://gis.stackexchange.com/questions/127300/postgis-replace-pixel-value-by-lookup-table
    maxRid = ls.execfetch('SELECT MAX(rid) FROM landscan')[0][0]
    minRid = extraHeaderPixels*tilesPerRow/TILESIZE+1  # this is the first rid that has area info
    
    # add two empty bands for tiles where we don't have an area estimate
    ndValue = ls.get_noDataValues()
    ls.dropConstraints(['nodata_values','num_bands','pixel_types','out_db'])  # otherwise, we can't add a new band
    ls.execute('''UPDATE landscan
                    SET rast = ST_AddBand(rast, ARRAY[ROW(2, '64BF', %(ndv)s, %(ndv)s), ROW(NULL, '64BF', %(ndv)s, %(ndv)s)]::addbandarg[]) 
                    WHERE rid<%(minRid)s''' %dict(ndv=ndValue,minRid=minRid))
    
    for ii, rid in enumerate(range(minRid, maxRid+1, tilesPerRow)):
        if ii%50==0: print('Now doing rid %d of %d' % (rid-minRid, maxRid-minRid+1))

        areaLookup_partial = str(areaLookup[areaLookup.popRid==rid].AREA.to_dict())[1:-1]
        assert areaLookup_partial!='' and (areaLookup.popRid==rid).sum()==30

        # Add in area as band 2, and density as band 3
        # Note: ST_MapAlgebra(pop.rast, 1, '16BSI', '[rast1.y]', ndv) returns the pixel row number (1-based)
        # ST_Reclass(ST_MapAlgebra(pop.rast, 1, '16BSI', '[rast1.y]', ndv), 1, '%s', '32BF', ndv) uses the pixel row number to look up the area
        ls.execute('''UPDATE landscan
                        SET rast = ST_AddBand(rast, array[ST_Reclass(ST_MapAlgebra(rast, 1, '16BSI', '[rast1.y]', %(ndv)s), 1, '%(alp)s', '64BF', %(ndv)s),
                                     ST_MapAlgebra(rast, ST_Reclass(ST_MapAlgebra(rast, 1, '16BSI', '[rast1.y]', %(ndv)s), 1, '%(alp)s', '64BF', %(ndv)s), 
                                            '[rast1]/[rast2]', '64BF', 'INTERSECTION', 'Null', 'Null', Null)]) 
                        WHERE rid>=%(minRid)s AND rid<%(maxRid)s''' %dict(alp=areaLookup_partial,ndv=ndValue,minRid=rid,maxRid=rid+tilesPerRow))
    
    logger.write('Adding back raster constraints')
    ls.addConstraints(['nodata_values','num_bands','pixel_types','out_db']) # Add back constraints
    
    # Floating point precision errors mean that the grid is slightly off. 
    logger.write('Snapping raster to grid')
    ls.execute('UPDATE landscan SET rast=ST_SnapToGrid(rast, 0, 0, 1/120::double precision, -1/120::double precision, maxerr:=0.0001)')
    ls.addSpatialIndex()

    # clean up
    ls.execute('DROP TABLE landscanarea')
    cmd = 'rm -r %s' % lsPath
    logger.write(cmd)
    os.system(cmd)
    logger.write('Completed loadLandScan\n')

    return

def aggregateLandscantoGadm(forceUpdate=False,logger=None):
    """Aggregates Landscan-based population and density to gadm polygons, at each id level"""
    """Run via osm_master"""
    if logger is None:
        logger = osmt.osm_logger('raster_AggregateLandscantoGadm',verbose=True, timing=True)

    db_=psqlt.dbConnection(schema='rasterdata', logger=logger)
    try:
        assert 'gadmraster' in db_.list_tables(schema='rasterdata')
    except AssertionError:
        logger.write( ' ERROR: aggregateLandscantoGadm needs gadmraster to run. Run gadm.createGADMCountryRaster before proceeding.')
        input('aggregateLandscantoGadm needs gadmraster to run. Run gadm.createGADMCountryRaster before proceeding.')
    from gadm import regions_and_neighbours, GADM_schema
    db=psqlt.dbConnection(verbose=True,logger=logger,schema=defaults['osm']['analysisSchema'])

    # see which tables we still have to do
    doneLevels = []
    newCols = ['ls_pop','ls_area','ls_pixels','ls_density']
    for gl in defaults['gadm']['ID_LEVELS']:
        cols=db.list_columns_in_table(defaults['gadm']['TABLE']+'_'+gl, schema=GADM_schema)
        doneLevels.append(all([cc in cols for cc in newCols]))
    if all(doneLevels) and not forceUpdate:
        logger.write( 'Already done aggregateLandscantoGadm. Skipping')
        return           
            
    ls = rasterCon('landscan')
    ga = rasterCon('gadmraster')
    idCols = ['id_0' if ii=='iso' else ii for ii in defaults['gadm']['ID_LEVELS']]
    nLevels = len(idCols)
    
    logger.write( 'Loading rasters for aggregateLandscanToGadm...')
    lsArray   = ls.asarray(bands=[1,2])
    gadmArray = ga.asarray(bands=list(range(1,nLevels+1)))

    logger.write( 'Preparing for aggregation...')
    # fill empty holes
    minRid = ga.execfetch('SELECT MIN(rid) FROM '+ga.name)[0][0]
    missingHeaderRows = int(float(minRid)/(43200./30))*30
    emptyArray1 = np.full((missingHeaderRows,43200,nLevels), np.nan)
    emptyArray2 = np.full((21600-missingHeaderRows-gadmArray.shape[0],43200, nLevels), np.nan)
    gadmArray = np.vstack([emptyArray1,gadmArray,emptyArray2])
    
    # flatten the array and then use pandas groupby to compute
    assert gadmArray.shape[:2]==lsArray.shape[:2]
    gadmArray = gadmArray.reshape((21600*43200,  gadmArray.shape[2]))
    lsArray = lsArray.reshape((21600*43200,  lsArray.shape[2]))
    
    df = pd.DataFrame(np.hstack([lsArray, gadmArray]), columns=['ls_pop','ls_area']+idCols)
    assert len(df)==43200*21600
    df.dropna(subset=idCols, inplace=True)
    if defaults['gadm']['VERSION']=='2.8':
        assert len(df)>22153900 and len(df)<221542000
        # for 3.6 len(df)=221876662

    # lookup from id_0 to iso
    isos = regions_and_neighbours().get_all_GADM_countries()
    id_0ToISO = dict([(isos[kk]['id_0'], kk) for kk in isos])
    df['iso'] = df.id_0.map(lambda x: id_0ToISO[int(x)])
            
    df.set_index(defaults['gadm']['ID_LEVELS'], inplace=True)
    df.drop('id_0',axis=1, inplace=True)
    
    logger.write( 'Loaded pandas dataframe. Dropped %d rows (%.1fpc) with missing gadmids' % (43200*21600-len(df), (1-len(df)/(43200.*21600))*100 ) )
    
    for gl in range(0,nLevels):
        logger.write( 'Aggregating gadm level %s...' %gl)
        if doneLevels[gl]:
            if not forceUpdate: continue
            cmd = 'ALTER TABLE '+defaults['gadm']['TABLE']+'_'+defaults['gadm']['ID_LEVELS'][gl]
            cmd += ', '.join([' DROP COLUMN IF EXISTS '+cc for cc in newCols])
            db.execute(cmd)
        aggDf = df.groupby(level=list(range(0,gl+1)))[['ls_pop','ls_area']].sum()
        aggDf['ls_pixels'] = df.groupby(level=list(range(0,gl+1))).size()
        aggDf['ls_density'] = aggDf.ls_pop.divide(aggDf.ls_area)

        # now write this to postgres
        db.update_table_from_array(aggDf, defaults['gadm']['TABLE']+'_'+defaults['gadm']['ID_LEVELS'][gl], 
                                   targetschema=GADM_schema, joinOnIndices=True)

def aggregateRastertoGeom(continents, raster, geomType=None, forceUpdate=False):
    """Adds density or GHSL data to the edges and nodes (runs addRasterDatatoVectorTable on them)
    if geomType is None, it does both nodes and edges in parallel
    """
    from create_roadgraph_tables import final_graph_builder

    assert raster in ['landscan', 'ghsl']  # others not yet supported
    assert geomType in [None,'edge','node']
    if geomType is None: geomType = ['edge','node'] 

    if any([isinstance(vv, list) for vv in [continents,geomType]]):
        if isinstance(geomType, str): geomType = [geomType]
        if isinstance(continents, str): continents = [continents]
        assert isinstance(continents, list) and isinstance(geomType, list)
    
        from cpblUtilities.parallel import runFunctionsInParallel
        funcs,names=[],[]
        for gt in geomType:
            for cont in continents:
                funcs+=[[aggregateRastertoGeom,[cont,raster,gt,forceUpdate]]]
                names+=['-'.join(['aggregateRastertoGeom',cont,raster,gt])]
        runFunctionsInParallel(funcs,names=names,maxAtOnce=None, parallel=defaults['server']['parallel'])
        return
    

    vectorCols  = {'landscan':['density','density_decile'], 'ghsl':['ghsl_yr','ghsl_frc','ghsl_frc_developed']}[raster]
    vectorIdCol = {'edge':'edge_id','node':'cluster_id'}[geomType]
    
    processingSchema = defaults['osm']['processingSchema']
    db = psqlt.dbConnection(processingSchema)
    analysisTables = db.list_tables(schema=processingSchema)
    # define safe_st_clip function to avoid topology exceptions on a handful of edges. See issue #123
    if not('safe_st_clip' in db.list_functions()):
        add_safe_st_clip()

    tables = list(osmt.define_table_names(continents)[geomType+'_tables'].values())
    assert isinstance(continents,str)
    fgb= final_graph_builder(continents)
    assert len(tables) == len(defaults['osm']['clusterRadii'])
    for clusterRadius in defaults['osm']['clusterRadii']: # The tables list above should correspond simply to these different clusterradii.
        if geomType=='node':
            fgb.drop_indices_cluster_tables(clusterRadius)
        else:
            fgb.drop_indices_roadgraph_tables(clusterRadius)

    for table in tables:
        if table in analysisTables:
            if any([vc in db.list_columns_in_table(table, schema=processingSchema) 
                   for vc in vectorCols]):
                if forceUpdate:
                    print(('Dropping %s columns that already exist for %s' % (vectorCols, table)))
                    db.execute('ALTER TABLE %s %s;' % (table, ', '.join(['DROP COLUMN IF EXISTS '+vc for vc in vectorCols])))
                else:
                    print(('%s columns already exists for %s...skipping' % (vectorCols, table)))
                    continue
            print(('Adding %s columns to %s' % (vectorCols, table)))
            geomName = 'geog' if 'geog' in db.list_columns_in_table(table, schema=processingSchema) else 'geom'
            if raster=='landscan':
                addRasterDatatoVectorTable(table, vectorIdCol, 'landscan', 3, vectorCols[0],geomName=geomName, vectorTableSchema=processingSchema)
                addDensityDecile(vectorIdCol, table, processingSchema)
            else:
                addGHSLdatatoVectorTable(table, vectorIdCol, geomName=geomName, vectorTableSchema=processingSchema)
        else:
            print(('%s does not exist...skipping adding %s columns' % (table, vectorCols)))
        
        #fgb.ensure_indices_are_built() # Can't do this, as it will try and create/drop indices for tables that are being run in parallel. This will do all radii, therefore, I think, all tables, above.
        for clusterRadius in defaults['osm']['clusterRadii']: 
            if geomType=='node':
                fgb.create_indices_cluster_tables(clusterRadius)
            else:
                fgb.create_indices_roadgraph_tables(clusterRadius)
        
    return

def loadGHSL(logger=None,forceUpdate=False):
    """Loads the GHSL settlement layer into PostGIS
    Note projection for aggregated is 54009, which is same as 954009 World_Mollweide (see readme)
    There is no nodata value listed, so we set it to NULL
    Each pixel indicates the proportion (on a 0-255 scale) that was developed in a given year
    Each band denotes a separate year: 1975 (band 1) through 2014 (band 4)
    """

    
    
    GHSLinputPath = paths['input'] + 'GHSL/AGG/' # 'GHSL/FULL/'
    GHSLPath = paths['scratch'] + 'GHSL/'
    GHSLyears = [1975, 1990, 2000, 2014]
    pgLogin = psqlt.getPgLogin()
    rc = rasterCon('ghsl',verbose=True,logger=logger)
    if 'ghsl' in rc.list_tables(schema='rasterdata') and not forceUpdate:
        logger.write(' Skipping loadGHSL():  ghsl table already exists. ')
        return
    
    # check srs 954009 is in spatial ref table
    db = osmt.pgisConnection(schema=rasterSchema)
    db.insert_srs(954009)

    # unzip files
    if os.path.exists(GHSLPath): os.system('rm -r %s' % GHSLPath)
    os.mkdir(GHSLPath)
    
    for yr in GHSLyears:
        logger.write('Uploading GHSL raster for %s. This will take a few hours per year' % yr)
        cmd = 'unzip %sT%s.zip -d %s' % (GHSLinputPath, yr, GHSLPath)
        logger.write(cmd)
        os.system(cmd)

        cmd = 'raster2pgsql -I -t "%sx%s" -s 954009 %sT%s_300m_Mollweide.tif -d %s.ghsl%s | psql -q %s' % (TILESIZE, TILESIZE, GHSLPath, yr, rasterSchema, yr, rc.psql_command_line_flags())
        logger.write(cmd)
        os.system(cmd)
        # Nodata value defaults to 255 here, but there is actually no nodata value
        cmd = 'psql -c "UPDATE %s.ghsl%s SET rast = ST_SetBandNoDataValue(rast,NULL)" %s' % (rasterSchema, yr, rc.psql_command_line_flags())
        logger.write(cmd)
        os.system(cmd)  
        
        # Change owner
        cmd = 'psql -c "ALTER TABLE %s.ghsl%s OWNER to osmusers" %s' % (rasterSchema, yr, rc.psql_command_line_flags())
        logger.write(cmd)        
        os.system(cmd)
                
        # Add constraints
        ghsl = rasterCon('ghsl'+str(yr),verbose=True,logger=logger) 
        ghsl.addConstraints(setall=True)
        
        print('Loaded %s' % yr)
    
    # Now create a 4-band raster from the 4 rasters we just imported
    rc.execute("DROP TABLE IF EXISTS ghsl; ") #
    assert len(GHSLyears)==4 # otherwise, SQL command below won't work
    #yr0 = GHSLyears[0]
    yrsAs  = ', '.join(['ghsl'+str(yr)+' AS g'+str(ii+1) for ii, yr in enumerate(GHSLyears)])
    rc.execute('''CREATE TABLE ghsl AS 
                  SELECT g1.rid AS rid, ST_AddBand(g1.rast,ARRAY[g2.rast, g3.rast, g4.rast]) AS rast
                  FROM %s WHERE g1.rid=g2.rid AND g1.rid=g3.rid AND g1.rid=g4.rid;''' % (yrsAs))
    rc.addConstraints(setall=True)
    rc.addSpatialIndex()

    # clean up: delete unzipped files and drop intermediate tables
    cmd = 'rm -r %s' % GHSLPath
    logger.write(cmd)
    os.system(cmd)
    for yr in GHSLyears: rc.execute('DROP TABLE %s.ghsl%s;' % (rasterSchema,yr))
    
    logger.write('Completed loadGHSL')
    logger.close()
    
    return

def addRasterDatatoVectorTable(vectorTable, vectorIDcol, rasterTable, rasterBand, newColName, geomName='geom', vectorTableSchema=None,parallel=False):
    """Adds a column to a vector table with (aggregated) raster data from pixels that intersect the geometry
       vectorIDcol (e.g. edge_id) must be unique in the vectorTable, and is used for aggregating to rows
       Does not (yet) deal with more than 1 raster band
       Parallel=False should work fine. True is an option if it turns out to be too slow"""
       
    pgLogin = psqlt.getPgLogin()
    rc = rasterCon(rasterTable) 
    if vectorTableSchema is None: vectorTableSchema=defaults['osm']['processingSchema']
    db = psqlt.dbConnection(schema=vectorTableSchema)

    # check vectorIDcol is distinct
    cmd = '''SELECT dcount=acount FROM 
                     (SELECT COUNT(*) AS dcount FROM (SELECT DISTINCT %(vc)s FROM %(vt)s) t1) AS d, 
                     (SELECT COUNT(*) AS acount FROM %(vt)s) AS a''' %{'vc':vectorIDcol,'vt':vectorTable}
    if not db.execfetch(cmd)[0][0]==True: raise Exception('vectorIDcol is not unique')

    cmdDict = {'rschema':rc.schema, 'vschema':vectorTableSchema,
               'srid':rc.getSRID(), 'vectorIDcol':vectorIDcol, 'geomName':geomName, 'newColName':newColName,
               'vectorTable':vectorTable, 'rasterTable':rasterTable, 'rasterBand':str(rasterBand)}
    rc.execute('DROP TABLE IF EXISTS %(vschema)s.%(vectorTable)s_rastagg_tmp;'%cmdDict)
    
    if parallel: # chop up table into chunks of 100000 each
        from cpblUtilities.parallel import runFunctionsInParallel 
        nRows=db.execfetch('SELECT COUNT(*) FROM %s' %vectorTable)[0][0]
        nChunks=int(math.ceil(nRows/100000.))
        # which rows are at the border?
        binBorders = []
        for chunk in range(nChunks):
            binBorders+=db.execfetch('SELECT %(vc)s FROM %(vt)s ORDER BY %(vc)s LIMIT 1 OFFSET %(chunk)s*10000' %{'vc':vectorIDcol,'vt':vectorTable,'chunk':chunk})[0]       
        binBorders+=db.execfetch('SELECT MAX(%(vc)s)+1 FROM %(vt)s' %{'vc':vectorIDcol,'vt':vectorTable,'chunk':chunk})[0]       
        
        def _createTableChunk(chunk,binBorders,cmdDict):
            cmdDictSub = dict(cmdDict, **{'chunk':chunk, 'startId':binBorders[chunk],'endId':binBorders[chunk+1]})
            rc.execute('DROP TABLE IF EXISTS %(vschema)s.%(vectorTable)s_rastagg_tmp%(chunk)s;'%cmdDictSub)
            cmd = '''CREATE TABLE %(vschema)s.%(vectorTable)s_rastagg_tmp%(chunk)s   AS
                 SELECT %(vectorIDcol)s, (ST_SummaryStats(ST_Union(safe_ST_Clip(rast, ST_Transform(v.%(geomName)s::geometry, %(srid)s))), %(rasterBand)s)).mean AS %(newColName)s
                 FROM %(vectorTable)s AS v, %(rschema)s.%(rasterTable)s as r
                 WHERE ST_Intersects(r.rast, ST_Transform(v.%(geomName)s::geometry, %(srid)s))
                       AND %(vectorIDcol)s>=%(startId)s AND %(vectorIDcol)s<%(endId)s
                 GROUP BY %(vectorIDcol)s;'''%cmdDictSub            
            rc.execute(cmd)
        
        toRun,runNames=[],[]
        for chunk in range(nChunks):
            toRun+=[[_createTableChunk,[chunk,binBorders,cmdDict]]]
            runNames+=['addRasterDatatoVectorTable-%s-%d'%(vectorTable,chunk)]
        runFunctionsInParallel(toRun,names=runNames,maxAtOnce=None,offsetsSeconds=0.1)
    
        # merge tables into one big one
        cmd = 'CREATE TABLE %(vschema)s.%(vectorTable)s_rastagg_tmp   AS\n' % cmdDict
        cmd+= ' \nUNION '.join(['SELECT * FROM %(vschema)s.%(vectorTable)s_rastagg_tmp'%cmdDict+str(cc) for cc in range(nChunks)])
        rc.execute(cmd)
        for chunk in range(nChunks): rc.execute('DROP TABLE %(vschema)s.%(vectorTable)s_rastagg_tmp'%cmdDict+str(chunk)+';')
        
    else: # not parallel
        cmd = '''CREATE TABLE %(vschema)s.%(vectorTable)s_rastagg_tmp   AS
                 SELECT %(vectorIDcol)s, (ST_SummaryStats(ST_Union(safe_ST_Clip(rast, ST_Transform(v.%(geomName)s::geometry, %(srid)s))), %(rasterBand)s)).mean AS %(newColName)s
                 FROM %(vectorTable)s AS v, %(rschema)s.%(rasterTable)s as r
                 WHERE ST_Intersects(r.rast, ST_Transform(v.%(geomName)s::geometry, %(srid)s))
                 GROUP BY %(vectorIDcol)s;'''%cmdDict
        rc.execute(cmd)
    
    # now merge in the table
    db.fix_permissions_of_new_table(vectorTable+'_rastagg_tmp')
    db.merge_table_into_table(from_table=vectorTable+'_rastagg_tmp', 
                              targettablename=vectorTable, commoncolumns=vectorIDcol) 
    db.execute(' DROP TABLE '+vectorTable+'_rastagg_tmp;')

def addGHSLdatatoVectorTable(vectorTable, vectorIDcol, geomName='geom', vectorTableSchema=None):
    """Similar to addRasterDatatoVectorTable. But there is a more complicated aggregation process for GHSL
    We want to add two columns to each edge:
    - modal year (when most of the grid cells that intersect the edge were developed)
    - frc developed in that year """
       
    pgLogin = psqlt.getPgLogin()
    rc = rasterCon('ghsl') 
    if vectorTableSchema is None: vectorTableSchema=defaults['osm']['processingSchema']
    db = psqlt.dbConnection(schema=vectorTableSchema)

    # check vectorIDcol is distinct
    cmd = '''SELECT dcount=acount FROM 
                     (SELECT COUNT(*) AS dcount FROM (SELECT DISTINCT %(vc)s FROM %(vt)s) t1) AS d, 
                     (SELECT COUNT(*) AS acount FROM %(vt)s) AS a''' %{'vc':vectorIDcol,'vt':vectorTable}
    if not db.execfetch(cmd)[0][0]==True: raise Exception('vectorIDcol is not unique')

    cmdDict = {'rschema':rc.schema, 'vschema':vectorTableSchema, 'tmpTableName':vectorTable+'ghsl_agg_tmp',
               'srid':rc.getSRID(), 'vectorIDcol':vectorIDcol, 'geomName':geomName, 'vectorTable':vectorTable}
    db.execute('DROP TABLE IF EXISTS %(vschema)s.%(tmpTableName)s;'%cmdDict)

    # The Add Band here creates another band that is just 1 or 0=nodata
    # Because our rasters are 8BUI and the range of the data is 0-255, we have no spare values to designate as no data 
    # when irregular rasters are created in ST_Clip. This is a workaround.
    cmd = '''CREATE TABLE %(vschema)s.%(tmpTableName)s   AS
                 SELECT %(vectorIDcol)s, (GREATEST(ghsl1,ghsl2,ghsl3,ghsl4)/npixels)::real AS ghsl_frc, ghsl_frc_developed,
                    CASE WHEN ghsl1=GREATEST(ghsl1,ghsl2,ghsl3,ghsl4) THEN 1975::smallint
                         WHEN ghsl2=GREATEST(ghsl1,ghsl2,ghsl3,ghsl4) THEN 1990::smallint
                         WHEN ghsl3=GREATEST(ghsl1,ghsl2,ghsl3,ghsl4) THEN 2000::smallint
                         WHEN ghsl4=GREATEST(ghsl1,ghsl2,ghsl3,ghsl4) THEN 2014::smallint
                         ELSE -1::smallint
                         END AS ghsl_yr
                  FROM (
                     SELECT  %(vectorIDcol)s, npixels, ghsl1_tot AS ghsl1, ghsl2_tot-COALESCE(ghsl1_tot,0) AS ghsl2,
                             ghsl3_tot-COALESCE(ghsl2_tot,0) AS ghsl3, ghsl4_tot-COALESCE(ghsl3_tot,0) AS ghsl4,
                             COALESCE(ghsl4_tot/npixels,0)::real AS ghsl_frc_developed
                     FROM 
                         (SELECT %(vectorIDcol)s, COALESCE((ST_SummaryStats(unionrast, 5)).count*255,0) AS npixels, 
                                (ST_SummaryStats(unionrast, 1)).sum AS ghsl1_tot, (ST_SummaryStats(unionrast, 2)).sum AS ghsl2_tot,
                                (ST_SummaryStats(unionrast, 3)).sum AS ghsl3_tot, (ST_SummaryStats(unionrast, 4)).sum AS ghsl4_tot
                         FROM 
                            (SELECT %(vectorIDcol)s, ST_Union(safe_ST_Clip(ST_AddBand(rast,'1BB'::text,1,0),
                                    ST_Transform(v.%(geomName)s::geometry, %(srid)s))) AS unionrast 
                                FROM %(vectorTable)s AS v, %(rschema)s.ghsl as r
                                WHERE ST_Intersects(r.rast, ST_Transform(v.%(geomName)s::geometry, %(srid)s))
                                GROUP BY %(vectorIDcol)s) AS ur1) AS ur2) AS ur3;'''%cmdDict
    db.execute(cmd)
    
    # now merge in the table
    db.merge_table_into_table(from_table=cmdDict['tmpTableName'], 
                              targettablename=vectorTable, commoncolumns=vectorIDcol) 
    db.execute('DROP TABLE %(vschema)s.%(tmpTableName)s;'%cmdDict)

def calculateDensityDeciles(cr='10', continents=None):
    """To speed things up (avoid an extra iteration), we hardcode the density deciles
    This function calculates them to insert into text
    This is ugly and a kludge, but the deciles don't change much and are easy to recreate when planet is loaded"""
    if continents is None:
        continents = [ 'australiaoceania', 'centralamerica', 'africa', 'southamerica', 'northamerica', 'asia', 'europe']
        #continents=['planet']
    
    db = psqlt.dbConnection(curType='default')
    cmd = '''SELECT unnest(percentile_disc(array[0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]) within group (ORDER BY density)) FROM \n('''
    cmd += '\nUNION ALL\n'.join(['SELECT density FROM edges_'+cont+'_'+cr for cont in continents])
    cmd += ') t1'
    deciles = db.execfetch(cmd)
    deciles = [dd[0] for dd in deciles]

    print(('Density deciles:\n%s' % deciles))
    print('Please paste the list in to getDensityDeciles in rasterTools.py')

    decilesFn = paths['working'] + 'densityDeciles_global.pyshelf'
    print(('Result also saved to %s' % decilesFn))
    shelfSave(decilesFn, deciles)

def getDensityDeciles():
    """Returns hard-coded deciles. You can update by pasting in results from calculateDensityDeciles()"""
    deciles = [0.0, 10.6378500992595, 58.2663201051982, 186.263236262511, 421.845897956662, 787.729823527081, 1328.66431181462, 2224.17228179167, 3937.36515450567, 8208.27789391849, 225905.58504505]
    return deciles

def addDensityDecile(vectorIDcol, table, schema=None):
    """Takes the density column in a table, and adds a new column that gives the decile"""
    
    # Load the deciles from history file, which is a bit of a kludge
    # Use the history because it has a complete set of all edges with density attached
    # Otherwise, we have to aggregate over all continents, some of which may not be complete at this point
    if schema is None: schema=defaults['osm']['processingSchema']
    db = psqlt.dbConnection(schema=schema)
    deciles = getDensityDeciles()
    
    # write a temp table with the decile of each edge or node
    tmpTableName = table+'_tmp_density_decile'
    caseStatements = 'WHEN density<=%f THEN 1\n' % deciles[1] 
    for ii in range(1,len(deciles)-1):
        caseStatements+='WHEN density>%f AND density<=%f THEN %d\n' % (deciles[ii], deciles[ii+1], ii+1)
    
    cmdDict = {'schema':schema,'table':table,'tmpTableName':tmpTableName,'vectorIDcol':vectorIDcol, 'caseStatements':caseStatements}
    cmd = '''CREATE TABLE %(schema)s.%(tmpTableName)s AS
             SELECT %(vectorIDcol)s, 
                    CASE %(caseStatements)s END AS density_decile
             FROM %(schema)s.%(table)s;''' % cmdDict
    db.execute(cmd)
    db.merge_table_into_table(from_table=tmpTableName, targettablename=table, 
                              commoncolumns=vectorIDcol, fromschema=schema,
                              targetschema=schema) 
    db.execute(' DROP TABLE %s.%s;' %(schema, tmpTableName))
    
    return

def arrayToImage(array,cmap,fname,legTitle=None,useLog=False,cbarPos=(0.1,0.5),vsize=0.3,ndColor=None,alphas=None):
    """Saves a numpy array to an image. 
    cbarPos is None gives no colorbar, otherwise cbarPos gives the top LH corner in (0,1) space
    useLog can be False, True or plus1 (where array will be log(x+1))
    vsize gives the vertical dimension of the colorbar as a fraction of the plot height. (doesn't do horizontal yet..)
    If alphas is an array of the same shape, it will be used to provide the alpha values (note: it should be prenormalized to (0,1)
    Tick labeling on colorbar is still a kludge
    DEPRECATED,  but keep here as old code not updated to use numpyToImage
    """
    
    import Image
    from cpblUtilities.color import addColorbarNonImage
    
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    assert useLog in [False, True, 'plus1']
    
    if ndColor is None: ndColor = [255,255,255,0] # RGBA transparent, but may not have desired effect 
    
    # convert to 0:1 range
    if useLog=='plus1':
        array = np.log(array+1)
    elif useLog is True:
        array = np.log(array)
    minVal, maxVal = np.nanmin(array), np.nanmax(array)
    scale = float(maxVal-minVal)
    
    norm = mpl.colors.Normalize(vmin=minVal, vmax=maxVal)   
    #imageArray = np.uint8(cmap((array-minVal)/scale)*255)
    imageArray = np.zeros((array.shape[0],array.shape[1],4),dtype=np.uint8)
    mask = np.isnan(array)
    imageArray[mask] = np.array(ndColor).astype(np.uint8)
    imageArray[np.logical_not(mask)] = np.uint8(cmap(norm(array[np.logical_not(mask)]))*255)
    
    if alphas is not None:
        if np.nanmin(alphas)<0 or np.nanmax(alphas)>1:
            raise Exception("Alpha array must be normalized to 0:1!")
        assert alphas.shape==array.shape
        alphas[np.isnan(alphas)]=0
        imageArray[:,:,3] = (alphas*255).astype(np.uint8)
        im = Image.fromarray(imageArray, 'RGBA')
    else:
        im = Image.fromarray(imageArray)
        
    if cbarPos is not None:       # create colorbar
        plt.close(6354)
        plt.figure(6354,figsize=(1+0.4*(legTitle is not None),3)) # Create a dummy axis to hang the colorbar on
        hax=plt.gca()
        cb = mpl.colorbar.ColorbarBase(hax, cmap=cmap, norm=norm,label=legTitle)
        hax.patch.set_visible(False)  # transparent
        if useLog=='plus1':
            tickLabels = mpl.ticker.LogLocator(numticks=7).tick_values(max(np.exp(minVal)-1,0.0000000001), np.exp(maxVal)-1)[3:-1]
            ticks = np.log(tickLabels+1)
            tickLabels = ['%.5g'%flt for flt in tickLabels]
        elif useLog is True:
            tickLabels = mpl.ticker.LogLocator(numticks=7).tick_values(np.exp(minVal), np.exp(maxVal))[1:-1]
            ticks = np.log(tickLabels)
            tickLabels = ['%.5g'%flt for flt in tickLabels]
        else:
            tickLabels = mpl.ticker.MaxNLocator(nbins=5).tick_values(minVal, maxVal)
            ticks = tickLabels
        cb.set_ticks(ticks)
        cb.set_ticklabels(tickLabels)
        plt.tight_layout()       
        plt.savefig(paths['scratch']+'cbartmp.png', transparent=True)
        cb_img=Image.open(paths['scratch']+'cbartmp.png')

        # add cbar to image
        bbox = im.getbbox()
        hpos = int((bbox[2]-bbox[0])*cbarPos[0]+bbox[0])
        vpos = int((bbox[3]-bbox[1])*cbarPos[1]+bbox[1])
        bbox = cb_img.getbbox() # should be 80x300 if figsize is 0.8x3
        scale = int(imageArray.shape[0]*float(vsize)/bbox[3])
        cb_img = cb_img.resize((bbox[2]*scale,bbox[3]*scale)) 
        im.paste(cb_img, (hpos,vpos))
        os.remove(paths['scratch']+'cbartmp.png')
    
    im.save(fname)
    print('Saved array image to %s' %fname)
    return
    
def arrayToPostGIS(array,rasterName,schema=None,ulx=-180,uly=90,cellsize=1./120,nodata_value=None,tileSize=None,forceUpdate=True,logger=None):
    """Loads a numpy array into PostGIS as a raster
    Based on http://gis.stackexchange.com/questions/150300/how-to-place-an-numpy-array-into-geotiff-image-using-python-gdal"""
    
    import gdal, osr, uuid
    
    if schema is None: schema = defaults['osm']['rasterSchema']
    pgLogin = psqlt.getPgLogin()
    rc = rasterCon(rasterName,schema=schema,verbose=True,logger=logger) 
    
    if rasterName in rc.list_tables():
        if forceUpdate: 
            rc.execute('DROP TABLE %s.%s' % (schema, rasterName))
        else:
            return
                
    dtypeDict = {np.float64:gdal.GDT_Float64, np.float32:gdal.GDT_Float32, np.int32:gdal.GDT_Int32, np.int16:gdal.GDT_Int16, 
                 np.int8:gdal.GDT_Byte, np.uint32:gdal.GDT_UInt32,   np.uint16:gdal.GDT_UInt16}
    if array.dtype.type not in dtypeDict: raise Exception('numpy data type not supported for GDAL translation')
    assert array.ndim in [2,3]
    nBands = 1 if array.ndim==2 else array.shape[2]

    # Write temporary raster to file as a geotiff   
    tempfileName = paths['scratch']+str(uuid.uuid4())+'.tiff'
    if logger: logger.write('Creating raster as temporary file %s ' % tempfileName)

    driver = gdal.GetDriverByName('GTiff')
    ds = driver.Create(tempfileName, array.shape[1], array.shape[0], nBands, dtypeDict[array.dtype.type])
    
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)
    ds.SetProjection(srs.ExportToWkt())
    gt = [ulx, cellsize, 0, uly, 0, -cellsize]
    ds.SetGeoTransform(gt)
    for band in range(nBands):
        arrayslice = array if nBands==1 else array[:,:,band]
        outband=ds.GetRasterBand(band+1)
        #outband.SetStatistics(np.min(arrayslice), np.max(arrayslice), np.average(arrayslice), np.std(arrayslice))
        if nodata_value is not None: outband.SetNoDataValue(nodata_value)
        outband.WriteArray(arrayslice)
        outband.FlushCache()
    ds = None
    if logger: logger.write('Created temporary file %s ' % tempfileName)
    
    # Upload temporary geotiff to PostGIS    
    if logger: logger.write('Uploading raster to %s' % rasterName)
    
    tileCmd = '' if tileSize is None else '-t "%sx%s" ' % (str(tileSize), str(tileSize))

    cmd = 'raster2pgsql -I %s -s 4326 %s -d -C -x %s.%s | psql -q %s' % (tileCmd, tempfileName, schema, rasterName, rc.psql_command_line_flags())
    if logger: logger.write(cmd)
    os.system(cmd)
    cmd ='psql -c "ALTER TABLE %s.%s OWNER to osmusers" %s' % (rasterSchema, rasterName, rc.psql_command_line_flags())
    if logger: logger.write(cmd)
    os.system(cmd)
        
    os.remove(tempfileName)
    if logger: logger.write('Raster %s created.' % rasterName)
    
class mapDensityDeciles():
    def __init__(self, forceUpdate=False):
        self.forceUpdate=forceUpdate
        self.db = psqlt.dbConnection()

        deciles = getDensityDeciles()
        reclassArg = ', '.join(['['+str(deciles[ii])+'-'+str(deciles[ii+1])+'):'+str(ii) for ii in range(10)])
        reclassArg = reclassArg.replace('):9',']:9') # replace top decile with closed interval
        self.reclassArg = reclassArg
    
    def createGlobalRaster(self):
        if 'density_decile' in self.db.list_tables(schema=defaults['osm']['rasterSchema']):
            if self.forceUpdate:
                self.db.execute('DROP TABLE %s.density_decile;' % defaults['osm']['rasterSchema'])
            else:
                return
        cmdDict = {'schema': defaults['osm']['rasterSchema'], 'reclassArg':self.reclassArg}
        self.db.execute('''CREATE TABLE %(schema)s.density_decile AS
                        SELECT rid, ST_Reclass(ST_Band(rast,3),1,'%(reclassArg)s','4BUI',255) AS rast
                        FROM landscan;''' % cmdDict)
                        
    def createCityRasters(self):
        from osm_cities import city_atlas
        list_of_cities = city_atlas().get_list_of_cities()['longname'].values
        add_safe_st_clip()
        tableNames = self.db.list_tables(schema='cities')
        for city in list_of_cities:
            if 'density_decile_'+city.lower() in tableNames:
                if self.forceUpdate:
                    self.db.execute('DROP TABLE cities.density_decile_%s;' % city.lower())
                else:
                    continue
            cmdDict = {'city': city,'reclassArg':self.reclassArg}
            print(('Creating density decile raster for city %s' % city))
            self.db.execute('''CREATE TABLE cities.density_decile_%(city)s AS
                            SELECT rid, ST_Reclass(ST_Band(safe_ST_Clip(rast,geom),3),1,'%(reclassArg)s','4BUI',15) AS rast
                            FROM landscan, cities.cityedges
                            WHERE city='%(city)s' AND iyear=3 
                                  AND ST_Intersects(rast,geom);''' % cmdDict)
      
    def getCityDensityDistribution(self, city):
        distribution = self.db.execfetch('''SELECT min, max, sum(count)::int FROM (
                   SELECT (ST_Histogram(rast, 1, 10, ARRAY[1,1,1,1,1,1,1,1,1,1])).*
                       FROM cities.density_decile_%s) t1 
                       GROUP BY min, max ORDER BY min;''' % city)
        df= pd.DataFrame([(dd['min'], dd['sum']) for dd in distribution], columns=['decile','count'])
        assert df.loc[df.decile>9,'count'].sum()==0
        df = df[df.decile<=9]
        df.decile=df.decile.astype(int)
        df.set_index('decile', inplace=True)
        dfsum =df['count'].sum()
        df['frc'] = df['count'].apply(lambda x: x*1./dfsum)
        return df

    def saveImage(self, filename, city=None):
        rc = rasterCon('none')
        gdal_command_line_flags = rc.gdal_command_line_flags()
        if city is not None:  # hack, because schema argument not working
            gdal_command_line_flags=gdal_command_line_flags.replace('rasterdata','cities')
            tableName = 'density_decile_'+city.lower()
        else:
            tableName = 'density_decile'
        
        # create colormap. So far, just blues, but this can be modified
        cmapFn = paths['scratch']+'colormap.tmp'
        with open(cmapFn,'w') as f:
            f.write('0% 247 251 255\n100% 8 48 107\nnv grey\nend')
        
        cmd = '''gdaldem color-relief PG:"%s table='%s' mode='2'" %s "%s"''''' % (gdal_command_line_flags, tableName, cmapFn, filename)
        print('Executing '+cmd)
        os.system(cmd)

def get_country_from_row_col(row, col, clusterradius, logger=None, db = None):
    """ This is excerpted from mapCell() to allow quiet (no plotting) country lookups.
row,col can be a pair of vectors
    """
    scalar=False
    if isinstance(row,int) and isinstance(col,int):
        row=[row]
        col=[col]
        scalar=True
    assert len(row)==len(col)
    db = psqlt.dbConnection(logger=logger, db=db)

    # get countries:
    countries=[]
    for arow,acol in zip(row,col):
        cmdDict = {'lat': (arow*1.0 / 21600. * -180) + 90,
               'lon': (acol*1.0 / 43200. *  360) - 180,
               }
        cmd = 'SELECT iso FROM '+ defaults['gadm']['TABLE']+''' WHERE ST_Intersects(the_geom, ST_SetSrid(ST_Makepoint(%(lon)s+1./240,  %(lat)s-1./240),4326)) LIMIT 1''' % cmdDict
        countryISO = db.execfetch(cmd)
        countryISO = '' if countryISO in [None,[]] else countryISO[0][0]
        countryname = osmt.country2ISOLookup()['ISOalpha2shortName'].get(countryISO,'')
        if countryname=='United States': countryname='USA'
        countries+=[(countryISO,countryname)]
    if scalar:
        countries=countries[0]
    return countries
        
def mapCell(row, col, clusterradius, titleStyle='analysis',clusterLabel=None, ax=None, usetex=True,logger=None, nbyn= None, db = None):
    """Saves an image of the street network in a raster cell
    if subTitleLab can be 'density', 'rowcol', or None
    titleStyle can be analysis, rowcol, mainFig or SI
    
    usetex=True allows for prettier formatting of the titles. 
    But this does not work in parallel (issue #454) so there is an option to turn it off"""
    nbyn = 1 if nbyn is None else nbyn
    db = psqlt.dbConnection(logger=logger, db=db)

    cmdDict = {'lat': (row / 21600. * -180) + 90,
               'lon': (col / 43200. *  360) - 180,
               'nbyn': nbyn,
               'edgeTableNames': '('+' UNION ALL '.join(['SELECT * FROM '+defaults['osm']['edge_tables'].replace('REGION',cont).replace('CLUSTERRADIUS',clusterradius)+', bounds WHERE ST_Intersects(geom, envelope)'  for cont in defaults['osm']['continents']])+')' 
               }
    
    # get country
    cmd = 'SELECT iso FROM '+ defaults['gadm']['TABLE']+''' WHERE ST_Intersects(the_geom, ST_SetSrid(ST_Makepoint(%(lon)s+1./240,  %(lat)s-1./240),4326)) LIMIT 1''' % cmdDict
    countryISO = db.execfetch(cmd)
    countryname = '' if countryISO in [None,[]] else osmt.country2ISOLookup()['ISOalpha2shortName'][countryISO[0][0]]
    if countryname=='United States': countryname='USA'
    
    # format lat/lon and label
    latlon = '(%.2f, %.2f)' % (cmdDict['lat']-1./240, cmdDict['lon']+1./240)
    if 0 and titleStyle in ['analysis', 'SI']:
        # Hyperlink will only work if matplotlib is initiated in pgf mode. But it should otherwise be ignored rather than cause trouble:  [Cannot get this to work. However, success with overwriting title to use href: raster_anecdotes
        latlon = r'\href{{https://www.google.ca/maps/@{lat},{lon},15z}}{{{ll}}}'.format(ll =  latlon,
        lat = cmdDict['lat'],
        lon = cmdDict['lon'])
    if clusterLabel is None: clusterLabel = '' 

    kludgeIfcolor = r'\bfseries' not in clusterLabel and usetex
    psubs = dict(clusterLabel=r'{\bfseries '*kludgeIfcolor+clusterLabel+'}'*kludgeIfcolor,
                 countryname=r'{\em '*usetex+countryname+'}'*usetex,
                 iso = countryISO,
                 ISO = countryISO,
                 rowcol=str((row,col)),
                 latlon=r'{\em '*usetex+latlon+'}'*usetex)
    psubs.update(latlon_to_urldict(cmdDict['lat']-1./240, cmdDict['lon']+1./240))
    
    if titleStyle in ['analysis','rowcol'] and 'ls_density' in db.list_columns_in_table('aggregated_by_cell_'+clusterradius):
        density = db.execfetch('SELECT ls_density FROM aggregated_by_cell_%s WHERE row=%s and col=%s;' % (clusterradius, row, col))
        densityStr = 'Density: N/A' if density==[] or density==[[None]] else 'Density: {:.0f}'.format(density[0][0])
    else:
        densityStr = 'Density: N/A'
    psubs.update(dict(densityStr=densityStr))            
    
    if titleStyle=='analysis' :
        plotTitle = 'Cluster {clusterLabel}. {countryname}\n{latlon}  {densityStr}'.format(**psubs)
    elif titleStyle=='rowcol': 
        plotTitle = 'Cluster {clusterLabel}. {countryname}\n{rowcol}  {densityStr}'.format(**psubs)
    elif titleStyle=='SI':
        plotTitle = 'Cluster {clusterLabel}.\nE.g. {countryname} {latlon}'.format(**psubs)
    elif titleStyle=='mainFig' or titleStyle=='mainFig_below':
        plotTitle = '{clusterLabel}\nE.g. {countryname} {latlon}'.format(**psubs)
    elif titleStyle=='none':
        plotTitle=''
    elif '{' in titleStyle:
        plotTitle= titleStyle.format(**psubs)
    else:
        raise Exception('titleStyle not recognized.')


    cmd = '''WITH bounds AS (SELECT ST_MakeEnvelope(%(lon)s, %(lat)s-%(nbyn)f/120::real, %(lon)s+%(nbyn)f/120::real, %(lat)s, 4326) AS envelope)
             SELECT ST_AsText((st_dump(ST_Intersection(geom,bounds.envelope))).geom), nocars FROM %(edgeTableNames)s AS edges, bounds;''' % cmdDict
    lines = db.execfetch(cmd)

    if ax is None:  # otherwise, use passed axes
        _, ax = plt.subplots(figsize=(2,2))
    for line in lines:
        if 'LINESTRING' not in line[0]: continue # this is just a single point that intersects the cell
        xs = [float(pt.split()[0]) for pt in line[0].strip('LINESTRING()').split(',')]
        ys = [float(pt.split()[1]) for pt in line[0].strip('LINESTRING()').split(',')]
        lw, lcolor = (0.75,'0.75') if line[1] is True else (1.5,'w') # thin gray line for nocars
        ax.plot(xs, ys,lw=lw,color=lcolor,linestyle='-')
    ax.set_facecolor('k')
    ax.set_xticks([])
    ax.set_xlim(cmdDict['lon'], cmdDict['lon']+nbyn*1./120)
    ax.set_ylim(cmdDict['lat']-nbyn*1./120, cmdDict['lat'])
    ax.set_yticks([])
    for spine in list(ax.spines.values()): # turn frame off. https://stackoverflow.com/questions/14908576/how-to-remove-frame-from-matplotlib-pyplot-figure-vs-matplotlib-figure-frame
        spine.set_visible(False)
    #ax.axis('scaled')  # constrain proportions
    if '_below' in titleStyle:
        ax.set_title(plotTitle, fontsize=7,y=-0.27, usetex=usetex)
    else:
        ax.set_title(plotTitle, fontsize=7, usetex=usetex)

    return ax

def make_and_save_gridcell_network_image(gridrow, gridcol, filename = None, nbyn= None,
                                         forceUpdate=False, clusterradius=None,
                                         db = None):
    """
    For the moment, this just makes a single figure for a single grid cell.
    """
    import matplotlib.pyplot as plt
    from kmeans_analysis import mapCell
    if filename is None:
        IMAGEPATH = paths['scratch']+'cellimages/'
        fn = IMAGEPATH+'network-{}-{}-{}.pdf'.format(ISO,gridrow,gridcol)
    else:
        fn= filename
    if os.path.exists(fn) and not forceUpdate:
        return
    fig,ax = plt.subplots(1)
    mapCell(gridrow, gridcol, clusterradius, ax = ax, nbyn=nbyn,  db=db)
    ax.set_title(r'')
    plt.axis('image')
    plt.savefig(fn)
    os.system(' pdfcrop {} {}'.format(fn,fn))
    return
        
