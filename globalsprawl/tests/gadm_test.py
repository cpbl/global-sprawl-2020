#!/usr/bin/python

"""GADM TEST"""

"""
Testing of GADM regions and gadm functions
"""

import os,sys
from osm_config import paths,defaults
import gadm
from tests.utils import print_,test_all
import pandas as pd

def test_build_lookup_from_list_of_tuplepairs():
    LUl2= gadm.build_lookup_from_list_of_tuplepairs(((1,2,3,4),(3,4,5,6)), 2)
    assert LUl2== {(1, 2): [(3, 4)], (3, 4): [(5, 6)]}
    LUl1= gadm.build_lookup_from_list_of_tuplepairs(((1,2),(3,4)),1)
    assert LUl1== {(1,): [(2,)], (3,): [(4,)]}

def test_adjacency_matrices():
    # deprecated
    if 0:  
        regi=gadm.regions_and_neighbours()
        allc=regi.get_all_GADM_countries()
        matl2=regi.get_GADM_adjacency_matrix(('DZA'),2)
        assert ('DZA', 26, 718) in matl2
        matl1=regi.get_GADM_adjacency_matrix(('DZA'),1)
        assert ('DZA',21) in matl1
        level_does_not_exist=regi.get_GADM_adjacency_matrix(('DZA'),3)
        assert all([cc[-1]==0 for cc in list(level_does_not_exist.values())[0]])  # level_does_not_exist is None
        print_ (len(regi.adjacencies))


#def GADM36_regions_and_neighbours():
#   ran=gadm.regions_and_neighbours()
#    ran2=ran.get_regions_and_neighbors(('DZA',39,1193), 2)
#VNM |   27 |  308 |  4376 |     0 |    0

def test_for_NaNs():
    ran=gadm.regions_and_neighbours()
    allc= ran.get_all_GADM_countries()
    cod = allc['COD']
    print(('COD',cod))
    assert cod['id_0']==0
    assert all([pd.notnull(x['id_0']) for x in list(allc.values())])
    

def test_GADM_regions_and_neighbors():

    ran=gadm.regions_and_neighbours()

    ran2=ran.get_regions_and_neighbors(('DZA',39,1193), 2)
    assert ('DZA',39,1199) in ran2
    assert ('DZA',39,1193) not in ran2 # Check against issue #108: core should not be in neighbours.
    assert len( ran2)==16  # 8 for a radius of 4000m, 16 for a radius of 10000m

    
    ran3=ran.get_regions_and_neighbors(('DZA',39,1193), 3)
    assert all([cc[-1]==0 for cc in ran3])  #ran3 is None # level 3 does not exist for that country.
    assert len(ran3)==len(ran2)

    ran2b=ran.get_regions_and_neighbors(('DZA',39), 2)
    assert ('DZA',12,354)  not in ran2b

    # Following should break: makes no sense for idlevel to be less deep than the region:
    try:
        ran1=ran.get_regions_and_neighbors(('DZA',39,1193), 1)
        raise('Invalid parameters were not caught. This_should_never_be_reached') # This should never be reached
    except AssertionError:
        pass

    # Test (version 3.6) that id_5=0 when it should:
    canadat = ran.get_regions_and_neighbors(('CAN',1,1),5)
    eg = canadat[0]
    print(eg)
    assert len(eg)==6 and eg[-1]==0
    # And that we are finding neighbours
    assert  ('CAN', 1, 2, 83, 0, 0) in  ran.get_regions_and_neighbors(('CAN',1,1),5) and  ('CAN', 1, 1, 33, 0, 0) in  ran.get_regions_and_neighbors(('CAN',1,2), 5)

    return

def test_overlapping_geometries(gadmLevel=None,skip=True):
    # GADM geometries should be (within some tolerance) spatially distinct
    # Otherwise, we need to remove an overlap

    # normally, we only want to test these once after installing a new version of gadm
    #   It takes too long and too many cores to run on a nightly basis!
    if skip: return 
    
    if gadmLevel is None:
        for gl in ['gid','iso','id_1','id_2','id_3','id_4','id_5']:
            test_overlapping_geometries(gl)
        return

    from osm_config import paths,defaults
    import postgres_tools as psqlt
    from cpblUtilities.parallel import runFunctionsInParallel
    import pandas as pd
    assert gadmLevel in ['gid','iso','id_0','id_1','id_2','id_3','id_4','id_5']

    parallel = defaults['server']['parallel']
    
    def getOverlapIds(iso, gl):
        db2=psqlt.dbConnection(curType='default')
        geomName = 'the_geom' if gl=='gid' else 'geom'
        tableName = defaults['gadm']['TABLE'] if gl=='gid' else defaults['gadm']['TABLE']+'_'+gl
        cmd = """WITH t1 AS (SELECT %s AS id1, %s AS g1 FROM %s WHERE iso='%s') 
                 SELECT id1, %s AS id2, ST_Area(ST_Intersection(%s, g1)::geography)/1000/1000
                 FROM %s, t1 WHERE (ST_Intersects(%s, g1) AND NOT ST_Touches(g1, %s))
                    AND %s!=id1""" % (gl, geomName, tableName, iso, gl, geomName, tableName, geomName, geomName, gl)
        fetched = db2.execfetch(cmd)
        return pd.DataFrame(fetched, columns=[gl+'1', gl+'2', 'overlap_kmsq']).set_index(gl+'1')
    
    # Parallelization is over isos, not gids, because they run so fast that parallel overhead becomes very large   
    # But this doesn't seem to parallelize well because of database locks...why? Does dbConnection need to
    db = psqlt.dbConnection(curType='default')
    isos = [(ii[0]) for ii in db.execfetch('SELECT iso from %s_iso' % (defaults['gadm']['TABLE']))]
    funcs,names=[],[]
    for iso in isos:
        funcs+=[[getOverlapIds,[iso, gadmLevel]]]
        names+=['Overlap test '+gadmLevel+'_'+iso]
    overlaps = runFunctionsInParallel(funcs,names=names,parallel=parallel)
    overlaps = pd.concat(overlaps[1])

    if overlaps.overlap_kmsq.max()>0.005:      # no polygon should have more than 0.05 km2 overlap with another polygon
        raise Exception ('Overlaps identified. See pandas dataframe for details: %s' % (paths['scratch']+'gadmOverlaps_'+gadmLevel+'.pickle'))
        overlaps.sort_values('overlap_kmsq', inplace=True)
        overlaps.to_pickle(paths['scratch']+'gadmOverlaps_'+gadmLevel+'.pickle')
        
    return
    
def test_overlapping_geometries2(skip=True):
    # A second test, which should be redundant with test_overlapping_geometries, 
    # is to dissolve sub-gadmLevel0 polygons and see if the areas match
    
    # normally, we only want to test these once after installing a new version of gadm
    #   It takes too long and too many cores to run on a nightly basis!
    if skip: return 
    
    import postgres_tools as psqlt
    import pandas as pd
    
    db = psqlt.dbConnection(curType='default')
    isos = [ii[0] for ii in db.execfetch('SELECT iso from %s_iso' % defaults['gadm']['TABLE'])]
    areasDf = pd.DataFrame()
    for iso in isos:
        cmd = """SELECT ST_Area(geom::geography) AS diss_area FROM %s_iso WHERE iso='%s'""" % (defaults['gadm']['TABLE'], iso) 
        areasDf.loc[iso, 'diss_area'] = db.execfetch(cmd)[0][0]
        cmd = """SELECT SUM(ST_Area(the_geom::geography)) AS all_area FROM %s WHERE iso='%s'""" % (defaults['gadm']['TABLE'], iso)
        areasDf.loc[iso, 'all_area'] = db.execfetch(cmd)[0][0]
    
    areasDf['areaDiff'] = (areasDf.all_area - areasDf.diss_area).abs()
    assert areasDf.areaDiff.max()<500, \
        areasDf.loc[ areasDf['areaDiff']>=500 ]  # note this is sq m
    
    return

def test_listit2():
    assert gadm.listit2(([('AUT'),('BUT'),('CAN')])) == [['AUT'], ['BUT'], ['CAN']]
    assert gadm.listit2(((('AUT'),('BUT'),('CAN')))) == [['AUT'], ['BUT'], ['CAN']]
    assert gadm.listit2((('AUT',2,2),('AUT',2,2))) == [['AUT', 2, 2], ['AUT', 2, 2]]
    assert gadm.listit2('AUT') == ['AUT']

def _main():
    current_module=sys.modules[__name__]
    test_all(current_module)
  
if __name__ == "__main__":
    _main()
