#! /usr/bin/python

import sys
from copy import deepcopy
import pandas as pd
import numpy as np
import postgres_tools as psqlt
import osm_lookups
from osm_config import defaults, GADM_params
from gadm import GADM_schema
from osmTools import CONTINENT_IDS,define_table_names

settings=defaults['tests']
VERBOSE=settings['verbose']
CONTINENTS=settings['test_continents']
SAMPLE_SIZE=settings['sample_size']
IGNORE=settings['ignore']

format_gadm_ids=lambda x : [ int(y) if not isinstance(y,str) else '\''+y+'\''
                             for y in x ]

class fakedata():
    """A fake data maker. Intended to be used for unit testing of various stages
    of the osm_master flow.

    fakedata(continent='fakedata',clusterradius='10',gadm_fake_version_name='fake')

    Example usage:
        datamaker=fakedata()
        datamaker.build_all() # Creates tables of fake node data, edge data,
            # GADM data, GADM adjacency data, lookup data, and disconnectivity
            # data. These tables are named with the same conventions as the
            # tables of other continents, using the class attributes continent,
            # clusterradius and GADM_TABLE.
        osm_lookups.GADM_TABLE=datamaker.GADM_TABLE # Set the GADM_TABLE
            # variable of the modules you're testing to the value of the
            # fakedata's GADM_TABLE variable
        ## Test stuff
        osm_lookups.GADM_TABLE=datamaker.clean_up() # Uncautiously delete all
            # tables containing the class attribute continent. Return the
            # original value of GADM_TABLE so it can be reset in the modules
            # being tested.
    """
 
    def __init__(self,continent='fakedata',clusterradius='10',
                 gadm_fake_version_name='fake'):
        self.continent=continent
        assert self.continent not in list(CONTINENT_IDS.keys()),"""
               Do not choose a real continent name, or all its data will be
               uncautiously erased from the db when the class's clean_up 
               function is called."""
        self.clusterradius=clusterradius
        self.original_GADM_VERSION= defaults['gadm']['VERSION']
        defaults['gadm'].update(GADM_params(gadm_fake_version_name)) #Global effect on defaults
        self.GADM_TABLE= defaults['gadm']['TABLE']

#             dict(#GADM_VERSION=gadmVersion,
#                      VERSION= self.GADM_TABLE,
#                      # Name of full GADM table. Can be changed to create a separate, customized
#                      # version of the GADM data.
#                      GADM_TABLE= self.GADM_TABLE,
#                      TABLE= self.GADM_TABLE,
#                      GADM_FILE=None,
#                      FILE=None,
#                      GADM_ID_LEVELS = ['iso', 'id_1', 'id_2', 'id_3', 'id_4', 'id_5'],
#                      ID_LEVELS = ['iso', 'id_1', 'id_2', 'id_3', 'id_4', 'id_5']
#                      ))
        
        # Size (in degrees squared) of the continent
        self.dist=1
                
    # Put some fake pointmetric data in the database
    #
    # Nodes contains columns cluster_id, onemetric, anothermetric, and
    # yetanothermetric
    #
    # Edges table is called edges_%s_%s%(self.continent,self.clusterradius) and
    # contains columns edge_id, onemetric, anothermetric, and
    # yetanothermetric
    #
    # Puts tables in the user's processingSchema
    def make_fake_node_and_edge_data(self,include_geom=True):
        self.nodedata=[ {'cluster_id':0,'onemetric':1,'anothermetric':2,
                        'yetanothermetric':3},
                        {'cluster_id':1,'onemetric':np.nan,
                         'anothermetric':np.nan,'yetanothermetric':np.nan},
                        {'cluster_id':2,'onemetric':np.nan,
                         'anothermetric':np.nan,'yetanothermetric':np.nan},
                        {'cluster_id':3,'onemetric':1,'anothermetric':2,
                         'yetanothermetric':3},
                        {'cluster_id':4,'onemetric':2,'anothermetric':3,
                         'yetanothermetric':4},
                        {'cluster_id':5,'onemetric':np.nan,
                         'anothermetric':np.nan,'yetanothermetric':np.nan},
                        {'cluster_id':6,'onemetric':np.nan,
                         'anothermetric':np.nan,'yetanothermetric':np.nan},
                        {'cluster_id':7,'onemetric':1,'anothermetric':2,
                         'yetanothermetric':3},               
                        {'cluster_id':8,'onemetric':2,'anothermetric':3,
                         'yetanothermetric':4},
                        {'cluster_id':9,'onemetric':np.nan,
                         'anothermetric':np.nan,'yetanothermetric':np.nan},
                        {'cluster_id':10,'onemetric':np.nan,
                         'anothermetric':np.nan,'yetanothermetric':np.nan},
                        {'cluster_id':11,'onemetric':np.nan,
                         'anothermetric':np.nan,'yetanothermetric':np.nan},
                        {'cluster_id':12,'onemetric':1,
                         'anothermetric':3,'yetanothermetric':1},                         
                      ]
        # Turn fake nodes data into pandas df...
        nodes_df=pd.DataFrame(self.nodedata)
        nodes_df['nocars'] = False
        nodes_df.loc[nodes_df.cluster_id==12,'nocars'] = True  # have one nocar node

        # ...and into a fake data table in the db
        db=psqlt.dbConnection()
        self.nodes_table_name=define_table_names(self.continent,
                                                 self.clusterradius)['node_tables'][self.clusterradius]
        db.df2db(nodes_df,self.nodes_table_name,
                 schema=defaults['osm']['processingSchema'])
        db.fix_permissions_of_new_table(self.nodes_table_name,
                                        schema=defaults['osm']['processingSchema'])
        if include_geom:
            self.make_fake_node_geom()

        # Make fake edges tables
        self.edgedata=deepcopy(self.nodedata)
        for i in range(len(self.edgedata)):
            _dict=self.edgedata[i]
            _dict['edge_id']=_dict['cluster_id']
            _dict['spt_cluster_id']=_dict['cluster_id']
            _dict['ept_cluster_id']=(_dict['cluster_id']+1)%len(self.nodedata)
            del _dict['cluster_id']
        edges_df=pd.DataFrame(self.edgedata)
        edges_df['nocars'] = False
        self.edges_table_name=define_table_names(self.continent,
                                                 self.clusterradius)['edge_tables'][self.clusterradius]
        db.df2db(edges_df,self.edges_table_name,
                 schema=defaults['osm']['processingSchema'])
        db.fix_permissions_of_new_table(self.edges_table_name,
                                        schema=defaults['osm']['processingSchema'])
        if include_geom:
            self.make_fake_edge_geom()

    def make_fake_edge_geom(self):
        db=psqlt.dbConnection()
        db.execute("""select addgeometrycolumn('{}','{}','geom',
                      4326,'LineString',2)
                      ;""".format(defaults['osm']['processingSchema'],
                                  self.edges_table_name)
                  )
        cmd="""with lgeoms as ( 
               with sgeoms as ( select geog::geometry sgeom,spt_cluster_id from 
                                {} edges,{} nodes where
                                cluster_id=spt_cluster_id ),
                egeoms as ( select geog::geometry egeom,ept_cluster_id from
                            {} edges,{} nodes where
                            cluster_id=ept_cluster_id )
                select sgeom,egeom,edge_id from sgeoms,egeoms,{} edges
                where edges.spt_cluster_id=sgeoms.spt_cluster_id and
                edges.ept_cluster_id=egeoms.ept_cluster_id
               )

               update {}
               set geom=st_makeline(sgeom,egeom) from lgeoms where 
               lgeoms.edge_id={}.edge_id
               ;""".format(self.edges_table_name,self.nodes_table_name,
                           self.edges_table_name,self.nodes_table_name,
                           self.edges_table_name,self.edges_table_name,
                           self.edges_table_name,self.edges_table_name
                          )
        db.execute(cmd)
        db.addColumnToTable(table=self.edges_table_name,columnname='length_m',
                            columntype='integer'
                           )
        cmd="""update {}
               set length_m=cast(round(st_length(geom::geometry)) as int)
               ;""".format(self.edges_table_name)
        db.execute(cmd)

    def make_fake_node_geom(self):
        db=psqlt.dbConnection()
        db.addColumnToTable(table=self.nodes_table_name,columnname='geog',
                            columntype='geography(Point,4326)'
                           )
        divisor=10/self.dist
        cmd="""update {}
               set geog=st_setsrid(st_point(((10%(cluster_id+1))/{})::double precision,
                                              (cluster_id/{})::double precision),
                                   4326)::geography
               ;""".format(self.nodes_table_name,divisor,divisor)
        db.execute(cmd)

    def make_fake_disconnectivity_data(self):
        self.disconnectivitydata=[ {'iso':'a','onemetric':1,'anothermetric':2,
                        'yetanothermetric':3,'n_nodes':40},
                        {'iso':'b','onemetric':8,
                         'anothermetric':8,'yetanothermetric':6,'n_nodes':20},
                        {'iso':'c','onemetric':4,
                         'anothermetric':2,'yetanothermetric':200,'n_nodes':74},
                        {'iso':'d','onemetric':1,'anothermetric':2,
                         'yetanothermetric':3,'n_nodes':21},
                        {'iso':'e','onemetric':2,'anothermetric':3,
                         'yetanothermetric':4,'n_nodes':5},
                        {'iso':'f','onemetric':2,
                         'anothermetric':9,'yetanothermetric':7,'n_nodes':11},
                        {'iso':'g','onemetric':7,
                         'anothermetric':7,'yetanothermetric':3,'n_nodes':42},
                        {'iso':'h','onemetric':1,'anothermetric':2,
                         'yetanothermetric':3,'n_nodes':95},               
                        {'iso':'i','onemetric':2,'anothermetric':3,
                         'yetanothermetric':4,'n_nodes':44},
                        {'iso':'j','onemetric':6,
                         'anothermetric':2,'yetanothermetric':7,'n_nodes':79},
                        {'iso':'k','onemetric':3,
                         'anothermetric':1,'yetanothermetric':1,'n_nodes':51},
                        {'iso':'l','onemetric':10,
                         'anothermetric':11,'yetanothermetric':7,'n_nodes':75}
                      ] 
        disconnectivity_df=pd.DataFrame(self.disconnectivitydata)
        # ...and into a fake data table in the db
        db=psqlt.dbConnection()
        db.df2db(disconnectivity_df,'disconnectivity_iso_%s_%s'%(self.continent,self.clusterradius),
                 schema=defaults['osm']['analysisSchema'])
        db.fix_permissions_of_new_table('disconnectivity_iso_%s_%s'%(self.continent,self.clusterradius),
                                        schema=defaults['osm']['analysisSchema'])

    # Put some fake lookup data in the database
    #
    # Cluster lookup table is called lookup_%s_cluster%s_%s%(self.continent,
    # self.clusterradius,self.GADM_TABLE)
    #
    # Edge lookup table is called lookup_%s_edge%s_%s%(self.continent,
    # self.clusterradius,self.GADM_TABLE)
    #
    # Puts tables in the user's processingSchema
    def make_fake_lookup_data(self):
        osm_lookups.GADM_TABLE=self.GADM_TABLE
        _continents=defaults['osm']['continents']
        _cr=defaults['osm']['clusterRadii']
        _landscan,_ghsl=defaults['osm']['landscan'],defaults['osm']['ghsl']
        defaults['osm']['continents']=[self.continent]
        defaults['osm']['clusterRadii']=[self.clusterradius]
        defaults['osm']['landscan'],defaults['osm']['ghsl'] = False, False
        osm_lookups.create_all_lookups(forceUpdate=True)
        osm_lookups.GADM_TABLE= GADM_params(self.original_GADM_VERSION)['TABLE'] # This effect is actually global, right?
        defaults['osm']['continents']=_continents
        defaults['osm']['clusterRadii']=_cr
        defaults['osm']['landscan'],defaults['osm']['ghsl']=_landscan,_ghsl
        
    # Put some fake gadm data in the database
    # Creates 6 new tables in the GADM_SCHEMA imported from the gadm module:
    #   # self.GADM_TABLE
    #   # self.GADM_TABLE+'iso'
    #   # self.GADM_TABLE+'id_1'
    #   # self.GADM_TABLE+'id_2'
    #   # self.GADM_TABLE+'id_3'
    #   # self.GADM_TABLE+'id_4'
    #   # self.GADM_TABLE+'id_5'
    #
    # IMPORTANT: If testing modules that import gadm's GADM_TABLE, you might
    # want to change the value of GADM_TABLE to the value of this class's
    # GADM_TABLE attribute (and change it back; see doc for class function 
    # clean_up()).
    def make_fake_gadm_data(self,include_geom=True):
        self.gadmdata=[ {'iso':'a','id_0':1,'id_1':1,'id_2':1,'id_3':0,'id_4':0,
                         'id_5':0},
                        {'iso':'a','id_0':1,'id_1':1,'id_2':1,'id_3':0,'id_4':0,
                         'id_5':0},
                        {'iso':'a','id_0':1,'id_1':2,'id_2':1,'id_3':0,'id_4':0,
                         'id_5':0},
                        {'iso':'b','id_0':2,'id_1':1,'id_2':1,'id_3':1,'id_4':0,
                         'id_5':0},
                        {'iso':'b','id_0':2,'id_1':2,'id_2':1,'id_3':1,'id_4':0,
                         'id_5':0},
                        {'iso':'b','id_0':2,'id_1':2,'id_2':1,'id_3':1,'id_4':0,
                         'id_5':0},
                        {'iso':'b','id_0':2,'id_1':2,'id_2':1,'id_3':1,'id_4':0,
                         'id_5':0},
                        {'iso':'c','id_0':3,'id_1':1,'id_2':1,'id_3':1,'id_4':1,
                         'id_5':0},
                        {'iso':'c','id_0':3,'id_1':1,'id_2':1,'id_3':1,'id_4':1,
                         'id_5':0},
                        {'iso':'c','id_0':3,'id_1':2,'id_2':2,'id_3':1,'id_4':1,
                         'id_5':0},
                        {'iso':'c','id_0':3,'id_1':2,'id_2':2,'id_3':1,'id_4':1,
                         'id_5':0},
                        {'iso':'c','id_0':3,'id_1':2,'id_2':2,'id_3':2,'id_4':1,
                         'id_5':0}
                      ]
 
        for _dict in self.gadmdata:
            _dict['ls_pop']=np.random.randint(10)
            _dict['ls_area']=np.nan
            _dict['ls_pixels']=np.nan
            _dict['ls_density']=np.nan
            _dict['uid']= '_'.join([str(_dict[k]) for k in defaults['gadm']['ID_LEVELS']])
        gadm_df=pd.DataFrame(self.gadmdata).drop_duplicates(subset=['iso',
                                                            'id_1','id_2',
                                                            'id_3','id_4',
                                                            'id_5']
                                                           )
        db=psqlt.dbConnection()
        db.df2db(gadm_df,self.GADM_TABLE,schema=GADM_schema)
        db.fix_permissions_of_new_table(self.GADM_TABLE,schema=GADM_schema)
        for _id in [ 'id_5','id_4','id_3','id_2','id_1','iso' ]:
            db.df2db(gadm_df,'%s_%s'%(self.GADM_TABLE,_id),schema=GADM_schema)
            del gadm_df[_id]

        if include_geom:
            self.make_fake_gadm_geom_data()

    def _get_split_pattern(self,n):
        # Find the arrangement of n smaller rectangles (srec) to fit inside a 
        # larger rectangle (brec) such that height(brec)/height(srec) ~=
        # width(brec)/width(srec)
        dim=int(n**0.5)
        dims=(dim,n/dim)
        while int(dims[1])!=dims[1]:
            dim-=1
            dims=(dim,n/dim)
        return (dims[0],int(dims[1]))

    def _get_intervals(self,limit,interval,rev=False):
        if not rev:
            retval=0
            while retval < limit:
                retval+=interval
                yield retval
        elif rev:
            retval=limit
            while retval > 0:
                retval-=interval
                yield retval

    # Yields the coordinates of fake gadm regions
    def _get_regions(self,dims):
        # Iterate in reverse for every other row of rectangle, to ensure all
        # regions are continuous
        istep=self.dist/dims[0]
        jstep=self.dist/dims[1]
        count=-1
        for j in self._get_intervals(self.dist,jstep):
            count+=1
            rev=(count%2==1)
            _iter=self._get_intervals(self.dist,istep,rev=rev)
            for i in _iter:
                if not rev:
                    yield (i-istep,j-jstep,i,j)
                elif rev:
                    yield (i,j-jstep,i+istep,j)

    # Make some fake geometry data by basically creating one big rectangle, and
    # dividing it up into as many equal pieces as there are fake gadm regions
    def make_fake_gadm_geom_data(self):
        db=psqlt.dbConnection()
       
        # Add geom_compromise column to fake gadm table
        db.execute("""select addgeometrycolumn('{}','{}','the_geom',
                      4326,'MultiPolygon',2)
                      ;""".format(GADM_schema,self.GADM_TABLE)
                  )
 
        # Get number of fake gadm regions
        cmd="select count(*) from {};".format(self.GADM_TABLE)
        db.execute(cmd)
        nregions=db.fetchone()[0]
        dims=self._get_split_pattern(nregions)
        
        # Make fake gadm regions
        # Give the arbitrary rectangle coordinates
        # ((0,0),(0,dist),(dist,dist),(dist,0))
        gadm_id_str=','.join(defaults['gadm']['ID_LEVELS'])
        for region in self._get_regions(dims):
            # Iterate through rows in the fake gadm table in order to ensure
            # each region's geometry is continuous
            cmd="select {} from {} where the_geom is null order by ({});".format(
                gadm_id_str,self.GADM_TABLE,gadm_id_str)
            db.execute(cmd)
            ids=db.fetchone()
            if isinstance(ids,type(None)):
                continue
            whereclause=' and '.join([ '{}={}'.format(gl,id_) for gl,id_ in
                                       zip(defaults['gadm']['ID_LEVELS'],format_gadm_ids(ids))
                                     ])
            # Add in fake geometry data
            cmd="update {} set the_geom=st_multi(st_makeenvelope({},4326)) where {};".format(
                self.GADM_TABLE,str(region).lstrip('(').rstrip(')'),
                whereclause)
            db.execute(cmd)

        # Add the_geom column
        if 0: # Following hard to comprehend
            cmd="select st_srid(geom_compromise) from {} limit 1;".format(self.GADM_TABLE)
            db.execute(cmd)
            srid=db.fetchone()[0]
        srid = defaults['osm']['srid_compromise']

        db.execute("""select addgeometrycolumn('{}','{}','geom_compromise',
                      {},'MultiPolygon',2)
                      ;""".format(GADM_schema,self.GADM_TABLE, srid))
        cmd="update {} set geom_compromise=st_transform(the_geom,{});".format(
             self.GADM_TABLE,srid)
        db.execute(cmd)

    def make_fake_gadm_adjacency_table(self):
        import gadm
        gadm.GADM_TABLE=self.GADM_TABLE
        assert 'fake' in defaults['gadm']['TABLE']
        gadm.regions_and_neighbours().build_GADM_adjacency_table(forceUpdate=True)
        gadm.GADM_TABLE=defaults['gadm']['TABLE']

    def build_all(self):
        self.make_fake_node_and_edge_data()
        self.make_fake_gadm_data()
        self.make_fake_gadm_adjacency_table()
        self.make_fake_lookup_data()
        self.make_fake_disconnectivity_data()

    # Uncautiously delete all tables containing self.continent.
    # Returns the value of GADM_TABLE imported directly from the gadm module
    # so it's easy to reset the GADM_TABLE variable for imported modules. Ex.
    #
    # import aggregate_connectivity_metrics as aggmetrics
    # datamaker=fakedata()
    # aggmetrics.GADM_TABLE=datamaker.GADM_TABLE
    # ###Test some stuff###
    # aggmetrics.GADM_TABLE=datamaker.clean_up()
    def clean_up(self):
        db=psqlt.dbConnection()
        db.delete_all_tables(contains=self.continent,force_do_not_ask=True)
        return defaults['gadm']['TABLE']

#Only prints if verbose is True
def print_(string=""):
    if VERBOSE:
        print (string)



# Run all the functions beginning with 'test_' in a given module.
#
# Parameters: - current_module (module) - The module to search for functions
#               from.
#             - **kwargs (optional) - A dict of arguments to pass to the
#               functions being run. If passed, these arguments will be passed
#               to EVERY function run.
def test_all(current_module,**kwargs):
    import inspect
    import psycopg2
    IGNORE.extend(['test_all','__nonzero__','print_'])
    all_functions = [(nn,ff) for nn,ff in
                     inspect.getmembers(current_module,inspect.isfunction) if
                     nn not in IGNORE and nn[:5]=='test_'
                    ]
    for nn,ff in all_functions:
        print_("====== NEXT TEST:%s:%s ======"%(current_module.__doc__,nn))
        try:
            ff(**kwargs)
        except Exception as e:
            if isinstance(e,psycopg2.ProgrammingError) and "does not exist" in str(e):
                print ("The table or column queried does not exist. You probably need to change test_continents in config.cfg")
                sys.exit(1)
            else:
                raise

def random_select(continent,n,obj,parallel=False,bridges=False):
        import numpy as np
        import postgres_tools as pgt
        dbc=pgt.dbConnection()

        if parallel:
            #Return random sample of parallel edges
            pass

        elif bridges:
            #Return random sample of bridges
            pass

        dbc.execute("""SELECT %s_id FROM %ss_%s_10"""%(obj,obj,continent))
        randsample=[ int(ii[0]) for ii in dbc.fetchall() ]
        to_select=np.random.randint(low=0,high=len(randsample)-1,size=n)
        randsample=[ sample for sample in randsample if randsample.index(sample) in to_select ]
        return randsample
