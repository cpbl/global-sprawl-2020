#!/usr/bin/python

"""AGGREGATION TEST"""

"""
Test that the aggregation procedure, executed by class functions in 
aggregate_connectivity_metrics.aggregate_node_and_edge_results, properly 
aggregates node and edge metrics compiled at the pointmetrics stage.
"""

import sys
import numpy as np
import pandas as pd
import copy
import osm_lookups
import postgres_tools as psqlt
import aggregate_connectivity_metrics as aggmetrics
from osm_config import defaults
from .utils import test_all
from .utils import fakedata

def _reset_db(object_=fakedata()):
    # Reset GADM_TABLE
    aggmetrics.GADM_TABLE=object_.clean_up()
    osm_lookups.GADM_TABLE=object_.clean_up()

# Tests if nodal metrics are the sums of nodal metrics at smaller GADM regions
# Assumes the aggregation step has already been run
# Can be used on 'real' continents as well
def _test_node_metrics(continent,clusters=['10','7'],nodal_metrics='default',
                       density=False,ghsl=False):

    if nodal_metrics=='default':
        nodal_metrics=aggmetrics.aggregate_node_and_edge_results().node_metrics

    db=psqlt.dbConnection(verbose=True)
    schema=defaults['osm']['processingSchema']
    density=[True,False] if density else [False]
    ghsl=[True,False] if ghsl else [False]
    for cluster in clusters:
        for gadmlevel in range(1,6):
            for densityItem in density:
                for ghslItem in ghsl:
                    for metric in nodal_metrics:
                        strdict={'gadm_ids':('id_'+', id_'.join([str(gl) for gl 
                                             in range(gadmlevel) ])).replace(
                                             'id_0','iso'),
                                 'schema':schema,
                                 'gadmlevel':'id_'+str(gadmlevel),
                                 'agggadmlevel':('id_'+str(
                                                 gadmlevel-1)).replace('id_0',
                                                 'iso'),
                                 'continent':continent,
                                 'cluster':cluster,
                                 'metric':metric,
                                 'density':',density_decile' if densityItem else '',
                                 'ghsl':',ghsl_yr' if ghslItem else '',
                                 '_density':'_density' if densityItem else '',
                                 '_ghsl':'_ghsl' if ghslItem else ''
                                } 
                        cmd="""
                        select %(gadm_ids)s%(density)s%(ghsl)s from
                        %(schema)s.aggregated_%(agggadmlevel)s_%(continent)s_%(cluster)s%(_density)s%(_ghsl)s
                        group by ( %(gadm_ids)s%(density)s%(ghsl)s )
                        ;"""%strdict
                        db.execute(cmd)
                        agggadmids=[ ids for ids in db.fetchall() ]
                        for aggid in agggadmids:
                            aggid=[ '\''+aggid[0]+'\'' ]+[ str(int(aid)) for aid 
                                                           in aggid[1:] ]
                            strdict['aggid']='('+','.join(aggid)+')'
                            cmd="""
                            with truesum as (
                                select sum(%(metric)s) ts from
                                %(schema)s.aggregated_%(gadmlevel)s_%(continent)s_%(cluster)s%(_density)s%(_ghsl)s
                                where (%(gadm_ids)s%(density)s%(ghsl)s)=
                                %(aggid)s
                            ),
                            checksum as (
                                select %(metric)s cs from
                                %(schema)s.aggregated_%(agggadmlevel)s_%(continent)s_%(cluster)s%(_density)s%(_ghsl)s
                                where (%(gadm_ids)s%(density)s%(ghsl)s)=
                                %(aggid)s
                            )
                            select truesum.ts,checksum.cs from truesum,checksum
                            ;"""%strdict
                            db.execute(cmd)
                            sums=db.fetchone()
                            sums=[ float(s) if s else 0.0 for s in sums ]
                            assert sums[0]==sums[1]

# Test aggregation procedure as a whole
def test_aggregation():
    db=psqlt.dbConnection()

    datamaker=fakedata()
    datamaker.build_all()
    # And make sure that all modules that reference it reference the correct
    # (fake) table
    aggmetrics.GADM_TABLE=datamaker.GADM_TABLE
    osm_lookups.GADM_TABLE=datamaker.GADM_TABLE
    
    # Aggregate
    aggregator=aggmetrics.aggregate_node_and_edge_results(datamaker.continent,
                                                        datamaker.clusterradius)
    aggregator.node_metrics={ 'sum_of_onemetric':'onemetric',
                              'sum_of_anothermetric':'anothermetric',
                              'sum_of_yetanothermetric':'yetanothermetric'
                            }
    aggregator.edge_metrics={ 'sum_of_ametric': '0' }
    aggregator.run_all()

    # Check metrics at id_5 level
    cmd="""
        select iso,id_1,id_2,id_3,id_4,id_5,
        sum_of_onemetric,sum_of_anothermetric,sum_of_yetanothermetric 
        from %s.aggregated_id_5_fakedata_10
        ;"""%defaults['osm']['processingSchema']
    res=db.execfetch(cmd)
    res=dict(list(zip([ tuple(r[:6]) for r in res ],[r[6:] for r in res ])))

    # Check that metrics are calculated as expected
    assert res['a',1,1,0,0,0]==[1.0,2.0,3.0]
    assert res['a',2,1,0,0,0]==[0,0,0]
    assert res['b',1,1,1,0,0]==[3.0,5.0,7.0]
    assert res['b',2,1,1,0,0]==[0,0,0]
    assert res['c',1,1,1,1,0]==[1.0,2.0,3.0]
    assert res['c',2,2,1,1,0]==[2.0,3.0,4.0]
    assert res['c',2,2,2,1,0]==[0,0,0]

    # Check that metrics sum as expected
    _test_node_metrics(datamaker.continent,clusters=[datamaker.clusterradius],
                        nodal_metrics=aggregator.node_metrics)

    # Clean up
    _reset_db(datamaker)

# Test scaling factor used to reweight nodal metrics during aggregation
def test_scaling_factor():
    db=psqlt.dbConnection()

    # Turn fake data into a fake data table in the db
    datamaker=fakedata()
    datamaker.build_all()

    # Change the value of GADM_TABLE so the aggregator finds the right table
    aggmetrics.GADM_TABLE=datamaker.GADM_TABLE

    # Select regionmetric data using instance of 
    # aggmetrics.aggregate_node_and_edge_results
    aggregator=aggmetrics.aggregate_node_and_edge_results(datamaker.continent,
                                                        datamaker.clusterradius,
                                                        0)
    aggregator.node_metrics={ 'sum_of_onemetric':'onemetric',
                              'sum_of_anothermetric':'anothermetric',
                              'sum_of_yetanothermetric':'yetanothermetric'
                            }
    aggregator.scale_nodal_sums=True
    res={ 'a':{'sum_of_onemetric':np.nan,'sum_of_anothermetric':np.nan,
               'sum_of_yetanothermetric':np.nan},
          'b':{'sum_of_onemetric':np.nan,'sum_of_anothermetric':np.nan,
               'sum_of_yetanothermetric':np.nan},
          'c':{'sum_of_onemetric':np.nan,'sum_of_anothermetric':np.nan,
               'sum_of_yetanothermetric':np.nan}
        }
    strdict={'continent':datamaker.continent,
             'clusterradius':datamaker.clusterradius,
             'gadmtable':datamaker.GADM_TABLE
            }
    for metric in list(aggregator.node_metrics.keys()):
        scaled_metric=aggregator.aggregate_node_metrics(metric)
        strdict['metric']=scaled_metric
        cmd="""
            with cluster_id_list as ( select cluster_id,iso from
                     lookup_%(continent)s_cluster%(clusterradius)s_%(gadmtable)s
                                    )
            select iso,%(metric)s from nodes_%(continent)s_%(clusterradius)s as 
            clusters,
            cluster_id_list as lookup
            where clusters.cluster_id=lookup.cluster_id
            group by iso
            ;"""%strdict
        aggdata=db.execfetch(cmd)
        assert len(aggdata)==3
        for row in aggdata:
            res[row[0]][metric]=row[1]

    # Check that its calculated as expected
    assert res['a']=={'sum_of_onemetric':1,'sum_of_anothermetric':2,
                      'sum_of_yetanothermetric':3}
    assert res['b']=={'sum_of_onemetric':3,'sum_of_anothermetric':5,
                      'sum_of_yetanothermetric':7}
    assert res['c']=={'sum_of_onemetric':3,'sum_of_anothermetric':5,
                      'sum_of_yetanothermetric':7}    

    # Clean up tables
    datamaker.clean_up()

        
def _main():
    current_module=sys.modules[__name__]
    test_all(current_module)

if __name__=='__main__':
    _main()
