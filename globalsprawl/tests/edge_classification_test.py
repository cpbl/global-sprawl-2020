#!/usr/bin/python

"""EDGE CLASSIFICATION TEST"""

"""
Test classifications of edges in edges tables.
"""

import sys
import time
import networkx as nx
import numpy as np
import create_roadgraph_tables as crt
import postgres_tools as pgt
import osm_networkx as osmnx
import osmTools as osmt
from osm_config import paths,defaults
from tests.utils import print_,test_all,SAMPLE_SIZE

global CONTINENT
CONTINENT=None
dbc=pgt.dbConnection()

gadmLevels={'monaco':'iso',
        'grid':'id_5',
        'hell':'id_5',
        'medieval':'id_5'
}

def test_grid_classification(continent=CONTINENT):
    if continent!='grid':
        return
    dbc.execute("""SELECT classification,spt_degree,ept_degree FROM edges_grid_10;""")
    info=dbc.fetchall()
    classifications,spt_deg,ept_deg=[],[],[]
    for ii in info: 
        classifications.append(ii[0])
        spt_deg.append(ii[1])
        ept_deg.append(ii[2])
    assert False not in [ ss in [ 3,4 ] for ss in spt_deg ]
    assert False not in [ ee in [ 3,4 ] for ee in ept_deg ]
    assert True not in [ cl in [ 'D','S' ] for cl in classifications ]

def test_selfloops_and_deadends(continent=CONTINENT):
    for cr in defaults['osm']['clusterRadii']:
        dbc.execute("""SELECT COUNT(DISTINCT edge_id) FROM edges_%s_%s;"""%(continent,cr))
        num_edges=int(dbc.fetchall()[0][0])
        dbc.execute("""SELECT edge_id,classification,spt_cluster_id,ept_cluster_id,spt_degree,ept_degree FROM edges_%s_%s"""%(continent,cr))
        edge_info=dbc.fetchall()
        random_edges=np.random.randint(num_edges-1,size=SAMPLE_SIZE)
        for re in random_edges:
            edge_id,classification,spt_clust_id,ept_clust_id,spt_deg,ept_deg=edge_info[re]
            assertionerror_="Problematic edge %d in table edges_%s_%s"%(edge_id,continent,cr)
            if classification!='D':
                assert not (spt_deg==1 or ept_deg==1),assertionerror_
            else:
                assert spt_deg==1 or ept_deg==1,assertionerror_
            if classification!='S':
                assert spt_clust_id!=ept_clust_id,assertionerror_
            else:
                assert spt_clust_id==ept_clust_id,assertionerror_

def test_parallels(continent=CONTINENT):
    #Assert all parallels edges have been correctly reclassified as cycles
    #or left as self-loops

    dbc.execute("SELECT classification FROM edges_continent_10 WHERE edge_id in (SELECT edge_id FROM edges_continent_10 WHERE ( least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id) ) in ( SELECT least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id) FROM edges_continent_10 GROUP BY least (spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id) HAVING COUNT(*) > 1 ));".replace('continent',continent))
    info=dbc.fetchall()
    parallel_edge_classifications=set([ ii[0] for ii in info ])
    assert 'B' not in parallel_edge_classifications
    assert 'D' not in parallel_edge_classifications

def test_unclassified(continent=CONTINENT):
    dbc.execute("SELECT edge_id from edges_continent_10 WHERE classification is null".replace('continent',continent))
    unclassified=[ ii[0] for ii in dbc.fetchall() ]
    for ue in unclassified:
        dbc.execute("SELECT edge_id from lookup_%s_edge10_gadm28 WHERE edge_id=%d"%(continent,int(ue)))
        assert len(dbc.fetchall())==0,ue

def _main():
    current_module=sys.modules[__name__]
    if sys.argv[1:]:
        continents=sys.argv[1:]
    else:
        from tests.utils import CONTINENTS as continents
    for cc in continents:
        CONTINENT=cc
        print_ ("In "+CONTINENT)
        test_all(current_module,continent=CONTINENT)

if __name__=='__main__':
    _main()
