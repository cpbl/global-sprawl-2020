#! /usr/bin/python

"""CONTINENT TABLE TESTS"""

"""
Tests involving queries of segs, juncs, edges and nodes tables.
"""

import sys
import numpy as np
import postgres_tools as pgt
import psycopg2
import osmTools as osmt
import create_roadgraph_tables as crt
from tests.utils import VERBOSE,SAMPLE_SIZE,print_,test_all
from osm_config import paths,defaults

dbc=pgt.dbConnection(verbose=VERBOSE)
global CONTINENT
CONTINENT=None
global CLUSTER
CLUSTER=None
gadmLevels={'monaco':'iso',
	'grid':'id_5',
	'hell':'id_5',
	'medieval':'id_5'
	}

# Make sure all juncs have node_ids
def test_junc_table_completeness(continent=CONTINENT,cluster=CLUSTER):
    if not(isinstance(cluster, list)): cluster = [cluster] 
    clusterer=crt.define_node_clusters(region=continent,clusterRadii=cluster,forceUpdate=False)
    junc_table=clusterer.table_names['junc_table']
    clusterer.db.execute("""SELECT COUNT(node_id IS NOT NULL) FROM %s"""%junc_table)
    with_indices=clusterer.db.fetchall()
    clusterer.db.execute("""SELECT COUNT(*) FROM %s"""%junc_table)
    assert with_indices==clusterer.db.fetchall()

#Ensure all starting and ending clusters in edges tables are in nodes table
def test_cluster_ids(continent=CONTINENT,cluster=CLUSTER):
    if not(isinstance(cluster, list)): cluster = [cluster] 
    for clust in cluster:
        dbc.execute("""SELECT spt_cluster_id,ept_cluster_id FROM edges_%s_%s""" % (continent,clust))
        edge_info=dbc.fetchall()
        eIDs=[]
        for i in range(len(edge_info)):
            eIDs.append(edge_info[i][0])
            eIDs.append(edge_info[i][1])
        eIDs=set(eIDs)
        dbc.execute("""SELECT cluster_ID from nodes_%s_%s""" % (continent,clust))
        nIDs=set([ nn[0] for nn in dbc.fetchall() ])
        assert not eIDs.difference(nIDs),'e'

#Test selecting minimum length of set of parallel edges
def test_min_length(continent=CONTINENT,sample_size=SAMPLE_SIZE,cluster=CLUSTER):
    #Select unique edges, and one of every set of parallel edges
    #Select length, or for parallel edges, the length of the shortest edge in the set of parallel edges
    if not(isinstance(cluster, list)): cluster = [cluster] 
    for clust in cluster:
        cmd="""SELECT DISTINCT ON (least(spt_cluster_id,ept_cluster_id),
            greatest(spt_cluster_id,ept_cluster_id))
        spt_cluster_id,ept_cluster_id,min(length_m)
        OVER (PARTITION BY (least(spt_cluster_id,ept_cluster_id),
            greatest(spt_cluster_id,ept_cluster_id)))
        FROM edges_%s_%s
        ;"""%(continent, clust)
        dbc.execute(cmd)
        lookup={}
        i=0
        for row in dbc.cursor:
            lookup[i]=[ tuple(sorted([ int(row[0]),int(row[1]) ])), row[2] ]
            i+=1
    
        #Ensure we've selected only unique edges
        assert len(list(lookup.keys()))==len(set([ val[0] for val in list(lookup.values()) ]))
  
        randsample=np.random.randint(low=0,high=len(list(lookup.keys()))-1,size=sample_size)
        for i in randsample:
            dbc.execute("""SELECT min(length_m)
            FROM edges_%s_%s
            WHERE (least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id))=%s
            ;"""%(continent,clust,str(lookup[i][0])))
            minlength=dbc.fetchall()[0][0]
            assert minlength==lookup[i][1]

#Make sure all edges totally within buffers were deleted at the segs stage
def test_small_edge_deletion(continent=CONTINENT,sample_size=SAMPLE_SIZE,cluster=CLUSTER):
    for clust in cluster:
        # Depending on the stage reached, we may have already converted the edge geom column to geog
        edgeGeoCol = 'geog' if 'geog' in dbc.list_columns_in_table('edges_CONTINENT_CLUSTER'.replace("CONTINENT",continent).replace("CLUSTER",clust)) else 'geom'
            
        dbc.execute("""
        WITH breproj as (
        WITH ereproj as ( select edge_id, EDGE_GEO_COL egeo,spt_cluster_id sclust,ept_cluster_id eclust from edges_CONTINENT_CLUSTER )
        SELECT edge_id,egeo,geom bgeom from CONTINENT_buffers_CLUSTER,ereproj
            WHERE cluster_id = ereproj.sclust or cluster_id = ereproj.eclust )
        SELECT edge_id from breproj where st_within(egeo::geometry,bgeom) and st_length(egeo::geography) < CUTOFF;
        """.replace("CONTINENT",continent).replace("CLUSTER",clust).replace('EDGE_GEO_COL',edgeGeoCol).replace("CUTOFF",str(4*int(clust))))
        assert not dbc.fetchone()

#Make sure all edges intersect their spt/ept cluster
# Note: this might fail legitimately. Edges are rerouted to the cluster centroid, which is occasionally outside the buffer
# To fix this (if it crops up), perhaps replace st_intersects(egeom,ngeom) with st_intersects(egeom,st_convexHull(ngeom))
def test_endpoint_clusters(continent=CONTINENT,sample_size=SAMPLE_SIZE,cluster=CLUSTER):
    for clust in cluster:
        edgeGeoCol = 'geog' if 'geog' in dbc.list_columns_in_table('edges_CONTINENT_CLUSTER'.replace("CONTINENT",continent).replace("CLUSTER",clust)) else 'geom'
        cmd = """
        with edges as ( select edge_id, EDGE_GEO_COL egeo,spt_cluster_id,ept_cluster_id from edges_CONTINENT_CLUSTER
            LIMITCLAUSE),
        nodes as ( select cluster_id, geog ngeog from nodes_CONTINENT_CLUSTER ),
        buffers as ( select cluster_id, geom bgeom from CONTINENT_buffers_CLUSTER )
        select edge_id from edges,nodes,buffers where
        ( nodes.cluster_id = edges.spt_cluster_id or nodes.cluster_id = edges.ept_cluster_id )
        and not st_intersects(egeo::geometry,ngeog::geometry) and
        ( not st_within(egeo::geometry,bgeom) or st_length(egeo::geography) > CUTOFF );
        """.replace("CONTINENT",continent).replace("CLUSTER",clust).replace('EDGE_GEO_COL',edgeGeoCol).replace("LIMITCLAUSE","limit %d"%sample_size if sample_size!="all" else "").replace("CUTOFF",str(2*int(clust)))
        dbc.execute(cmd)
        assert not dbc.fetchone()

def test_annealing(continent=CONTINENT,cluster=CLUSTER):
    for clust in cluster:
        dbc.execute("""select count(*) from edges_%s_%s where ( spt_degree=2 or ept_degree=2 )
        and spt_cluster_id!=ept_cluster_id;"""%(continent,clust))
        assert not dbc.fetchone()[0]

#Make sure clean_segs_table deletes all isolated self-loops (see #141)
def test_isolated_selfloops_are_gone(continent=CONTINENT,sample_size=SAMPLE_SIZE,cluster=CLUSTER):
    dbc.execute("select source from osm_%s_segs where source=target"%continent+("""
        limit %s"""%str(sample_size))*(sample_size!='all')+";")
    loopNodes=[ i[0] for i in dbc.fetchall() ]
    for node in loopNodes:
        dbc.execute("""select distinct edge_id from
        ( select edge_id,source from osm_%s_segs where source=%d
        union
        select edge_id,target source from osm_%s_segs where target=%d )
        foo;"""%(continent,node,continent,node))
        edges=[ i[0] for i in dbc.fetchall() ]
        assert len(edges)>1

#Make sure edge lengths aren't significantly different from seg lengths
# Note: this function requires keeping the pre-anneal backup table, withdeg2_edges_*, which is dropped in the production version
# So normally this test will be skipped because the table does not exist
def test_edge_lengths(continent=CONTINENT,sample_size=SAMPLE_SIZE,cluster=CLUSTER):
    from osmTools import CONTINENT_IDS
    for clust in cluster:
        try:
            dbc.execute("""with edgesComp as (
                with segs as (
                select edge_id,st_length(geom::geography) length_m from osm_CONTINENT_segs limit SAMPLESIZE )
                select segs.edge_id sid,edges.edge_id eid,segs.length_m sl,edges.length_m el,spt_cluster_id,ept_cluster_id
                from segs join withdeg2_edges_CONTINENT_CLUSTER edges
                on edges.edge_id=CLUSTER*CID*10000000000+segs.edge_id )
                select sid,eid,sl,el,sum(nnodes)
                from edgesComp left join CONTINENT_buffers_CLUSTER
                on (spt_cluster_id=cluster_id or ept_cluster_id=cluster_id)
                group by (sid,eid,sl,el)
                ;""".replace('CONTINENT',continent).replace('SAMPLESIZE',str(sample_size)).replace('CLUSTER',clust).replace('CID',str(CONTINENT_IDS[continent])))
        except Exception as e:
            if isinstance(e,psycopg2.ProgrammingError) and "does not exist" in str(e): 
                print('withdeg2_edges table does not exist. Skipping test_edge_lengths')
                continue
            else:
                print((str(e)))
                sys.exit(1)

        info=[ ii for ii in dbc.fetchall() ]
        edges=[ ii[:2] for ii in info ]
        lengths=[ ii[2:4] for ii in info ]
        buffersizes=[ ii[4] for ii in info ]
        for i in range(len(lengths)):
            assert abs(lengths[i][0]-lengths[i][1])<=buffersizes[i]*int(clust) if buffersizes[i] else 1, \
            'Seg id: %d\nEdge id: %d\nSeg length: %d\nEdge length: %d'%(edges[i][0],edges[i][1],lengths[i][0],lengths[i][1])

def _main():
    current_module=sys.modules[__name__]
    continents=sys.argv[1:]
    CLUSTER=[ arg for arg in sys.argv if arg in [ '10','7' ] ]
    if not CLUSTER:
        CLUSTER=defaults['osm']['clusterRadii']
    if len(continents)==0:
        from tests.utils import CONTINENTS as continents
    for cc in continents:
        CONTINENT=cc
        print_("In "+CONTINENT)
        test_all(current_module,continent=CONTINENT,cluster=CLUSTER)    

if __name__=='__main__':
    _main()
