#!/usr/bin/python

"""RASTERTOOLS TEST"""

"""
Tests for functions in rasterTools.
"""

import os,sys,random
import numpy as np
from osm_config import paths,defaults
import rasterTools as rt
from tests.utils import print_,test_all

def test_rasterTiling():
    ls = rt.rasterCon('landscan')
    assert ls.name=='landscan'
    
    # test no data values
    print_('Testing no data values...')
    ndvs = ls.get_noDataValues()
    assert isinstance(ndvs, dict)
    assert all([ndvs[ii]==-2147483647 for ii in [1,2,3]])
    
    # shape and tiling
    print_('Testing shape and tiling...')
    assert ls.get_bbox() == ((-180, -90), (-180, 90), (180, 90), (180, -90), (-180, -90))
    assert np.round(ls.get_scale()[0], 10)==np.round(-1./120,10)
    assert np.round(ls.get_scale()[1], 10)==np.round(1./120,10)
    assert ls.get_tileSizeDegrees()==(-0.25, 0.25)
    assert ls.get_tileSizePixels() ==(30,30)
    assert ls.get_nTiles() == (180/0.25)*(360/0.25)
    assert ls.get_nTilesYX() == ((180/0.25), (360/0.25))
    assert ls.get_nBands() == 3

def test_LandscanValues():
    #Test some points - Brixton, London; St Viateur Bagel, Montreal; Aus. desert
    print_('Testing Landscan values...')
    ls = rt.rasterCon('landscan')
    for pop, pixelarea, lon, lat in [(5578, 0.5343848524, -0.106,   51.463),
                                     (2418, 0.6009640797, -73.603, 45.523),
                                     (0, 0.7888303878, 124.337, -23.118),]:
        assert ls.get_lonLat((lon, lat),1)==pop
        assert np.round(ls.get_lonLat((lon, lat),2),10)==pixelarea
    # try an ocean
    oceanLonLat = (-142, 2)
    assert np.isnan(ls.get_lonLat(oceanLonLat,1))
    assert np.isnan(ls.get_lonLat(oceanLonLat,2))
        
    # make sure areas are the same at a given lat
    for nlat in range(50):
        lat = random.random()*180-90
        area = np.nan
        for nlon in range(100):
            lon = random.random()*360-180
            newarea = ls.get_lonLat((lon, lat),2)
            assert area==newarea or np.isnan(area) or np.isnan(newarea)
            area=newarea

def test_get_gadmArray():
    print_('Testing get_gadmArray() and get_gadmAgg()...')
    ls = rt.rasterCon('landscan')
    # population and area values are from gadm v 3.6. If you use a different version of GADM (or Landscan), need to adjust
    # These are test areas at different GADM levels (e.g. a country, a US state,etc.)
    for iso,id_1,id_2,id_3,id_4,id_5, pop, area, wtdens in [('PER',None,None,None,None,None,29816454,1295958,23.01),
                                                            ('LUX',None,None,None,None,None,512636, 2569,199.55),
                                                            ('USA',8,None,None,None,None,946332,5156,183.54),
                                                            ('FRA',7,46,168,1752,19298,439,4,115.25)]:
        GADM_ids = [gadmId for gadmId in [iso,id_1,id_2,id_3,id_4,id_5] if gadmId is not None]
        print_('Testing %s' % iso)
        array    = ls.get_gadmArray(GADM_ids,bands=[1,2,3])
        poparray = ls.get_gadmArray(GADM_ids)
        aggpop   = ls.get_gadmAgg(GADM_ids)
        aggarray = ls.get_gadmAgg(GADM_ids,bands=[1,2,3])
        
        assert np.nansum(array[:,:,0]) == pop
        assert np.nansum(poparray)     == pop
        assert aggpop['sum'] == pop
        assert aggarray['sum'][0] == pop
        assert np.round(np.nansum(array[:,:,1])) == area
        assert np.round(aggarray['sum'][1]) == area
        
        assert np.round(aggarray['mean'][2],2)==wtdens
        assert abs(np.round(aggarray['mean'][0]/aggarray['mean'][1])-np.round(aggarray['mean'][2]))<=1
        
        density = array[:,:,0]/array[:,:,1] 
        assert np.all(np.isclose(density, array[:,:,2], 1e-08, equal_nan=True))
            
def test_FullLandscanArray():
    print_('Testing full landscan array (this may take a while)...')
    ls = rt.rasterCon('landscan')
    array = ls.asarray([1,2,3])
    poparray = ls.asarray()
    
    assert poparray.shape==(21600, 43200)
    assert array.shape==(21600, 43200,3)
    assert np.nansum(poparray)==7094525520  # world population
    assert np.nansum(array[:,:,0])==7094525520 
    assert np.round(np.nansum(array[:,:,1]))==147042963 # world land area
    
    density = array[:,:,0]/array[:,:,1] 
    assert np.all(np.isclose(density, array[:,:,2], 1e-08, equal_nan=True))
    
    # test some random coordinate
    for lon, lat in [(-0.106,   51.463), (-73.603, 45.523), (124.337, -23.118),]:
        cell = rt.latLonToCell(lat,lon)
        pop = ls.get_lonLat((lon, lat),1)
        pixelarea = ls.get_lonLat((lon, lat),2)
        assert poparray[cell]==pop
        assert array[cell][0]==pop
        assert array[cell][1]==pixelarea
        
def test_FullGadmArray():
    print_('Testing full GADM array (this may take a while)...')
    ga = rt.rasterCon('gadmraster')
    array = ga.asarray(list(range(1,7)))
    minRid = ga.execfetch('SELECT MIN(rid) FROM '+ga.name)[0][0]
    missingHeaderRows = int(float(minRid)/(43200./30))*30
    assert array.shape==(17190, 43200,6)
    for lat, lon in [(83.5916666666, -33.883333), (20.5839791, -105.230),
                          (45.8307432, 0.0960251),
                          (33.6512638, 133.6515272)]:
        gadmIds = np.array([ga.get_lonLat((lon,lat),band) for band in range(1,7)])
        cell = rt.latLonToCell(lat,lon,missingHeaderRows)
        assert np.all(gadmIds == array[cell])

def _main():
    print_('Skipping rasterTools_test; test rasters don\'t currently exist')
    return
    current_module=sys.modules[__name__]
    test_all(current_module) 

if __name__ == "__main__":
    _main()
