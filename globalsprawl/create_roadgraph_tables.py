#!/usr/bin/python
from osm_config import paths,defaults
import os,scipy, math
import pandas as pd
import numpy as np
import postgres_tools as psqlt
import osmTools as osmt
import psycopg2
from rawOSMinfo import SEGS_TO_DELETE


def str_reps(dosubs,ss):
    for a,b in list(dosubs.items()):
        ss=ss.replace(a,b)
    return(ss)



class define_node_clusters():
    """
    This acts on the initial segs table import from OSM. 
    The objective is to create a juncs table of nodes, to create buffers around each node, 
    and then to group sets of nearby nodes into "clusters" or "junctions".

    This is packaged into a class only because it is normally run all together.
    """
    def __init__(self, region, clusterRadii=None, forceUpdate=True):
        """
        Define the single region, and the list of clusterRadii, to be processed.
        """
        if clusterRadii is None:
            clusterRadii=defaults['osm']['clusterRadii']
        assert isinstance(clusterRadii,list) and [isinstance(cr, str) for cr in clusterRadii]
        self.region=region
        table_names=osmt.define_table_names(region, clusterRadii)
        self.table_names=table_names
        self.logger=osmt.osm_logger('define_node_clusters_'+region, verbose=True, timing=True, prefix=region)
        # Tables in this section go on the "osmdata" schema:
        self.db = osmt.pgisConnection(schema = defaults['osm']['osmSchema'], verbose=True, logger=self.logger)
        self.forceUpdate=forceUpdate

    def create_all(self):
        self.clean_segs_table()
        self.create_junctions()
        #self.create_nodebuffer_tables()  # deprecated - no longer needed
        for cluster_radius in list(self.table_names['nodebuffer_tables'].keys()):
            self.assign_buffers_and_calculate_cluster_degree(cluster_radius)
        self.create_merged_buffer_geom_tables()

    def clean_segs_table(self):
        """
        Some anomalies in the data break the osm_master run.
        If the number of these is relatively small, we consider them anomalies and delete them.
        Before doing any processing of the raw data, here we delete a few problematic segs.
        The final run, in fact, does not delete any of these segs
        """
        if self.region in list(SEGS_TO_DELETE.keys()):
            self.logger.write( 'Deleting %d segs from %s: %s'%(len(SEGS_TO_DELETE[self.region]),self.region,str(SEGS_TO_DELETE[self.region])) )
            where_condition=osmt.transform_psql_output_to_qgis_query(SEGS_TO_DELETE[self.region]).replace('edge','osm')
            seg_table=self.table_names['seg_table']
            self.db.execute("""DELETE FROM %s WHERE %s;"""%(seg_table,where_condition))

        #Delete isolated self-loops (see #141)
        self.logger.write( 'Deleting isolated self loops from segs table' )
    
        cmd="""DELETE FROM %(seg_table)s WHERE source=target
        AND source NOT IN (
            SELECT source FROM %(seg_table)s
            GROUP BY source HAVING COUNT(source)>1
            UNION
            SELECT target source FROM %(seg_table)s
            GROUP BY target HAVING count(target)>1
            )
        RETURNING ST_Length(geom::geography);"""%{'seg_table':self.table_names['seg_table']}
        self.db.execute(cmd)
        lengths=[ i[0] for i in self.db.fetchall() ]
        self.logger.write( 'Deleted self loops have lengths %s'%', '.join(map(str,lengths)) )
  
    def create_junctions(self): 
        """ Grab all the start nodes and the end nodes from the edges in seg_table. Put them together, take a unique set, and create a new table of those nodes.

        Does not depend on cluster_radii.
        """

        seg_table,junc_table=self.table_names['seg_table'],self.table_names['junc_table']
        cmd="""
        DROP TABLE IF EXISTS JUNCTABLE ;"""*self.forceUpdate +"""
        CREATE TABLE JUNCTABLE AS 
                SELECT DISTINCT ON (osm_node_id) * FROM (
                (SELECT r.osm_source_id AS osm_node_id, r.edge_id, ST_StartPoint(r.geom)::geography as geog
                FROM SEGTABLE as r
                UNION
                SELECT rr.osm_target_id AS osm_node_id, rr.edge_id, ST_EndPoint(rr.geom)::geography as geog
                FROM SEGTABLE as rr) 
        ) as fooo; 
        """
        self.logger.write('Creating junctions table')
        self.db.execute(cmd.replace('SEGTABLE',seg_table).replace('JUNCTABLE',junc_table))
        self.logger.write('Creating indices')
        self.db.execute('''ALTER TABLE '''+junc_table+''' ADD COLUMN node_id SERIAL UNIQUE''' )
        try:
            self.db.create_indices(junc_table,key='node_id', non_unique_columns=['osm_node_id'], gadm=False, geog=True)
        except: 
                raise Exception("Couldn't Make Index on %s" % junc_table)
        return

    def create_nodebuffer_tables(self):
        """
        This Function (re)creates the needed buffer tables by making a buffer around each node. It does not merge/cluster them yet.
        
        It does this for all cluster_radii identified in the class instantiation.
        
        DEPRECATED as the merged buffers are created directly from the junctions table.
        But this function is left here in case we want it for visualization
        """
        junc_table=self.table_names['junc_table']

        self.logger.write('Starting create_nodebuffer_tables')

        for buff,bufTableName in list(self.table_names['nodebuffer_tables'].items()): 
            assert '.' not in buff
            self.logger.write( bufTableName)
            if self.forceUpdate:
                self.db.execute('''DROP TABLE IF EXISTS %s''' % bufTableName)

            cmdDict = {'bufTableName':bufTableName, 'buff':buff, 'junc_table':junc_table}
            self.db.execute('''CREATE TABLE %(bufTableName)s AS
               (SELECT ST_Buffer(p.geog, %(buff)s)::geometry AS geom, 
                       p.node_id AS node_id
                 FROM %(junc_table)s as p)''' % cmdDict)

            self.db.create_indices(bufTableName,'node_id',geom='geom')

    def assign_buffers_and_calculate_cluster_degree(self,clusterRadius,forceUpdate=False, selfloopFactor=4):#, deleteBuffers=True):
        """
        Calculate nodal degree of each intersection (i.e. clusters of buffers)
        """
        if not forceUpdate:
            forceUpdate=self.forceUpdate
        self.logger.write('Starting calculate_buffer_values')
        seg_table,junc_table,buf_table=self.table_names['seg_table'],self.table_names['junc_table'],self.table_names['nodebuffer_tables'][clusterRadius]

        # Create sparse matrix of which junctions overlap (i.e. are adjacent)
        self.logger.write( "Creating adjacency list for buffer %s" % clusterRadius)  #, (time.time() - starttime) / 60)  )
        adj_list = self.make_adjacency_list(clusterRadius)

        # return array of cluster_ids for each of connected buffers
        # cluster_ids is a list of cluster ids in order of junction id
        # note that unconnected buffers have their own cluster_id
        numClusters, cluster_ids = scipy.sparse.csgraph.connected_components(adj_list)
        del adj_list  # save memory
        
        # Continent IDs make cluster IDs globally unique
        continentIds=osmt.CONTINENT_IDS
        # By adding a large integer, give each cluster a unique name across clusterRadius values. Skip for incomplete continents (so that we don't have to change CONTINENT_IDS if we add a new frc complete)
        cluster_id_offset=0 if 'incomplete' in self.region else int(int(clusterRadius)*1e9)+  int((continentIds[self.region])*1e11)  
        cluster_ids = [cid+cluster_id_offset for cid in cluster_ids]
        
        # Write this to a temp table, that can then be joined to the buffer, junction, and segs tables
        # Easier to do this from a pandas dataframe. Note: relies on continuous index of junctions starting at 0
        self.logger.write( "Writing node-to-cluster lookup table for buffer %s" % clusterRadius) 
        lookupTableName = self.region+'_tmp_cluster_lookup_'+clusterRadius
        clusterIdDf = pd.DataFrame(cluster_ids, columns = ['cluster'+clusterRadius+'_id'])
        clusterIdDf.index.name='node_id'
        
        self.db.df2db(clusterIdDf.reset_index(),lookupTableName)
        
        # Use this new table to update the nodes (junctions) table
        self.db.merge_table_into_table(from_table=lookupTableName, 
                                       targettablename=junc_table, commoncolumns='node_id',
                                       forceUpdate=forceUpdate)
        self.db.execute('DROP TABLE %s' % lookupTableName)

        # Calculate nodal degree - how many segs intersect each junction?
        # Write this to nodes (junctions) table first
        self.logger.write( "Calculating nodal degree for buffer %s" % clusterRadius) 
        tmpTable = self.region+'_tmp_'+clusterRadius
        cmdDict = {'seg_table':seg_table,'junc_table':junc_table,'tmpTable':tmpTable,'cr':clusterRadius, 'maxL':float(clusterRadius)*selfloopFactor}
        cmd = """CREATE TABLE %(tmpTable)s AS
                 WITH edges AS
                    (SELECT sj.cluster%(cr)s_id AS startcluster, tj.cluster%(cr)s_id AS endcluster
                            FROM  %(seg_table)s AS s, %(junc_table)s AS sj, %(junc_table)s AS tj
                            WHERE osm_source_id=sj.osm_node_id AND osm_target_id = tj.osm_node_id
                                  AND (sj.cluster%(cr)s_id!=tj.cluster%(cr)s_id OR ST_Length(s.geom::geography)>%(maxL)s))
                 SELECT node_id, degree::int AS degree%(cr)s FROM
                    (SELECT cluster_id, SUM(n_edges) AS degree FROM (
                    SELECT startcluster AS cluster_id, COUNT(*) AS n_edges FROM edges GROUP BY startcluster
                    UNION ALL
                    SELECT endcluster   AS cluster_id, COUNT(*) AS n_edges FROM edges GROUP BY endcluster) AS ntemp
                GROUP BY cluster_id) AS clusters, %(junc_table)s AS nodes
                WHERE nodes.cluster%(cr)s_id = clusters.cluster_id""" % cmdDict
        self.db.execute(cmd)
        self.db.merge_table_into_table(from_table=tmpTable, targettablename=junc_table, 
                                       commoncolumns='node_id', forceUpdate=forceUpdate)
        self.db.execute('DROP TABLE %s' % tmpTable)

        # Write nodal degree to edges
        self.logger.write('Writing nodal degree to edges')
        cmd = """CREATE TABLE %(tmpTable)s AS
                 SELECT s.edge_id, sj.degree%(cr)s AS spt_deg_%(cr)s, sj.cluster%(cr)s_id AS spt_clust_%(cr)s, 
                        tj.degree%(cr)s AS ept_deg_%(cr)s, tj.cluster%(cr)s_id AS ept_clust_%(cr)s
                 FROM %(seg_table)s AS s, %(junc_table)s AS sj, %(junc_table)s AS tj
                 WHERE sj.osm_node_id=s.osm_source_id AND tj.osm_node_id=s.osm_target_id
                       AND (sj.cluster%(cr)s_id!=tj.cluster%(cr)s_id OR ST_Length(s.geom::geography)>%(maxL)s)""" % cmdDict
        self.db.execute(cmd)
        self.db.merge_table_into_table(from_table=tmpTable, targettablename=seg_table, 
                                       commoncolumns='edge_id', forceUpdate=forceUpdate)
        self.db.execute('DROP TABLE %s' % tmpTable)
                
        return
        

    def create_merged_buffer_geom_tables(self):
        """
        This merges the buffers around each node-buffer into cluster buffers, creating a new table with those outlines.  
        It only does the ones with multiple nodes assigned. 
        """
        nodebuffer_tables,clusterbuffer_tables=self.table_names['nodebuffer_tables'],self.table_names['buffer_tables']        
        self.logger.write('Starting create_buffer_tables (ie cluster-buffers)')
        for cluster_radius in list(self.table_names['nodebuffer_tables'].keys()):
            cmdDict = {'schema': defaults['osm']['osmSchema'],
                       'buff_table': self.table_names['buffer_tables'][cluster_radius], 
                       'junc_table': self.table_names['junc_table'], 'cr': cluster_radius} 
            if self.forceUpdate:
                self.db.execute('''DROP TABLE IF EXISTS %(schema)s.%(buff_table)s''' % cmdDict)
            self.logger.write('Creating merged buffers around junctions')

            cmd = """CREATE TABLE %(schema)s.%(buff_table)s AS
                     WITH c as (SELECT cluster%(cr)s_id, COUNT(node_id) AS nnodes FROM %(schema)s.%(junc_table)s GROUP BY cluster%(cr)s_id) 
                     SELECT ST_Union(ST_Buffer(geog, %(cr)s)::geometry) AS geom, 
                            cluster%(cr)s_id AS cluster_id, AVG(degree%(cr)s) AS degree, nnodes
                          FROM (SELECT j.*, c.nnodes FROM %(schema)s.%(junc_table)s j, c 
                                  WHERE j.cluster%(cr)s_id=c.cluster%(cr)s_id and c.nnodes>1) t1
                          GROUP BY cluster%(cr)s_id, nnodes;""" % cmdDict
            self.db.execute(cmd)
            
            self.db.create_indices(clusterbuffer_tables[cluster_radius],'cluster_id',geom='geom')
            _schema=self.db.default_schema
            self.db.ensure_consistent_geom(clusterbuffer_tables[cluster_radius],
                                           column='geom', schema=_schema)
    
    def make_adjacency_list(self,cluster_radius):
        """
        This Function builds an adj_list using a python dictionary.
        This is used to determine which buffers touch each other instead
        of merging potentially large numbers of geometry.

        Returns a NxN sparse matrix
        with (i,j) = True if buffer i intersects buffer j

        This is a separate function in part for memory management reasons? (Claim not checked)
        """
        #b = str(cluster_radius)
        #buf_table = self.table_names['nodebuffer_tables'][str(cluster_radius)]
        seg_table,junc_table=self.table_names['seg_table'],self.table_names['junc_table']

        nodeCount=self.db.execfetch('''SELECT COUNT (j.node_id)  FROM %s as j;''' % junc_table)[0][0]
        # note that dok_matrix is more efficient for inserts; will convert to csr afterwards
        # size is N+1 x N+1 because buffers are indexed from 1..N, not 0...N-1
        outMatrix = scipy.sparse.dok_matrix((nodeCount + 1, nodeCount + 1), dtype=bool)

        # Get all pairs of nodes which are within twice the cluster_radius of each other
        ls_out=self.db.execfetch('''SELECT a.node_id, b.node_id
                            FROM %s AS a, %s as b
                            WHERE ST_DWITHIN(a.geog,b.geog, %d) AND
                            a.node_id != b.node_id;''' % (junc_table, junc_table, 2*int(cluster_radius)))
        ls_out = [(item[0],item[1]) for item in ls_out]  # convert list of lists to list of tuples

        # now convert to a dictionary with True values for each (i,j) pair, and that dictionary populates the matrix
        outMatrix._update(zip(ls_out, [True] * len(ls_out)))

        outMatrix = outMatrix.tocsr()  # convert to compressed sparse row format
        return outMatrix

class final_graph_builder():
    """
    Class to create the cluster tables, the roadgraph tables, and their respective indices, using the road_segs and road_juncs tables.
    Instantiate it with the settings you want to use.  Call it without continents to do all of them.

    Note that we now have a multigraph, since many dual carriageways will be  going from/to the same cluster nodes. We have choices about what to do with them.

    These steps are all pretty quick; they take half a day.
    """
    def __init__(self,region,clusterRadii=None,forceUpdate=False,   build_all=False,  db=None, logger= None):

        if clusterRadii is None:
            clusterRadii=defaults['osm']['clusterRadii']
        assert isinstance(clusterRadii,list)
        self.clusters=clusterRadii
        self.region=region
        table_names=osmt.define_table_names(region, clusterRadii)
        self.table_names=table_names
        if logger is None:
            self.logger=osmt.osm_logger('final_graph_builder'+self.region, verbose=True, timing=True, prefix=region)
        else:
            self.logger=logger
        if db is None:
            self.db = osmt.pgisConnection(verbose=True, logger=self.logger, schema = defaults['osm']['osmSchema'], )
        else:
            self.db=db
        self.forceUpdate=forceUpdate

        if build_all:
            self.build_all()

    def build_all(self):
        #for continent in self.CONTINENTS:
        for cluster in self.clusters:
            # Following methods also call the create_indices methods whenever table is recreated
            self.create_clusters_tables_from_nodes_tables(cluster)
            self.create_roadgraph_tables_from_segs_and_clusters_tables(cluster)


    def create_clusters_tables_from_nodes_tables(self, cluster):
        tnames=osmt.define_table_names(self.region)
        tablename,junc_table=tnames['cluster_tables'][cluster], defaults['osm']['osmSchema']+'.'+tnames['junc_table'] 
        if self.db.check_whether_table_exists(
                                               schema=defaults['osm']['processingSchema'],
                                               tablename=tablename,
                                               drop_if_exists=self.forceUpdate
                                             ):
            return
        strs=dict(
                    CONTINENT=self.region,
                    NNCL=cluster,
                    TABLENAME=defaults['osm']['processingSchema']+'.'+tablename,
                    JUNCTABLE=junc_table
                 )
        cmd=str_reps(strs,"""
    CREATE TABLE TABLENAME
    (
      geog geography(Point,4326),
        cluster_id BIGINT,
      degree integer
    );
        """)
        """
      CONSTRAINT TABLENAME_cluster_id_key UNIQUE (cluster_id)
    )
    WITH (
      OIDS=FALSE
    );
    ALTER TABLE TABLENAME
      OWNER TO osmusers;
    GRANT ALL ON TABLE TABLENAME TO osmusers;


    insert into  TABLENAME (cluster_id,degree, geog)
    select clusterNNCL_id,AVG(degreeNNCL),ST_CENTROID(ST_UNION(nodes.geog::geometry))::geography from JUNCTABLE as nodes
    group by clusterNNCL_id;

    """

        self.db.safely_execute(cmd)
        self.db.fix_permissions_of_new_table(tablename)
        self.db.create_primary_key_constraint(tablename,'cluster_id')
        # split up into chunks to overcome memory constraints. #447
        tableSize = self.db.execfetch('SELECT COUNT(*) FROM %s;' % junc_table)[0][0] # will be larger than n cluster, 
        chunkPctiles = ','.join([str(cc) for cc in np.arange(0, 1, 1./math.ceil(float(tableSize)/defaults['osm']['roadgraphChunkSize']))]) + ', 1.0'
        chunkBounds = self.db.execfetch(str_reps(strs,'SELECT percentile_disc(ARRAY['+chunkPctiles+']) WITHIN GROUP (ORDER BY clusterNNCL_id) FROM JUNCTABLE;'))[0][0]
        chunkBounds[-1] +=1  # because last interval is open
        
        self.logger.write('Creating clusters table with %d clusters in %d chunks' % (tableSize, math.ceil(float(tableSize)/defaults['osm']['roadgraphChunkSize'])))
        for b1, b2 in zip(chunkBounds[:-1],chunkBounds[1:]):
            strs.update({'LOWERB':str(b1), 'UPPERB': str(b2)})
            self.db.execute(str_reps(strs,"""            
                insert into  TABLENAME (cluster_id,degree, geog)
                select clusterNNCL_id,AVG(degreeNNCL),ST_CENTROID(ST_UNION(nodes.geog::geometry))::geography from JUNCTABLE as nodes
                where clusterNNCL_id>=LOWERB AND clusterNNCL_id<UPPERB
                group by clusterNNCL_id; """))
        
        self.create_indices_cluster_tables(cluster=cluster)

    def create_roadgraph_tables_from_segs_and_clusters_tables(self, cluster):
        tnames=osmt.define_table_names(self.region)
        tablename,segs_table=tnames['edge_tables'][cluster], tnames['seg_table']
        if self.db.check_whether_table_exists(
                                            schema=defaults['osm']['processingSchema'],
                                            tablename=tablename,
                                            drop_if_exists=self.forceUpdate
                                          ):
            return
        self.drop_indices_roadgraph_tables(cluster)
        # Continent IDs make edge IDs globally unique.  With cluster radius, they are unique across clusters too
        edgeIdOffset=int(cluster) if 'incomplete' in self.region else int(cluster)*int(osmt.CONTINENT_IDS[self.region])*1e10
        
        strsubs=dict(CONTINENT=self.region,NNCL=cluster,
                     TABLENAME=defaults['osm']['processingSchema']+'.'+tablename,
                     SEGSTABLE=defaults['osm']['osmSchema']+'.'+segs_table,
                     CLUSTERTABLE=defaults['osm']['processingSchema']+'.'+tnames['cluster_tables'][cluster],
                     EDGEIDOFFSET=str(edgeIdOffset),
        )
        self.db.execute(str_reps(strsubs,"""        
        CREATE TABLE TABLENAME
        (
          edge_id BIGINT NOT NULL,
          geom geometry(LineString,4326),
          spt_cluster_id BIGINT,
          ept_cluster_id BIGINT,
          spt_degree integer,
          ept_degree integer,
          nocars boolean,
          -- classification character(1), -- C=cycle basis; B=bridge or dead end; S=self loop  (This column only gets added when we merge in those values)
          length_m integer -- Rounded length in metres
        );
        """))
        self.db.fix_permissions_of_new_table(tablename)
        self.db.create_primary_key_constraint(tablename,'edge_id')
        
        self.db.execute( str_reps(strsubs,"""
            -- Drop (don't copy) all edges which are inside a buffer/cluster. All edges have either zero or two endpoints inside a cluster.

            -- Following needs to be trimmed, most likely
            insert into  TABLENAME (
              edge_id,
              geom,
             --  osm_id, osm_name, osm_meta osm_source_id, osm_target_id, clazz, flags, source, target, km, kmh, cost, reverse_cost, x1, y1, x2, y2,
              spt_cluster_id,
              ept_cluster_id,
              spt_degree,
              ept_degree,
              nocars )

            select   
              edge_id+EDGEIDOFFSET, 
             geom,
            -- osm_id, osm_name, osm_meta osm_source_id, osm_target_id, clazz, flags, source, target, km, kmh, cost, reverse_cost, x1, y1, x2, y2,
            -- Following columns are differently named in copy-from table
              spt_clust_NNCL,
              ept_clust_NNCL,
              spt_deg_NNCL,
              ept_deg_NNCL,
              (clazz>50 AND clazz!=63) -- whether a street is included in aggregations (nocars=True)
             from SEGSTABLE as edges
            WHERE edges.ept_clust_NNCL IS NOT NULL ;

                """))

        #This check is to catch issue #63 (Extra edges connecting disparate clusters)
        print(('Getting longest road in %s'%self.region))
        self.db.execute('SELECT MAX(CAST(ROUND(ST_LENGTH(geom::geography)) AS INT)) FROM %s'%strsubs['TABLENAME'])
        max_length=self.db.fetchall()[0][0]
        print('Longest road: %d'%max_length)
    
        """
        Extend the edges of each edge to the centroids of each cluster
        AND split the line at the buffer, dropping segments that start in the buffer
        Exclude edges tangent to buffer boundaries
        """
        if not('safe_st_split' in self.db.list_functions()):
            fncmd="""CREATE FUNCTION Safe_ST_Split(input geometry, blade geometry)
                RETURNS geometry AS
                $$
                BEGIN
                RETURN ST_Split($1,$2);
                EXCEPTION WHEN SQLSTATE 'XX000' THEN
                    RAISE NOTICE 'TopologyException';
                    RETURN ST_Union(input);
                END;
                $$ LANGUAGE plpgsql;"""
            self.db.execute(fncmd)

        # split up into chunks to overcome memory constraints. #448
        tableSize = self.db.execfetch('SELECT COUNT(*) FROM %s;' % self.table_names['node_tables'][cluster])[0][0] 
        chunkPctiles = ','.join([str(cc) for cc in np.arange(0, 1, 1./math.ceil(float(tableSize)/defaults['osm']['roadgraphChunkSize']))]) + ', 1.0'
        chunkBounds = self.db.execfetch('SELECT percentile_disc(ARRAY['+chunkPctiles+']) WITHIN GROUP (ORDER BY cluster_id) FROM '+self.table_names['node_tables'][cluster]+';')[0][0]
        chunkBounds[-1] +=1  # because last interval is open
        self.logger.write('Extending edges to centroids with %d clusters in %d chunks' % (tableSize, math.ceil(float(tableSize)/defaults['osm']['roadgraphChunkSize'])))

        for pt,newptpos,pgFunc in [('spt',0,'ST_StartPoint'), ('ept',-1,'ST_EndPoint')]: 
            cmdDict = dict([('newptpos',newptpos),('pt',pt),('pgFunc',pgFunc),
                            ('buff_table',self.table_names['buffer_tables'][cluster]),
                            ('edge_table', self.table_names['edge_tables'][cluster]),
                            ('node_table', self.table_names['node_tables'][cluster])])

            # Iterate over chunks for memory safety
            for b1, b2 in zip(chunkBounds[:-1],chunkBounds[1:]):
                cmdDict.update({'lowerb':str(b1), 'upperb': str(b2)})
                                
                cmd = """WITH nodelist AS (SELECT cluster_id FROM %(buff_table)s
                                           WHERE nnodes>1 AND cluster_id>=%(lowerb)s AND cluster_id<%(upperb)s),
                              newsegtable AS ( 
                                SELECT edge_id, cluster_id, ST_AddPoint(St_Makeline(linegeom ORDER BY segnum), nodegeom, %(newptpos)s) AS newline FROM (
                                    SELECT edge_id, cluster_id, nodegeom, path AS segnum, geom AS linegeom FROM
                                        (SELECT edge_id, %(pt)s_cluster_id AS cluster_id, nodes.geog::geometry AS nodegeom, buffer.geom AS buffgeom,
                                            (ST_Dump(Safe_ST_Split(edges.geom, ST_Boundary(buffer.geom)))).*
                                         FROM ( SELECT edge_id, %(pt)s_cluster_id, geom FROM %(edge_table)s ) AS edges, %(buff_table)s as buffer, %(node_table)s as nodes, nodelist
                                         WHERE %(pt)s_cluster_id=buffer.cluster_id AND %(pt)s_cluster_id=nodes.cluster_id
                                                AND %(pt)s_cluster_id=nodelist.cluster_id) t1 
                                    WHERE Not(ST_Within(ST_LineSubstring(geom,0.01, 0.99), buffgeom)) -- ST_LineSubstring allows for a tolerance in ST_Within. See issue #129
                                    ORDER BY edge_id, segnum) t2
                                GROUP BY edge_id, cluster_id, nodegeom)
                        UPDATE %(edge_table)s AS edges
                        SET geom = newline FROM newsegtable WHERE edges.edge_id = newsegtable.edge_id;""" % cmdDict

                self.db.execute(cmd)
            
            # We also need to extend edges to the cluster centroid where they were missed by the above (usually edges completely within the buffer). This fixes #131
            cmd = '''UPDATE %(edge_table)s AS edges 
                     SET geom=ST_AddPoint(edges.geom,nodes.geog::geometry, %(newptpos)s)
                     FROM %(node_table)s AS nodes
                     WHERE edges.%(pt)s_cluster_id = nodes.cluster_id AND NOT(ST_Intersects(%(pgFunc)s(edges.geom), nodes.geog::geometry));''' % cmdDict
            self.db.execute(cmd)
        
        self.db.execute(str_reps(strsubs,"""        
         update TABLENAME set length_m=cast(round(st_length(geom::geography)) as int) ;
        """))
    
        print(('Getting longest road for %s after extending edges.'%self.region))
        self.db.execute('SELECT MAX(length_m) FROM %s;'%strsubs['TABLENAME'])
        theMaxLength=self.db.fetchall()[0][0]

        assert theMaxLength<=max_length*1.05,'Bug while making edges tables for %s. \
        Try run osm_master --from=clusters -f'%self.region

        # create index, but don't create spatial index (spatidx=False), 
        # as they will just be dropped again in the next step (anneal_degree2nodes_from_roadgraph_table)
        self.create_indices_roadgraph_tables(cluster=cluster, spatidx=False)

    def prepare_tables_for_QGIS(self, clusterradius=None):
        """
        It seems this step causes some trouble (took >6 days), so it's being put separately here. 
        If there's still trouble: Maybe we should drop indices before doing it? Or recreate the table with a definition incorporating the NOT NULL, and copy over the contents?
        """
        for cluster in [clusterradius] if clusterradius is not None else defaults['osm']['clusterRadii']:
            self.db.execute('  ALTER TABLE '+self.table_names['edge_tables'][cluster]+ ' ALTER COLUMN edge_id SET NOT NULL;  ')

    def ensure_indices_are_built(self, clusterradius=None):
        for cluster in [clusterradius] if clusterradius is not None else defaults['osm']['clusterRadii']:
            self.create_indices_cluster_tables(cluster, forceUpdate=False)
            self.create_indices_roadgraph_tables(cluster, forceUpdate=False)

    def drop_indices_cluster_tables(self, cluster):
        tnames=osmt.define_table_names(self.region)
        self.db.drop_indices(tnames['cluster_tables'][cluster],'cluster_id', gadm=False,geog='geog')
        return

    def create_indices_cluster_tables(self, cluster, forceUpdate=True):
        tnames=osmt.define_table_names(self.region)
        self.db.create_indices(tnames['cluster_tables'][cluster],'cluster_id', gadm=False, geog='geog', forceUpdate=forceUpdate)
        return

    def drop_indices_roadgraph_tables(self, cluster):
        tnames=osmt.define_table_names(self.region)
        self.db.drop_indices(tnames['edge_tables'][cluster],'edge_id',['spt_cluster_id','ept_cluster_id'], gadm=False,geom='geom')
        return

    def create_indices_roadgraph_tables(self, cluster, spatidx=True, forceUpdate=True):
        tname=osmt.define_table_names(self.region)['edge_tables'][cluster]
        if spatidx is False:
            geom, geog = False, False
        elif 'geog' in self.db.list_columns_in_table(tname, schema=defaults['osm']['processingSchema']):
            geom, geog = False,'geog'
        else:
            geom, geog = 'geom',False
        self.db.create_indices(tname,'edge_id',['spt_cluster_id','ept_cluster_id'], gadm=False, geom=geom, geog=geog, forceUpdate=forceUpdate)
        return

    def anneal_degree2nodes_from_roadgraph_table(self,cluster,firstPass=True,makeBackup=False):
        """
        Create instance of module anneal_postgis_graph class Anneal
        to collapse all edges connected by deg.2 nodes into one edge.

        After this function is called, the only remaining deg.2 nodes in
        the PostGIS graph should be incident to self-loops.

        N.B. This is tricky; will it be faster to delete and rebuild indices each iteration?
        
        firstPass=True reinitializes the logger (otherwise it will be overwritten when we reanneal)
        """
        tnames=osmt.define_table_names(self.region)

        # Make sure there is no spatial index while doing updating
        assert self.db.execfetch("SELECT count(*) FROM pg_indexes where tablename='%s' and indexname = '%s_spat_idx'" % (tnames['edge_tables'][cluster], tnames['edge_tables'][cluster]))[0][0]==0        
        
        #Step 1
        #self.edges_geog_to_geom(cluster)  # in case we are restarting after an aborted attempt. no longer needed
        import anneal_postgis_graph
        # make backup makes backup withdeg2 tables
        anneal_postgis_graph.Anneal(self.region,cluster,overwriteLogger=firstPass).anneal(makeBackup=makeBackup)
      
        # drop island edges and nodes (where both ends are degree 1)
        self.db.execute('''DELETE FROM %s 
                           USING (SELECT spt_cluster_id, ept_cluster_id FROM %s WHERE spt_degree=1 AND ept_degree=1) t1
                           WHERE cluster_id=spt_cluster_id OR cluster_id=ept_cluster_id;''' % (tnames['cluster_tables'][cluster],tnames['edge_tables'][cluster]))
        self.db.execute('''DELETE FROM %s 
                           WHERE spt_degree=1 AND ept_degree=1;''' % (tnames['edge_tables'][cluster]))
        
        # Convert geom column of edges to geog. No, let's leave them as geom - speeds up intersects
        #self.edges_geom_to_geog(cluster)
        
        # Recreate all indices after dropping many edges and nodes
        self.ensure_indices_are_built(cluster)
        
        # This is the final processing step for roadgraphs, so let's check that the annealing process didn't introduce bugs
        # Issue #337 check
        result = self.db.execfetch('''SELECT MAX(d), MIN(d) FROM                                            (SELECT *, length_m-length_new AS d FROM                                                 (SELECT edge_id, length_m, CAST(ROUND(ST_Length(geom::geography)) AS integer)  AS length_new                                                     FROM %s) t1                                              WHERE  ABS(length_m-length_new)>4) t2''' % tnames['edge_tables'][cluster])
        if result[0][0] is not None or result[0][1] is not None:
            raise Exception('Failed length test (issue #337)')

    def add_nocarfield_to_nodes(self, cluster):
        """
        Now that the annealing is done, transfer the nocar information from edges to nodes
        """
        tnames=osmt.define_table_names(self.region)
        strsubs=dict(CONTINENT=self.region,NNCL=cluster,
                     CLUSTERTABLE=defaults['osm']['processingSchema']+'.'+tnames['cluster_tables'][cluster],
                     TMPTABLE=defaults['osm']['processingSchema']+'.'+tnames['cluster_tables'][cluster]+'_tmpnocar',
                     EDGETABLE=defaults['osm']['processingSchema']+'.'+tnames['edge_tables'][cluster],
        )
        
        # UNION ALL is needed to get the correct count. 
        # This version also counts nodes as nocars=True where a node without the nocar edges would have been degree-2
        # Thus, we want to ignore these nodes in aggregation (issue #437)
        self.db.execute(str_reps(strsubs, """CREATE TABLE TMPTABLE AS
                 SELECT cluster_id, (bool_and(nocars) OR ((COUNT(*)-SUM(nocars::integer))=2)) AS nocars 
                 FROM
                     (SELECT spt_cluster_id AS cluster_id, nocars FROM EDGETABLE
                        UNION ALL
                      SELECT ept_cluster_id AS cluster_id, nocars FROM EDGETABLE) t1
                 GROUP BY cluster_id;"""))
        oldLen = self.db.execfetch("SELECT COUNT(*) FROM %(CLUSTERTABLE)s;" % strsubs)
        self.db.merge_table_into_table(from_table=tnames['cluster_tables'][cluster]+'_tmpnocar',
                        targettablename=tnames['cluster_tables'][cluster], 
                        fromschema=defaults['osm']['processingSchema'], targetschema=defaults['osm']['processingSchema'], 
                        commoncolumns='cluster_id')
        assert oldLen == self.db.execfetch("SELECT COUNT(*) FROM %(CLUSTERTABLE)s;" % strsubs)
        self.db.execute('DROP TABLE %(TMPTABLE)s;' % strsubs)

    def dropParallelEdges(self, cluster):
        """Drops parallel edges, but only where at least one is nocars=False and at least one has nocars=True
        Parallel defined as same start and end node
        See issues #437 and #441"""
        edgetname=osmt.define_table_names(self.region)['edge_tables'][cluster]
        clustertname=osmt.define_table_names(self.region)['cluster_tables'][cluster]
        cmdDict = {'edgetname':edgetname, 'clustertname':clustertname}
        
        # UNION here is to catch parallel edges which run in different directions
        cmd = '''WITH dropedges AS 
                    (SELECT * FROM (
                       SELECT n1, n2, COUNT(*) AS n, AVG(nocars::integer) AS nocars_avg from 
                         (SELECT spt_cluster_id n1, ept_cluster_id n2, nocars from %(edgetname)s
                             UNION ALL
                          SELECT ept_cluster_id n1, spt_cluster_id n2, nocars from %(edgetname)s) t1
                       GROUP BY n1, n2) t2
                     WHERE nocars_avg>0 AND nocars_avg<1)
                 DELETE FROM %(edgetname)s e USING dropedges d
                 WHERE d.n1=e.spt_cluster_id AND d.n2=e.ept_cluster_id
                        AND nocars;''' % cmdDict
        
        self.db.execute(cmd)
        self.logger.write('Dropped %s parallel edges' % str(self.db.cursor.rowcount))
        
        # Update nodal degree
        cmd = '''WITH newdegrees AS (SELECT cluster_id, COUNT(*) AS degree FROM 
                    (SELECT ept_cluster_id AS cluster_id FROM %(edgetname)s
                       UNION ALL
                     SELECT spt_cluster_id AS cluster_id FROM %(edgetname)s) t1
                 GROUP BY cluster_id)
                 UPDATE %(clustertname)s o 
                 SET degree=n.degree 
                 FROM newdegrees n 
                 WHERE o.cluster_id=n.cluster_id AND o.degree!=n.degree;''' % cmdDict
        self.db.execute(cmd)

        for pt in ['spt','ept']:
            cmdDict.update({'pt':pt})
            cmd = '''UPDATE %(edgetname)s e
                     SET %(pt)s_degree=n.degree 
                     FROM %(clustertname)s n
                     WHERE e.%(pt)s_cluster_id=n.cluster_id AND e.%(pt)s_degree!=n.degree;''' % cmdDict
            self.db.execute(cmd)
        
        # Drop indexes in preparation for anneal
        self.drop_indices_cluster_tables(cluster)
        self.drop_indices_roadgraph_tables(cluster)
        
    def edges_geom_to_geog(self, cluster):
        """Final part is to create a geography column (for use in lookups and subsequent stages) and drop the geom column"""
        tname=osmt.define_table_names(self.region)['edge_tables'][cluster]
        schema = defaults['osm']['processingSchema']
        if not ('geog' in self.db.list_columns_in_table(tname, schema=schema)):   
            self.db.execute('ALTER TABLE %s.%s ADD COLUMN geog geography(LineString, 4326);' % (schema, tname))
            self.db.execute('UPDATE %s.%s SET geog = geom::geography;' % (schema, tname))
            self.db.execute('ALTER TABLE %s.%s DROP COLUMN geom;' % (schema, tname))
            self.create_indices_roadgraph_tables(cluster)

    def edges_geog_to_geom(self, cluster):
        """Converts geog to geom and drops the geog column. Useful if we are restarting the roadgraph stage after already annealing"""
        tname=osmt.define_table_names(self.region)['edge_tables'][cluster]
        schema = defaults['osm']['processingSchema']
        if not ('geom' in self.db.list_columns_in_table(tname, schema=schema)):
            self.db.create_geom_col_from_geog(tname, geomType='LineString', schema=schema, spatindex=True)
            self.db.execute('ALTER TABLE %s.%s DROP COLUMN geog;' % (schema, tname))                
        
################################################################################################
################################################################################################
################################################################################################
if __name__ == '__main__':
################################################################################################
################################################################################################
################################################################################################
    import sys
    runmode= 'default' if len(sys.argv)<2 else sys.argv[1].lower()


    if runmode not in ['default','test']:
        print('\n\n UNRECOGNIZED RUN MODE...\n\n')

    if runmode in ['test']:
        #create_testcontinent_from_sql_files()
        #final_graph_builder(forceUpdate=True,   build_all=True)
        pass
