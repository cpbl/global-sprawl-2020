#!/usr/bin/python
"""
Take a simple graph (networkx-like, with latlon properties, ie spatial layout) and turn it into a quasi-infite tiling.  The single unit is a "unit graph", and we "tile" it to create large (crystalline) graphs with repeated patterns.

"""
import networkx as nx
import numpy as np
import pandas as pd
import time
import os
import glob
import pandas as pd
import pylab as plt
import sys
import os

# sys.path.append(os.path.dirname("../"))
from osm_networkx import Graph as osmnxGraph
from unitgraphs import stylized_unit_graphs

__author__ = 'cpbl'


class Node:
    """ Class to host a node while tiling. """

    def __init__(self, id, lon, lat):
        self.id = id
        self.lon = lon
        self.lat = lat
        self.tags = {}

    def set_id(self, id):
        self.id = id

    def set_pos(self, lon, lat):
        self.lon = lon
        self.lat = lat

    def copy(self):
        return(Node(self.id, self.lon, self.lat))


class graph_tiler():
    """
    Tile square unit graphs, with optional location info.

    Implementation: Store edges and nodes directly, without using any network package.

    """

    def __init__(self, unitgraph=None, glue_rules=None, N=None,  verbose=False, spacing_factor=0.05):
        """
        The unitgraph and glue_rules come from the unitgraphs module. The former is in the form of a networkx graph, typically with one data field for nodes: "latlon".

        spacing_factor =0 produces a closely-packed tiled graph.  Larger factors move the latitudes and longitudes of each unit further apart in the tiling, ie space out the constituent units.
        """
        self.verbose = verbose
        self.spacing_factor = spacing_factor

        self.unit_nodes = None
        self.longitude_range = None
        self.latitude_range = None
        self.unit_edges = None
        self.glue_rules = None
        self.layout = None
        self.layoutN = None
        self.nodes = None
        self.edges = None

        if unitgraph:  # try:
            assert glue_rules
            assert N
            self.initialize_unit_graph(unitgraph, glue_rules, N)
            self.test_nodes()
            self.glue_all_units()
            # self.show_progress()

            # self.test_nodes()
            self.drop_deleted_nodes()

        # self.test_nodes()
        # Below for debugging only. To get a graph out in a particular format, another member function should be called.
        # self.show_progress()
        # self.G=G

    def initialize_unit_graph(self, unitgraph, glue_rules, N):
        self.unit_nodes = [Node(nid, dd['latlon'][1], dd['latlon'][0])
                           for nid, dd in unitgraph.nodes(data=True)]
        longs = [u.lon for u in self.unit_nodes]
        lats = [u.lat for u in self.unit_nodes]
        self.longitude_range = max(longs)-min(longs)
        self.latitude_range = max(lats)-min(lats)
        self.unit_edges = unitgraph.edges()
        self.glue_rules = glue_rules
        self.layout = np.arange(N*N).reshape((N, N))
        self.layoutN = N
        # One could use np.array(nodes).reshape(N,N,len-of-one), and replace elements with nan to drop them.
        self.nodes, self.edges = self.duplicate_units()

    def glue_all_units(self):
        for ir in range(self.layoutN):
            for ic in range(self.layoutN):
                self.glue_one_unit(ir, ic)
                self.show_progress()

    def drop_deleted_nodes(self):
        for ir in range(self.layoutN):
            for ic in range(self.layoutN):
                self.nodes[ir, ic] = [nn for nn in self.nodes[ir, ic]
                                      if nn.newname in ['_master', None]]

    def flattened_nodes(self):
        """
        Return list of the nodes in a flat list (not an NxN array).
        """
        return [item for sublist in self.nodes.flatten() for item in sublist]

    def flattened_edges(self):
        """
        Return list of the edges in a flat list (not an NxN array)
        """
        return [item for sublist in self.edges.flatten() for item in sublist]

    def show_progress(self):
        if self.verbose is False:
            return

        plt.clf()
        self.as_osm_networkx().plot_geographic()
        plt.show(), plt.draw()
        input('Press enter to continue')

    def test_edges(self, innodes=None, inedges=None):
        if self.verbose is False:
            return
        if innodes is None:
            nodes = self.nodes
        else:
            nodes = innodes
        if inedges is None:
            edges = self.edges
        else:
            edges = inedges

        if isinstance(nodes, list):
            flatnodes = nodes
        else:
            flatnodes = [item for sublist in nodes.flatten()
                         for item in sublist]
        if isinstance(edges, list):
            flatedges = edges
        else:
            flatedges = [item for sublist in edges.flatten()
                         for item in sublist]

        nnames = [nn.id for nn in nodes]
        badedges = []
        for ic in range(self.layoutN):
            for ir in range(self.layoutN):
                badedges += [(ir, ic, a*(a not in nnames)+b*(b not in nnames), (a, b))
                             for a, b in self.edges[ir, ic] if a not in nnames or b not in nnames]
        assert not badedges

    def test_nodes(self, innodes=None, no_lat=False):
        if self.verbose is False:
            return

        if innodes is None:
            nodes = self.nodes
        else:
            nodes = innodes
        if isinstance(nodes, list):
            flatnodes = nodes
        else:
            flatnodes = [item for sublist in nodes.flatten()
                         for item in sublist]
        if not no_lat:
            assert np.all([b.lat for b in flatnodes])
            lats = [b.lat for b in flatnodes]
            assert np.all(lats)
            assert len(lats) == len(flatnodes)
        ids = [b.id for b in flatnodes]
        assert len(ids) == len(np.unique(ids))

        if innodes is None:
            print([(nn.id, nn.newname) for nn in flatnodes])
            nnames = [nn.id for nn in flatnodes]
            assert '1-sw' in nnames
            nkeepnames = [
                nn.id for nn in flatnodes if nn.newname in [None, '_master']]
            assert '1-sw' in nkeepnames
        #assert len(lats)==len(self.unit_nodes)*N*N

    def glue_one_pair(self, ir1, ic1, ir2, ic2, rules):
        """
        Drop nodes and rename ways in the first unit, according to the glue rules.

When glueing two nodes, there are three possibilities (because we  never glue north or west). 

  (1) Neither node has been renamed. In this case, rename (ie to drop, later), the first one. Also, mark the second as the master name by giving it newname "_masternode"
  (2.1) The first node has been marked as master, but the second has not been renamed. Do as for case 2.2.
  (2.2) The first node has already been renamed. The name is different from the second node's.  In this case, rename the second's to the name of the first. That means we also need to rename some edges in the second ((( and the first???)
  (2) The first node has already been renamed. The name is the same as the second node's.  In this case, do nothing.

Nodes that are renamed will later be dropped. It will never be the case that the second nodes has been renamed. (subtle)

        """
        nodes1 = self.nodes[ir1, ic1]
        edges1 = self.edges[ir1, ic1]
        nodes2 = self.nodes[ir2, ic2]
        edges2 = self.edges[ir2, ic2]

        # Update the rules based on name changes already done. This is used only for edges1, not for nodes1
        alreadyRenamed = dict(
            [(nn.id, nn.newname) for nn in nodes1 if nn.newname not in [None, '_master']])
        masterNames = [nn.id for nn in nodes1 if nn.newname == '_master']

        # Implement these updates in our rules list (and convert it to a dict)
        # Exclude rules which imply overwriting a "_master" named node.
        drules = dict([(a, alreadyRenamed.get(b, b))
                       for a, b in rules if a not in masterNames])

        for node1 in nodes1:
            target = drules.get(node1.id, None)
            if target is None:
                continue
            node2 = next(obj for obj in nodes2 if obj.id == target)
            #node2=[nn for nn in nodes2 if nn.id==target][0]

            # Case (1): Mark the  glued nodes in the first unit as to be dropped, by storing their new name:
            if node1.newname is None and node2.newname is None:
                node1.newname = target
                node2.newname = "_master"
            # Case (2): Mark the unit two nodes, and fix their edges
            elif node1.newname != target:
                newname = node1.id if node1.newname == "_master" else node1.newname
                if self.verbose:
                    print(('       '+str(node2.id)+' -->'+str(newname)))
                node2.newname = newname
                assert not newname == "_master"

                # print('
                edges2 = [(a if not a == target else newname,
                           b if not b == target else newname) for a, b in edges2]
                assert not any([(a, b) for a, b in edges1 if a ==
                                target or b == target])  # Really?
                #                 edges1=[(a  if not a==target else newname ,b if not b==target else newname)  for a,b in edges1] # Really?

                if self.verbose:
                    print(('    Updating edges2 to '+str(edges2)))
                #assert not (ir2==1 and ic2==1)
                #assert  node1.id !='1-sw'

            #assert not nn.newname

        e1 = [(a, b) for a, b in edges1]
        # Rename edges in unit 1, using rules that are updated based on changes that have already happened in unit 2
        edges1 = [(drules.get(a, a), drules.get(b, b)) for a, b in edges1]
        n1n = [nn.id if nn.newname is None else nn.newname for nn in nodes1]
        # newname if nn.newname is not None else nn.id
        n1nn = [alreadyRenamed.get(nn.id, nn.id) for nn in nodes1]
        # [nn.id for nn in nodes2]
        n2n = [nn.id if nn.newname is None else nn.newname for nn in nodes2]
        #print  [(a,b)    for a,b in edges1  if a not in n1nn and a not in n2n]
        self.nodes[ir1, ic1] = nodes1
        self.nodes[ir2, ic2] = nodes2

        self.edges[ir1, ic1] = edges1
        self.edges[ir2, ic2] = edges2
        self.test_nodes(nodes1)
        self.test_nodes(nodes2)
        if self.verbose:
            print(('----------------------\n %d,%d  <-- %d,%d' %
                  (ir1, ic1, ir2, ic2)))

            print(('%d,%d: ' % (ir1, ic1)+str(self.edges[ir1, ic1])))
            print(('%d,%d: ' % (ir2, ic2)+str(self.edges[ir2, ic2])))

            print(('%d,%d: ' % (ir1, ic1) +
                  str([(nn.id, nn.newname) for nn in self.nodes[ir1, ic1]])))
            print(('%d,%d: ' % (ir2, ic2) +
                  str([(nn.id, nn.newname) for nn in self.nodes[ir2, ic2]])))

            print(drules)
            print(alreadyRenamed)
            print('- - - ')
            #assert np.all([ a in n1n or a in n2n   for a,b in edges1])
            #assert np.all([ b in n1n or b in n2n   for a,b in edges1])
            # assert_node_healthy(ir,ic,'3-ee')

    def assert_node_healthy(ir, ic, nn):
        nodes = self.nodes[ir, ic]
        foie

    def glue_one_unit(self, ir, ic):
        # Translate glue rules:
        prefix = self.layout[ir, ic]
        if ic+1 < self.layoutN:  # Glue to the East rules:
            prefixE = self.layout[ir, ic+1]
            rulesE = [('%d-%s' % (prefix, str(a)),  '%d-%s' % (prefixE, str(b)))
                      for a, b, in list(self.glue_rules['E'].items())]
            self.glue_one_pair(ir, ic, ir, ic+1, rulesE)

        if 0:
            OG = self.as_osm_networkx()
        # OG.plot_geographic()
        # plt.show()
        # raw_input()

        if ir+1 < self.layoutN:  # Glue to the South rules:
            prefixS = self.layout[ir+1, ic]
            rulesS = [('%d-%s' % (prefix, str(a)),  '%d-%s' % (prefixS, str(b)))
                      for a, b, in list(self.glue_rules['S'].items())]
            self.glue_one_pair(ir, ic, ir+1, ic, rulesS)
        # self.as_osm_networkx().plot_geographic()
        # raw_input()

    def duplicate_units(self):
        # dtype=object allows us to assign lists as elements in the matrix!
        nodes = np.zeros((self.layoutN, self.layoutN), dtype=object)
        # dtype=object allows us to assign lists as elements in the matrix!
        edges = np.zeros((self.layoutN, self.layoutN), dtype=object)
        for ic in range(self.layoutN):
            for ir in range(self.layoutN):
                nn, ee = self.translate_one_unit(
                    self.unit_nodes, self.unit_edges, ir, ic)
                nodes[ir, ic] = nn
                edges[ir, ic] = ee
        #assert len(duplicated_units)== self.layoutN*self.layoutN
        # nodes,edges=zip(*duplicated_units)
        return(nodes, edges)  # list(nodes),list(edges))

    def translate_one_unit(self, nodes, edges, ir, ic):
        """
        Given a unit graph, move it nx units to the right and ny units down by changing the names of nodes and the lat/lon of nodes.

        Also, we endow each node with an extra member, "newname" to later keep track of glued nodes.
        """
        newnodes = [nn.copy() for nn in nodes]  # [ee.copy() for ee in edges]
        prefix = self.layout[ir, ic]
        for nn in newnodes:
            # The 1.3 below makes the units geographically separate (for debugging, really)
            nn.lon = nn.lon+ic*self.longitude_range*(1+self.spacing_factor)
            nn.lat = nn.lat-ir*self.latitude_range*(1+self.spacing_factor)
            nn.id = '%d-%s' % (prefix, str(nn.id))
            nn.newname = None
        newedges = [('%d-%s' % (prefix, str(a)),  '%d-%s' %
                     (prefix, str(b))) for a, b, in edges]
        return(newnodes, newedges)

    def as_dict_and_list(self):
        """ Return self (the tiled outcome) as a dict of nodes (with data) and a list of edges.
        This ignores all the  non-master nodes have been dropped, because it will ignore them.
        """
        nodes = self.flattened_nodes()
        nodes = [nn for nn in nodes if nn.newname in ['_master', None]]
        self.test_nodes(nodes)
        edges = self.flattened_edges()
        self.test_edges(nodes, edges)

        # Build a graph from these nodes and edges
        dnodes = dict([(nn.id, {'latlon': (nn.lat, nn.lon)}) for nn in nodes])
        assert len(dnodes) == len(nodes)
        assert np.all(dnodes.values)
        ##latlon=dict([(nn.id,(nn.lat,nn.lon))  for nn in nodes])
        return(dnodes, edges)

    def as_osm_networkx(self, lax=True):
        """ Return self (the tiled outcome) as an osm_networkx object. """
        dnodes, edges = self.as_dict_and_list()
        latlon = dict([(a, b['latlon']) for a, b in list(dnodes.items())])
        OG = osmnxGraph(edges, latlon=latlon)
        return(OG)

    def convert_nodes_to_integers(self):
        """ Return a dict to assign numbers to each node"""
        nodenames = [node.id for node in self.flattened_nodes()]
        nodelookup = dict(list(zip(nodenames, list(range(len(nodenames))))))
        return(nodelookup)

    def as_ids():
        """ Return a list of node ids as integers, and a list of edges using those ids """

    def as_SNAPgraph(self):
        import snap
        NL = self.convert_nodes_to_integers()
        G1 = snap.TUNGraph.New()
        for node in self.flattened_nodes():
            G1.AddNode(NL[node.id])
        edges = self.flattened_edges()
        np.random.shuffle(edges)
        for edge in edges[:(len(edges)/2)]:
            G1.AddEdge(NL[edge[0]], NL[edge[1]])
        EdgeV = snap.TIntPrV()  # Initialize the empty array
        snap.GetEdgeBridges(G1, EdgeV)
        for edge in EdgeV:
            print("edge: (%d, %d)" % (edge.GetVal1(), edge.GetVal2()))


def large_tiled_graph_as_nx(n, archetype='grid'):
    """
    This is the external interface to this module. Returns a graph of a given size (it gets squared) of a given archetype.
    """
    A = stylized_unit_graphs(archetype)
    gt = graph_tiler(A, A.get_glue_rules(), n)
    G = gt.as_osm_networkx()
    return(G)


def large_tiled_graph_as_dict_and_list(n, archetype='grid'):
    """
    This is the external interface to this module. Returns a graph of a given size (it gets squared) of a given archetype.
    """
    A = stylized_unit_graphs(archetype)
    gt = graph_tiler(A, A.get_glue_rules(), n)
    N, E = gt.as_dict_and_list()
    return(N, E)


Nsizes = [int(s) for s in sorted(np.logspace(10, 1, 20, base=2))]
#Nsizes=[int(s) for s in sorted(np.logspace(3,1,20, base=2))]


def create_and_save_large_graphs(forceUpdate=False, timed=False):
    """ Use parallel processing to create a test suite of graph files, by tiling the archetypes."""
    from cpblUtilities import shelfSave, shelfLoad
    from cpblUtilities.parallel import runFunctionsInParallel
    import os

    def wrapper(n, arch, fn):
        D, E = large_tiled_graph_as_dict_and_list(n, archetype=arch)
        shelfSave(fn, (D, E))
        return()
    fcalls, names = [], []
    ug = stylized_unit_graphs('grid')
    tseconds = []
    if timed:
        # The first one is slow, due to imports, so run a dummy
        wrapper(2, 'grid', 'tmpdummy')
    for n in Nsizes:
        for arch in ug.available_graphs():
            fn = 'bigGraphNE_%s_%d' % (arch, n)
            if forceUpdate or not os.path.exists(fn+'.pyshelf'):
                fcalls += [[wrapper, [n, arch, fn]]]
                names += [fn]
            if timed:
                sTime = time.time()

                A = stylized_unit_graphs('grid')
                gt = graph_tiler(A, A.get_glue_rules(), n)
                elapsedTiler = (time.time()-sTime)
                G = gt.as_osm_networkx()
                elapsedNX = (time.time()-sTime) - elapsedTiler
                # G=wrapper(n,arch,fn)
                elapsed = (time.time()-sTime)
                tseconds += [[arch, n, len(G.nodes()),
                              elapsedTiler, elapsedNX]]
                print(('Finished %dx%d %s.  %d nodes.  %s s' %
                      (n, n, arch, len(G.nodes()), elapsed)))
                if len(tseconds) > 10:
                    df = pd.DataFrame(tseconds, columns=[
                                      'arch', 'N', 'nNodes', 'ttiler', 'tNX'])
                    df.to_pickle('tiling_speed_ud.pandas')
                    print(('Saved '+'tiling_speed_ud.pandas'))

    # fcalls=[[wrapper,[n,arch,fn]] for  n in Nsizes  for arch in ['colonial_urbanism' ,'culdesac_hell' ,'old_world','grid'] ]
    if timed:

        print(df)
        #df=pd.DataFrame(tseconds, columns=['arch','nNodes','N','elapsed'])
        # df.to_pickle('tiling_speed.pandas')
        fooo
    else:
        runFunctionsInParallel(fcalls, names=names, parallel=True)


tiling_speeds_harcoded = pd.DataFrame([['grid', 2, 25, 0.017724990844726562, ],  ['grid', 2, 25, 0.0056059360504150391, ],  ['grid', 3, 49, 0.0080699920654296875, ],  ['grid', 5, 121, 0.015796184539794922, ],  ['grid', 7, 225, 0.029628992080688477, ],  ['grid', 10, 441, 0.052656888961791992, ],  ['grid', 14, 841, 0.20278000831604004, ],  ['grid', 19, 1521, 0.17244195938110352, ],  ['grid', 27, 3025, 0.4793999195098877, ],  ['grid', 38, 5929, 1.0041549205780029, ],  [
                                      'grid', 53, 11449, 2.2095611095428467, ],  ['grid', 74, 22201, 5.3537290096282959, ],  ['grid', 102, 42025, 13.422350168228149, ],  ['grid', 142, 81225, 39.522817134857178, ],  ['grid', 198, 157609, 126.9215190410614, ],  ['grid', 275, 303601, 432.52824592590332, ],  ['grid', 382, 585225, 1917.2341229915619, ],  ['grid', 531, 1129969, 7654.082102060318, ],  ['grid', 737, 2175625, 26245.019078016281, ],  ['grid', 1024, 4198401, 93693.446398019791], ], columns=['arch', 'N', 'nNodes', 't'])


def report_speeds():
    df = pd.read_pickle('tiling_speed_ud.pandas')
    df['nNodes'] = df.N
    plt.loglog(df.nNodes, df.ttiler)
    plt.plot(df.nNodes, df.tNX)
    plt.show()


if __name__ == "__main__":
    import pylab as plt
    test_large_tiled_graphs()
    okay
    report_speeds()
    yay
    create_and_save_large_graphs(forceUpdate=True, timed=True)
    stophere
    wowo
    foo

    oiouo
