#!/usr/bin/python
"""
Manipulations relating ONLY to GADM and PostGIS.
These functions load the GADM data (country and sub-national boundaries)
into PostGIS and create derivative tables
In particular, it creates tables of adjacent geometries, that are 
used later on when tiling the computation of network measures

Note that some of the functions are deprecated, but were developed to
handle GADM v2.8. The published work uses GADM v3.6.
See gadm.org
"""
from osm_config import paths, defaults
SP = paths['scratch']
WP = paths['working']
import os
import sys
import numpy as np
from cpblUtilities import shelfSave, shelfLoad, dgetget, dsetset
from cpblUtilities.parallel import runFunctionsInParallel
import postgres_tools as psqlt
import rasterTools as rt
import osmTools
import shutil

#GADM_ID_LEVELS = defaults['gadm']['ID_LEVELS']
# Do NOT define these here. defaults can be updated after this module is imported! (Could define them in __init__ of classes below)
#GADM_VERSION = defaults['gadm']['VERSION']
#GADM_TABLE = defaults['gadm']['TABLE']
#GADMFILE = paths['input']+defaults['gadm']['FILE']
#if GADM_VERSION == '3.6':
#    GADM_ID_LEVELS = ['id_0', 'id_1', 'id_2', 'id_3', 'id_4', 'id_5']
# Do NOT define these here. defaults can be updated after this module is imported:
GADM_schema = defaults['osm']['boundarySchema']


def listit2(x):
    """ Subtle conversion to make tuples but not strings lists, nestedly. 
        Python ignores parentheses around a single object. Convert tuples to lists so as not to confuse lone string with a alist, in compareGADM_string first argument. 
        N.B.! This function is intentionally duplicated in osm_lookups.py.
    """
    assert isinstance(x, (list, tuple, int, str))
    if isinstance(x, (list, tuple)):
        if len(x) == 1 or isinstance(x[1], int):
            return list(x)
        return([[k] if not isinstance(k, (list, tuple)) else list(k) for k in x])
    return [x]


def compareGADM_string(list_of_ISOandIDs, tablename=None):
    """ Return the postgres string expression comparing database columns to values for a particular region, or to any of a list of regions. 

    tablename is a prefix for the columns names (typically, "lookup").

    to do : This should be made more efficient, so as not to duplicate comparisons for common elements.
    N.B.! This function is duplicated in osm_lookups.py. Nov2016: not exactly...the osm_lookups is the old, less efficient version   


    """
    columnNames = [cn for cn in defaults['gadm']['ID_LEVELS']]
    if tablename:
        columnNames = [tablename+'.'+cc for cc in columnNames]
    list_of_ISOandIDs = listit2(list_of_ISOandIDs)
    if isinstance(list_of_ISOandIDs[0], str):  # a single tuple was passed
        O1 = columnNames[0]+" = '%s'" % list_of_ISOandIDs[0]
        if len(list_of_ISOandIDs) > 1:
            O2 = ' and '.join(['%s = %s' % (columnNames[1:][ii], RR) for ii, RR in enumerate(
                list_of_ISOandIDs[1:]) if RR not in [0]])
            if O2:
                O1 += ' and '+O2
        return(' WHERE '+O1)
    else:  # a list of tuples was passed. In this case, we should be smarter, by recursively sorting the conditions to combine similar comparisons.
        # return(' or '.join(['(%s)'%compareGADM_string(LL) for LL in  list_of_ISOandIDs])  )  # old way
        # for why this is better, see http://stackoverflow.com/questions/6672665/doing-a-where-in-on-multiple-columns-in-postgresql
        idLevels = np.unique([len(cc) for cc in list_of_ISOandIDs])
        maxIdLevel = max(idLevels)
        if maxIdLevel == 1:  # iso only
            paddedList = [cc[0] for cc in list_of_ISOandIDs]  # unnest
            GADMstr = ' WHERE iso IN %s' % str(tuple(paddedList))
        else:
            gadmColNames = '('+', '.join(defaults['gadm']['ID_LEVELS'][:maxIdLevel])+')'
            paddedList = [str(tuple(cc)+(0,)*(maxIdLevel-len(cc)))
                          for cc in list_of_ISOandIDs]         # pad to max gadm level
            colAliases = '('+', '.join(['v'+str(ii)
                                        for ii in range(maxIdLevel)])+')'
            GADMstr = ', (VALUES %s) v%s \n WHERE %s = %s' % (
                ', '.join(paddedList), colAliases, gadmColNames, colAliases)
        # need to add an OR where we want to have a higher-level GADM
        # loop over all other idLevels (already done maxIdLevel)
        for idLevel in idLevels[:-1]:
            gadmColNames = '('+', '.join(defaults['gadm']['ID_LEVELS'][:idLevel])+')'
            colAliases = '('+', '.join(['v'+str(ii)
                                        for ii in range(idLevel)])+')'
            colAliases2 = '('+', '.join(['v'+str(ii)
                                         for ii in range(idLevel, maxIdLevel)])+')'
            zeroList = '(' + \
                ', '.join(['0' for ii in range(idLevel, maxIdLevel)])+')'
            GADMstr += ' OR (%s = %s AND %s = %s)' % (gadmColNames,
                                                      colAliases, colAliases2, zeroList)
        assert '()' not in GADMstr  # issue 156
        return(GADMstr)


def reinstall_all_GADM(forceUpdate=False, logger=None):
    """
    N.B.: Following insert_srs will fail the first time, 
    because it will be called by you, a non-privileged user. 
    It will prompt for you to enter the query as a superuser.  
    (not needed as now we use geographies for distance and area calculations)
    """
    if logger is None:
        logger = osmTools.osm_logger(
            'reinstall_all_GADM_called_directly', verbose=True, timing=True, prefix='')

    osmTools.pgisConnection(verbose=True, logger=logger).insert_srs(
        [defaults['osm']['srid_equalarea'], defaults['osm']['srid_compromise']])
    logger.write(' Calling     import_raw_GADM_into_postGIS()')
    import_raw_GADM_into_postGIS(forceUpdate=forceUpdate, logger=logger)

    
    logger.write(
        ' Calling   build_aggregate_geometries(None, forceUpdate={}) without internal logging...'.format(forceUpdate))
    # Don't pass logger; this will parallelize.
    build_aggregate_geometries(resolution=None, forceUpdate=forceUpdate)
    logger.write(
        ' Calling   regions_and_neighbours().build_GADM_adjacency_table() (takes a day or two)')
    regions_and_neighbours().build_GADM_adjacency_table(forceUpdate=forceUpdate)
    # build_all_GADM_adjacency_matrices(forceUpdate=forceUpdate)
    if defaults['osm']['landscan']:
        logger.write(
            ' Calling   createGADMCountryRaster() without internal logging...')
        createGADMCountryRaster(forceUpdate=forceUpdate)
        logger.write(
            ' Calling rt.aggregateLandscantoGadm(logger,forceUpdate={})'.format(forceUpdate))
        rt.aggregateLandscantoGadm(logger=logger, forceUpdate=forceUpdate)

    else:
        logger.write(
            ' Skipping  createGADMCountryRaster() since landscan == False')
    check_reinstall_all_GADM(logger=logger)
    
def check_reinstall_all_GADM(logger=None):
    # run tests
    from tests import gadm_test
    if logger:
        logger.write(' Running   gadm tests...')
    gadm_test._main()
    gadm_test.test_overlapping_geometries(skip=False)


class regions_and_neighbours():
    def __init__(self):
        self.adjacencies = {}  # Only load adjacency lookups once
        self.GADMcountries = None
        #self.GADMcountriesBySize =None
        self.id0123s = None
        self.db = osmTools.pgisConnection(schema=GADM_schema)
        self.adjTableName = defaults['gadm']['GADM_TABLE']+'_adjacencies'

    def get_GADM_neighbors(self, ISO_and_idlist):
        """
        Returns a list of level-5 neighbours for a given region. 
        The region is expressed as a string (country ISO) or as a tuple (ISO, id1, id2, ...)
        """
        if isinstance(ISO_and_idlist, list):
            ISO_and_idlist = tuple(ISO_and_idlist)

        if isinstance(ISO_and_idlist, str):
            countryISO = ISO_and_idlist
            idlevel = 0
        else:
            countryISO = ISO_and_idlist[0]
            # Note: we could strip the trailing zeros off here. Should we??
            idlevel = len(ISO_and_idlist)-1

        if 0:  # old method
            AM = dgetget(self.adjacencies, [countryISO, idlevel, 5], None)
            if AM is None:
                assert isinstance(countryISO, str)
                assert isinstance(idlevel, int)
                AM = self.load_GADM_adjacency_matrix(countryISO, idlevel, 5)
                dsetset(self.adjacencies, [countryISO, idlevel, 5], AM)
            return(AM[ISO_and_idlist])
            # set is needed to remove duplicates. See #166
            return(list(set(AM[ISO_and_idlist])))
        else:  # new method using postgres table
            if isinstance(ISO_and_idlist, str):
                ISO_and_idlist = (ISO_and_idlist,)
            cmdDict = {'whereClause': ' AND '.join([gidName+"='"+str(gid)+"'" for gidName, gid in zip(defaults['gadm']['ID_LEVELS'][:len(ISO_and_idlist)], ISO_and_idlist)]),
                       'nidCols': ','.join(['neighbour_'+gid for gid in defaults['gadm']['ID_LEVELS']]),
                       'notsame': ' OR '.join([" neighbour_%s!='%s' " % (anid[0], anid[1]) for anid in zip(defaults['gadm']['ID_LEVELS'][:len(ISO_and_idlist)], ISO_and_idlist)]),
                       'adjTable': self.adjTableName}
            AM = self.db.execfetch(
                '''SELECT DISTINCT %(nidCols)s FROM %(adjTable)s WHERE %(whereClause)s AND (%(notsame)s);''' % cmdDict)
            return [tuple(aa) for aa in AM]

    def get_GADM_adjacency_matrix(self, countryISO, idlevel, neighbor_idlevel=None):
        """ Lookup a region adjacency matrix in a saved file, for the current GADM version. Do not reload it if the class instance already has done.
        DEPRECATED"""
        raise Warning("May be deprecated...")

        AM = dgetget(self.adjacencies, [
                     countryISO, idlevel, neighbor_idlevel], None)
        if AM is None:
            assert isinstance(countryISO, str)
            assert isinstance(idlevel, int)
            AM = self.load_GADM_adjacency_matrix(
                countryISO, idlevel, neighbor_idlevel)
            dsetset(self.adjacencies, [countryISO,
                                       idlevel, neighbor_idlevel], AM)
        return(AM)

    def get_regions_and_neighbors(self, ISO_and_idlist, idlevel, neighbor_idlevel=None):
        """Return regions and their neighbours for an entire country, if the first argument is a country ISO.
        If the first argument is a list or tuple, the 2nd and remaining arguments are a subregion to restrict the response.
        The subregion must be at least as coarse as the idlevel.

        For instance, I could ask for all neighbour sets at id_2 level for a whole country; 
        I could ask for all neighbour sets at id_1 level for an id_1 region; 
        but I could not ask for all neighbour sets at id_1 level for an id_2 region.

        GADM_regions_and_neighbors(("CAN",4), idlevel=2)      
            returns regions and neighbours at the level of CDs but only for the 4th province.

        Note that the region tuples exclude the first element, 
            ie the ISO, which is present everywhere else when identifying a GADM region uniquely.

        neighbor_idlevel is by default the same as idlevel, 
            but sometimes we want a different set of neighbors (e.g. for countries, usually a lower level)

        Subtle change Apr2016: does not return regions nested within ISO_and_idlist. Use get_regions_within for that
        """
        if idlevel is None:
            idlevel = 2
        if isinstance(ISO_and_idlist, str):
            ISO_and_idlist = [ISO_and_idlist]
        # The subregion must be at least one level up from idlevel.
        assert idlevel >= len(ISO_and_idlist)-1
        ISO = ISO_and_idlist[0]

        if 0:  # old method
            LU = self.get_GADM_adjacency_matrix(
                ISO, idlevel=idlevel, neighbor_idlevel=neighbor_idlevel)
            if LU is None:
                print(('  Level %d is not available for country %s. No adjacency available' % (
                    idlevel, ISO)))
            else:
                LU = dict([(kk, vv) for kk, vv in list(LU.items()) if all(
                    [rr == kk[ii] for ii, rr in enumerate(ISO_and_idlist)])])
            return(LU)
        else:
            # wrapper around get_GADM_neighbors
            ISO_and_idlist = ISO_and_idlist[:idlevel+1]
            allNeighbours = self.get_GADM_neighbors(ISO_and_idlist)
            if neighbor_idlevel is None:
                neighbor_idlevel = idlevel
            return (list(set([nn[:neighbor_idlevel+1] for nn in allNeighbours])))

    def get_all_GADM_countries(self):  # Returns a dict
        """ Also, see osm_lookups for count_nodes_by_GADM_ISO if you want to be able to sort them by size
        """
        if not self.GADMcountries:
            cur = osmTools.pgisConnection(schema=GADM_schema)
            # order by iso,id_0,name_0;')
            cur.execute(
                    'select distinct on (ISO) iso,id_0, name_0 from '+defaults['gadm']['GADM_TABLE']+';')
            self.GADMcountries = dict(
                    [(a, {'name': c, 'id_0':  int(b)}) for a, b, c in cur.fetchall()])
        return self.GADMcountries

    def get_all_GADM_regions(self, level = 3):
        """ Get GADM regions down to specified level. Returns DataFrame
        """
        import pandas as pd
        if not self.id0123s:
            cur = osmTools.pgisConnection(schema=GADM_schema)
            # There are 42726 level-2 regions. Does this include all level 1 regions without any level 2??
            gadmlevels = defaults['gadm']['ID_LEVELS'][:level+1]
            cmd =  'select distinct {gadmlevels} from {table};'.format(gadmlevels = ','.join(gadmlevels),
                                                                       table = defaults['gadm']['GADM_TABLE'])
            x= pd.DataFrame([list(r) for r in cur.execfetch(cmd)], columns = gadmlevels)
            if level==3:
                self.id0123s = x
        return(x)

    def get_regions_within(self, ISO_and_idlist):
        """ Use a fresh select  to find all regions within a given region, since it should be a complete list.
        (could instead  store results for re-use?...?)
        """
        if isinstance(ISO_and_idlist, str):
            ISO_and_idlist = [ISO_and_idlist]
        # This is already an "overspecified" or highest-def region
        if ISO_and_idlist[-1] == 0 or len(ISO_and_idlist) > 6:
            return([])
        # ISO=ISO_and_idlist[0]
        whereStr = compareGADM_string(ISO_and_idlist, tablename=defaults['gadm']['GADM_TABLE'])
        idlist = defaults['gadm']['ID_LEVELS'][:len(ISO_and_idlist)+1]
        cur = osmTools.pgisConnection(schema=GADM_schema)
        cur.execute('select distinct '+','.join(idlist) +
                    ' from '+defaults['gadm']['GADM_TABLE']+whereStr+';')
        idns = cur.fetchall()
        idns = [[rr[0]]+[int(anid) for anid in rr[1:]] for rr in idns]
        return(idns)

    def id0ToIsoLookup(self):
        """Return lookup dictionaries"""
        countries = self.get_all_GADM_countries()
        return dict([(countries[iso]['id_0'], iso) for iso in countries])

    def load_GADM_adjacency_matrix(self, countryISO, idlevel=2, neighbor_idlevel=None):
        """ Lookup a region adjacency matrix in a saved file.
        """
        deprecated
        assert idlevel in [0, 1, 2, 3, 4, 5] and neighbor_idlevel in [
            None, 0, 1, 2, 3, 4, 5]
        savedName = self.get_GADM_adjacency_matrix_filename(
            countryISO, idlevel, neighbor_idlevel)
        # But do not rely on calculating these on the fly; otherwise we might get multiple processes trying to do it at once. Instead, they should be pre-built.
        if not os.path.exists(savedName):
            self.build_GADM_adjacency_matrix(
                countryISO, idlevel=idlevel, neighbor_idlevel=neighbor_idlevel, forceUpdate=False)
        LU = shelfLoad(savedName)
        return(LU)

    def get_GADM_adjacency_matrix_filename(self, countryISO, idlevel=2, neighbor_idlevel=None):
        raise Warning("May be deprecated...")
        assert idlevel in [0, 1, 2, 3, 4, 5] and neighbor_idlevel in [
            None, 0, 1, 2, 3, 4, 5]
        savedName = WP+'GADMadjacency/'+'GADMadjacency_v%s_%s_id%d_id%d.pyshelf' % (str(
            defaults['gadm']['GADM_VERSION']), countryISO, idlevel, idlevel if neighbor_idlevel is None else neighbor_idlevel)
        return(savedName)

    def build_GADM_adjacency_table(self, radius=None, forceUpdate=False):
        """Creates a table lookup of the id_5 to id_5 lookup matrix (global)"""

        # list_tables() has changed since this code was written
        # Explicitly pass schema as argument here
        if self.adjTableName in self.db.list_tables() and not forceUpdate:
            print(('%s already exists. Run forceUpdate=True to recreate' %
                  self.adjTableName))
            return
        if radius is None:
            radius = defaults['osm']['GADM_neighbourRadius']

        self.db.execute('DROP TABLE IF EXISTS %s' % self.adjTableName)
        dtypeDict = dict([(gid, 'integer')
                          for gid in defaults['gadm']['ID_LEVELS'][1:]]+[('iso', 'varchar'), ('id_0', 'varchar')])
        # psql syntax for "two regions are not the same"
        notsame = ' or '.join([' g1.%s!=g2.%s ' % (anid, anid)
                               for anid in defaults['gadm']['ID_LEVELS']])
        # Why not this, simpler:
        notsame = ' g1.uid != g2.uid '
        
        g1_ids = ', '.join(['g1.'+id+'::'+dtypeDict[id]
                            for id in defaults['gadm']['ID_LEVELS']])
        g2_ids = ', '.join(['g2.'+id+'::'+dtypeDict[id] +
                            ' AS neighbour_'+id for id in defaults['gadm']['ID_LEVELS']])

        cmdDict = dict(adjTableName=self.adjTableName, table_name=defaults['gadm']['GADM_TABLE'],
                       g1_ids=g1_ids, g2_ids=g2_ids, radius=radius, notsame=notsame)

        cmd = """CREATE TABLE %(adjTableName)s AS
                SELECT %(g1_ids)s,\n%(g2_ids)s, ST_Distance(g1.geom_compromise,g2.geom_compromise)::int AS approx_distance
                FROM %(table_name)s g1, %(table_name)s g2 
                WHERE ST_DWithin(g1.geom_compromise,g2.geom_compromise,%(radius)s) AND (%(notsame)s)""" % cmdDict
        
        print(cmd)
        self.db.execute(cmd)
        self.db.create_indices(
            self.adjTableName, non_unique_columns=defaults['gadm']['ID_LEVELS'])

    def build_GADM_adjacency_matrix(self, countryISO, idlevel=2, neighbor_idlevel=None, radius=None, forceUpdate=False):
        """
        For each country, save a dict or sparse matrix recording the adjacency of regions in GADM. 
        This is a separate question/record for each idlevel, ie geographic scale, within GADM.

        If radius is 'touches', the adjacency matrix uses ST_Touches (and excludes adjacent regions in a different country)
        Otherwise, DWithin and the passed radius (in meters) are used.
        If radius is None, then the distance specified in the config (cfg) file is used.

        While this function can be used to generate a lookup for any ISO on the fly, it should never be, since our preferred method is to have all the lookups already built,
        using a global id5-to-id5 neighbour search, by build_all_GADM_adjacency_matrices()

        """
        if not (idlevel == 5 and countryISO == 'global' and neighbor_idlevel == 5):
            print('\n\n\nThis function is completely deprecated. You should only use the all-at-once pre-build-all function if you want to include neighbours in other countries, etc.\n\n')
            assert input(
                '  Override and continue? (no/yes) ').lower() in ['y', 'yes']
        cur = osmTools.pgisConnection(schema=GADM_schema)
        assert idlevel in [0, 1, 2, 3, 4, 5] and neighbor_idlevel in [
            None, 0, 1, 2, 3, 4, 5]
        savedName = self.get_GADM_adjacency_matrix_filename(
            countryISO, idlevel, neighbor_idlevel)
        if neighbor_idlevel is None:
            neighbor_idlevel = idlevel
        # can't ask for (say) iso neighbors for id1
        assert idlevel <= neighbor_idlevel or neighbor_idlevel
        try:
            os.makedirs(WP+'GADMadjacency/')
        except OSError:
            if not os.path.isdir(WP+'GADMadjacency/'):
                raise
        if not forceUpdate and os.path.exists(savedName):
            print(('Skipping complete country '+countryISO))
            return
        cur.execute("select distinct "+','.join(defaults['gadm']['ID_LEVELS'][:(idlevel+1)])+" from "+defaults['gadm']['GADM_TABLE'] + (
            countryISO != "global") * (" where iso='"+countryISO+"'") + ";")
        idns = cur.fetchall()
        idns = [[rr[0]]+[int(anid) for anid in rr[1:]] for rr in idns]
        print(('  %d ids for %s' % (len(idns), countryISO)))
        if len(idns) < 1:
            shelfSave(savedName, None)
            return
        # Below we will want to ensure that all the existing regions, including those with no neighbours, are included:
        # We also want to name them properly: trailing zeros in the GDAM tuple name should be dropped.
        includingEmpties = dict([[tuple(idlff), []] for idlff in idns])
        # Loop over top-level regions?  For the moment, let's just do an entire country at a time:
        sids = ['ISO', 'id_1', 'id_2', 'id_3', 'id_4', 'id_5'][:idlevel+1]
        with_ids = ', '.join(sids)
        nids = ['ISO', 'id_1', 'id_2', 'id_3', 'id_4', 'id_5'][:neighbor_idlevel+1]
        select_ids = ', '.join(['R1.%s' % anid for anid in sids]) + \
            ' , ' + ', '.join(['R2.%s' % anid for anid in nids])
        # psql syntax for "two regions are not the same"
        notsame = ' or '.join([' R1.%s != R2.%s ' %
                               (anid, anid) for anid in sids])
        if radius is None:
            radius = defaults['osm']['GADM_neighbourRadius']
        if radius in ['touches']:
            deprecated
            cmd = """  WITH regions as (SELECT %(with_ids)s,  ST_Union(the_geom) AS the_geom FROM %(table_name)s """+(countryISO != "global") * "WHERE iso='%(ISO)s' "+""" GROUP BY %(with_ids)s)
                SELECT %(select_ids)s  FROM regions AS R1,regions AS R2 WHERE ST_Touches(R1.the_geom, R2.the_geom);
                """ % dict(table_name=defaults['gadm']['GADM_TABLE'], ISO=countryISO, select_ids=select_ids, with_ids=with_ids)
        else:
            cmd = (""" WITH isoregions AS (SELECT %(with_ids)s, geom_compromise FROM %(table_name)s  """+(countryISO != "global") * "WHERE iso='%(ISO)s' "+""" ) SELECT DISTINCT %(select_ids)s FROM isoregions AS R1, gadm28 AS R2 
                    WHERE ST_DWithin(R1.geom_compromise,R2.geom_compromise,%(radius)s) AND (%(notsame)s) ;
                    """) % dict(table_name=defaults['gadm']['GADM_TABLE'], ISO=countryISO, select_ids=select_ids, with_ids=with_ids, radius=radius, notsame=notsame)
        print(cmd)
        cur.execute(cmd)
        fa = cur.fetchall()
        if not fa:  # PSE (Palestine) has two level-1 regions, but they are not contiguous!
            shelfSave(savedName, includingEmpties)
            return
        nl = len(sids)
        pairs2n = [tuple([row[0]]+[int(nn) for nn in row[1:(nl)]]+[row[nl]]+[int(nn)
                                                                             for nn in row[nl+1:]]) for row in fa]  # Tuple so it's hashable in sparselu
        assert len(sids)+len(nids) == len(pairs2n[0])
        print(' Building..')
        sparselu = build_lookup_from_list_of_tuplepairs(pairs2n, nl)
        includingEmpties.update(sparselu)
        shelfSave(savedName, includingEmpties)


def build_lookup_from_list_of_tuplepairs(listOf2Ns, nv):
    sparselu = {}
    for row in listOf2Ns:
        neigh1, neigh2 = row[:nv], row[nv:]
        # if len(neigh1)==1: neigh1=neigh1[0]  # return tuple even if tuple is length 1?
        #if len(neigh2)==1: neigh2=neigh2[0]
        if neigh1 in sparselu:
            sparselu[neigh1] += [neigh2]
        else:
            sparselu[neigh1] = [neigh2]
    return(sparselu)


def delete_all_GADM_adjacency_matrices():
    """ To do: should take a continent or ISO name as argument. 
    IF continent, delete lookups for all ISOs which touch that continent"""
    if os.path.exists(WP+'GADMadjacency'):
        shutil.rmtree(WP+'GADMadjacency/')


def strip_zeros_from_id(GADM_ids):
    """
    This returns [ISO] when passed a string, ISO, and otherwise strips any trailing zeros from a tuple, like ('CAN',2,3,0,0,0)
    The idlevel of the region is therefore the len of the returned list, less 1.
    """
    if isinstance(GADM_ids, str):
        return([GADM_ids])
    stripped = [aa for aa in GADM_ids if aa not in [0]]
    assert not any([aa in [0] for aa in GADM_ids[:len(stripped)]])
    return stripped

def build_all_GADM_adjacency_matrices(forceUpdate=False):
    """ New approaches:  Only build lookups at level 5 <--> level 5
    (1)  [Not implemented]
    Just to make it speedier: first, calculate all level-0 neighbours. Then parallelize over ISOs, looking for core regions in the ISO and for neighbouring id5 regions in the ISO and all the level-0 neighbours of the ISO.
    Will this be faster, if they're using the same database?

    (2) [Implemented below]
    Alternatively, simply run the world at once at idlevel 5 (this takes 24 hours):

    gadm.regions_and_neighbours().build_GADM_adjacency_matrix('global', idlevel=5, forceUpdate=True)

    That results in an id5 to id5 adjacency lookup (one large file for the world).  It's about 0.8 GB.

    and then explode out the results:
     - For each ISO, save the subset of the world file, ie an id5 to id5 lookup for each ISO
     - For each ISO, make idn to id5 adjacency lookup for idn in [0,1,2,3,4]
     - make id0 to id0 adjacency lookup (one file for whole world)

 """
    logger = osmTools.osm_logger(
        'GADMadjacencyPrebuild', verbose=True, timing=True)

    # Not-parallel method:
    ran = regions_and_neighbours()
    if forceUpdate:
        logger.write(' You specified forceUpdate. Actually, at the moment you will need to delete the global id5 adjacency file in order to truly force updating of that.')
        #ran.build_GADM_adjacency_matrix('global', idlevel=5, forceUpdate=forceUpdate)
    bigm = ran.get_GADM_adjacency_matrix('global', 5, 5)
    ISOs = np.unique([kk[0] for kk in list(bigm.keys())])
    neighbourISOs = {}
    for ISO in ISOs:  # or use ran.get_all_GADM_countries()
        savedName = ran.get_GADM_adjacency_matrix_filename(ISO, 5, 5)
        if not forceUpdate and os.path.exists(savedName):
            logger.write('Skipping extant '+savedName)
            continue
        thisISO = dict([[kk, dd] for kk, dd in list(bigm.items()) if kk[0] == ISO])
        shelfSave(savedName, thisISO)
        logger.write('Country %s has %d regions at level 5, including %d islands' % (
            ISO, len(thisISO), len([dd for dd in list(thisISO.values()) if not dd])))
        # Also make all other idlevels to id5 lookups, by aggregating
        debuglist = {}  # Not for production; just for testing
        for idn in [4, 3, 2, 1, 0]:
            aggd = {}
            # for kk,dd in thisISO.items(): aggd[kk[:4]]=  dd if kk[:4] not in aggd else aggd[kk[:4]]+dd   # One line version
            for kk, dd in list(thisISO.items()):
                kn = kk[:idn+1]
                # Remove self from neighbours list:
                cdd = [add for add in dd if not add[:idn+1] == kn]
                # Aggregate
                if kn in aggd:
                    aggd[kn] = aggd[kn]+cdd
                else:
                    aggd[kn] = cdd
            debuglist[idn] = aggd.copy()
            savedName = ran.get_GADM_adjacency_matrix_filename(ISO, idn, 5)
            bn = sum([len(LL) for LL in list(aggd.values())])
            aggd = dict([[kk, list(set(LL))] for kk, LL in list(aggd.items())])
            logger.write('        Restricted list to unique elements... (%d --> %d)' %
                         (bn,  sum([len(LL) for LL in list(aggd.values())])))
            thisISO = aggd
            if idn == 0:
                # For level 0 GADM identifiers, use a string rather than a tuple
                aggd = dict([[kk[0], dd] for kk, dd in list(aggd.items())])
                neighbourISOs[ISO] = list(
                    np.unique([ids[0] for ids in list(thisISO.values())[0]]))
                logger.write('     Country %s has %d neighbours at level 5' % (
                    ISO, len(list(aggd.values())[0])))
            else:
                logger.write('     Country %s has %d regions at level %d, including %d islands' % (
                    ISO, len(aggd), idn, len([dd for dd in list(aggd.values()) if not dd])))

            shelfSave(savedName, aggd)

        assert len(aggd) == 1
        assert ISO == list(aggd.keys())[0]
        logger.write('  Country %s neighbours: ' %
                     ISO + ','.join(neighbourISOs[ISO]))
    savedName = ran.get_GADM_adjacency_matrix_filename('global', 0, 0)
    shelfSave(savedName, neighbourISOs)


def str_reps(dosubs, ss):
    for a, b in list(dosubs.items()):
        ss = ss.replace(a, b)
    return(ss)


def build_aggregate_geometries(resolution=None, forceUpdate=False,  parallel=None, skip_vacuum=False):
    """
    When run in parallel, this skips "vacuum"ing the database until all threads are complete.

    """
    parallel = parallel if parallel is not None else defaults['server']['parallel']
    if resolution is None:
        funcs, names = [], []
        skip_postVacuum = forceUpdate is False and defaults['gadm']['TABLE']+'_'+defaults['gadm']['ID_LEVELS'][0] in osmTools.pgisConnection(verbose=True, schema=GADM_schema).list_tables()
        for resolution in [0, 1, 2, 3, 4, 5]:
            funcs += [[build_aggregate_geometries,
                       [resolution, forceUpdate], dict(skip_vacuum=True)]]
            names += ['Agg GADM to %s' % defaults['gadm']['ID_LEVELS'][resolution]]
        runFunctionsInParallel(
            funcs, names=names, maxAtOnce=None, parallel=parallel)
        db = osmTools.pgisConnection(verbose=True, schema=GADM_schema)
        if not skip_postVacuum:
            db.execute('VACUUM')
        return

    db = osmTools.pgisConnection(verbose=True, schema=GADM_schema)
    new_table = defaults['gadm']['GADM_TABLE']+'_'+defaults['gadm']['ID_LEVELS'][resolution]
    if new_table in db.list_tables(schema=GADM_schema) and not forceUpdate:
        print((' Skipping GADM aggregation: table '+new_table +
              ' already exists. Use forceUpdate to recreate it'))
        return

    GADM_keys = ','.join(defaults['gadm']['ID_LEVELS'][:resolution+1])
    strdict = dict(GADM_ID_LIST=GADM_keys,
                   GADMLEVELNONZERO=defaults['gadm']['ID_LEVELS'][resolution] +
                   ' != 0' if resolution > 0 else "iso != ''",
                   NEWTABLE=defaults['gadm']['GADM_TABLE']+'_'+defaults['gadm']['ID_LEVELS'][resolution],
                   FROMTABLE=defaults['gadm']['GADM_TABLE'],
                   FROMGEOM='the_geom',
                   )

    db.execute(str_reps(strdict, '       DROP TABLE IF EXISTS NEWTABLE ;    '))
    db.execute(str_reps(strdict, """
        CREATE TABLE NEWTABLE AS 
        SELECT geom AS geom, geom::geography AS geog, GADM_ID_LIST
              FROM (SELECT ST_Union(ST_MakeValid(FROMGEOM)) geom, GADM_ID_LIST 
                    FROM FROMTABLE
                    GROUP BY GADM_ID_LIST) t1 ; 
        """))

    db.fix_permissions_of_new_table(new_table)
    db.drop_indices(
        new_table, None, non_unique_columns=defaults['gadm']['ID_LEVELS'][:resolution+1], geom='geom', geog='geog')
    db.create_indices(
        new_table, None, non_unique_columns=defaults['gadm']['ID_LEVELS'][:resolution+1], geom='geom', geog='geog', skip_vacuum=skip_vacuum)
    db.ensure_consistent_geom(new_table, column='geom', schema=GADM_schema)


def import_raw_GADM_into_postGIS(forceUpdate=False, logger=None):
    """
    for reference only:
    GADM_VERSION='2.8'
    GADM_TABLE={2.8:'gadm28', 2:'gadm2'}[GADM_VERSION]
    GADM_ID_LEVELS=['iso','id_1','id_2','id_3','id_4','id_5']

    """
    if logger is None:
        logger = osmTools.osm_logger(
            'import_raw_GADM_into_postGIS', verbose=True, timing=True, prefix='')
    db = osmTools.pgisConnection(
        verbose=True, schema=GADM_schema, logger=logger)
    db.logger.write('Beginning import_raw_GADM_into_postGIS({},{})'.format(
        defaults['gadm']['GADM_VERSION'], forceUpdate))

    if defaults['gadm']['GADM_TABLE'] in db.list_tables(schema=GADM_schema) and not forceUpdate:
        print((' Skipping import_raw_GADM_into_postGIS because ' +
              defaults['gadm']['GADM_TABLE']+' table already exists...'))
        return
    if forceUpdate:
        print('Deleting all gadm tables...')
        for table in db.list_tables(schema=GADM_schema):
            if defaults['gadm']['GADM_TABLE'] in table:
                db.execute('DROP TABLE '+table)
    # Do this in two steps: First, load data into "raw" table, then massage it (e.g. replace NULLs with 0) and strip down the columns:
    if defaults['gadm']['GADM_TABLE']+'raw' not in db.list_tables(schema=GADM_schema):
        print('Loading GADM data into pgis...')
        #db.execute('DROP TABLE IF EXISTS '+GADM_schema+'.'+defaults['gadm']['GADM_TABLE'])
        os.system("""shp2pgsql -s 4326 -g the_geom -I -W "latin1" """+paths['input']+defaults['gadm']['GADM_FILE']+' ' +
                  GADM_schema+'.'+defaults['gadm']['GADM_TABLE']+"""raw  | psql -q %s """ % (db.psql_command_line_flags()))
        db.fix_permissions_of_new_table(defaults['gadm']['GADM_TABLE']+'raw')
    # Second step: copy the table to a new one with 0s instead of NULLs:
    # I was hoping also to reduce the number of columns to just a few, but in the end I'm keeping several groups of columns:
    if defaults['gadm']['GADM_TABLE'] not in db.list_tables(schema=GADM_schema):
        sql=("""
        CREATE TABLE {table} AS
           SELECT the_geom,  gid, gid_0, uid,
        """+ ','.join( [" coalesce(id_{idn}, '0') as id_{idn} ".format(idn=idn) for idn in range(6)])+ """,
        """+ ','.join( [" type_{idn} ".format(idn=idn) for idn in range(1,6)])+ """,
        """+ ','.join( [" engtype_{idn} ".format(idn=idn) for idn in range(1,6)])+ """,
        """+ ','.join( [" name_{idn} ".format(idn=idn) for idn in range(6)])+ """
         FROM {table}raw  ;""").format(table= defaults['gadm']['TABLE'])
        db.execute(sql)
        db.fix_permissions_of_new_table(defaults['gadm']['GADM_TABLE'])
        
    if defaults['gadm']['GADM_VERSION']=='3.6':
        db.execute("""
        alter table gadm36 add column iso character varying(80);
        update gadm36 set iso = gadm36.gid_0;
        """)
        db.logger.write(
        ' Inserted ISO column to make GADM3.6 look a bit like GADM2.8.')


        
    # Drop Antarctica, because that intersects the South Pole and means reprojection not possible
    db.execute('''SELECT count(*) FROM '''+defaults['gadm']['GADM_TABLE'] +
               ''' WHERE ST_Intersects(the_geom, ST_GeomFromText('Linestring(-180 -90, 180 -90)', 4326))''')
    nDrop = db.fetchall()[0][0]
    print('Dropping %d rather cold features that intersect the South Pole' % nDrop)
    db.execute('''DELETE FROM '''+defaults['gadm']['GADM_TABLE'] +
               ''' WHERE ST_Intersects(the_geom, ST_GeomFromText('Linestring(-180 -90, 180 -90)', 4326))''')

    if 'gadm28.shp' in paths['input']+defaults['gadm']['GADM_FILE']:
        # Drop country XCA, (id_0=44 in 2.8), which appears to be a single row = the Caspian Sea
        db.execute('''DELETE FROM '''+defaults['gadm']['GADM_TABLE']+''' WHERE ISO='XCA';''')
        # Drop erroneous geometries that were picked up in tests and manually inspected (e.g. bits of Manitoba in the middle of Greenland)
        gids = [97928, 31917]
        for gid in gids:
            cmd = """WITH t1 AS (SELECT the_geom g1 FROM %s WHERE gid=%s),
                      t2 AS (SELECT the_geom g2 FROM %s, t1 WHERE gid!=%s AND ST_Intersects(the_geom, g1) AND NOT ST_Touches(the_geom, g1)),
                      t3 AS (SELECT ST_Union(ST_CollectionExtract(ST_Intersection(g1, g2),3)) AS geom_intersect FROM t1, t2)
                  UPDATE %s SET the_geom = ST_Multi(ST_SymDifference(g1, geom_intersect)) FROM t1, t3 
                  WHERE gid=%s""" % (defaults['gadm']['GADM_TABLE'], gid, defaults['gadm']['GADM_TABLE'], gid, defaults['gadm']['GADM_TABLE'], gid)
            db.execute(cmd)

    if 'Canada' in defaults['gadm']['GADM_VERSION'] and forceUpdate:
        # Add northamerica census areas
        import install_canada_census_areas as install
        install.main()

    # Reproject the geometry
    # No, leave as 4326
    #print ('Reprojecting gadm geometries')
    # db.execute('''ALTER TABLE '''+defaults['gadm']['GADM_TABLE']+''' ALTER COLUMN the_geom
    #            SET DATA TYPE geometry(MultiPolygon, 3857)
    #            USING ST_Transform(the_geom, 3857);''')

    # check that polygons are valid

    print('Fixing invalid geoms')
    invalidGeoms = db.execfetch(
        '''UPDATE %s SET the_geom = ST_MakeValid(the_geom) WHERE not(ST_IsValid(the_geom)) RETURNING gid, iso;''' % defaults['gadm']['GADM_TABLE'])
    print(('Fixed %d invalid geoms (please check in QGIS that the fix works) with the following gids and isos:\n %s' % (
        len(invalidGeoms), invalidGeoms)))
    invalidGeoms = db.execfetch(
        'SELECT gid, iso FROM %s WHERE not(ST_IsValid(the_geom))' % defaults['gadm']['GADM_TABLE'])
    assert invalidGeoms is None or len(invalidGeoms) == 0

    # Create a geom column in a compromise projection, use for most distance calculations where performance is more important than precision
    db.execute("SELECT AddGeometryColumn('%s', '%s','geom_compromise', %s,'MultiPolygon', 2);" % (
        GADM_schema, defaults['gadm']['GADM_TABLE'], defaults['osm']['srid_compromise']))
    db.execute('UPDATE %s SET geom_compromise = ST_Transform(the_geom, %s);' % (
        defaults['gadm']['GADM_TABLE'], defaults['osm']['srid_compromise']))

    # Also have a column with a geography which can be used for distance and area calculations. If we index this, the lookups using DWithin are much faster
    db.execute('ALTER TABLE %s.%s ADD COLUMN geog geography(MULTIPOLYGON, 4326);' % (
        GADM_schema, defaults['gadm']['GADM_TABLE']))
    db.execute('UPDATE '+defaults['gadm']['GADM_TABLE']+' SET geog = the_geom::geography;')
    db.execute('DROP INDEX IF EXISTS %s_the_geom_idx ;' %
               defaults['gadm']['GADM_TABLE'])  # duplicate index created by shp2pgsql
    db.create_indices(defaults['gadm']['GADM_TABLE'], key=None, non_unique_columns=None,
                      gadm=True, geom='the_geom', geog='geog')
    db.execute('CREATE INDEX %s_geom_compromise_idx ON %s USING gist (geom_compromise);' % (  defaults['gadm']['GADM_TABLE'], defaults['gadm']['GADM_TABLE']))  # index the extra geom column too
    db.execute('CLUSTER %s USING %s_spat_idx' % (defaults['gadm']['GADM_TABLE'], defaults['gadm']['GADM_TABLE']))  # issue 226

    # Should turn this on: db.execute('DROP TABLE '+defaults['gadm']['GADM_TABLE']+'raw CASCADE; ') 
    return(defaults['gadm']['GADM_TABLE'])


def createGADMCountryRaster(iso=None, hemisph=None, id_1s=None, forceUpdate=False):
    """
    Rasterize GADM ids for an individual country (in parallel), and align with the Landscan density raster
       Then merge them all together into a global raster
       If hemisph=='E' or 'W', only does one hemisphere (for countries that straddle 180 degrees
       If id_1s is not None, do a subset (needed for Russia, which is too big)

    Don't use a logger, since this is called in parallel.
    """
    rasterSchema = defaults['osm']['rasterSchema']
    db = osmTools.pgisConnection(schema=rasterSchema)
    if 'gadmraster' in db.list_tables(schema='rasterdata') and not forceUpdate:
        print('GADM raster table already exists...skipping')
        return

    # Create a raster table for each individual country, in parallel
    if iso is None:
        ran = regions_and_neighbours()
        isoDict = ran.get_all_GADM_countries()
        EWcountries = ['UMI', 'NZL', 'USA', 'KIR',
                       'FJI', 'RUS']  # cross 180 degrees longitude
        parallel = defaults['server']['parallel']
        funcs, names = [], []

        # Split Russia and Canada up
        russiaId_1s = [ii[0] for ii in db.execfetch(
            "SELECT DISTINCT id_1::integer FROM "+defaults['gadm']['GADM_TABLE']+" WHERE iso='RUS'")]
        canadaId_1s = [ii[0] for ii in db.execfetch(
            "SELECT DISTINCT id_1::integer FROM "+defaults['gadm']['GADM_TABLE']+" WHERE iso='CAN'")]
        funcs += [[createGADMCountryRaster, ['RUS'],
                   {'hemisph': 'E', 'id_1s': russiaId_1s[:10],  'forceUpdate':forceUpdate}]]
        funcs += [[createGADMCountryRaster, ['RUS'],
                   {'hemisph': 'E', 'id_1s': russiaId_1s[10:20],  'forceUpdate':forceUpdate}]]
        funcs += [[createGADMCountryRaster, ['RUS'],
                   {'hemisph': 'E', 'id_1s': russiaId_1s[20:30],  'forceUpdate':forceUpdate}]]
        funcs += [[createGADMCountryRaster, ['RUS'],
                   {'hemisph': 'E', 'id_1s': russiaId_1s[30:40],  'forceUpdate':forceUpdate}]]
        funcs += [[createGADMCountryRaster, ['RUS'],
                   {'hemisph': 'E', 'id_1s': russiaId_1s[40:60], 'forceUpdate':forceUpdate}]]
        funcs += [[createGADMCountryRaster, ['RUS'],
                   {'hemisph': 'E', 'id_1s': russiaId_1s[60:],  'forceUpdate':forceUpdate}]]
        funcs += [[createGADMCountryRaster, ['CAN'],
                   {'id_1s': canadaId_1s[:3], 'forceUpdate':forceUpdate}]]  # Alberta, BC, Manitoba
        funcs += [[createGADMCountryRaster, ['CAN'],
                   {'id_1s': canadaId_1s[3:4], 'forceUpdate':forceUpdate}]]  
        funcs += [[createGADMCountryRaster, ['CAN'],
                   {'id_1s': canadaId_1s[4:5], 'forceUpdate':forceUpdate}]]  
        funcs += [[createGADMCountryRaster, ['CAN'],
                   {'id_1s': canadaId_1s[5:], 'forceUpdate':forceUpdate}]]
        names += ['GADM raster RUS_E1a','GADM raster RUS_E1b','GADM raster RUS_E1c','GADM raster RUS_E1d', 'GADM raster RUS_E2',
                  'GADM raster RUS_E3',
                  'GADM raster CAN1', 'GADM raster CAN2', 'GADM raster CAN3', 'GADM raster CAN4', ]
        # n.b. untested 201808: I split up the non-CAN1 further, since it was now very rate-limiting (USAW would be next, but not nec.)

        # Now add regular countries
        for iso in isoDict:
            if iso in EWcountries or iso == 'CAN':
                continue
            funcs += [[createGADMCountryRaster,
                       [iso], {'forceUpdate': forceUpdate}]]
            names += ['GADM raster %s' % iso]
        for iso in EWcountries:  # do the eastern hemisphere for countries that cross the dateline
            for hemisph in ['E', 'W']:
                if iso == 'RUS' and hemisph == 'E':
                    continue
                funcs += [[createGADMCountryRaster, [iso],
                           {'hemisph': hemisph, 'forceUpdate': forceUpdate}]]
                names += ['GADM raster %s_%s' % (iso, hemisph)]

        runFunctionsInParallel(
            funcs, names=names, maxAtOnce=None, parallel=parallel)

        # Aggregate all rids
        db.execute('DROP TABLE IF EXISTS gadmraster')
        tableNames = [tt for tt in db.list_tables(
        ) if tt.startswith('gadmraster_')]
        unionNames = ' UNION ALL '.join(
            ['SELECT * FROM '+tname for tname in tableNames])
        cmd = """CREATE TABLE gadmraster AS 
                    SELECT rid, ST_Union(rast) AS rast FROM  ("""+unionNames+""") t1
                    GROUP BY rid"""
        print('Creating union of all individual country rasters...')
        db.execute(cmd)
        # add constraints and index
        print('Adding constraints and index...')
        rc = rt.rasterCon('gadmraster')
        rc.addConstraints(setAllExceptExtent=True)
        rc.execute("""ALTER TABLE gadmraster ADD PRIMARY KEY (rid);""")
        rc.addSpatialIndex()
        for tname in tableNames:
            db.execute("DROP TABLE "+tname)

        return

    countryRastName = 'gadmraster_' + \
        iso.replace('-', '_')  # the replace is for SP-
    if hemisph is not None:
        countryRastName += hemisph
    if id_1s is not None:
        countryRastName += str(id_1s[0])
    if countryRastName in db.list_tables() and not forceUpdate:
        return

    db.execute('DROP TABLE IF EXISTS '+countryRastName)
    # max id value is 41932. So we can fit this in a 16BUI, with a nodata value of 65535
    idLevels = ', '.join(defaults['gadm']['ID_LEVELS']).replace(
        'iso', 'id_0').replace("'", "")
    pixTypes = "ARRAY[" + ", ".join(["'16BUI'"]*len(defaults['gadm']['ID_LEVELS'])) + "]"
    noDataVals = "ARRAY[" + ", ".join(["65535"]*len(defaults['gadm']['ID_LEVELS'])) + "]"
    addBandArgs = ", ".join(
        ["ROW(Null, '16BUI', 65535, 65535)"]*len(defaults['gadm']['ID_LEVELS']))
    ls = rt.rasterCon('landscan')
    # take the max of the bbox if the yscale is negtive
    ycoord = 'YMax' if ls.get_scale()[0] < 0 else 'YMin'

    # What's the strategy here? These people suggest using ST_MapAlgebra to burn in tiles, but that's SLOW and runs out of memory
    # http://geospatialelucubrations.blogspot.com/2014/05/a-guide-to-rasterization-of-vector.html
    # So we create a union of (i) a blank tile in the bottom left corner, which controls the tile alignment
    # This is the raster that is created with ST_AddBand(ST_MakeEmptyRaster(rast)...
    # and (ii) the rasterized geometry.

    # Clause to get GADM polygons is different if the country crosses 180 degrees.
    if hemisph is not None:
        polyClause = """WITH gadmpolys AS (SELECT * FROM (SELECT """+idLevels+""", (ST_Dump(the_geom)).geom AS geom 
                        FROM """+defaults['gadm']['GADM_TABLE']+""" WHERE iso = '"""+iso+"""') gp1 WHERE ST_X(ST_Centroid(geom)) > 0)"""
        if hemisph == 'W':
            polyClause = polyClause.replace('>', '<')
    else:
        polyClause = """WITH gadmpolys AS (SELECT """+idLevels+""", the_geom AS geom 
                        FROM """+defaults['gadm']['GADM_TABLE']+""" WHERE iso = '"""+iso+"""')"""
    if id_1s is not None: #Shouldn't this just use the compareGADM_string function(s) instead? TODO
        if len(id_1s)==1:
            polyClause = polyClause[:-1] + \
            """ AND id_1 = """+str(id_1s[0])+""")"""
        else:
            polyClause = polyClause[:-1] + \
            """ AND id_1 IN """+str(tuple(id_1s))+""")"""

    cmd = """CREATE TABLE """+countryRastName+""" AS """ + polyClause+"""                 
                SELECT ST_Tile(ST_Union(rast), ARRAY"""+str(list(range(1, len(defaults['gadm']['ID_LEVELS'])+1)))+""", 30, 30, True) rast FROM (
                    SELECT ST_AddBand(ST_MakeEmptyRaster(rast), ARRAY["""+addBandArgs+"""]::addbandarg[]) rast
	                    FROM """+rasterSchema+""".landscan,
	                        (SELECT ST_SetSRID(ST_Point(ST_XMin(bbox), ST_"""+ycoord+"""(bbox)), 4326) AS pt FROM 
                            (SELECT ST_Envelope(ST_Collect(geom)) AS bbox FROM gadmpolys) t1) t2
	                    WHERE ST_Intersects(pt, rast)
            UNION
                SELECT ST_AsRaster(geom, 1/120::double precision, -1/120::double precision, 0, 0, """+pixTypes+""", 
                                   value:=ARRAY["""+idLevels+"""]::integer[], nodataval:="""+noDataVals+""")  AS rast
                FROM gadmpolys) t3;"""
    db.execute(cmd)
    db.execute("""ALTER TABLE """+countryRastName +
               """ ADD COLUMN rid integer""")
    cmd = """UPDATE """+countryRastName+""" r1 SET rid = r2.rid FROM landscan r2 
                WHERE ST_Intersects(r1.rast, r2.rast) 
                    AND round(ST_UpperLeftX(r1.rast)::numeric, 10)=round(ST_UpperLeftX(r2.rast)::numeric, 10)
                    AND round(ST_UpperLeftY(r1.rast)::numeric, 10)=round(ST_UpperLeftY(r2.rast)::numeric, 10)"""
    db.execute(cmd)
    if hemisph is not None:
        db.execute("""DELETE FROM """+countryRastName +
                   """ WHERE rid is Null AND ST_UpperLeftX(rast)::integer=180""")
    db.execute("""ALTER TABLE """+countryRastName +
               """ ADD PRIMARY KEY (rid);""")


def simplifyGeomTest(tols=None, forceUpdate=False):
    """
    We might want to simplify the GADM geometries, but it's not clear what tolerance to use before the geometries are too deformed.
    See http://gis.stackexchange.com/questions/11910/meaning-of-simplifys-tolerance-parameter
    This function loops over various tolerances (passed in tols, or uses default), and calculates the area

    IMPORTANT: tolerances are in projected units, so we need to project before simplifying (otherwise units are degrees)
    Here, we project to a conic projection first to minimize distortion, 
    then to an equal area projection to calculate the area
    Saves the chart to scratch, and returns the dataframe with areas

    Note: uses ST_SimplifyPreserveTopology, which is much slower than ST_Simplify
    """
    import matplotlib.pyplot as plt
    import pandas as pd

    outFn = paths['scratch'] + 'simplificationTolerances.pandas'
    if os.path.exists(outFn) and not forceUpdate:
        return pd.read_pickle(outFn)

    if tols is None:
        tols = [1, 2, 3, 4, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    tolStrs = [str(tt) for tt in tols]
    # None makes it easier to parse into a dataframe
    db = osmTools.pgisConnection(curType='default')
    # Conical project, minimizes distortion. Use this to simplify.
    srsSimp = 954027
    srsArea = 954009   # equal area projection. Use to calculate areas

    # Base query - areas without simplifying
    cmd = 'SELECT iso, ST_Area(geog)/1000/1000 FROM %s_iso' % (defaults['gadm']['GADM_TABLE'])
    areaDf = pd.DataFrame(db.execfetch(cmd), columns=[
                          'iso', 'area0']).set_index('iso')

    for tol in tolStrs:
        print('Calculating tolerance %s' % tol)
        cmd = 'SELECT iso, ST_Area(ST_Transform(ST_SimplifyPreserveTopology(ST_Transform(geom, %s), %s), %s))/1000/1000 FROM %s_iso' % (
            srsSimp, tol, srsArea, defaults['gadm']['GADM_TABLE'])
        areaDf['area'+tol] = pd.DataFrame(db.execfetch(cmd),
                                          columns=['iso', 'area']).set_index('iso')
        areaDf['diff_'+tol] = areaDf['area'+tol] - areaDf['area0']
        areaDf['diffpc_'+tol] = areaDf['diff_'+tol]*100./areaDf['area0']

    areaDf.to_pickle(outFn)

    # plot the results
    maxDiff = [areaDf['diff_'+str(tol)].abs().max() for tol in tolStrs]
    meanDiff = [areaDf['diff_'+tol].abs().mean() for tol in tolStrs]
    maxDiffPc = [areaDf['diffpc_'+tol].abs().max() for tol in tolStrs]
    meanDiffPc = [areaDf['diffpc_'+tol].abs().mean() for tol in tolStrs]

    fig, axes = plt.subplots(1, 2, figsize=(8, 4))
    ax2 = axes[0].twinx()
    ax3 = axes[1].twinx()

    axes[0].plot(tols, maxDiff, lw=2, color='r', label='Max absolute')
    axes[1].plot(tols, meanDiff, lw=2, color='b', label='Mean absolute')
    ax2.plot(tols, maxDiffPc, lw=1, color='r', label='Max pc')
    ax3.plot(tols, meanDiffPc, lw=1, color='b', label='Mean pc')

    axes[0].set_ylabel('Square kilometers')
    axes[1].set_ylabel('Square kilometers')
    ax2.set_ylabel('Per cent')
    ax3.set_ylabel('Per cent')
    axes[0].set_xlabel('Tolerance (meters)')
    axes[1].set_xlabel('Tolerance (meters)')
    fig.suptitle(
        'Impact of simplification tolerance on country area', fontsize=16)

    # legend (dummy entries make it easier to compile)
    axes[0].plot(0, 0, lw=1, color='r', label='Max pc')
    axes[1].plot(0, 0, lw=1, color='b', label='Mean pc')
    axes[0].legend(loc=2, fontsize=12)
    axes[1].legend(loc=2, fontsize=12)
    fig.tight_layout(rect=(0, 0, 1, 0.95))

    fig.savefig(outFn.replace('.pandas', 'Fig.pdf'))

    return areaDf


################################################################################################
################################################################################################
################################################################################################
if __name__ == '__main__':
    ################################################################################################
    ################################################################################################
    ################################################################################################
    import sys
    runmode = 'default' if len(sys.argv) < 2 else sys.argv[1].lower()

    if runmode not in ['default', 'test', 'init', 'aggregate', 'prep', 'reinstall', 'raster', 'neighbours']:
        print('\n\n UNRECOGNIZED RUN MODE...\n\n')

    if runmode in ['test']:
        build_all_GADM_adjacency_matrices(forceUpdate=True)
        woeoeo
        # But this is full-size, so the test is not faster.
        defaults['gadm']['GADM_TABLE'] = 'tmptestgadm'
        import_raw_GADM_into_postGIS()
        build_aggregate_geometries(resolution=None, forceUpdate=True)
    if runmode in ['reinstall']:
        reinstall_all_GADM(forceUpdate=True)

    if runmode in ['init']:
        import_raw_GADM_into_postGIS()
    if runmode in ['init', 'aggregate']:
        build_aggregate_geometries(resolution=None, forceUpdate=True)
    if runmode in ['init', 'raster']:
        createGADMCountryRaster(forceUpdate=True)
    if runmode in ['prep']:  # Build aggregates and adjacency lookups
        build_aggregate_geometries(resolution=None, forceUpdate=False)
    if runmode in ['prep', 'neighbours']:  # Build aggregates and adjacency lookups
        build_all_GADM_adjacency_matrices(forceUpdate=True)
    if runmode in ['simptest']:
        simplifyGeomTest()
