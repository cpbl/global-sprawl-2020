#!/usr/bin/python
"""
Usage:
  server_setup.py -h | --help
  server_setup.py add_user <username>
  server_setup.py add_db_ab_initio <dbname>
  server_setup.py add_db <dbname> [--template=<templatename>]
  server_setup.py drop_db <dbname>
  server_setup.py prepare_OS
  server_setup.py setup_sql
  server_setup.py add_srids <dbname>

Options:
  -h --help      Show this screen.

This module contains critical code for setting up the original operating system environment (Ubuntu) and the postGIS server.

But, once things are set up, it also manages the creation of new databases once a template database with GADM and raster data has been set up (using osm_master)


It's been a bit difficult to run psql commands from os.system. So I've used "cd ~postgres" to avoid one permisisons error, and I've put sql script files in /tmp/ to avoid another permissions error.

add_db now looks for an already-created "osmdb_template" database, and copies it. If it doesn't exist, use add_db_ab_initio to create a plain one (faster).
That is, to create the osmdb_template database, do the following:

   ./server_setup.py add_db_ab_initio osmdb_template    > osmdb_template_creation.log 2>&1


Then, populate it with the GADM, GHSL, and landscan data by running the "default" mode of osm_master using a small/dummy continent:

    ./osm_master  default --db=osmdb_template --continent=monaco -f         >> osmdb_template_creation.log 2>&1

Then, load the cities borders: (Caution: this is hardcoded to use the old template! )

    ./osm_cities copy_template --db=osmdb_template

Then, delete the "monaco" tables, and leave this db as a read-only template:

 import osmTools as osmt    
 db = osmt.pgisConnection(verbose=True, db='osmdb_template') 
 db.delete_all_tables(startswith='tmp') 
 db.delete_all_tables(contains='monaco')

Then it looks something like:

 \d
                   List of relations
   Schema    |        Name        |   Type   |  Owner   
-------------+--------------------+----------+----------
 cities      | citychanges        | table    | osmusers
 cities      | cityedges          | table    | osmusers
 osmanalysis | gadm36             | table    | osmusers
 osmanalysis | gadm36_adjacencies | table    | osmusers
 osmanalysis | gadm36_gid_seq     | sequence | osmusers
 osmanalysis | gadm36_id_1        | table    | osmusers
 osmanalysis | gadm36_id_2        | table    | osmusers
 osmanalysis | gadm36_id_3        | table    | osmusers
 osmanalysis | gadm36_id_4        | table    | osmusers
 osmanalysis | gadm36_id_5        | table    | osmusers
 osmanalysis | gadm36_iso         | table    | osmusers
 osmanalysis | urban_thresholds   | table    | osmusers
 public      | geography_columns  | view     | postgres
 public      | geometry_columns   | view     | postgres
 public      | raster_columns     | view     | postgres
 public      | raster_overviews   | view     | postgres
 public      | spatial_ref_sys    | table    | postgres
 rasterdata  | gadmraster         | table    | osmusers
 rasterdata  | ghsl               | table    | osmusers
 rasterdata  | landscan           | table    | osmusers
 rasterdata  | landscan_rid_seq   | sequence | osmusers
(21 rows)



"""






"""
# Additional setup notes:

# Changes that we made to postgresql.conf
# For more documentation, see: 
# https://wiki.postgresql.org/wiki/Tuning_Your_PostgreSQL_Server
# http://dba.stackexchange.com/questions/10208/configuring-postgresql-for-write-performance
# https://www.postgresql.org/docs/9.5/static/runtime-config-wal.html    # Why is this listed here? What should we be doing with WAL?

# Also note that apparently you can play with work_mem at run time: set work_mem = '8MB'


# Allows remote users to connect. Also need to specify IP addresses in pg_hba.conf
listen_addresses = '*'

# Done by McGill CS???
ssl = true 
ssl_cert_file = '/etc/ssl/certs/cs.mcgill.ca.pem'               # (change requires restart)
ssl_key_file = '/etc/ssl/private/cs.mcgill.ca.key'              # (change requires restart)
ssl_ca_file = '/etc/ssl/certs/intermediate.crt'

# Memory allocation, etc. Mostly based on https://wiki.postgresql.org/wiki/Tuning_Your_PostgreSQL_Server 
shared_buffers = 195840MB  
effective_cache_size = 587520MB      # " Changing this setting does not require restarting the database (HUP is enough)."
work_mem = 1002700kB
maintenance_work_mem = 2GB
min_wal_size = 1GB
max_wal_size = 15GB  # Increased Aug2016 from 2GB. May improve write performance for raster import?
checkpoint_completion_target = 0.9 # Increased Aug2016 from 0.7. May improve write performance for raster import?
wal_buffers = 16MB   
default_statistics_target = 100



"""
import docopt
import os
import numpy as np
from osm_config import paths,defaults
SQLscratch = '/tmp/'+defaults['server']['postgres_role']+'/' # Put SQL files somewhere globally-readable, in case the user "postgres" can't read in our scratch path
os.system('mkdir -p '+SQLscratch)
import osmTools as osmt

# I guess the following should be un-hardcoded here. And put into config
KNOWN_OSM_USERS = [] # e.g. ['amy', 'bob']
KNOWN_SPRAWL_USERS = [] # e.g. ['amy', 'bob']
KNOWN_DBs= ['osm',]
KNOWN_SRIDs = list(set([vv for kk,vv in list(defaults['osm'].items()) if kk.startswith('srid_') and kk in ['srid_compromise', 'srid_equalarea', 'srid_equaldistance'] ]))
KNOWN_SCHEMAS = np.unique([vv for kk,vv in list(defaults['osm'].items()) if kk.endswith('Schema')])

def ossystem(str):
    print(str)
    os.system(str)
def ensure_files_removed(filepaths):
    # Make sure we don't use old files:
    for sqlfn in filepaths if isinstance(filepaths,list) else [filepaths]:
        try:
            os.remove(sqlfn)
        except OSError:
            pass

def prepare_OS():
    prepare_OS_script="""
    # Install some requirements for the osm code:
    sudo apt-get install emacs  ipython htop meld  tmux curl  python-pip cifs-utils smbclient lsyncd xclip whois xdotool org-mode sshfs feh  pdftk  git ipython-doc ipython-notebook ipython-qtconsole python-matplotlib  python-numpy lyx  ipython python-rpy2 git  htop meld auctex tmux curl inkscape python-pyquery biber  qgis   python-pip   lsyncd xclip whois xdotool org-mode sshfs  feh pyrenamer pdftk  enscript  hunspell-en-ca emacs  python-mpltoolkits.basemap gftp xvfb    traceroute   latexmk latex2html tex4ht  python-pyside ipython-qtconsole    latexdiff  python-pandas chromium-browser links   automake texinfo mercurial python-scipy nautilus gedit  python-imposm imposm python-networkx python-pygraphviz python-sqlalchemy default-jre npm optipng
    ## Set up postGIS service: 
    sudo apt-get install postgis pgadmin3 postgresql-contrib python-psycopg2

    sudo npm install svgexport -g
    ## Create a group for sharing
    sudo groupadd sprawl
    sudo groupadd osm
    ## Set up a shared folder
    sudo mkdir /home/projects
    sudo mkdir /home/projects/sprawl
    sudo mkdir /home/projects/osm
    sudo chgrp  sprawl /home/projects/sprawl
    sudo chmod  g+w /home/projects/sprawl
    sudo chgrp  osm /home/projects/osm
    sudo chmod  g+w /home/projects/osm
    ## Set the setgid bit:
    sudo chmod  g+s /home/projects/sprawl
    sudo chmod  g+s /home/projects/osm
    #####sudo setfacl -R -m g:sprawl:rwx /home/projects/sprawl
    ## Set the default ACLs:
    sudo setfacl  -d -m u::rwx,g::rwx,g:sprawl:rwx,o::--- /home/projects/sprawl
    sudo setfacl  -d -m u::rwx,g::rwx,g:osm:rwx,o::--- /home/projects/osm
    ## Make this folder private, so non-group members cannot see it: (already done in line above)
    #sudo chmod o-rx /home/projects/sprawl
    #sudo setfacl -d -m o::--- /home/projects/sprawl
    ##
    """

    # Install Python dependencies.
    # TODO: Include installation script for pip, or make the installer configurable.
    prepare_python_environ_script="while read line ; do pip install --user --upgrade `echo $line | cut -f 1 -d '='` ; done < requirements.txt"
    
    sql="""
        CREATE ROLE osmusers NOLOGIN;                      
    """
    ossystem(prepare_OS_script)
    ossystem("""cd ~postgres && sudo su postgres  -c "psql -d postgres -c 'CREATE ROLE osmusers NOLOGIN;' "  """)

    
def add_all_SRIDs(dbname, srids=None):
    if srids is None:
        srids =  KNOWN_SRIDs
    sql_srid_fn= SQLscratch + 'server_setup_add_db_srid.sql'
    ensure_files_removed(sql_srid_fn)    
    print((' Initial SRIDs are: ',srids))
    sql = osmt.pgisConnection(verbose=False).insert_srs(srids, return_sql=True, skip_check=True) #Can also remove first argument, to do all known
    with open(sql_srid_fn,'wt') as fout:
        fout.write(sql)
    ossystem('cd ~postgres && sudo su postgres -c "psql -d {dbname} --file={sql_srid_fn} " '.format(dbname=dbname, sql_srid_fn=sql_srid_fn))

def add_schema(schema):
    """ Just returns sql code to add a schema to current db """
    return '\n'.join(["""
            -- NEW SCHEMA                                                           
            CREATE SCHEMA {schema}                             
              AUTHORIZATION postgres;                          

            GRANT ALL ON SCHEMA {schema} TO postgres;          
            GRANT USAGE ON SCHEMA {schema} TO public;          
            GRANT ALL ON SCHEMA {schema} TO osmusers;          

            ALTER DEFAULT PRIVILEGES IN SCHEMA {schema}        
                GRANT INSERT, SELECT, UPDATE, DELETE ON TABLES 
                TO osmusers;                                   

            GRANT ALL PRIVILEGES ON                  SCHEMA {schema} TO osmusers; 
            GRANT ALL PRIVILEGES ON ALL TABLES    IN SCHEMA {schema} TO osmusers; 
            GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA {schema} TO osmusers; 

            GRANT ALL PRIVILEGES ON                  SCHEMA public TO osmusers;   
            GRANT ALL PRIVILEGES ON ALL TABLES    IN SCHEMA public TO osmusers;   
            GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO osmusers;   

           -- this last one ensures that we can all drop tables that others created
           -- see http://stackoverflow.com/questions/26645599/automatically-allow-access-to-tables-in-postgres-from-a-user  
            ALTER DEFAULT PRIVILEGES IN SCHEMA {schema}        
                    GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO osmusers;   
           """.format(schema=schema)])

def add_user(username):
    """
    Generate an sql script and generate an sh script which calls it.
    Lets assume that the psql username is also already an extant OS username.
    """
    sh=''
    postgres_sql=''
    shfn= paths['scratch'] + 'server_setup_add_user.sh'
    sql_postgres_fn= SQLscratch + 'server_setup_add_user_postgres.sql'
    ensure_files_removed([shfn,sql_postgres_fn])

    if not username in KNOWN_OSM_USERS: # and not username in KNOWN_SPRAWL_USERS 
        raise Exception(' You must list the user in KNOWN_...USERS (hardcoded) first.')

    if username in KNOWN_OSM_USERS:
        sh+="""
        # Ensure group already exists
        sudo groupadd -f osm
        ## Add user to a "osm" group:
        sudo usermod -a -G osm    {user}
        """.format(user=username)

        postgres_sql+="""
        CREATE ROLE {user} LOGIN;                              
        GRANT osmusers TO {user}
        --  Now you must assign everyone a password! This is not done here, but the necessary calls are of the form:
        --       alter role {user} password 'mylongacronym';

        """.format(user=username)
        
    if username in KNOWN_SPRAWL_USERS:
        sh+="""
        ## Add user to a "sprawl" group:
        sudo usermod -a -G sprawl    {user}
        """.format(user=username)

    if postgres_sql:
        sh+="""
        cd ~postgres
        sudo su postgres -c "psql -U postgres --file={}"
        """.format(sql_postgres_fn)
        
    with open(sql_postgres_fn,'wt') as fout:
        fout.write(postgres_sql)
    os.system('chmod o+r '+sql_postgres_fn)
    with open(shfn,'wt') as fout:
        fout.write(sh)
    ossystem('sudo bash '+shfn)

def copy_template_db(dbname, template_db=None):


    TEMPLATE_DB = 'osm_rasters_template'
    if defaults['gadm']['GADM_VERSION'] =='3.6':
        TEMPLATE_DB = 'osmdb_template'
    # Override from command line option:
    TEMPLATE_DB = template_db if template_db is not None else TEMPLATE_DB
    
    os.system("""cd ~postgres && sudo su postgres  -c "createdb -O postgres -T {template} {newdb}"  && echo "New db {newdb} created with all base data installed." """.format(newdb=dbname,template=TEMPLATE_DB))
    # Following seems not to get copied over in the above template copy:
    psql_cmd="""
ALTER DATABASE {dbname} SET search_path = osmanalysis, regionboundaries, osmprocessing, osmdata, rasterdata, cities, public;
ALTER DATABASE {dbname}
  SET postgis.gdal_enabled_drivers = 'GTiff PNG JPEG';
GRANT CONNECT, TEMPORARY ON DATABASE {dbname} TO public;
GRANT ALL ON DATABASE {dbname} TO postgres;
GRANT ALL ON DATABASE {dbname} TO osmusers;
""".format(dbname=dbname)
    sql_db_fn= SQLscratch + 'server_setup_add_db_from_template_postadjustment.sql'
    with open(sql_db_fn,'wt') as fout:
        fout.write(psql_cmd)
    os.system('chmod o+r '+sql_db_fn)
    ossystem('cd ~postgres && sudo su postgres  -c "psql -d postgres --file={sql_db_fn} " '.format(sql_db_fn=sql_db_fn))
        
def add_db_ab_initio(dbname):
    """
    I think this could be condensed to a single sql call by postgres by using "on database {dbname}" in the schema section. I've implemented what we used to have in sql scripts.
    """
    sh=''
    postgres_sql_1=''
    postgres_sql_2=''
    db_sql=''
    shfn= paths['scratch'] + 'server_setup_add_db.sh'
    sql_postgres_fn1= SQLscratch + 'server_setup_add_db_postgres_1.sql'
    sql_postgres_fn2= SQLscratch + 'server_setup_add_db_postgres_2.sql'
    sql_db_fn= SQLscratch + 'server_setup_add_db_db.sql'
    ensure_files_removed( [shfn,    sql_postgres_fn1,     sql_postgres_fn2,     sql_db_fn, ] )
    
    postgres_sql_1+= """
      CREATE DATABASE {dbname}
        WITH OWNER = postgres
             ENCODING = 'UTF8'
      --       TABLESPACE = pg_default
      --       LC_COLLATE = 'en_CA.UTF-8'
      --       LC_CTYPE = 'en_CA.UTF-8'
             CONNECTION LIMIT = -1;

    """.format(dbname=dbname)

    with open(sql_postgres_fn1,'wt') as fout:
        fout.write(postgres_sql_1)
    os.system('chmod o+r '+sql_postgres_fn1)

    for schema in KNOWN_SCHEMAS:
        db_sql+=add_schema(schema)
    db_sql+="""
      CREATE EXTENSION postgis;
      CREATE EXTENSION postgis_topology;
      CREATE EXTENSION fuzzystrmatch;
      CREATE EXTENSION postgis_tiger_geocoder;
    """

    with open(sql_db_fn,'wt') as fout:
        fout.write(db_sql)
    os.system('chmod o+r '+sql_db_fn)
        
    # Can I set the schema search path before creating the schemas?   If so, this can be simplified.

    with open(sql_postgres_fn2,'wt') as fout:
        fout.write("""
    -- Create a default search_path so that all schemas are considered when looking for tables
      ALTER DATABASE {dbname} SET search_path TO {allschemas},public;

       --  To change access permissions (e.g. where someone can login from), edit /home/projects/var_lib_pgsql_9.3/data/pg_hba.conf, or run pgadmin3 with sudo, and use its editor

    -- ALTER DATABASE {dbname}
    --   SET search_path =  public, osmanalysis, osmdata, rasterdata;
      GRANT CONNECT, TEMPORARY ON DATABASE {dbname} TO public;
      GRANT ALL ON DATABASE {dbname} TO postgres;
      GRANT ALL ON DATABASE {dbname} TO osmusers;
      ALTER DATABASE {dbname} SET postgis.gdal_enabled_drivers TO 'GTiff PNG JPEG';

        """.format(dbname=dbname, allschemas = ','.join(KNOWN_SCHEMAS)))
    os.system('chmod o+r '+sql_postgres_fn2)

        

        
        
    ossystem('cd ~postgres && sudo su postgres  -c "psql -d postgres --file={sql_postgres_fn1} " '.format(sql_postgres_fn1=sql_postgres_fn1))
    ossystem('cd ~postgres && sudo su postgres  -c "psql -d {dbname} --file={sql_db_fn} " '.format(dbname=dbname, sql_db_fn=sql_db_fn))
    ossystem('cd ~postgres && sudo su postgres  -c "psql -d postgres --file={sql_postgres_fn2} " '.format(sql_postgres_fn2=sql_postgres_fn2))
    add_all_SRIDs(dbname)    
    return





################################################################################################
################################################################################################
################################################################################################
if __name__ == '__main__':
################################################################################################
################################################################################################
################################################################################################
# Docopt is a library for parsing command line arguments

    try:
        # Parse arguments, use file docstring as a parameter definition
        arguments = docopt.docopt(__doc__)
        knownmodes = [aa for aa in arguments if not aa.startswith('-') and not aa.startswith('<')]
        runmode=''.join([ss*arguments[ss] for ss in knownmodes])
        runmode= None if not runmode else runmode
        
        if runmode == 'add_user':
            add_user(arguments['<username>'])
        if runmode == 'add_db':
            dbtemplate =  arguments['--template']
            defaults['server']['postgres_db'] = arguments['<dbname>']
            print(('Setting default db to '+defaults['server']['postgres_db']))
            copy_template_db(arguments['<dbname>'], template_db = dbtemplate)
        if runmode == 'add_db_ab_initio':
            defaults['server']['postgres_db'] = arguments['<dbname>']
            print(('Setting default db to '+defaults['server']['postgres_db']))
            add_db_ab_initio(arguments['<dbname>'])
        if runmode == 'prepare_OS':
            prepare_OS()
        if runmode == 'setup_sql':
            prepare_OS()
            for db in KNOWN_DBs:
                add_db(db)
            for user in KNOWN_OSM_USERS:
                add_user(user)
        if runmode =="drop_db":
            print(""" uhh, no. You do that yourself, please:
            cd ~postgres
            sudo su postgres
            psql
            drop database THEDBNAME ;
            """)
        
    # Handle invalid options
    except docopt.DocoptExit as e:
        print(e.message)


