#!/usr/bin/python
"""
Analyzes log files to identify bottlenecks in code and prioritize performance improvements

Usage:
  analyze_log_files plot  [options]
  analyze_log_files -h | --help

Options:
  -h --help      Show this screen.
  --continent=<continent>   Process only a single continent
  --cluster-radius=<radius>   Specify a single cluster radius to override config file setting
  --db=<database>      Specify database to use (e.g. osm, osmdev, etc)
  -f --force-update    Overwrite/recalculate database tables and outputs
  -s --serial       Turn off parallel computation; useful for debugging 
  -v --verbose        Verbose output for debugging

"""
from docopt import docopt

from osm_config import paths,defaults,  update_paths_to_be_specific_to_database,parse_common_docopt_options
SP=paths['scratch']
WP=paths['working']
import os,re,datetime
import numpy as np
import pandas as pd

from cpblUtilities import dgetget,dsetset,transLegend
from cpblUtilities.mathgraph import xyticksExponentiate
from cpblUtilities.mathgraph import  dfOverplotLinFit
from cpblUtilities.matplotlib_utils import  plot_diagonal

def get_final_logger_sections_in_files(continent):
    import glob
    return '\n'.join([get_final_logger_section_in_file(lfn) for lfn in glob.glob(paths['working']+'logs/roadgraph_connectivity_{}_10*.log'.format(continent))])

def get_final_logger_section_in_file(lfn):
    """ Actually, this now takes the final esection in every file, since the log file can now be split up into many"""
    if not os.path.exists(lfn):
        print(('Failed to find {}'.format(lfn)))
        return ''
    txt=open(lfn,'rt').read()
    loggers=[SS.split('\n') for SS in txt.split('NEW LOGGER: ')]
    nlines=[len(SS) for SS in loggers]
    print((' Found %d logger initiations in %s  (lengths '%(len(loggers)-1, lfn)  + ','.join([str(nn) for nn in nlines]) ))
    txtlines=loggers[-1]
    return('\n'.join(txtlines))

def dt2seconds(ss, nan=np.nan):
    return float(ss[:-1]) if ss.endswith('s') else 60*float(ss[:-1]) if ss.endswith('m') else nan
    
def get_goodsize_events(rtxt):
    ff=re.findall(r':([^\n]*GOOD SIZE.*?Wrote to file.*?Wrote to file.)',rtxt,re.DOTALL)
    print(('     Found %d "good size" events...'%len(ff)))
    eventDicts=[]
    for anff in ff:
        allL=re.findall(r'dt=([^\]]*)](.*?)\n',anff+'\n')
        firstline=anff.split('\n')[0]
        nCore,nTotal = [int(a) for a in allL[0][1].strip().split(' ')[0].split(',')]
        region=firstline.split(': ')[1]
        # If you want to break out some other times here (build, analyze, choose) do it here.
        totalTime= sum([dt2seconds(tt,nan=np.nan) for tt,ss in allL])
        eventDicts+=[dict(nCore=nCore,region=region,totalTime=totalTime, allL=allL)]
    return(eventDicts)

def get_nochoice_events(rtxt):
    """ This is ugly. Output format should be changed so we don't need to do this, ie put count on "No gadm ids" line [DONE; update below when rerun afer Nov2016]."""
    ff=re.findall(r'(Found roughly[^\n]*\n[^\n]*\n[^\n]*No gadm ids.*?Wrote to file.*?Wrote to file.)',rtxt,re.DOTALL)
    print(('     Found %d "no gadm ids deeper" events...'%len(ff)))
    eventDicts=[]
    for anff in ff:
        allL=re.findall(r'dt=([^\]]*)] : (.*?)\n',anff+'\n')
        firstline=anff.split('\n')[0]
        nCore = int(firstline.split(' roughly ')[1].split(' nodes')[0])
        region = firstline.split('nodes in ')[1].split(':')[0]
        # If you want to break out some other times here (build, analyze, choose) do it here.
        totalTime= sum([dt2seconds(tt,nan=np.nan) for tt,ss in allL])
        eventDicts+=[dict(nCore=nCore,region=region,totalTime=totalTime, allL=allL)]
    return(eventDicts)

def parse_times_from_linepairslist(lpl):
    """ This takes the output of the line
        allL=re.findall(r'dt=([^\]]*)] : (.*?)\n',anff+'\n')
        and notes a few particular times
    """
    rows=[['time_sql','WITH all'],
          ['time_edges','Classified edges',],
          ['time_nodes','Analyzed nodes',],
          ['time_build','Built',],
          ['time_chooseregion','  Chose a region',], # Deprecated. Here for backwards compatibility
          ['time_retrievededges','  Retrieved edges',],
          ['time_writeedges','Wrote to',],  # currently is the sum of the two writes, but dominated by write edges. Write code is updated to log differentiated messages.
          ]
    if  lpl[0][1].startswith('WITH all'): # These are probably just ISO-neighbours cases or something? tbd
        if not lpl[1][1].startswith('WITH all'):
            print((' Strange: second line is '+lpl[1][1]))
        elif 'TOO BIG' in lpl[0][1]:
            if not lpl[3][1].startswith('WITH all') and lpl[4][1].startswith('WITH all'):
                print(' Strange: TOO BIG format unexpected.')
    
    times=dict([  [nn, [dt2seconds(tt,nan=np.nan) for tt,ss in lpl if ss.startswith(sw)]] for nn,sw in rows]+
               [['totalTime2',sum([dt2seconds(tt,nan=np.nan) for tt,ss in lpl])]] )
    times['time_retrieve2']= sum(times['time_chooseregion']+times['time_retrievededges'])
    times['time_retrieve1']= np.nan if len(times['time_sql'])<2 else times['time_sql'][1]
    for kk in ['time_build','time_edges','time_nodes', 'time_writeedges']:
        times[kk]=sum(times[kk])
    return pd.Series(dict([kk,vv] for kk,vv in list(times.items()) if kk in ['time_retrieve1', 'time_retrieve2', 'time_build', 'totalTime2', 'time_edges', 'time_nodes',  'time_writeedges']))
    
def combine_all_log_file_records(forceUpdate=False):
    alfFN=paths['scratch']+'analyze_log_files.pandas'
    if not os.path.exists(alfFN) or forceUpdate:
        alldfs=[]
        for continent in defaults['osm']['continents']:
            txt=get_final_logger_sections_in_files(continent)
            nc=get_nochoice_events(txt)
            dfnc=pd.DataFrame(nc)
            dfnc['continent']=continent
            dfnc['howchose']='finest'

            gs=get_goodsize_events(txt)
            dfgs=pd.DataFrame(gs)
            dfgs['continent']=continent
            dfgs['howchose']='goodsize'

            alldfs+=[dfnc,dfgs]

        df=pd.concat(alldfs).set_index(['continent','region','howchose'])
        ###df=df.reindex(range(len(df)))
        print(' Grabbing various delta-times')
        dftimes=df.allL.apply(parse_times_from_linepairslist)
        df=df.merge(dftimes, left_index=True, right_index=True)
        assert (df.totalTime==df.totalTime2).all()

        df['ISO']= df.reset_index().region.map(lambda ss: ss if len(ss)==3 else ss[2:5]).values
        df.to_pickle(alfFN)
    else:
        df=pd.read_pickle(alfFN)
    return df.reset_index()
    

def plot_stats(forceUpdate=False):
    dfw=combine_all_log_file_records(forceUpdate=forceUpdate)
    pd.set_option('display.width', 1000)

    for groupvar in ['continent','ISO']:
        sums=dfw.reset_index().groupby(groupvar).sum()
        frctime=sums[[cc for cc in sums.columns if cc.startswith('time_')]]
        frctime=frctime.divide(sums.totalTime, axis='index').applymap(lambda ff: '%.1f%%'%(ff*100))    
        frctime['totalTime']=sums['totalTime']
        frctime['nRegions'] = dfw.reset_index().groupby(groupvar)['region'].count()
        frctime['nNodes'] = sums['nCore'].astype(int)
        frctime.sort_values('totalTime', inplace=True)
        frctime['sTotalTime']=frctime.totalTime.map(lambda ss:  str(datetime.timedelta(seconds=ss)).split('.')[0])
        frctime=frctime[[cc for cc in frctime if cc not in ['totalTime']]]
        print(frctime)

    
    for continent in dfw.continent.unique():
        df=dfw[dfw.continent==continent]
        dff=df[df.howchose=='finest']
        dfg=df[df.howchose=='goodsize']
        import pylab as plt
        plt.figure(1)
        plt.clf()
        ax=plt.subplot(311)
        plt.hist([dfg.nCore.dropna(),dff.nCore.dropna()], bins=100, alpha=.5,  label=['good size nodes','no-choice nodes'], color=('g','r'))
        plt.legend()
        plt.title(continent)
        plt.subplot(312)
        plt.hist(df[df.nCore<1000].nCore, bins=100, alpha=.5, label='nodes in core')

        plt.legend()
        plt.xlabel('N (number of nodes in core)')
        ax=plt.subplot(313)
        plt.hist(df.totalTime.dropna().map(np.log10), bins=100, alpha=.5, )
        xyticksExponentiate(base10=True, ax=ax)
        
        plt.xlabel('Seconds for whole region')
        plt.show(), plt.draw()
        fn=paths['scratch']+'nodes_in_core_'+continent+'.png'
        plt.savefig(fn)
        print(('Wrote '+fn))

    for nv in ['nCore']:
        fig,ax=plt.subplots(1)
        # We should look at different steps, and analyze speed this way:
        for continent in dfw.continent.unique():
            df=dfw[dfw.continent==continent]
            ax.loglog( df[nv],df['totalTime'],'.', label=continent, alpha=.5)
            ax.set_xlabel(nv),  plt.ylabel('time (s)')
            plot_diagonal( df[nv],df['totalTime'], ax=ax)
            dfOverplotLinFit(df.query('nCore>10'), nv, 'totalTime', ax=ax) # Automatically detects log axes!
            transLegend()
        plt.grid()
        plt.axis('image')
        plt.savefig(paths['scratch']+nv+'_v_time.png')
    plt.show()
    return dfw
  

    
################################################################################################
if __name__ == '__main__':
################################################################################################
################################################################################################
    from cpblUtilities.configtools import get_docopt_mode
    arguments = docopt(__doc__)
    forceUpdate = parse_common_docopt_options(defaults, arguments)
    runmode = get_docopt_mode(arguments)
    if runmode in ['plot']:
        df=plot_stats(forceUpdate = forceUpdate)

