#!/usr/bin/python
"""
Various tools for use with osm analysis

"""
import os, sys, platform, time, psutil, operator
import pandas as pd
import numpy as np
from scipy import spatial
from collections import OrderedDict

from osgeo import osr
from osm_config import paths,defaults
import postgres_tools as psqlt
import cpblUtilities as utils
from cpblUtilities.stats import df_impute_values_ols
#from cpblUtilities.country_tools import country_tools
from rawOSMinfo import available_test_continents
#from cpblUtilities.parallel import runFunctionsInParallel

# We never used the numeric codes for these; if we need them, we should hardcode them here.
CONTINENT_IDS=dict([(cont, contid+10) for contid, cont in enumerate(
                   ['australiaoceania','northamerica','africa', 'asia',  'centralamerica', 'europe','southamerica', 'grid','hell','medieval','monaco','macedonia','lux','hamburg','hamburgallways','hamburgnottrackservice','rhinel','rhinelallways','rhinelnots']+available_test_continents)]) # This list really should only ever be added to; do not change global ids
CONTINENT_IDS.update({'planet':1})
ID2CONTINENT= {v: k for k, v in list(CONTINENT_IDS.items())}
GADM_CONTINENT_PRETTYNAMES = dict([[cc,cc[0].upper()+cc[1:]] for cc in list(CONTINENT_IDS.keys())])
GADM_CONTINENT_PRETTYNAMES.update({'australiaoceania': 'Australia/Oceania',
                              'northamerica': 'North America',
                              'centralamerica':  'Central America',
                              'southamerica': 'South America',
                               })
GHSL_YEARS = dict(years =       OrderedDict(list(zip([1975,1990,2000,2014,'Stock'],['$<$1975','1975--89','1990--99', '2000--13', 'Stock']))),
                  years_short = OrderedDict(list(zip([-1,'Stock',1975,1990,2000,2014],['Stock','Stock','$<$1975','75--89','90--99', '2000--13'] ))),
                  years_shortest = OrderedDict(list(zip([1975,1990,2000,2014],['$<$1975',"'75\n--'89","'90\n--'99", "'00\n--'13"] ))),
                  ranges = OrderedDict(list(zip([1975,1990,2000,2014], ([1900,1974], [1975,1989], [1990,1999], [2000,2013]))))
              )

# Pretty names for our connectivity metrics are in aggregate_connectivity_metrics

def slug(text, encoding=None, permitted_chars='abcdefghijklmnopqrstuvwxyz0123456789_'):
    """This is the 'slugify' command that removes special charachters and accents. This helps avoid some issues with
        some of the street names being stored as Postgres text (which do not allow some of these things). It changes the current format
        to the ascii equivilant. Takes the input text and defults to the list of 'approved' charachters and assumes it is just plain text encoding"""
    from unicodedata import normalize

    if isinstance(text, str):
        text = text.decode(encoding or 'ascii')
        clean_text = text.strip().replace(' ', '_').lower()
        while '--' in clean_text:
            clean_text = clean_text.replace('--', '_')
        ascii_text = normalize('NFKD', clean_text).encode('ascii', 'ignore')
        strict_text = [x if x in permitted_chars else '' for x in ascii_text]
        return ''.join(strict_text)
    Returning_None_badcall

def transform_psql_output_to_qgis_query(ids,kind='edge',complement=False):
    
    ids=[ str(int(ii)) for ii in ids ]
    qgisQuery=''
    for ii in ids:
        qgisQuery+='%s_id'%kind+'!'*complement+'=%s '%ii+{False:'OR ',True:'AND '}[complement]
    qgisQuery=qgisQuery.rstrip(' OR ').rstrip(' AND ')
    return qgisQuery

class pgisConnection(psqlt.dbConnection):
    """  Inherit postgres stuff from dbConnection.
    And add postGIS-specific methods: insert_srs, spatial indices, ...
    """
    def __init__(self, schema=None, verbose=False,  host=None, db=None, curType='DictCursor', logger=None, command=None):
        psqlt.dbConnection.__init__(self,schema=schema,verbose=verbose,host=host,db=db,curType=curType,logger=logger, command=command)

    def insert_srs(self, srs_list=None, return_sql=False, skip_check=False):
        """
        Inserts spatial reference system. srs is the srid, or a list of srids.
        NOTE: srs is NOT the srs of the authority (e.g. ESRI), but the ID in the PostGIS table (defined below)
        srs must be an integer or list of integers

        Accepts a dict of srs, as another alternative to using the hardcoded list here.

        This requires superuser access. It fails for normal users. So if an SRS is not yet in there, just copy the code and do it as postgres user.
        Alternatively, if return_sql=True, then it returns the SQL code rather than trying to execute it.

        skip_check can be used along with return_sql to return code without checking whether the SRID is already in the database.

        By default (srs_list not specified), this will insert all known SRIDs.
        """
        # each projection need srid (unique ID), auth_name, auth_srid, proj4txt, WKT representation. See http://postgis.net/docs/manual-2.0/using_postgis_dbmanagement.html#spatial_ref_sys
        # you can get the string from spatialreference.org (then click on 'PostGIS spatial_ref_sys INSERT statement')

        # Note that 96864 is the same as 3857, but many products are defined as EPSG 3857 rather than sr-org 96864
        srs_dict = {900913: """900913, 'sr-org', 7483, '+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs', 'PROJCS["WGS 84 / Pseudo-Mercator",GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],PROJECTION["Mercator_1SP"],PARAMETER["central_meridian",0],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["X",EAST],AXIS["Y",NORTH],EXTENSION["PROJ4","+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs"],AUTHORITY["EPSG","3857"]]'""",
                    954027: """954027, 'esri', 54027, '+proj=eqdc +lat_0=0 +lon_0=0 +lat_1=60 +lat_2=60 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs ', 'PROJCS["World_Equidistant_Conic",GEOGCS["GCS_WGS_1984",DATUM["WGS_1984",SPHEROID["WGS_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Equidistant_Conic"],PARAMETER["False_Easting",0],PARAMETER["False_Northing",0],PARAMETER["Central_Meridian",0],PARAMETER["Standard_Parallel_1",60],PARAMETER["Standard_Parallel_2",60],PARAMETER["Latitude_Of_Origin",0],UNIT["Meter",1],AUTHORITY["EPSG","54027"]]'""",
                    954009: """954009, 'esri', 54009, '+proj=moll +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs ', 'PROJCS["World_Mollweide",GEOGCS["GCS_WGS_1984",DATUM["WGS_1984",SPHEROID["WGS_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Mollweide"],PARAMETER["False_Easting",0],PARAMETER["False_Northing",0],PARAMETER["Central_Meridian",0],UNIT["Meter",1],AUTHORITY["EPSG","54009"]]'""",
                    96864:   """96864, 'sr-org', 6864, '+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +a=6378137 +b=6378137 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs ', 'PROJCS["WGS 84 / Pseudo-Mercator",GEOGCS["Popular Visualisation CRS",DATUM["Popular_Visualisation_Datum",SPHEROID["Popular Visualisation Sphere",6378137,0,AUTHORITY["EPSG","7059"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6055"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4055"]],UNIT["metre",1,AUTHORITY["EPSG","9001"]],PROJECTION["Mercator_1SP"],PARAMETER["central_meridian",0],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],AUTHORITY["EPSG","3785"],AXIS["X",EAST],AXIS["Y",NORTH]]'""",
                    962:     """962,   'sr-org', 62, '+proj=aea +lat_1=29.83333333333334 +lat_2=45.83333333333334 +lat_0=37.5 +lon_0=-96 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs ', 'PROJCS["Albers Equal area",GEOGCS["WGS 84",DATUM["World Geodetic System 1984",SPHEROID["WGS 84",6378137.0,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0.0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.017453292519943295],AXIS["Geodetic latitude",NORTH],AXIS["Geodetic longitude",EAST],AUTHORITY["EPSG","4326"]],PROJECTION["Albers_Conic_Equal_Area"],PARAMETER["central_meridian",-96.0],PARAMETER["latitude_of_origin",37.5],PARAMETER["standard_parallel_1",29.833333333333336],PARAMETER["false_easting",0.0],PARAMETER["false_northing",0.0],PARAMETER["standard_parallel_2",45.833333333333336],UNIT["m",1.0],AXIS["Easting",EAST],AXIS["Northing",NORTH],AUTHORITY["EPSG","41110"]]'""",
                    954030:  """954030, 'esri', 54030, '+proj=robin +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs ', 'PROJCS["World_Robinson",GEOGCS["GCS_WGS_1984",DATUM["WGS_1984",SPHEROID["WGS_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Robinson"],PARAMETER["False_Easting",0],PARAMETER["False_Northing",0],PARAMETER["Central_Meridian",0],UNIT["Meter",1],AUTHORITY["EPSG","54030"]]'"""}

        if srs_list is None: srs_list = list(srs_dict.keys())
        if isinstance(srs_list, int): srs_list = [srs_list]   # convert to list
        if isinstance(srs_list,dict):
            srs_dict=srs_list
            srs_list=list(srs_dict.keys())
        assert all([isinstance(jj, int) for jj in srs_list])  # make sure all srs are ints

        # get existing SRSs
        if not skip_check:
            self.execute('''SELECT srid FROM spatial_ref_sys''')
            existing_srs = self.fetchall()

        allsql=''
        for srs in srs_list:
            if not skip_check and [srs] in existing_srs:
                print(('\tSpatial reference %d already exists' % srs))
            else:
                sql='''INSERT into spatial_ref_sys (srid, auth_name, auth_srid, proj4text, srtext) values (%s); ''' % srs_dict[srs]
                if return_sql:
                    allsql+= sql+'\n'
                else:
                    try:
                        self.execute(sql)
                        print(('Inserted spatial reference %d' % srs))
                    except:
                        print(('Insertion failed for spatial reference %d.  Try this as a super user:\n\n\n' % srs))
                        print(('''INSERT into spatial_ref_sys (srid, auth_name, auth_srid, proj4text, srtext) values (%s); \n\n\n''' % srs_dict[srs]))
                        input('  Likely, you need to do this insertion as a super user ("sudo su postgres"; then "cd"; then "psql {}"). Press enter after doing this.'.format(defaults['osm']['postgres_db']))

        if return_sql: return(allsql)

    def find_parallel_edges(self,continent,cluster='10',unique=False,complement=False,qgisFormat=True,edgelist=None,outfile=None):
        """Outputs all the information of all parallel edges for a given continent,
        and returns a string that can be copy and pasted into QGIS's query builder
        to quickly identify, visualize and classify parallel edges.

        Thanks, StackOverflow (as usual):
        http://stackoverflow.com/questions/10997043/postgres-table-find-duplicates-in-two-columns-regardless-of-order
        """
        cmd="""SELECT """+"""DISTINCT ON ( least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id) )"""*unique+"""edge_id
        FROM edges_continent_CLUSTER WHERE ( least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id) ) in
        (
                SELECT least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id)
                FROM edges_continent_CLUSTER
                GROUP BY least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id)
                HAVING COUNT(*)>1
        )"""+"""AND edge_id in edgelist"""*(edgelist != None)+"""
        ;"""
        edgelist=str(edgelist).lstrip('[').rstrip(']').replace('L','')
        edgelist='('+edgelist+')'
        self.report('Long query suppressed here. It has form '+cmd)
        self.silently_execute(cmd.replace('continent',continent).replace('CLUSTER',cluster).replace('edgelist',edgelist))
        if not qgisFormat:
            return self.fetchall()
        qgisQuery=transform_psql_output_to_qgis_query([ ii[0] for ii in self.fetchall() ],kind='edge',complement=complement)
        if outfile:
            outfile=open(outfile,'w')
            outfile.write(qgisQuery+'\n')
            outfile.close()
        return qgisQuery


    def create_indices(self, table, key=None, non_unique_columns=None,
                       gadm=False, geog=False, geom=False, skip_vacuum=False,
                       forceUpdate=True, schema=None):
        """ This creates indices, but it's a bit too postGIS-customized to be in dbConnection.
        key is a unique row identifier.

        Basic use is to specify the index column, which is a unique index, as key.
        One can also or instead specify other non-unique columns.

        Special options for gadm and geom create GADM level and spatial indices for our postGIS/OSM project.
        You can create multicolumn indices, e.g. id0,id1,id2, but it's subtle as to when one method will be fastest.
        """
        if schema is not None:
            schema+='.'
        else:
            schema=''

        if len(table)>54: # limit is 63, but suffixes like _spat_idx mean that we are limited to 54
            raise Exception('Table name %s is too long. Postgres limits index names to 63 characters.' % table)
        if isinstance(key, str) and len(table+key)>58:
            raise Exception('Table name %s and key name %s are too long. Postgres limits index names to 63 characters.' % (table,key))

        strlu=dict(table=table, schema=schema,
                   pkey=key,geom='geom' if geom is True else geom, geog='geog' if geog is True else geog,
                   IFNOTEXISTS=' IF NOT EXISTS '*(not forceUpdate)  )
        if key:
                if forceUpdate:
                    self.drop_indices(table,key=key)
                self.execute('CREATE UNIQUE    INDEX  %(IFNOTEXISTS)s %(table)s_idx_%(pkey)s ON %(schema)s%(table)s (%(pkey)s);'%strlu)
                if 0:
                    self.execute('ALTER TABLE %(schema)s%(table)s ALTER COLUMN %(pkey)s SET NOT NULL;'%strlu)
        if non_unique_columns:
                if forceUpdate:
                    self.drop_indices(table,non_unique_columns=non_unique_columns)
                for col in non_unique_columns:
                        self.execute('CREATE  INDEX %(IFNOTEXISTS)s '%strlu+table+'_idx_'+col+' ON '+schema+table+' ('+col+') ;')
        if gadm:
                if forceUpdate:
                    self.drop_indices(table,gadm=True)
                self.execute('CREATE INDEX  %(IFNOTEXISTS)s %(table)s_idx_iso12 ON %(schema)s%(table)s (ISO,id_1,id_2);'%strlu)
                #self.execute('CREATE INDEX  %(IFNOTEXISTS)s %(table)s_idx_iso ON %(schema)s%(table)s (ISO);'%strlu)
                #self.execute('CREATE INDEX  %(IFNOTEXISTS)s %(table)s_idx_id1 ON %(schema)s%(table)s (id_1);'%strlu)
                #self.execute('CREATE INDEX  %(IFNOTEXISTS)s %(table)s_idx_id2 ON %(schema)s%(table)s (id_2);'%strlu)
                self.execute('CREATE INDEX  %(IFNOTEXISTS)s %(table)s_idx_id3 ON %(schema)s%(table)s (id_3);'%strlu)
                self.execute('CREATE INDEX  %(IFNOTEXISTS)s %(table)s_idx_id4 ON %(schema)s%(table)s (id_4);'%strlu)
                self.execute('CREATE INDEX  %(IFNOTEXISTS)s %(table)s_idx_id5 ON %(schema)s%(table)s (id_5);'%strlu)
        if geom:
                if forceUpdate:
                    self.drop_indices(table,geom=True)
                self.execute('CREATE INDEX  %(IFNOTEXISTS)s %(table)s_spat_idx  ON %(schema)s%(table)s  USING gist  (%(geom)s);'%strlu)
                print('CREATE INDEX  %(IFNOTEXISTS)s %(table)s_spat_idx  ON %(schema)s%(table)s  USING gist  (%(geom)s);'%strlu)
        if geog:
                if forceUpdate:
                    self.drop_indices(table,geog=True)
                self.execute('CREATE INDEX  %(IFNOTEXISTS)s %(table)s_geog_idx  ON %(schema)s%(table)s  USING gist  (%(geog)s);'%strlu)
                print('CREATE INDEX  %(IFNOTEXISTS)s %(table)s_geog_idx  ON %(schema)s%(table)s  USING gist  (%(geog)s);'%strlu)
        if not skip_vacuum:
            self.execute('VACUUM  ANALYZE %(schema)s%(table)s'%strlu)

    def drop_indices(self,table,key=None,non_unique_columns=None,gadm=False, geog=False, geom=False):
        """ Drop tables probably created by the create_indices method.
        Dropping indices can be important before doing a big update to a table.

        See create_indices()
        """
        self.report('Dropping indices if exist...')
        strlu=dict(table=table,pkey=key)
        if key:
                self.execute('DROP INDEX IF EXISTS  %(table)s_idx_%(pkey)s ;'%strlu)
        if non_unique_columns:
                for col in non_unique_columns:
                        self.execute('DROP INDEX IF EXISTS  '+table+'_idx_'+col+' ;')
        if gadm:
                self.execute('DROP INDEX IF EXISTS  %(table)s_idx_iso ;'%strlu)
                self.execute('DROP INDEX IF EXISTS  %(table)s_idx_iso12 ;'%strlu)
                self.execute('DROP INDEX IF EXISTS  %(table)s_idx_123 ;'%strlu)
                self.execute('DROP INDEX IF EXISTS  %(table)s_idx_id1 ;'%strlu)
                self.execute('DROP INDEX IF EXISTS  %(table)s_idx_id2 ;'%strlu)
                self.execute('DROP INDEX IF EXISTS  %(table)s_idx_id3 ;'%strlu)
                self.execute('DROP INDEX IF EXISTS  %(table)s_idx_id4 ;'%strlu)
                self.execute('DROP INDEX IF EXISTS  %(table)s_idx_id5 ;'%strlu)
        if geom:
                self.execute('DROP INDEX IF EXISTS  %(table)s_spat_idx ;'%strlu)
        if geog:
                self.execute('DROP INDEX IF EXISTS  %(table)s_geog_idx ;'%strlu)

    def create_geom_col_from_geog(self, table, geomType='polygon', srs=4326, schema=None, spatindex=False, forceUpdate=False, newcol='geom'):
        """Given a geogragphy column, create a geometry equivalent"""

        if 'geom' in self.list_columns_in_table(table,schema=schema):
            if forceUpdate:
                fullTableName=schema+'.'+table if schema else table
                self.execute('ALTER TABLE %s DROP COLUMN geom' % fullTableName)
            else:
                return
        if schema:
            self.execute('''SELECT AddGeometryColumn ('%s','%s','%s',%s,'%s',2);''' % (schema, table, newcol, srs, geomType))
        else:
            self.execute('''SELECT AddGeometryColumn ('%s','%s',%s,'%s',2);''' % (table, newcol, srs, geomType))
        self.execute('UPDATE %s SET %s = ST_Transform(geog::geometry, %s)' % (table, newcol, srs))
        if spatindex: self.create_indices(table, geom= newcol )


def country2WBregionLookup():
    """
    Returns lookup of World Bank region to country (by ISOalpha3)
    Indexed by WB region code ('GroupCode')

    201711: I don't like the WB regions. Augment them with Gallup classification (or find others?)
        # Other resources:
        # https://raw.githubusercontent.com/datasets/country-codes/master/data/country-codes.csv
        # https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes/blob/master/all/all.csv ie https://raw.githubusercontent.com/lukes/ISO-3166-Countries-with-Regional-Codes/master/all/all.csv, now in CountryData as raw.githubusercontent.com_lukes_ISO-3166-Countries-with-Regional-Codes_master_all_all.csv
    """
    import pandas as pd
    WB2ISO = loadOtherCountryData()[['WBcode','WBcode2015','ISOalpha3']]
    WBregions = pd.read_csv(paths['otherinput']+'CountryData/WBregions.csv')
    WBregions = WBregions.merge(WB2ISO, left_on='CountryCode', right_on='WBcode2015').set_index('GroupCode')
    WBregions.drop('CountryCode', axis=1, inplace=True) # duplicate to WBcode
    topWBregions = ['World','Africa',
                          'European Union',
       'East Asia & Pacific (all income levels)',
                          'South Asia', 'North America' ,
       'Latin America & Caribbean (all income levels)',
         'Central Europe and the Baltics',
                          'High income',                          'Middle income',       'Low income',]
    # Put the above list first
    notIntop = list(set(WBregions.GroupName.unique())-set(topWBregions))
    lwb = len(WBregions)
    WBregions = WBregions.reset_index().set_index('GroupName').loc[topWBregions+notIntop].reset_index().set_index('GroupCode')
    assert len(WBregions) == lwb

    cl = country_tools().get_country_list()
    cl_missing = WBregions.loc[~(WBregions.ISOalpha3.isin(cl.ISOalpha3.values))] # Only Kosovo missing! :)
    cl = cl[cl.region5!='']
    assert not any([rr in cl.region22.values for rr in cl.region5.unique()])
    # Use existing names?? (or vice versa?)
    cl = cl.set_index('ISOalpha3').join(WBregions.reset_index().set_index('ISOalpha3')[['CountryName']]).reset_index()
    # Add in two more sets of regions. The GroupCode will be the same as its name
    cl5 = cl.rename(columns = {'region5':'GroupName'}).set_index('GroupName', drop=False)[['ISOalpha3', 'GroupName', 'CountryName']].drop_duplicates()
    cl22 = cl.rename(columns = {'region22':'GroupName'}).set_index('GroupName', drop=False)[['ISOalpha3', 'GroupName', 'CountryName']].drop_duplicates()

    # Use GADM lookup!? Nice definitions of continents, but not unique assignments to continents. Or did I get someone to code these by hand? No, that was maybe cities.

    # Use gallup? It's not very complete, but also nice definitions
    if 0:
        gallupRegions = pd.read_table(paths['otherinput']+'CountryData/gallup-region-country.tsv').set_index('country')
        wbr=WBregions[['ISOalpha3','CountryName']].set_index('CountryName').drop_duplicates()
        g2iso = gallupRegions.join(wbr)
        problemCountries = g2iso.loc[~(g2iso.index.isin(wbr.index))]
        problemCountries2 = wbr.loc[~(wbr.index.isin(g2iso.index))]
        # too hard for th emoment. Try something else:


    # So just Make some of our own regions, using the WB ones?
    afr = WBregions.query('GroupName=="Africa"').reset_index()#.ISOalpha3.values
    ssa = WBregions.loc['SSA'].reset_index()#.ISOalpha3.values
    mena = WBregions.query('GroupName=="Middle East & North Africa (all income levels)"').reset_index()#.ISOalpha3.values

    northAfr = afr[~(afr.ISOalpha3.isin(ssa.ISOalpha3.values))]
    # this produces setting with copy warning: northAfr['GroupName'] = 'North Africa'
    northAfr = northAfr.assign(GroupName = 'North Africa')
    northAfr = northAfr.assign(GroupCode = 'NorthAfrica') #    northAfr['GroupCode'] = 'NorthAfrica'
    mideast =  mena[~(mena.ISOalpha3.isin(afr.ISOalpha3.values))]
    mideast = mideast.assign(GroupName = 'Middle East') #mideast['GroupName'] = 'Middle East'
    mideast = mideast.assign(GroupCode = 'MiddleEast') #mideast['GroupCode'] = 'MiddleEast'

    # WBregions = pd.concat([WBregions, cl5, cl22])  # not yet used
    return WBregions

def loadOtherCountryData(forceUpdate=False, imputation=True):
    """
    Loads dataframe of other variables at the country level
    Returns a dataframe (not indexed, but columns  have ISO and WB codes)
    Complicating factor is that World Bank alpha codes are almost but not quite the same as ISO alpha codes...be careful!
    Romania is the easy way to see this (ROU in ISO, ROM in WB)
    NOTE: in 2016, the WB changed its country codes to align more with the ISO codes
    """
    import pandas as pd

    dataFn = paths['scratch'] + 'otherCountryData.pandas'
    if os.path.exists(dataFn) and not forceUpdate:
        df = pd.read_pickle(dataFn)
        return df

    print("(Re)creating other country data file")
    DP = paths['otherinput']+'CountryData/'  # data path for raw data files

    df = pd.read_table(DP+'countrycodes.tsv', dtype={'ISO3digit':'str'})
    df.ISO3digit = df.ISO3digit.str.zfill(3)
    df.set_index('ISO3digit', inplace=True)
    ncountries = len(df)

    # Some are indexed by ISO3digit
    for fn in ['fuelprices2014.tsv', ]:
        newDf = pd.read_table(DP + fn, dtype={'ISO3digit':'str'}).set_index('ISO3digit')
        if 'ISOalpha3' in newDf.columns: newDf.rename(columns={'ISOalpha3':'ISOalpha3new'}, inplace=True)
        df = df.combine_first(newDf) # will update country and add new columns
        if 'ISOalpha3new' in newDf.columns: # verify that the ISO codes match
            assert all((df.ISOalpha3==df.ISOalpha3new) | pd.isnull(df.ISOalpha3) | pd.isnull(df.ISOalpha3new))
            df.drop('ISOalpha3new', axis=1, inplace=True)
            assert ncountries == len(df)  # make sure no new countries have been added!

    # From here on, data is indexed by ISOalpha3
    df.reset_index(inplace=True)
    df.set_index('ISOalpha3', inplace=True)

    for fn in ['CO2_2014.tsv','oilreserves.tsv','internetUsers.tsv']:
        newDf = pd.read_table(DP + fn).set_index('ISOalpha3')
        if 'pop2014_millions' in newDf.columns: newDf.rename(columns={'pop2014_millions':'pop2014_millions_IEA'}, inplace=True)
        df = df.combine_first(newDf)
        assert ncountries == len(df)  # make sure no new countries have been added!

    # rugged is in csv format
    newDf = pd.read_csv(DP + 'rugged_data.csv').set_index('isocode')
    newDf = newDf[newDf.country!='Serbia and Montenegro']   # this is not a current country!
    newDf = newDf[['rugged', 'rugged_popw', 'rugged_slope', 'rugged_lsd', 'rugged_pc']]
    df = df.combine_first(newDf)
    assert ncountries == len(df)

    # Road length data
    #newDf = pd.read_csv(DP + 'CIA_roadlength.tsv', sep='\t').set_index('ISOalpha3') # old version, from CIA World Factbook
    newDf = pd.read_csv(DP + 'IRF_2014_World_Road_Statistics.tsv', sep='\t').set_index('ISOalpha3') # entered from IRF World Road Statistics
    df = df.combine_first(newDf[['roads_datayear','roads_km','roads_pc_paved','roads_excludes_urban_or_local']])

    # From here on, data is indexed by WBcode
    assert df.index.name=='ISOalpha3' or df.index.name is None
    df.index.name='ISOalpha3' # this got lost
    df.reset_index(inplace=True)
    df.set_index('WBcode2015', inplace=True)

    for fn in ['governance.tsv']:
        newDf = pd.read_table(DP + fn).set_index('WBcode')
        newDf.index.name='WBcode2015'
        df = df.combine_first(newDf)
        assert ncountries == len(df)  # make sure no new countries have been added!

    # WDI data is special - in CSV, and also we want a subset of the data
    WDIdf = pd.read_csv(DP + 'WDIData.csv')

    # drop non-countries
    countriesToDrop = ['ARB', 'CSS', 'CEB', 'CHI', 'EAR','EAS', 'EAP', 'EMU', 'ECS', 'ECA', 'EUU', 'FCS', 'HPC', 'HIC', 'IBD','IBT','IDA','IDB','IDX','LTE','NOC', 'OEC', 'LCN', 'LAC',
                       'LDC', 'LMY', 'LIC', 'LMC', 'MEA', 'MNA', 'MIC', 'NAC', 'INX', 'OED', 'OSS', 'PRE','PSS','PST', 'SST', 'SAS', 'SSF', 'SSA', 'TEA','TLA','TEC','TMN','TSA','TSS','UMC', 'WLD']
    WDIdf['keep'] = WDIdf['Country Code'].apply(lambda x: False if x in countriesToDrop else True)
    WDIdf = WDIdf[WDIdf.keep]

    WDIyear = '2012'
    df.reset_index(inplace=True)
    df.set_index('WBcode', inplace=True)
    WDIindicators = [('Birth rate, crude (per 1,000 people)', 'crudeBirthRate'), ('Death rate, crude (per 1,000 people)', 'crudeDeathRate'),
                     ('Electricity production (kWh)', 'kWhelec'),
                     ('Electricity production from coal sources (kWh)', 'kWhCoal'), ('Electricity production from hydroelectric sources (kWh)', 'kWhHydro'),
                     ('Electricity production from natural gas sources (kWh)', 'kWhNatGas'), ('Electricity production from nuclear sources (kWh)', 'kWhNuclear'),
                     ('Electricity production from oil sources (kWh)', 'kWhOil'), ('Electricity production from renewable sources (kWh)', 'kWhRenewable'),
                     ('Energy imports, net (% of energy use)', 'energyImportsPc'), ('Energy production (kt of oil equivalent)', 'energyProduction_ktoe'),
                     ('Energy use (kg of oil equivalent per capita)', 'energyUse_kgoe_pc'), ('Oil rents (% of GDP)', 'oilRents_pcofGDP'),
                     ('Pump price for diesel fuel (US$ per liter)', 'dieselUSDlitre_WB'), ('Pump price for gasoline (US$ per liter)', 'gasolineUSDlitre_WB'),
                     ('Rail lines (total route-km)', 'railRouteKm'), ('Railways, goods transported (million ton-km)', 'rail_Mtkm'),
                     ('Railways, passengers carried (million passenger-km)', 'rail_MpassKm'), ('Literacy rate, adult total (% of people ages 15 and above)', 'literacyPcAdult'),
                     ('Internet users (per 100 people)', 'internetPer100'), ('Mobile cellular subscriptions (per 100 people)', 'mobilePhonesPer100'),
                     ('Urban population', 'urbanPop'+WDIyear), ('Urban population (% of total)', 'urbanPopPc'+WDIyear), ('Urban population growth (annual %)', 'urbanPopPcGrowth'+WDIyear),
                     ('Population, total', 'pop'+WDIyear), ('Population density (people per sq. km of land area)', 'popDensity_sqkm'+WDIyear),
                     ('Population growth (annual %)', 'popPcGrowth'), ('Population in urban agglomerations of more than 1 million', 'popMegaCity'),
                     ('Land area (sq. km)', 'landSqKm'), ('GINI index (World Bank estimate)', 'gini'),
                     ('Exports of goods and services (% of GDP)', 'exportsPc'), ('GDP (constant 2005 US$)', 'gdp2005USD_WB'),
                     ('GDP per capita (constant 2005 US$)', 'gdpCapita2005USD_WB'), ('GDP growth (annual %)', 'gdpGrowthPc_WB'),
                     ('GDP per capita, PPP (constant 2011 international $)', 'gdpCapitaPPP_WB'), ('Final consumption expenditure (constant 2005 US$)', 'fincons2005USD_WB'),
                     ('CPIA policy and institutions for environmental sustainability rating (1=low to 6=high)', 'sustPolicyRating')]


    for (indicator, colName) in WDIindicators:
        newDf = WDIdf[WDIdf['Indicator Name']==indicator][['Country Code', WDIyear]].set_index('Country Code')
        newDf.rename(columns={WDIyear: colName}, inplace=True)   # rename column to indicator name

        df = df.combine_first(newDf)

        assert ncountries == len(df)  # make sure no new countries have been added!

    assert df.index.name=='WBcode' or df.index.name is None
    df.index.name='WBcode' # this got lost
    df.reset_index(inplace=True)

    notfloat=[[vv,df[vv].dtype] for vv in df.columns if str(df[vv].dtype) not in ['float64']]
    assert [vv in ['ISO3digit','WBcode','countryname'] for vv,dt in notfloat]

    """
    Impute GDP for missing countries

    Argentina, Myanmar and Somalia (and maybe some of the small countries like Andorra)
    have other measures of GDP available, just not the PPP version that we use.
    So we cam impute GDPCapitaPPP from the version at market exchange rates.
    """
    if imputation is True or (imputation not in [False,None] and  'gdpCapitaPPP_WB' in imputation):
        df['gdpCapita_GDP2014PPP_bn2005USD']=1e9*df['GDP2014PPP_bn2005USD']/df.pop2012
        # This one's simple: four four countries, we have an R^2=.99 scalar linear proxy
        x,y='gdpCapita_GDP2014PPP_bn2005USD','gdpCapitaPPP_WB'
        newvals,df2=df_impute_values_ols(df,y,x)
        print((' Added '+y+' to following countries:\n'+str(df.loc[newvals.index]['ISOalpha3'])))
        if not 'not verbose':
            dfm=df[df.ISOalpha3.map(lambda ss: ss in ['ARG', 'MMR', 'SOM', 'SYR', 'AND', 'LIE', 'MCO', 'SMR'])]
            gdpvars=[vv for vv in df.columns if vv.lower().startswith('gdp')]
            print(dfm[['ISOalpha3']+gdpvars])
            #df.set_index('gdpCapitaPPP_WB')[['gdpCapita2005USD_WB']].plot(kind='scatter')
            from cpblUtilities.mathgraph import cpblScatter
            cpblScatter(df,x,y,labels='ISOalpha3')
            for iso,arow in dfm.set_index('ISOalpha3').iterrows():
                plt.plot([arow[x],arow[x]],plt.gca().get_ylim())
                if pd.notnull(arow[x]):
                    plt.gca().text(arow[x],plt.gca().get_ylim()[0],iso)
        df=df2
    if imputation  is True or (imputation not in [False,None] and 'internetUsersper100_2013' in imputation):
        y='internetUsersper100_2013'
        newvals,df=df_impute_values_ols(df,y,' np.log(gdpCapitaPPP_WB) + GovernanceVoice2013 + np.log(pop2012) ')
        print((' Added '+y+' to following countries:\n'+str(df.loc[newvals.index]['ISOalpha3'])))

    df.to_pickle(dataFn)
    return df

def getUrbanEdgeThresholds(forceUpdate=False):
    """
    Returns a database with the GHSL (global) and density (national) thresholds for "urban".
    Note: forceUpdate will only work if the edge tables are all populated with GHSL and density information.
    In other words, don't run this function as part of the osm_master sequence
    Cluster radius is hard coded to 10
    See notes in Urban_edges_threshold_notes.txt
    """
    outFn=paths['baseworking']+'urban_cutoffs.pandas'
    db = pgisConnection(schema=defaults['osm']['processingSchema'], verbose=True)
    if os.path.exists(outFn) and not forceUpdate:
        """Just make sure that the table is loaded in postgres
            This allows us to avoid a dependence on the cities table when creating a new database
            as long as the pandas file exists"""
        df = pd.read_pickle(outFn)
        db.df2db(df, 'urban_thresholds', index=True)
        db.create_primary_key_constraint('urban_thresholds','iso')
        return df

    pctiles = [0.02,0.05,0.10]
    # Density deciles of edges, by country
    cmd = ''' SELECT iso, percentile_cont(ARRAY%s) WITHIN GROUP (ORDER BY density) AS density_pctiles
              FROM (
                 SELECT iso, density
                 FROM cities.cityedges AS c, edges_planet_10 AS e, %s_iso AS g
                 WHERE ST_Intersects(e.geom, c.geom) AND iyear=3
                   AND ST_Intersects(ST_Transform(c.geom, 4326), ST_Transform(g.geom, 4326))) t1
              GROUP BY iso;''' % (str(pctiles), defaults['gadm']['TABLE'])
    result=db.execfetch(cmd)
    df = pd.DataFrame([[rr[0]]+rr[1] for rr in result], columns=['ISOalpha3','pctile2','pctile5','pctile10']).set_index('ISOalpha3')

    cmd = ''' SELECT percentile_cont(ARRAY%s) WITHIN GROUP (ORDER BY ghsl_frc_developed) AS ghsl_devel_pctiles
              FROM cities.cityedges AS c, edges_planet_10 AS e
                 WHERE ST_Intersects(e.geom, c.geom) AND iyear=3;''' % (str(pctiles))
    result=db.execfetch(cmd)
    df.loc['ghsl'] = result[0][0]

    # now impute thresholds for the missing countries based on 3 nearest neighbors
    # distance measures are problematic as we have both spatial and non-spatial distances.
    # Here, we use Mahalanobis distance
    # But note that this means that we measure distance from centroid of country, not adjacency of polygons
    xys = db.execfetch('SELECT iso, ST_X(st_centroid(geom)), ST_Y(st_centroid(geom)) FROM %s_iso;' % defaults['gadm']['TABLE'])
    xys = pd.DataFrame([list(rr) for rr in xys], columns=['ISOalpha3','lon','lat']).set_index('ISOalpha3')
    countryDf = loadOtherCountryData().set_index('ISOalpha3').join(xys).join(df)


    countryarray1 = np.array(countryDf[['lat','lon','gdpCapitaPPP_WB','urbanPopPc2012']].dropna())  # full dataset
    countryarray2 = np.array(countryDf.loc[df.index,['lat','lon','gdpCapitaPPP_WB','urbanPopPc2012']].dropna()) # only countries for which we have values
    idx2iso1 = {ii:iso for ii, iso in zip(list(range(countryarray1.shape[0])),countryDf[['lat','lon','gdpCapitaPPP_WB','urbanPopPc2012']].dropna().index)}
    idx2iso2 = {ii:iso for ii, iso in zip(list(range(countryarray2.shape[0])),countryDf.loc[df.index,['lat','lon','gdpCapitaPPP_WB','urbanPopPc2012']].dropna().index)}

    dists = spatial.distance.cdist(countryarray1,countryarray2,metric='mahalanobis')
    neighbors = {idx2iso1[ii]:[idx2iso2[jj] for jj in dists[ii].argsort()[:4] if idx2iso1[ii]!=idx2iso2[jj]][:3] for ii in range(countryarray1.shape[0])}

    df['imputed'] = False
    isosWithData = list(df.index)
    for iso in xys.index.values:
        if iso not in isosWithData:
            if iso in neighbors:
                df.loc[iso] = df.loc[neighbors[iso]].mean()
            else: # no GDP and frc urban data, just use global average
                df.loc[iso] = df.loc[isosWithData].mean()
            df.loc[iso,'imputed']=True


    df.imputed=df.imputed.astype(bool)
    df.index.name='iso'
    df.to_pickle(outFn)
    db.df2db(df, 'urban_thresholds', index=True)
    db.create_primary_key_constraint('urban_thresholds','iso')

    # How many do we drop?
    isoPop = db.execfetch('SELECT DISTINCT iso, ls_pop FROM %s_iso;' % defaults['gadm']['TABLE'])
    imputedCountries = df[df.imputed].index.values
    print(('%d of %d (%.3f) GADM-defined countries use imputed data' % (len(imputedCountries),len(df), len(imputedCountries)/float(len(df)))))
    totPop = sum([cc[1] for cc in isoPop if cc[1] is not None])
    imputedPops = {cc[0]:cc[1] for cc in isoPop if cc[1] is not None and cc[0] in imputedCountries}
    print(('%d of %d population (%.3f) in countries with imputed data' % (sum(imputedPops.values()), totPop, sum(imputedPops.values())/float(totPop))))
    maxImputed = max(iter(imputedPops.items()), key=operator.itemgetter(1))[0]
    print(('Largest imputed country: %s (%d population)' % (maxImputed, imputedPops[maxImputed])))

    return df


def densityLookup(iso, id_1=None, id_2=None, returnAllValues=False, percentiles=None, density=None, excludeZeros=True, cur=None):
    """
    Looks up the density distribution of a GADM polygon
      - cur is the connection to the postgres database. If None, it creates its own connection using con (useful for parallelization)
      - iso (specifies country) is required. id_1 and id_2 are optional
      - excludeZeros, if True, excludes grid cells with zero population
    There are three variants:
      - passing percentiles (list) returns the density for each percentile of grid cells, as a dictionary
      - passing density returns the percentile for that density
      - returnAllValues returns the full array of densities, and an array of frequencies (i.e., in how many cells that density occurs)
    """
    """
    This can be slow, but most of the time is dissolving the GADM geometries, not creating random points and looking up the densities
    # In principle, we could do this for the whole world, rather than country by country, but I couldn't figure out how to do this computationally
    """
    import numpy as np

    assert returnAllValues or (percentiles is None and density is not None) or (density is None and percentiles is not None)
    if cur is None: cur = psqlt.dbConnection()
    whereClause = "WHERE iso='%s'" % iso
    if id_1 is not None: whereClause+=" AND id_1='%s'" % id_1
    if id_2 is not None: whereClause+=" AND id_2='%s'" % id_2

    # One option is to rasterize the countries, but this turned out to be slower
    # see http://geospatialelucubrations.blogspot.ca/2014/05/a-guide-to-rasterization-of-vector.html for the basics of this approach
    if 0:
        """
        this is a random points approach - might be useful for larger geometries, but for now let's go with the exact approach
        # define the postgres functions is defined
        # adapted from http://trac.osgeo.org/postgis/wiki/UserWikiRandomPoint, modified to return multiple points from multiple polygons
        """
        cmd = """CREATE OR REPLACE FUNCTION RandomPoint (geom Geometry, maxiter INTEGER DEFAULT 1000)
                        RETURNS Geometry
                        AS $$
                DECLARE
                        i INTEGER := 0;
                        x0 DOUBLE PRECISION;
                        dx DOUBLE PRECISION;
                        y0 DOUBLE PRECISION;
                        dy DOUBLE PRECISION;
                        xp DOUBLE PRECISION;
                        yp DOUBLE PRECISION;
                        rpoint Geometry;
                BEGIN
                        -- find envelope
                        x0 = ST_XMin(geom);
                        dx = (ST_XMax(geom) - x0);
                        y0 = ST_YMin(geom);
                        dy = (ST_YMax(geom) - y0);

                        WHILE i < maxiter LOOP
                                i = i + 1;
                                xp = x0 + dx * random();
                                yp = y0 + dy * random();
                                rpoint = ST_SetSRID( ST_MakePoint( xp, yp ), ST_SRID(geom) );
                                EXIT WHEN ST_Within( rpoint, geom );
                        END LOOP;

                        IF i >= maxiter THEN
                                RAISE EXCEPTION 'RandomPoint: number of interations exceeded %', maxiter;
                        END IF;

                        RETURN rpoint;
                END;
                $$ LANGUAGE plpgsql;
                """
        cur.execute(cmd)

        cmd = """CREATE OR REPLACE FUNCTION RandomPointsMulti (geom Geometry, num_points integer)
                        RETURNS SETOF geometry
                        AS $$
                DECLARE
                        n_ret integer := 0;
                        n INTEGER := 0; -- total number of geometries in collection
                        g INTEGER := 0; -- geometry number in collection to find random point in
                        total_area DOUBLE PRECISION := 0; -- total area
                        pt Geometry;
                        areas double precision[];

                BEGIN
                        n = ST_NumGeometries(geom);
                        FOR g IN 1..n LOOP
                            areas[g] = ST_Area(ST_GeometryN(geom, g));
                            total_area := total_area + areas[g];
                        END LOOP;

                        WHILE n_ret < num_points LOOP
                                LOOP
                                    g = ceil(random() * n)::int;
                                    EXIT WHEN random() < areas[g]/total_area; -- weight the probability of selecting a subpolygon by its relative area
                                END LOOP;
                                n_ret := n_ret + 1;
                                pt = RandomPoint( ST_GeometryN(geom, g) );
                                RETURN NEXT pt;
                        END LOOP;
                END;
                $$ LANGUAGE plpgsql;"""
        cur.execute(cmd)

        # Use the RandomPointsMulti() function to get random points with a gadm id, and lookup the density
        # density is band 3 of the raster
        # We need to dissolve the gadm geometries using ST_Union
        # to avoid double weighting areas with overlapping lower-level geometries
        # also project to 954009 (Mollweide, equal area) to avoid oversampling high latitudes

        cmd="""WITH pts AS (SELECT ST_Transform(RandomPointsMulti(ST_Union(ST_Transform(the_geom, 954009)), %s), 4326) AS pt_geom
                              FROM (SELECT the_geom from gadm %s) t1)
               SELECT ST_Value(rast, 3, pt_geom)
               FROM landscan, pts WHERE ST_Intersects(rast, pt_geom)""" % (npts, whereClause)
        cur.execute(cmd)
        densities = np.array([dd[0] for dd in cur.fetchall() if dd[0] is not None])

    # Exact approach
    # the idea first is that we:
    # (i) aggregate the country geometries, and dump the multipolygons (e.g. different islands) as polygons. This is table c
    # (ii) identify the relevant raster tiles, and whether a tile is fully or only partially contained within the country boundaries. This is table r.
    #           Note that r only has rid, not the raster tile -- otherwise, postgres tries to save all the raster to disk and we run out of disk space!
    #               This is also why we do the preliminary table e first, before aggregating
    # (iii) for tiles that are fully contained, get the value counts of the density
    # (iv) for tiles that are partially contained, clip them to the each geometry and union them, and then get the value counts. (This is expensive, so we only want to do it for partially contained tiles)
    # There are two variants.
    #   - Where we want to find the percentile of a density, we just get the count of pixels that are above and below that density.
    #   - Where we want to find the percentiles, we get counts of all the density values and use a cumulative sum to calculate the value at each percentile
    if density is not None:
        exclZeroText = '(' if excludeZeros else '['  # open or closed interval from zero?
        cmd = """WITH c AS (SELECT (ST_Dump(ST_Union(the_geom)).* FROM (SELECT the_geom FROM %s %s) t1),
                      e AS (SELECT rid, ST_Contains(geom, ST_Envelope(rast)) AS contains_full from c, landscan WHERE ST_Intersects(rast, geom)),
                      r AS (SELECT rid, bool_or(contains_full) AS contains from e GROUP BY rid)
                 SELECT value, SUM(count) FROM
                    (SELECT r.rid, (ST_ValueCount(ST_Reclass(rast, 3,  '%s0-%s]:0, (%s-99999999999:1', '8BSI', -2147483648), 3)).* FROM r, landscan WHERE contains='t' AND r.rid=landscan.rid
                   UNION ALL
                     SELECT rid, (ST_ValueCount(ST_Reclass(unionrast, 1, '%s0-%s]:0, (%s-99999999999:1', '8BSI', -2147483648))).* FROM
                        (SELECT r.rid, ST_Union(ST_Clip(rast, 3, geom, True)) AS unionrast  FROM c, r, landscan
                                WHERE contains='f' AND ST_Intersects(geom, rast) AND r.rid=landscan.rid GROUP BY r.rid) t2) t3
                  GROUP BY value""" % (defaults['gadm']['TABLE'], whereClause, exclZeroText, density, density, exclZeroText, density, density)
        cur.execute(cmd)
        results = dict(cur.fetchall())
        return float(results[0]) / (results[0]+results[1])
    else:  # get percentiles
        exclZeroText = 'WHERE value>0 ' if excludeZeros else ''
        cmd = """WITH c AS (SELECT (ST_Dump(ST_Union(the_geom))).* FROM (SELECT the_geom FROM %s %s) t1),
                      e AS (SELECT rid, ST_Contains(geom, ST_Envelope(rast)) AS contains_full from c, landscan WHERE ST_Intersects(rast, geom)),
                      r AS (SELECT rid, bool_or(contains_full) AS contains from e GROUP BY rid)
                 SELECT value, SUM(count) FROM
                    (SELECT r.rid, (ST_ValueCount(rast, 3)).* FROM r, landscan WHERE contains='t' AND r.rid=landscan.rid
                   UNION ALL
                     SELECT rid, (ST_ValueCount(unionrast, 1)).* FROM
                        (SELECT r.rid, ST_Union(ST_Clip(rast, 3, geom, True)) AS unionrast  FROM c, r, landscan
                                WHERE contains='f' AND ST_Intersects(geom, rast) AND r.rid=landscan.rid GROUP BY r.rid) t2) t3
                  %s GROUP BY value ORDER BY value""" % (defaults['gadm']['TABLE'], whereClause, exclZeroText)
        cur.execute(cmd)
        densities, counts = list(zip(*cur.fetchall()))
        if returnAllValues: return densities, counts
        pctileValues = np.array(counts).cumsum()/float(sum(counts))*100

        return dict([(pp, densities[np.nonzero(pctileValues>=pp)[0][0]]) for pp in percentiles])

def disk_space_fraction_used():#whichDisk=None):
    """
    # Find local disk space. This is really just tuned for Sprawl server.
    """

    import os
    import re

    ss=os.popen("df 2>&1 ").read()#| grep -v Permission|grep -v okai|grep -v OKAI|grep /home |tail -n 1").read()
    rr= re.findall(' (\d*)\%',ss)
    localFractionUsed=max([int(fr) for fr in rr])*1.0/100
    return {None:localFractionUsed, 'local':localFractionUsed, 'max':localFractionUsed}

def checkonce_server_load_to_email():
    """
    This should be launched in a nohup once per boot cycle, so we get warning earlier than NCS.  See the executable launch_RAM_monitor_daemon, whic is a wrapper for this.
    """
    if RAM_fraction_used()>.75:
        os.system('echo "Alert: Sprawl RAM load too high:%f"| mail YOUR_EMAIL_ADDRESS@gmail.com '%RAM_fraction_used())
    dsf=disk_space_fraction_used()[None]
    if  dsf>.95:
        os.system('echo "Alert: Sprawl DISK usage too high:%f"| mail YOUR_EMAIL_ADDRESS@gmail.com '%dsf)

def monitor_server_load_to_email():
    """
    This should be launched in a nohup once per boot cycle, so we get warning earlier than NCS.  See the executable launch_RAM_monitor_daemon, whic is a wrapper for this.
    """
    from time import sleep
    while (1):
        checkonce_server_load_to_email()
        sleep(120)

def RAM_fraction_used():
    fractionRAMused = psutil.virtual_memory().percent/100.  # This answer is confusing. Is it because most of what htop is showing is actually cached/buffered, and not counted here?
    try:
        ss=os.popen("free -m|head -n 3|tail -n 1").read()
        used,free=[nn for nn in ss.strip().split(' ') if nn][2:]
        fractionRAMused1=float(used)/(float(used)+float(free))
    except KeyError: # maybe for OSX
        raise Exception(" Is this on OSX? Can we find the right code for RAM use?")
    return(fractionRAMused)

def subsISO3forISO2inWorldMap():
    """
    Blank world maps currently have ISO2 codes labelling countries. Make ISO3 versions.
    """
    assert os.path.exists(IP+'svgmaps/countrycode_main.tsv')
    import xml.etree.ElementTree as ET
    masterCountryList=utils.tsvToDict(IP+'svgmaps/countrycode_main.tsv',dataRow=4,keyRow=3)
    import pandas as pd
    dfCountryDict= pd.DataFrame(masterCountryList)[['twoletter_AlexShultz_svg', 'countryCode_ISO3']].dropna()
    dfCountryDict = dfCountryDict[(dfCountryDict.countryCode_ISO3.str.len()==3) & (dfCountryDict.twoletter_AlexShultz_svg.str.len()==2)]
    dfCountryDict = dfCountryDict.set_index('twoletter_AlexShultz_svg').countryCode_ISO3.to_dict()
    # manually add countries that are missing
    dfCountryDict.update({'ai':'AIA', 'as': 'ASM', 'aw':'ABW', 'bl': 'BLM', 'bm':'BMU', 'cd':'COD', 'cw':'CUW', 'eh':'ESH',
        'fk':'FLK', 'fm':'FSM', 'fo':'FRO', 'gg':'GGY', 'gi':'GIB', 'gl':'GRL', 'gp':'GLP', 'gs':'SGS', 'gu':'GUM', 'im':'IMN', 'je':'JEY', 'kp':'PRK',
        'ky':'CYM', 'li':'LIE', 'md':'MDA', 'me':'MNE', 'mf':'MAF', 'mp':'MNP', 'mq':'MTQ', 'ms':'MSR', 'nc':'NCL', 'nf':'NFK', 'pf':'PYF',
        'pm':'SPM', 'pn':'PCN', 'ps':'PSE', 're':'reu', 'sh':'SHN', 'ss':'SSD', 'sx':'SXM', 'tc':'TCA', 'tk':'TKL', 'tl':'TLS', 'tz':'TZA',
        'va':'VAT', 'vg':'VGB', 'vi':'VIR', 'wf':'WLF', 'yt':'MYT'})


    for ff in ['BlankMap-World6-noAntarctica.svg','BlankMap-World6.svg']:
        svg=open(IP+'svgmaps/'+ff,'rt').read()
        svg = svg.replace('/*COLOURINGCOUNTRIES*/', '/*COLOURINGREGIONS*/')
        tree = ET.fromstring(svg)
        for r in tree.getiterator():
            if 'id' in list(r.keys()):
                id = r.get('id')
                id = dfCountryDict[id] if id in dfCountryDict else id
                r.set('id', id)
            if 'class' in list(r.keys()):
                classList = r.get('class').split(' ')
                classList = [dfCountryDict[cc]  if cc in dfCountryDict else cc for cc in classList]
                r.set('class', ' '.join(classList))

        ET.ElementTree(tree).write(IP+'svgmaps/'+ff.replace('.svg','_ISO3.svg'), encoding='UTF-8')

    return


class osm_logger():
    """
    Keep a log of progress/status messages.  Could be rewritten to use python's logging module?
    Optionally displays time between messages.
    Optionally prints to stdout as well.

    Passing topic=None skips any file writing, but still (if verbose = True) writes to stdout.
    """
    def __init__(self,topic, verbose=True, timing=False, prefix=None, overwrite=False,  showRAM=False):
        """
        If topic  is None, then no file is written.

        overwrite = True will delete/recreate the log file instead of appending to it
        """
        self.prefix='' if prefix is None else prefix+': '
        assert timing in [True,False]
        self.timing=timing
        logdir=paths['working']+'logs/'
        if not os.path.exists(logdir):
            os.mkdir(logdir)
        if topic is None:
            self.fout=open(os.devnull,"w")
            topic='null logger: no file recording'
        else:
            fname=logdir+topic+'.log'
            self.fout=open(fname, ['a','w'][int(overwrite)])
        self.verbose=verbose
        self.showRAM=showRAM
        self.startime=time.time()
        self.latesttime=self.startime
        self.write('---------- NEW LOGGER: '+self.prefix+topic+':   '+time.strftime("%Y:%m:%d:%H:%M:%S")+' ------------[u={}]\n'.format(defaults['server']['postgres_role']))
    def prependTime(self,sss):
        if not self.timing: return sss
        nowt=time.time()
        dtime=nowt-self.latesttime
        etime=nowt-self.startime
        self.latesttime=nowt
        RAMstring = '' if not self.showRAM else 'RAM:%5.02f%% |'%(100*RAM_fraction_used())
        return  '['+time.strftime("%Y:%m:%d:%H:%M:%S", time.localtime(nowt))+'][At '+('%5d s'%etime if etime<61 else '%5.1f m'%(etime*1.0/60) )+'|'+RAMstring+ 'dt='+('%7.2g s'%dtime if dtime<61 else ' %7.2f m'%(dtime*1.0/60) )   + '] '+sss

    def write(self,sss):
        outs=self.prependTime(self.prefix+sss)
        if self.verbose:
            print(outs)
        self.fout.write(outs+'\n')
    def close(self):
        self.fout.close()


class country_tools():
    def __init__(self, forceUpdate=False):
        """
        allc = pd.read_csv(paths['otherinput']+'CountryData/raw.githubusercontent.com_lukes_ISO-3166-Countries-with-Regional-Codes_master_all_all.csv')

         # https://raw.githubusercontent.com/datasets/country-codes/master/data/country-codes.csv
         # https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes/blob/master/all/all.csv ie https://raw.githubusercontent.com/lukes/ISO-3166-Countries-with-Regional-Codes/master/all/all.csv, now in CountryData as raw.githubusercontent.com_lukes_ISO-3166-Countries-with-Regional-Codes_master_all_all.csv

        """
        self.forceUpdate=forceUpdate
        SP= paths['scratch']
        self.useful_sites={'lukes':{'tmpfile':SP+'countryTools_lukes_tmp.pandas', 'url': 'https://raw.githubusercontent.com/lukes/ISO-3166-Countries-with-Regional-Codes/master/all/all.csv',
                                    'rename':{'region':'region5', 'sub-region':'region22', 'name':'countryName'}},
                           # N.B. The following includes six languages for all countries,  not every country's official language
                     'multilang':{'tmpfile':SP+'countryTools_gitdatasets_tmp.pandas', 'url': 'https://raw.githubusercontent.com/datasets/country-codes/master/data/country-codes.csv',
                                  'rename':{}}}

        return
    @staticmethod
    def country2ISOLookup():
        """
        Returns dict of dicts
        cname2ISO is country name to ISO alpha 3 lookup
        ISO2cname is the reverse
        ISO2shortName is a consistent dict of short names (they might not be officials)
            which also strip out non-ASCII128 characters (sorry, Cote d'Ivoire...)

        This could easily be adapted to return WB codes as well
        """
        import pandas as pd
        lookup = pd.read_table(paths['otherinput']+'CountryData/countrycodes.tsv', dtype={'ISO3digit':'str'})
        cname2ISO = lookup.set_index('countryname').ISOalpha3.to_dict()

        ISO2cname = lookup.set_index('ISOalpha3').countryname.to_dict()
        ISO2cname['ALL'] = 'World'
        ISOalpha2ISOdigit = lookup.set_index('ISOalpha3').ISO3digit.to_dict()
        ISOdigit2ISOalpha = lookup.set_index('ISO3digit').ISOalpha3.to_dict()

        lookup = pd.read_table(paths['otherinput']+'CountryData/shortNames.tsv', dtype={'ISO3digit':'str'})
        ISOalpha2shortName = lookup.set_index('ISOalpha3').shortName.to_dict()
        ISOalpha2shortName['ALL'] = 'World'

        # Add in some alternate names manually (different variants of country name)
        # these are only used in going TO iso from the country name
        cname2ISO.update({'Russia': 'RUS', 'The Bahamas': 'BHS', 'United Republic of Tanzania': 'TZA', 'Ivory Coast': 'CIV',
                          'Republic of Serbia': 'SRB', 'Guinea Bissau': 'GNB', 'Iran': 'IRN', 'Democratic Republic of the Congo': 'COD',
                          'Republic of Congo': 'COG', 'Syria': 'SYR', 'Venezuela': 'VEN', 'Bolivia': 'BOL', 'South Korea': 'KOR', 'Laos': 'LAO',
                          'Brunei': 'BRN', 'East Timor': 'TLS', 'Vietnam': 'VNM',  'North Korea': 'PRK', 'Moldova': 'MDA', 'Vatican City': 'VAT',
                          'Macedonia': 'MKD', 'United Kingdom': 'GBR', 'Tanzania':'TZA', 'Cape Verde':'CPV', 'Reunion':'REU', 'Falkland Islands':'FLK',
                          'Micronesia':'FSM', 'United States':'USA'})

        return {'cname2ISO':cname2ISO, 'ISO2cname': ISO2cname, 'ISOalpha2ISOdigit': ISOalpha2ISOdigit, 'ISOdigit2ISOalpha': ISOdigit2ISOalpha, 'ISOalpha2shortName':ISOalpha2shortName }

    def update_downloads(self):
        import urllib.request, urllib.error, urllib.parse
        for kk,dd in list(self.useful_sites.items()):
            if self.forceUpdate or not os.path.exists(dd['tmpfile']):
                # Keep Pandas from translating Namibia's "NA" to NaN:
                try:
                    tdf = pd.read_csv(dd['url'],na_filter = False).to_pickle(dd['tmpfile']) #, encoding='utf8'
                    print(('   Loaded '+ dd['url']))
                except urllib.error.URLError:
                    fooo
    def load_and_rename_cols(self,key):
        """ Load one of the files in useful_sites, and apply the column renamings
        """
        self.update_downloads()
        df = pd.read_pickle(self.useful_sites[key]['tmpfile'])
        df.rename(columns = self.useful_sites[key]['rename'], inplace=True)
        return df

    def get_country_list(self, columns=None):
        """ columns can be used to imply which info you need. See the useful_sites "rename"s for column names
        """
        df = self.load_and_rename_cols('lukes')
        assert df[pd.isnull(df.region5)].empty
        ###df = df[pd.notnull(df.region)]
        df['iso2'] =df['iso_3166-2'].str[-2:].values
        df['ISOalpha3'] =df['alpha-3']
        if columns is not None:
            assert all([cc in df for cc in columns])
            df=df[columns]

        #df2=self.load_and_rename_cols('multilang')
        return df
country2ISOLookup = country_tools.country2ISOLookup




def define_table_names(region, clusterRadii=None):
    """ Provide names for postgresql tables.
     This should be consistent with the seg and junc names defined in: from process_raw_osm import define_table_names

Shoulds schemas be built in to these names?
    """
    region=region.lower()  # avoids inconsistencies where some table names are defined as lowercase regions, but some aren't
    if clusterRadii is None:
        clusterRadii=defaults['osm']['clusterRadii']
    if isinstance(clusterRadii,str):
        clusterRadii=[clusterRadii]
    return(dict(
        seg_table = defaults['osm']['seg_table'].replace('REGION',region),
            junc_table = defaults['osm']['junc_table'].replace('REGION',region),
            nodebuffer_tables = dict([[cr,defaults['osm']['nodebuffer_tables'].replace('REGION',region).replace('CLUSTERRADIUS',cr)] for cr in clusterRadii]),
            buffer_tables = dict([[cr,defaults['osm']['buffer_tables'].replace('REGION',region).replace('CLUSTERRADIUS',cr)] for cr in clusterRadii]),
        # Cluster tables to be deprecatd. They're node tables
            cluster_tables = dict([[cr,defaults['osm']['cluster_tables'].replace('REGION',region).replace('CLUSTERRADIUS',cr)] for cr in clusterRadii]),
            node_tables = dict([[cr,defaults['osm']['cluster_tables'].replace('REGION',region).replace('CLUSTERRADIUS',cr)] for cr in clusterRadii]),
            edge_tables = dict([[cr,defaults['osm']['edge_tables'].replace('REGION',region).replace('CLUSTERRADIUS',cr)] for cr in clusterRadii]),
    ))

def compileAllPlots(figs,figFn,figSize=None):
    """Take a list of figures and clean up and generate a single PDF"""
    from cpblUtilities.mathgraph import remove_underscores_from_figure
    if figSize is None:
        try:
            figSize = figSizePage # if in globals
        except:
            figSize = (7, 8.75)  # for full-page figures

    outList = []
    for fignum, fig in enumerate(figs):
        fig.set_size_inches(figSize)
        remove_underscores_from_figure()
        fig.tight_layout()
        outFn = figFn+str(fignum)+'.pdf'
        fig.savefig(outFn)
        outList.append(outFn)
    utils.mergePDFs(outList, figFn+'.pdf')



def get_EPSG_SRS_from_esri_prj_file(shapeprj_path, verbose=False):
      """ This finds the right projection from a shp file's .prj file.
      """
      prj_file = open(shapeprj_path, 'r')
      prj_txt = prj_file.read()
      srs = osr.SpatialReference()
      srs.ImportFromESRI([prj_txt])
      if verbose:
          print('Shape prj is: %s' % prj_txt)
          print('WKT is: %s' % srs.ExportToWkt())
          print('Proj4 is: %s' % srs.ExportToProj4())
      srs.AutoIdentifyEPSG()
      if verbose:
          print('EPSG is: %s' % srs.GetAuthorityCode(None))
      return srs.GetAuthorityCode(None)

# The following is used in assignSplitColormapEvenly (See #330) for whatever data range you want for each disconnectivity metric.
# The first value is for the highly-connected exteme (blue), the third is sprawly extreme (red), and you can set the middle (purple) to a midpoint/median, etc.
# (You can send assignSplitColormapEvenly a sample of your data if you want the colours to be spread out optimally)
# red and blue adapted from from colorbrewer2


threeSprawlyColors =  [[0,0,1],[.5,0,.5],[1,0,0]]

def degree_colors(): #Obselete, in light of value above?
    """
# red                                          blue
#['#d73027','#fc8d59','#fee090','#91bfdb','#4575b4']
#  (1,2)     (2.5)       (3)      (3.5)    (4)
"""
    nodes = {1:'#d73027',  3:'#fee090', 4:'#4575b4'}
    edges = {2:'#d73027', 2.5:'#fc8d59', 3:'#fee090', 3.5:'#91bfdb', 4:'#4575b4' }
    nodes_and_edges = {1:'#d73027',  2:'#d73027', 2.5:'#fc8d59', 3:'#fee090', 3.5:'#91bfdb', 4:'#4575b4'}
    return(nodes_and_edges)

from cpblUtilities.color import unordered_discrete_colors as discrete_colors # Deprecated. Use proper name!

# fig sizes with 1 column height. These are estimated (except for 1 col)
"""
Figures in Science are commonly reduced to fit in 1, 1.5, or 2 columns in the print publication (1 column = 13.4 picas, 2.3 inches, or 5.8 cm). In some cases, the suggested size will be marked on the edited copy of the paper. If not, assume that we will try to make dimensions of the printed figure as small as possible. If one figure in particular is key, please indicate that it should be given some preference in sizing.
"""
figSizes = {'1col':(2.3,2.3), '1.5col':(2.3*1.5,2.3), '2col':(4.78,2.3), '2x2col':(4.78,4.78), '2.5col':(5.46,2.3), 'full': (7.25,2.3), 'portrait':(7.25,9.5)}

################################################################################################
if __name__ == '__main__':
################################################################################################
    country2ISOLookup()
    country2WBregionLookup()
    nononono
    print(' Some demos of tools here: ')
    # Discrete colours
    discrete_colors(display=True)
    # Continuous and sequential colours
    import matplotlib.pyplot as plt
    from cpblUtilities.color import assignSplitColormapEvenly, addColorbarNonImage
    mydata2colors = assignSplitColormapEvenly([2,2.5,3,3.5,4] , splitdataat = 3, RGBpoints = threeSprawlyColors)
    plt.figure(1001), plt.clf()
    for ii,ay in enumerate(np.random.normal(3, 1, 100)):  plt.plot(ii,ay ,'o', color = mydata2colors(ay))
    addColorbarNonImage(datarange=[1,4], data2color=mydata2colors)
    plt.show()
