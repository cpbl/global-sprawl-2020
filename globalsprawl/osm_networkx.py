#!/usr/bin/python
import networkx as nx
from networkx.drawing import nx_agraph
import pylab as plt
import time

"""
Provides a version of networkx with a few extra properties. Includes direct interfaces to OSM and to PostGIS. Allows a node attribute "latlon" and geographic plotting.

"""

class Graph(nx.Graph):
    """A networkx graph with some extended attributes and methods.
This was first written by Tassia Araujo as OSM_graph in April 2016. 
I am  rewriting to remove the "data" elements in nodes and replace them with networkx's own node properties.

The new implementation of lat, lon and maybe other characteristics (clusterIDs, etc) should be hidden through function calls. For the moment, I'm keeping them as dicts rather than properties of nodes.

    """
    def __init__(self, data=None, name="", bbox=[],osm_file="", latlon=None):
        assert latlon is None or data is not None # data needed if latlon specified
        super(Graph, self).__init__(data)# latlon=latlon)
        #nx.Graph.__init__(self)
        #G=None
        #if data: # This can be anything that networkx.Graph allows for initialization
        #    G=nx.Graph(data)
        #if G:
        #    self.__dict__.update(G.__dict__)
        if name:
            self.name = name
        if bbox:
            # bbox format: [left, bottom, right, top]
            osm_file = self.download_osm(tuple(bbox))
        if osm_file:
            G = self.extract_osm_roads(osm_file)
            self.__dict__.update(G.__dict__)
        if latlon is not None:
            assert self
            # pos format: {id: (lat,long), ...}
        #    assert G
            # Following fails on Apollo
            #nx.set_node_attributes(self,'data',{id:Node(id, LL[1],LL[0]) for id,LL in latlon.items()})
            # N.B.: set_node_attributes is a property of nx, not nx.Graph
            if nx.__version__[0]=='1':
                nx.set_node_attributes(self,'latlon',latlon)#dict([[id, (LL[0]] for id,LL in latlon.items()]))
            else:  
                nx.set_node_attributes(self,latlon, name='latlon')#dict([[id, (LL[0]] for id,LL in latlon.items()]))

        self.verboseTiming=True
        self.network_distances=None
        self.network_distance_cutoff =None

    def set_name(self,name):
        """ Trivial method to define the graph's name."""
        self.name = name

    def get_name(self):
        """ Trivial method to retrieve the graph's name."""
        return self.name

    def verbose_timing(self,setting):
        """ Set verbosity to giving timing information, or not """
        assert setting in [True,False]
        self.verboseTiming=setting

    #def get_data(self):
    #    return( [b['data'] for a,b, in self.nodes(data=True)] )

    @staticmethod
    def download_osm(left,bottom,right,top):
        """ Return a filehandle to the downloaded data."""
        fp = urlopen("http://api.openstreetmap.org/api/0.6/map?bbox=%f,%f,%f,%f"
                     %(left,bottom,right,top))
        return fp
    
    @staticmethod
    def extract_osm_roads(filename_or_stream):
        """ Based on osm.py from brianw's osmgeocode. Available at
            http://github.com/brianw/osmgeocode."""
        osm = OSM(filename_or_stream)
        G = nx.Graph()
    
        for w in osm.ways.values():
            if 'highway' not in w.tags:
                continue
            if 'highway' in w.tags and (w.tags['highway'] == 'steps' or
                                        w.tags['highway'] == 'footway'):
                continue
            if 'highway' in w.tags and (w.tags['highway'] == 'cycleway' or
                                        w.tags['highway'] == 'bridleway'):
                continue
            if 'highway' in w.tags and w.tags['highway'] == 'path':
                continue
            # @TA: typo in lenght ?
            # @TA: And is this storing more data than we need? It seems when this is used in reading OSM, it storing data redundantly. It looks like rather than have the tags in the nx node, it has one tag which is an object with redundant info. Is this because we won't always want to use networkx?
            G.add_path(w.nds, id=w.id, data=w, weight=w.lenght)
    
        for n_id in G.nodes_iter():
            n = osm.nodes[n_id]
            G.node[n_id] = dict(data=n)
        return G

    def create_psql_table(self,schemaname,tablename):
        nodes_as_ints = list(range(len(self.nodes())))
        nodes_as_ints = dict(list(zip(self.nodes(), nodes_as_ints)))
        edge_id = 0
        cmd = """DROP TABLE IF EXISTS %s;
CREATE TABLE %s.%s(
        edge_id integer NOT NULL,
        osm_id bigint,
        source bigint,
        target bigint,
        osm_source_id bigint,
        osm_target_id bigint,
        clazz integer DEFAULT 30,
        flags integer DEFAULT 3,
        x1 double precision,
        y1 double precision,
        x2 double precision,
        y2 double precision,
        geom geometry(LineString,4326)
);
""" % (tablename,schemaname,tablename)
        for ee in self.edges():
                cmd += "INSERT INTO %s.%s VALUES (%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,%f,%f,ST_GeomFromText(\'LINESTRING(%f %f, %f %f)\',4326));\n" \
                % (schemaname, tablename, edge_id, edge_id, nodes_as_ints[ee[0]], nodes_as_ints[ee[1]], nodes_as_ints[ee[0]], nodes_as_ints[ee[1]],
                   30, 3,
                   self.node[ee[0]]['latlon'][1],  \
                self.node[ee[0]]['latlon'][0], self.node[ee[1]]['latlon'][1], self.node[ee[1]]['latlon'][0], self.node[ee[0]]['latlon'][1],  \
                self.node[ee[0]]['latlon'][0], self.node[ee[1]]['latlon'][1], self.node[ee[1]]['latlon'][0])
                edge_id += 1

        return cmd

    def get_max_connected(self):
        """ Return the maximum connected component of the graph. """
        g = max(nx.connected_component_subgraphs(self),key=len)
        while nx.number_connected_components(g) > 1:
            g = max(nx.connected_component_subgraphs(g),key=len)
        return g

    def draw_min_cut(self, position):
        """ Uses matplotlib to draw graph and paint min cut edges in red. """
        layout = nx.spring_layout(self)
        plt.subplot(position)
        plt.axis('off')
        nx.draw_networkx_nodes(self,layout,node_color='r',
                               node_size=100,alpha=0.8)
        nx.draw_networkx_edges(self,layout,alpha=0.5,width=3)
        nx.draw_networkx_labels(self,layout,font_size=10,
                                font_family='sans-serif')
        if nx.is_connected(self):
            #edgelist=self.minimum_edge_cut()
            nx.draw_networkx_edges(self,layout,
                                   edgelist=self.minimum_edge_cut(),
                                   width=8,alpha=0.5,edge_color='r')

    def draw_graphviz(self):
        """ Draw graph using graphviz. """
        A = nx_agraph.to_agraph(self)
        nx_agraph.pygraphviz_layout(self,prog='neato')
        nx.draw_networkx_edges(self,nx.spring_layout(self),
                               edgelist=self.minimum_edge_cut(),
                               width=8,alpha=0.5,edge_color='r')
        nx_agraph.view_pygraphviz(self) 

    def plot_geographic(self,with_labels=True,**kwargs):
        """ Use lat/lon information to draw the nodes """
        pos=dict([  [id,(nn[1],nn[0])] for id,nn in list(nx.get_node_attributes(self,'latlon').items())])
        if with_labels=='truncate':
                labels=dict(list(zip(list(pos.keys()),[ str(ii)[-4:] for ii in list(pos.keys()) ])))
                with_labels=True
        elif with_labels:
                labels=dict(list(zip(list(pos.keys()),list(pos.keys()))))
        nx.draw(self, pos, with_labels=with_labels, labels=labels, **kwargs)
        # But axis('tight') doesn't always work.  Rescale ourselves using the lat/long range: (following necessary in networkx v1.11
        longs,lats=list(zip(*(list(pos.values()))))
        dlong=max(longs)-min(longs)
        dlat=max(lats)-min(lats)
        plt.xlim([min(longs)-.1*dlong, max(longs)+.1*dlong])
        plt.ylim([min(lats)-.1*dlat, max(lats)+.1*dlat])

        plt.show()

    def plot_edge_classification(self,with_labels=True):
        edgelookup= self.classify_edges_dendricity()
        """NB: In Monaco, len(edgelookup.keys())==391, while 393 edges are unclassified."""
        #for ee in [ ee for ee in self.edge if ee not in edgelookup ]:
        #        edgelookup[ee]=edgelookup.get(ee,'null')
        edges,cats=list(zip(*(list(edgelookup.items()))))
        ax=plt.subplot(111)
        self.plot_geographic(edgelist=edges,edge_color=[{'S':'r','null':'k'}.get(cc,cc.lower()) for cc in cats], ax=ax, width=3, node_color='white',with_labels=with_labels)#"None")
        self.plot_geographic(edgelist=[],nodelist=[edge[0] for edge,cat in list(edgelookup.items()) if cat =='S'],
                            node_color='r',  ax=ax,with_labels=with_labels)
        plt.show()
        print(' (C)yan for cycle bases, (B)lue for bridges, black for unclassified and red for self loop nodes')
        
    def anneal_degree2_nodes(self,inline=True,attribute_merger=None, accumulate_attributes=None,attributes_sum=None):
        """
        Remove degree2 nodes from a graph, since we ignore them for all our connectivity metrics.

        When we find a degree-2 node, we want to do something smart with the attributes of the two edges. Pass a function merge_attributes to generate a dict of attributes given two dicts of attributes.
        
        A bug exists in treating long (ie multi-node-path) self-loops.
        In general, one must think carefully about dealing with triangles.

        I suppose we could classify edges before we simplify. Otherwise, we need to think carefully about how we think about multigraphs.

        Specify merge_attributes(dict1,dict2) function for custom merging of attribute values.
        """
        self.network_distances=None # If this has been calculated, it will become invalid

        if attribute_merger is None and accumulate_attributes is  None and attributes_sum is  None:
            attribute_merger=lambda a,b: a
        if accumulate_attributes is not None or attributes_sum is not None:
            assert attribute_merger is None
            attribute_merger=merge_attributes_generator(accumulate_attributes=accumulate_attributes,attributes_sum=attributes_sum)

        if inline:
            G=self
        else:
            G=self.copy()
        if self.verboseTiming: rstarttime = time.time()
        #deg=nx.degree(G) # Why does this not work? (on apollo?)
        if nx.__version__[0]=='1':
            d2nodes=[nn for nn,dd in G.degree_iter() if dd==2]
        else:
            d2nodes=[nn for nn,dd in list(G.degree) if dd==2]
        print(('       Found %d degree-2 nodes'%len(d2nodes)))
        for node in  d2nodes:
            twoedges=G.edges(node,data=True)
            if len(twoedges)==1: continue # We're not removing self-loops (It would need to be an isolated node anyway.)
            n1,n2=twoedges[0][1],twoedges[1][1]
            a1,a2=twoedges[0][2],twoedges[1][2]
            assert isinstance(a1,dict)
            assert isinstance(a2,dict)
            attr=attribute_merger(a1,a2)
            if n2 in G.edge[n1]:
                # If the new edge already exists, we have a multi-node self-loop. The add_edge will replace the extant one, leaving a dead-end rather than self-loop or multipaths.
                # Keep track of the attributes by merging in what we're about to overwrite:
                attr=attribute_merger(attr, G.edge[n1][n2])
            G.add_edge(n1,n2,attr)
            G.remove_node(node)
        if 0 and self.verboseTiming: anneal=reportTime(rstarttime,'annealed %d d-2 nodes'%len(d2nodes))
        # This line for checking result: [gg[2]['edge_id'] for gg in G.edges(data=True)]
        if not inline:
            return(G)

    def edgesFromCycle(self,nodelist):
        """
        Iterator (created in final (return) line) giving node tuples for all edges in a cycle.
        """
        wrapped=list(nodelist)
        if wrapped[0] != wrapped[-1]:
            wrapped=wrapped+wrapped[:1]
        return (tuple(sorted(wrapped[i:i+2])) for i in range(len(wrapped)-1))
    
    def classify_edges_dendricity(self, identify_deadends=False):
        """
        Classify edges into bridges ('B'), circuits ('C'), and self-loops ('S').  Don't bother summing lengths; that can be done separately, e.g. in postGIS.

        if identify_deadends is True, classify deadends as deadends ('D'), rather than bridges.  N.B.: This does not work (June 2016) for some reason. Why not just do this separately, later, with a single line in postgres?

        Method: Find all edges which contribute to a circuit. This seems synonymous with excluding bridges. 
              Thus, they make up the "ring" and "web" components (combined) of Xie and Levinson.
              See my LyX file on network theory and my definition of dendricity

        Relies on algorithm of:  Paton, K. An algorithm for finding a fundamental set of cycles of a graph. Comm. ACM 12, 9 (Sept 1969), 514-518.
        This is implemented in networkx. It seems to ignore selfloops.

        This implementation is too slow, so use the SNAP package for this function in large datasets. Or, better, subdivide the network to minimize the power-law execution time.
        
        Since there are parallel edges and we're instantiating an instance of a simple nx graph, we're missing some edge_ids for parallel edges. This is dealt with by postgres in pointmetrics_calculations.
        """

        selfloops= self.selfloop_edges()  #data=True)
        cycles=nx.cycle_basis(self,root=None)
        
        # Generate a list by edge, and generate a list by edgeIDs (each edge could have a list of multiple edgeIDs)
        # In both cases, we classify into B, C, and S (bridges and deadends, cycles, and self-loops)   
        byedge=dict([(tuple(sorted(edge)),'B') for edge in self.edges()])  # by default, all edges are sorted

        if identify_deadends:
            # Overwrite deadends as type 'D'
            # deg1nodes= sorted([nn for nn,dd in self.degree().items() if dd==1])  # why sort?
            deg1nodes=[nn for nn,dd in list(self.degree().items()) if dd==1]         
            for deg1node in deg1nodes:
                byedge[tuple(sorted([deg1node,list(self.edge[deg1node].keys())[0]]))]='D'
                #print('Assigning deadend: %s : edge ids: %s'%(str(tuple(sorted([deg1node,self.edge[deg1node].keys()[0]]))),
                #                                              str(self.edge[deg1node].values()[0]['edge_id'])))

            """
            #flatten = lambda *n: (e for a in n for e in (flatten(*a) if isinstance(a, (tuple, list)) else (a,)))            
            # Following is a generator, in which we've flattened the list, which contains ints and lists

            culde[ for nn in deg1nodes]
            culdesacs = [self.edge[nn].values()[0]['edge_id'] for nn in deg1nodes] 
            culdesacs = flatten(   [self.edge[nn].values()[0]['edge_id'] for nn in deg1nodes] )
            for edge in culdesacs:
            wowo"""
        # Overwrite cycle basis edges as "C"
        for cycle in cycles:
            for edge in self.edgesFromCycle(cycle):
                byedge[tuple(sorted(edge))]='C'
                
        # Overwrite self-loops as "S"
        for edge in selfloops:
            byedge[tuple(sorted(edge))]='S'

        return(byedge)

    def classify_edges_dendricity_by_ID(self,idname='edge_id',identify_deadends=False):
        """
        Classify edges and return a lookup for them, keyed by their edge_ids.
        Basically a wrapper for classify_edges_dendricity()
        """
        byedge= self.classify_edges_dendricity(identify_deadends)
        byID = {self.edge[ee[0]][ee[1]][idname]: eclass for (ee, eclass) in byedge.items()}

        return byID

    def get_network_distance_matrix(self, cutoff=10000, length='length'):
        """
        When called for the first time, calculate (using the Dijkstra method) the distance betwen every pair of nodes when that distance is less than cutoff.  Use the length property of edges to measures distance, or denote the length of every edge as 1.
        If this method has already been called, use the old value if cutoff hasn't changed.
        """
        if self.network_distances is None  or not self.network_distance_cutoff == cutoff:
            self.network_distances= nx.all_pairs_dijkstra_path_length(self, cutoff=cutoff, weight=length)
            self.network_distance_cutoff = cutoff
            #reportTime(networklengthstime,'dijkstra with cutoff %s m'%str(cc))        
        return self.network_distances


    def tabulate_edge_classification(self, classified=None):
        """ We cannot calculate dendricity without lengths. But I propose to leave that for postGIS. So this function is just informational, at best        """
        if classified is None:
            classified=self.classify_edges_dendricity()
        histoc=dict(B=0,C=0,S=0)
        if 'D' in list(dict(classified).values()):histoc['D']=0
        for cc in  list(dict(classified).values()):
            histoc[cc]+=1
        return(histoc)   

    #Not meaningful for osmnxGraph instantiations of continents (issue #14)
    #If needed, implement minimum_node_cut algorithm
    def count_minimum_edge_cuts(self,origin=None,destinations=[ None ],**kwargs):
        """ Provides totals to calculate the average number of edges that disconnect a graph, or between an origin node and a set of destination nodes.
        """
        min_cuts_a=set()
        for destination in [ dd for dd in destinations if \
         (origin!=dd and dd in self) or dd is None ]:
                min_cuts_a.update( nx.minimum_edge_cut(self,s=origin,t=destination,**kwargs ))
        return min_cuts_a, len(min_cuts_a)
        #return(  len(min_cuts_a), sum(min_cuts_a)  )

    def sprawl_metric_nodal_degree(self):
        """ Return the mean nodal degree of the entire graph. """
        # @TA: is this function superfluou if  we store node degrees for each node in the d.b., ie unaggregated
        nodes_degrees = self.degree(self.nodes())
        return np.mean(list(nodes_degrees.values()))

    def sprawl_metric_edge_degree(self):
        """ Return the mean edge degree of the entire graph. The degree of an
            edge is defined by the mean degrees of the two nodes it links. """
        edges_degrees = {}
        for edge in self.edges():
            # the first two elements of the edge tuple carries the nodes
            nodes_degrees = self.degree(list(edge)[:2])
            edges_degrees[edge] = np.mean(list(nodes_degrees.values()))
        return np.mean(list(edges_degrees.values()))

    def db_circle_neighborhood(self, connection, relation, source_id, radius):
        """ Returns a dict of nodes within the circle neighborhood defined
            by radius in meters, along with the corresponding distances to the
            source node. Distances between nodes are fetched from the relation
            through the open connection with the database."""
        
        cmd = "SELECT neighbor.osm_node_id, ST_Distance(node.geom,neighbor.geom) FROM %s neighbor, %s node WHERE node.osm_node_id=%s AND ST_DWithin(node.geom,neighbor.geom,%d);" % (relation,relation,source_id,radius)

        connection.execute(cmd)
        neighbors = connection.fetchall()
        euclidean_distances = {}
        for neighbor in neighbors:
            euclidean_distances[str(neighbor[0])] = neighbor[1]

        return euclidean_distances

    def db_ring_neighborhood(self, connection, relation, node, inner_radius, outer_radius):
        """ Returns a dict of nodes within the ring neighborhood defined
            by inner and outer radius in meters, along with the corresponding
            distances to the source node. """
        outer_circle_distances = self.db_circle_neighborhood(connection,relation,node,outer_radius)
        ring_distances = {}
        for node in list(outer_circle_distances.keys()):
            if outer_circle_distances[node] >= inner_radius:
                ring_distances[node] = outer_circle_distances[node]

        return ring_distances 

    def square_neighborhood(self, source, radius):
        """ Return a dict of nodes within the square neighborhood defined
            by radius in meters, along with the corresponding distances to the
            source node. """
        euclidean_distances = {}
      
        # for simplicity, assuming longitude degree is 111km in all latitudes
        degree_radius = (radius/float(1000))/111
        source = self.node[source]['data']
        for node_id in self.nodes():
            node = self.node[node_id]['data']
            if ((source.lat-degree_radius <=node.lat<= source.lat+degree_radius) and (source.lon-degree_radius <=node.lon<= source.lon+degree_radius)):
                euclidean_distances[node_id] = source.euclidean_distance(node)
        print(len(euclidean_distances))
        return euclidean_distances

    def network_circle_neighborhood(self, source, radius):
        """ Return a list of nodes within the circular neighborhood (radius in
            meters) over the network - not the cartesian circle neighborhood -
            along with the corresponding distances to the source node. The
            algorithm is an implementation of Breadth First Search (BFS),
            limited by the distance from source that defines the
            neighborhood. 

            Comment: This will miss nodes in the neighbourhood which are only connect by paths that venture outside the neighbourhood
            """

        #if nx.__version__[0]=='1':
        neighbors = self.neighbors_iter  # CAution! These looks incompatible with networkx v2+. Needs to be migrated
        visited = set([source])
        queue = deque([(source, neighbors(source))])
        distances_to_source = {}
        distances_to_source[source] = 0
        
        while queue:
            parent, children = queue[0]
            try:
                child = next(children)
                # distance coming from this parent
                distance = (distances_to_source[parent]+
                            self[parent][child]['weight'])
                if distance <= radius:
                    if child not in visited:
                        #nodes_within_radius.append(child)
                        distances_to_source[child] = distance
                        visited.add(child)
                        queue.append((child, neighbors(child)))
                    else:
                        if distances_to_source[child] > distance:
                            distances_to_source[child] = distance
            except StopIteration:
                queue.popleft()

        return distances_to_source

    def network_ring_neighborhood(self, node, inner_radius, outer_radius):
        """ Returns a dict of nodes within the ring neighborhood defined
            by inner and outer radius in meters, along with the corresponding
            distances to the source node. """
        outer_circle_distances = self.circle_neighborhood(node,outer_radius)
        ring_distances = {}
        for node in list(outer_circle_distances.keys()):
            if outer_circle_distances[node] >= inner_radius:
                ring_distances[node] = outer_circle_distances[node]

        return ring_distances 

    def sprawl_db_neighborhood_metrics(self,inner_radius,outer_radius=0,sample=None):
        """ Adaptation of sprawl_neighborhood_metrics to consider db-based
            methods. Neighborhood distances are now euclidean distances, fetched
            directly from postGIS. Network distances will be computed by
            dijkstra's shortest path. """
        pass

    def sprawl_neighborhood_metrics(self,inner_radius,outer_radius=0,sample=None):
        """ Return a tuple of neighborhood metrics (distance_ratio, min_cut)
            for a set of samples or the entire graph. Distance_ratio is
            defined by the ratio euclidean / network distances, which is
            computed for pairs of nodes in a neighborhood. Min_cut is the
            number of edges needed to be removed to disconnect the network
            between each pair of nodes within a neighborhood."""

        distance_ratio = {}
        sum_euclidean = {}
        sum_network = {}
        sum_min_cut = {}
 
        if not sample:
            sample = self.nodes()
        for node_id in sample:
            total_euclidean_distance = 0
            # if outer_radius is provided, ring neighborhood is used
            if outer_radius:
                network_distances = self.network_ring_neighborhood(node_id,
                                         inner_radius,outer_radius)
            # else, use circle neighborhood
            else:
                network_distances = self.network_circle_neighborhood(node_id,
                                         inner_radius)

            node = self.node[node_id]['data']
            neighbors = [self.node[node_id]['data'] for node_id in
                         list(network_distances.keys())]
           
            euclidean_distances = [node.euclidean_distance(neighbor)
                                   for neighbor in neighbors]

            min_cuts = [len(nx.minimum_edge_cut(self,node_id,neighbor.id))
                        for neighbor in neighbors if neighbor.id != node_id]

            sum_euclidean[node_id] = sum(euclidean_distances)
            sum_network[node_id] = sum(network_distances.values())
            sum_min_cut[node_id] = sum(min_cuts)

        distance_ratio = np.mean(float(sum(sum_euclidean.values()))/
                                 sum(sum_network.values()))

        min_cut = np.mean(float(sum(sum_min_cut.values()))/
                          len(self))

        return (distance_ratio,min_cut)



def merge_attributes_generator(accumulate_attributes=None,attributes_sum=None):
    """
    Build and return a function to handle attribute merging.
    e.g. accumulate_attributes=['TLID'] to keep TLID values from both edges.
    Or, attributes_sum=['length']
    """
    def handle_attributes(a1,a2):
        if not a1 and not a2: return({})
        attr=a1.copy()
        if attributes_sum is not None:
            for anatt in attributes_sum:
                attr[anatt]=a1[anatt]+a2[anatt]
        if accumulate_attributes is not None:
            for anatt in accumulate_attributes:
                if isinstance(a1[anatt], list) and isinstance(a2[anatt], list):
                    attr[anatt]= a1[anatt]+a2[anatt]
                elif isinstance(a1[anatt], list):
                    attr[anatt]= a1[anatt]+[a2[anatt]]
                elif isinstance(a2[anatt], list):
                    attr[anatt]= [a1[anatt]]+a2[anatt]
                else:
                    attr[anatt]=[a1[anatt],a2[anatt]]
        return(attr)
    return(handle_attributes)

def build_osmnx_graph_from_edges_table(continent,cluster,logger=None,where=None):
    """
    This function takes a continent as input, and returns a Graph instance with the vector data in the
    table edges_continent_cluster as edges.
    """
    import postgres_tools as psqlt

    dbc=psqlt.dbConnection(logger=logger)
    cmd="""SELECT spt_cluster_id,ept_cluster_id,edge_id,ST_AsText(geom) FROM edges_%s_%d """%(continent,cluster)+ \
        where*isinstance(where,str)+""";"""
    dbc.execute(cmd)
    info=dbc.fetchall()
    edgesOfInterest=[ tuple(sorted((ii[0],ii[1]))) for ii in info ]
    edge_ids=[ ii[2] for ii in info ]
    geoms=[ ii[3] for ii in info ]

    testGraph=Graph(edgesOfInterest)
    eid=dict(list(zip(edgesOfInterest,edge_ids)))
    egeom=dict(list(zip(edgesOfInterest,geoms)))
    if nx.__version__[0]=='1':
        nx.set_edge_attributes(testGraph,'edge_id',eid)
        nx.set_edge_attributes(testGraph,'geom',egeom)
    else:
        nx.set_edge_attributes(testGraph,eid, name='edge_id')
        nx.set_edge_attributes(testGraph,egeom, name='geom')
    return testGraph,edgesOfInterest,edge_ids,eid,egeom
   
if __name__ == "__main__":

    # a set of networkx sample graphs
    a = nx.complete_graph(3)
    b = nx.make_small_graph(["edgelist","Sprawl sample",9,
                           [[1,2],[2,3],[1,4],[2,5],[3,6],[4,7],[5,8],[6,9]]])
    c = nx.icosahedral_graph()
    d = nx.grid_2d_graph(3,3)
    e = nx.balanced_tree(3,3)
    f = nx.house_graph()

    


if __name__ == "__main__":
    test_osm_networkx()    

