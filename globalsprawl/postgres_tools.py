#!/usr/bin/python
import os, sys, re
from osm_config import paths,defaults
import psycopg2
import psycopg2.extras
import time
import pandas as pd
from decimal import Decimal
from cpblUtilities.parallel import runFunctionsInParallel
"""
These are general postgres tools that are not specific to our particular graph/roads application. 
postGIS-specific (ie spatial) tools should be kept elsewhere.
"""


def getPgLogin(host=None, su=False, db=None, forceUpdate=False):
    """
    Returns dictionary of credentials to login to postgres
    host can be ['local', 'apollo', 'tunnel']: 
       this refers to the server where postgres is running.  When host is defined as 'local', the field is suppressed from the return value, allowing the connection to proceed through local login.
    
    There are 3 types of role in postgres:
        - postgres (superuser). We shouldn't need this role, but maybe ocassionally to drop/add schemas.
            no password (login from apollo only)
            su=True will login as postgres
        - PIs (full rights to osmdata and public schemas). 
            no password needed from apollo
            if logging in from outside apollo, will prompt for password
        - osmusers (group role). PI usernames are just used for login. After login (via Python), the role is then 
            changed to osmusers. If we don't do this, amb won't be able to delete tables that cpbl created, and vice versa.
            
    To change access permissions (e.g. where someone can login from), edit /home/projects/var_lib_pgsql_9.3/data/pg_hba.conf

    For setup of the database (from scratch) and the permissions, see osm_master.py or the OS setup script cpbl made for the new server.
    """
    # see if we already have the information - avoid asking for password again
    if 'pgLogin' in globals() and (host is None or host==globals()['pgLogin']['host']) and not forceUpdate:
        return globals()['pgLogin']
    
    import getpass
    pgPassword = ''    
    if defaults['server']['postgres_prompt_for_password']:
        pgPassword = getpass.getpass('Enter postgres password for %s: ' % defaults['server']['postgres_role'])
    
    pgLogin = {'db': defaults['server']['postgres_db'] if db is None else db, 'user': defaults['server']['postgres_role'], }
    if host not in ['tunnel']:
        pgLogin['pw'] = pgPassword
    if host is None: # use default in config file
        host=defaults['server']['postgres_host']
    elif host == 'local':
        pass   # don't want to include the host in pgLogin credentials
    elif host == 'tunnel':
        pgLogin.update({'host':'localhost','port':'9432'})# ,'tunnel':True}) # put a "tunnel" note for pgEngine to see.
        pgLogin.pop('user')
        assert 'user' not in pgLogin
    else:
        pgLogin.update({'host': host})
    return pgLogin

def getPgEngine():
    """
    returns an engine object that connects to postgres
    From the docs: You only need to create the engine once per database you are connecting to
    """
    from sqlalchemy import create_engine
    pgLogin = getPgLogin()
    thehost= '' if 'host' not in pgLogin else pgLogin['host']+':5432' if 'port' not in pgLogin['host'] else pgLogin['host']
    thepw = '' if not  pgLogin['pw'] else ':'+ pgLogin['pw']
    
    # Special case: if 'tunnel' was specified in defaults, it will not show up in pgLogin host. So re-catch it here ("host" is not accepted as an argument of the getPgEngine method, so just look to defaults)
    if defaults['server']['postgres_host']=='tunnel':
        thehost = 'localhost:9432'
        engine = create_engine('postgresql://{thehost}/{db}'.format(thehost=thehost, db=pgLogin['db']))
        return engine
    engine = create_engine('postgresql://%s%s@%s/%s' % (pgLogin['user'],thepw, thehost, pgLogin['db']))
    return engine

def exec_many_commands_in_parallel(cmds,schema=None,commands_at_once= 5999900, verbose=False,  host=None, curType='DictCursor', logger=None, parallel=True, command_description=None):
    """
    It is (?) faster when making many small postgres updates to have multiple threads going at it. 
    Chop up the long list of commands (cmds) into sets with length commands_at_once.
    Use a separate db connection for each process.
    
    By default, this should be done silently, but the logger is used to provide a message about the entire operation.
    """
    if command_description is None: command_description =""
    reps = int(len(cmds) / float(commands_at_once)) + 1
    funcs, names = [] , []
    for rep in range(0, reps):  # loops over cmds in chunks of commands_at_once
        #print "\tMultiprocessing %d of %d" %(rep+1, reps)
        cmdsSubset = cmds[rep * commands_at_once:min(len(cmds), (rep + 1) * commands_at_once)]
        funcs+=[[dbConnection,[schema],dict(command=';\n'.join(cmdsSubset))]]
        names+=[command_description+' Chunk%05d'%rep]
    if logger:
        logger.write('Beginning parallel execution of %d commands in %d chunk%s' % (len(cmds),reps, 's'*(reps>1)) )
    runFunctionsInParallel(funcs,names=names, expectNonzeroExit=True, parallel=parallel) # Each will return a dbConnection instance
    if logger:
        logger.write('Finished parallel execution of %d commands in %d chunk%s' % (len(cmds),reps, 's'*(reps>1)) )
    

class dbConnection():
    def __init__(self, schema=None, verbose=False,  host=None, db=None, curType='DictCursor', logger=None, command=None):
        """
        This returns a connection to the database.

        schema should be "osm" or "processing" or "analysis" or another choice of schema (not used), and defaults to the processingSchema in the config file.

        If command is provided, execute that command and close the connection [latter part not implemented]

        curType right now is either DictCursor or the default (tuples)
        note that default is default for psycopg2, not the default for this function

        At the moment, we only refresh the cursor after doing a fetch, not after every execute. 
        If we did the latter, we'd have to disallow fetch's except when they were called as part of an execfetch, 
        since we can't fetch if we've deleted the cursor which made the execute.

        Rather than properly set this thing up as "class dbConnection(psycopg2.extras.DictCursor)", 
        we will just replicate a couple of methods: execute, fetchall.
        """
        if host is None:
            host=defaults['server']['postgres_host']
        self.pgLogin = getPgLogin(host=host, db=db)
        # This is somewhat forgiving, letting people use multiple names for the same thing
        schema = {
                  'osm':defaults['osm']['osmSchema'],
                  'osmdata':defaults['osm']['osmSchema'],
                  'osmprocessing':defaults['osm']['processingSchema'],
                  'processing':defaults['osm']['processingSchema'],
                  'osmanalysis':defaults['osm']['analysisSchema'],
                  'analysis':defaults['osm']['analysisSchema'],
                  'rasterdata':defaults['osm']['rasterSchema'],
                  'rasters':defaults['osm']['rasterSchema'],
                  'cities':defaults['osm']['citiesSchema'],
                  None:defaults['osm']['processingSchema']
                 }[schema]
        self.default_schema=schema  
        self.verbose=verbose
        self.logger=logger
        if logger:
            self.verbose=True
        assert curType in ['DictCursor', 'default']
        self.curType=curType
        # Connect to database
        coninfo = ' '.join([{'db':'dbname','pw':'password'}.get(key,key)+' = '+val   for key,val in list(self.pgLogin.items()) if val])
        con = psycopg2.connect(coninfo)
        con.set_isolation_level(0)   # autocommit - see http://stackoverflow.com/questions/1219326/how-do-i-do-database-transactions-with-psycopg2-python-db-api
        self.cursor=None
        self.connection=con
        # See documentation for schemas. Search path order determines  schema use. Putting the default first ensures new tables will be created there if not specified explicitly.
        self.search_path=[self.default_schema]+[sp for sp in 
                            [sch for kk,sch in list(defaults['osm'].items())
                            if kk.endswith('Schema')
                            ] if not sp==self.default_schema]+['public']
        self.execute('SET search_path = '+','.join(self.search_path))   
        self.execute('''SET role osmusers;''') # ensures that new tables are owned by osmusers, not the PIs, and so we can all drop them
        assert not self.cursor.closed
        self.refreshCursor=True
        if command:
            self.silently_execute(command)
    def report(self,ss):
        """
        A wrapper for "print". In verbose mode, it prints comments and SQL commands.  If a logger has been provided, it calls logger.write() instead of print().
        """
        if not self.verbose: return
        if self.logger:
            self.logger.write(ss)
        else:
            print(ss)
    def cur(self):
        """ Create a new cursor. This can be done frequently.
        The cursor is a lightweight object, and can be deleted after each use. That might help with postgres memory use.

        The latest cursor is always available as self.cursor but the only intended use is outside calls of the form:
          thisobject.cur().execute('pg command')
        """
        if self.curType=='DictCursor':
                cur = self.connection.cursor(cursor_factory=psycopg2.extras.DictCursor)  # So evertying coming from the database will be in POython dict format.
        elif self.curType=='default': # will return data as tuples - easier to convert to pandas
                cur = self.connection.cursor()
        del self.cursor
        self.cursor=cur
        return cur
    def psql_command_line_flags(self):
        """ Return a string to be used in a psql command line to identify host, user, and database.

        Not yet tested with passwords
        """
        outs = '' if 'host' not in self.pgLogin else '-h %s ' % self.pgLogin['host']
        outs += ' -d %s -U %s ' % (self.pgLogin['db'], self.pgLogin['user'])
        return outs
    def gdal_command_line_flags(self, schema=None):
        """ Return a string to be used in a gdal command line to identify host, user, and database.
        Not yet tested with passwords"""
        if schema is None: schema = self.default_schema
        outs = '' if 'host' not in self.pgLogin else '-h %s ' % self.pgLogin['host']
        outs += ' dbname=%s user=%s schema=%s ' % (self.pgLogin['db'], self.pgLogin['user'], schema)
        if self.pgLogin['pw']!='': outs += 'password=%s ' % (self.pgLogin['pw']) 
        return outs
    def execute_sql_file(self,cmdfile, nice=True, quiet=True):
        """
        Sometimes rather than using the cursor, we want to run a command using os.system(psql...). 
        Provide an interface for that here.
        """
        assert os.path.exists(cmdfile)
        os.system(nice* 'nice -n 19 ' + ' psql '+ self.psql_command_line_flags()+ quiet*' -q '+ ' -f "%s"' % cmdfile)

    def execute(self,cmd):
        """ If verbose is True, report on the cmd and the duration of the operation. """
        if self.cursor is None:
                self.cur()
        if self.verbose:
            self.report(re.sub(r'\s+',' ',cmd.replace('\n',r' \n ')))
            nowt=time.time()
        if cmd:
            retval=self.cursor.execute(cmd)
        else:
            retval='No command to execute'
        if self.verbose:
            dtime=time.time()-nowt
            name='PostgreSQL'
            if not self.logger: # This is too much, unless verbose is True but logger is None
                self.report('       T: %25s took '%name+ (' %.3g s '%dtime if dtime<120 else ' %.3g m '%(dtime/60)) )
        return(retval)

    def silently_execute(self,cmd):
        """ Silent execute: even if verbose is True, do not echo the command or timing. """
        if self.cursor is None:
                self.cur()
        return self.cursor.execute(cmd)
    def safely_execute(self,cmd):
        try:
            self.execute(cmd)
        except psycopg2.ProgrammingError as err:
            self.report(' FAILED!!!!!!!!!!!!!!!!!!!')
            self.report( str(err))

    def fetchall(self):
        fout=self.cursor.fetchall()
        if self.refreshCursor:
                del self.cursor
                self.cursor=None
        return fout
    def fetchone(self):
        fout=self.cursor.fetchone()
        if self.refreshCursor:
                del self.cursor
                self.cursor=None
        return fout
    def execfetch(self,cmd):
        """
        Execute an SQL command and fetch (fetchall) the result.
        """
        self.execute(cmd)
        return self.fetchall()
    def safely_execfetch(self,cmd):
        """
        Execute an SQL command and fetch (fetchall) the result, returning -1 if it fails..
        """
        try:
            self.execute(cmd)
            return self.fetchall()
        except psycopg2.ProgrammingError as err:
            self.report(' FAILED!!!!!!!!!!!!!!!!!!!')
            self.report( str(err))
            return(-1)
    def silently_execfetch(self,cmd):
        """
        Override self.verbose setting, and suppress display of command for execute. Return the fetchall result.
        """
        self.silently_execute(cmd)
        return self.fetchall()

    def df2db(self, df, tablename, if_exists='replace', index=False, 
              chunksize=100000, schema=None):
        """
        Create a table using tabular data in a Pandas dataframe.
        """
        if not schema:
            schema=self.default_schema
        engine=getPgEngine()
        df.to_sql(tablename, engine, schema=schema, if_exists=if_exists, 
                    index=index, chunksize=chunksize)
        self.fix_permissions_of_new_table(tablename)

    def db2df(self,tablename, savefile= None, schema = None,  drop_geom = True, columns=None, as_int_cols = None, where_clause=None):
        """
        Return a Pandas dataframe, and optionally save a pandas file, from a postgres table.
        If savefile is None or not specified, no file is saved.
        If savefile is True, the tablename will be used as the filename.
        If a filename is specified, it will be used instead of the tablename when saving the new data frame.
        You can specify columns explicitly or let drop_geom=True try to identify and drop geometry and geography columns.

        as_int_cols = True will convert ALL columns to integer. 
        as_int_cols = [list of columns] specifies integer columns

        where_clause should be of the form "WHERE ..." or something else like "ORDER BY ..."

        N.B. This can be used to return a query as well, if it looks like a list of derived values (use columns=[...]) and a where clause. Just use "savefile=False".
        """
        assert columns is None or not drop_geom
        if isinstance(savefile,str):
            savefile = paths['scratch']*(not os.path.split(savefile)[0]) +savefile+'.pandas'*(not savefile.endswith('.pandas'))
        elif savefile is True:
            savefile = paths['scratch']+tablename+'.pandas'
            
        cols = self.list_columns_in_table(tablename, schema=schema)
        if schema is not None: 
            tablename = schema+'.'+tablename
        if drop_geom:
            cols = [cc for cc in cols if 'geom' not in cc and 'geog' not in cc]
        if columns:
            cols = columns # [cc for cc in cols if cc in columns] # No checking?!
            
        sql="""
        SELECT """+','.join(cols)+' FROM '+tablename + ' '+ (where_clause if where_clause is not None else '') + """;
        """
        self.execute(sql)
        ldd=self.fetchall()
        if columns: 
            cols = [c if ' as ' not in c.lower() else c.split(' as ')[1] if ' as ' in c else c.split(' AS ')[1]                for c in cols] 
        df=pd.DataFrame([list(dd.values()) for dd in ldd], columns=cols)
        if as_int_cols is True:
            as_int_cols = df.columns
        if as_int_cols:
            for cc in as_int_cols:
                df[cc]= df[cc].astype(int)
        if savefile:
            df.to_pickle(savefile)
        return(df)

    def fix_permissions_of_new_table(self,tablename,schema=None):
        """
        Newly created tables have the owner of the person running the code, 
        whereas we want them to be owned by osmusers.
        This cannot be done from the cursor without changing the role, 
        so it's easier to do it from a separate psql call.
        """
        if not tablename==tablename.lower():
            warning=' WARNING: Your tablename {} has capital letters. This will cause you anguish with postgres!'.format(tablename)
            self.report(warning)
            print(warning)
            tablename=tablename.lower()
        if schema:
            schema+='.'
        else:
            schema=''
        # We could do this with self.cursor, too; but we'd have to change the role back to a person from osmusers.
        os.system('psql -c "ALTER TABLE '+schema+tablename+' OWNER to osmusers" %s '%(self.psql_command_line_flags()))

    def list_columns_in_table(self,table,schema=None):
        if not schema:
            schema=self.default_schema

        return [rr[0] for rr in self.silently_execfetch("""
        SELECT 
        -- attrelid::regclass, attnum, 
        attname
        FROM   pg_attribute
        WHERE  attrelid = '"""+schema+'.'+table+"""'::regclass
        AND    attnum > 0
        AND    NOT attisdropped
        ORDER  BY attnum;
        """)]
        """ -- other slower method
        SELECT *
        FROM information_schema.columns
        WHERE table_schema = '"""+schema+"""'
          AND table_name   = '"""+table+"""'   ; """

    def merge_table_into_table(self, from_table, targettablename, commoncolumns,
        fromschema=None, targetschema=None, drop_old=True, 
        drop_incoming_duplicates=False, recreate_indices=True, join_type='LEFT', 
        forceUpdate=False):
        """To merge two tables (with different but overlapping columns),
        the efficient method is to delete the original tables and create a new one.
        The from_table should contain no columns in common with targettable,
        except for the matching column(s), "commoncolumns".   If it does,
        an error will be thrown unless forceUpdate is True, in which case the common columns 
        will simply be dropped (as opposed to a nicer merge).

        Often the commoncolumns is the primary key or should be unique. 
        In this case, drop_incoming_duplicates will use only one copy of each row with a distinct
        key value in from_table.

        Note that when one renames tables, one does not rename their indices automatically.
        """
        if not fromschema:
            fromschema=self.default_schema
        if not targetschema:
            targetschema=self.default_schema

        if isinstance(commoncolumns, list): commoncolumns=','.join(commoncolumns)

        # Check we don't have a conflict:
        ftc=self.list_columns_in_table(from_table,schema=fromschema)
        ttc=self.list_columns_in_table(targettablename,schema=targetschema)
        conflictc=[ cc for cc in list(set(ftc) & set(ttc)) if cc not in commoncolumns.split(',') ]

        # Case where conflict exists
        if conflictc:
            if forceUpdate:
                self.report("""Found existing update columns (%s) in target table %s.
                    Deleting ALL existing values in {%s} in "%s" because forceUpdate=True.
                    """%(','.join(conflictc),targettablename,','.join(conflictc),targettablename))
                recreate_indices=True
                ttc=[cc for cc in ttc if cc not in conflictc]
            else:
                self.report("""Conflict in columns.
                    We could implement something fancier to stitch together the changed
                    and unchanged rows... or just stop here""")
                self.report("""Maybe you need to force-update your run so that the target table
                    is dropped/recreated?  Or maybe something else is wrong.
                    It looks like columns you are insterting, other than the index column,
                    already exist in the target.""")
                self.report('         '+fromschema+'.'+from_table+':'+str(ftc)
                    +'\n         '+targetschema+'.'+targettablename+':'+str(ttc))
                raise Exception("""Columns being inserted, other than the index column,
                    already exist in the target table. Try passing forceUpdate as True.""") 

        # Merge tables
        if drop_incoming_duplicates:
                from_table="""(SELECT DISTINCT ON ("""+commoncolumns+""") """+','.join(ftc)+"""
                            from """+fromschema+'.'+from_table+""") as distinct_right_table"""
        else:
            from_table = fromschema+'.'+from_table
        left_table=( """ (SELECT """+','.join(ttc)+""" FROM 
                    """+targetschema+'.'+ targettablename+""" ) AS lefttable """
                   ) if conflictc else targetschema+'.'+targettablename

        # Recreate indices
        if recreate_indices: 
            indices=self.execfetch("""SELECT indexdef FROM pg_indexes
                WHERE schemaname='%s' and tablename='%s'
                """%(targetschema,targettablename))

        cmd="""CREATE TABLE """+targetschema+""".new_"""+targettablename+"""
                 AS (\n SELECT * FROM """+left_table+"""
                 """+join_type+""" JOIN """+from_table+""" USING ("""+commoncolumns+"""));"""
        self.execute(cmd)
        self.execute("""DROP TABLE IF EXISTS """+targetschema+""".old_"""+targettablename+""";""")
        self.execute("""ALTER TABLE """+targetschema+'.'+targettablename+"""
             RENAME TO old_"""+targettablename+';')
        if drop_old: self.execute(""" DROP TABLE """+targetschema+""".old_"""+targettablename+';')
        self.execute("""ALTER TABLE """+targetschema+""".new_"""+targettablename+""" RENAME TO 
            """+targettablename+';')

        if recreate_indices:
            for idxCmd in indices: self.execute(idxCmd[0])

    def update_table_from_array(self, data, targettablename, targetschema=None, 
                                joinOnIndices=False, temptablename=None, 
                                tempschema=None, if_exists='replace', 
                                preserve_uppercase=False, 
                                drop_incoming_duplicates=False, forceUpdate=False):
        """
        Take a tab-separated file, or a Pandas dataframe, and use its first column to join the remaining columns (which should not exist in the targettablename) to update the matching target table rows.

        data can be a df or a filename
        If joinOnIndices is True, will join on a (Multi)Index. Otherwise, join on the first column.

        forceUpdate is passed only to merge_table_into_table to allow overwriting (dropping) of existing columns.

        Comments: 
        Uses sqlalchemy to handle buffering/inserts to SQL.

        By default, pandas' to_sql uses quotes around any column names if they (any) contain capital letters. That can mess up a lot of stuff. So by default, this function will convert column names to lowercase.
        To do: Make sure that the data do not contain duplicates of the intended primary key.
        """
        if not targetschema:
            targetschema=self.default_schema
        if not tempschema:
            tempschema=self.default_schema
        cmds=[]
        engine=getPgEngine()
        ntmp=temptablename if temptablename else 'tmputfa_'+targettablename
        if isinstance(data,str):
                self.report(' Loading '+data)
                df=pd.read_table(data)
        else:
                df=data
        df.columns=df.columns.map(lambda ss: ss.lower())
        self.report(' Inserting (from df) to '+ntmp)
        if joinOnIndices:
            df.index.names=[ss.lower() for ss in df.index.names]
            indexcolumn=df.index.names
            df.to_sql(ntmp, engine, schema=tempschema, if_exists=if_exists, \
                      index=True, chunksize=100000)
        else:
            indexcolumn=df.columns[0]
            df.to_sql(ntmp, engine, schema=tempschema, if_exists=if_exists, 
                      index=False, chunksize=100000)
        self.fix_permissions_of_new_table(ntmp,schema=tempschema)
        self.report(' Merging '+ntmp+' into '+targettablename)
        self.merge_table_into_table(ntmp, targettablename, indexcolumn, 
            fromschema=tempschema, targetschema=targetschema, 
            drop_incoming_duplicates=drop_incoming_duplicates, 
            forceUpdate=forceUpdate)

    def create_primary_key_constraint(self,table,key,schema=None):
        
        if schema:
            schema+='.'
        else:
            schema=''
        self.execute("alter table "+schema+table+" ADD CONSTRAINT "+key.replace(',','')+"_constraint_"+table+" PRIMARY KEY ("+key+"); ")

    def drop_primary_key_constraint(self,table,key,schema=None):
        if schema:
            schema+='.'
        else:
            schema=''
        self.execute("ALTER TABLE "+schema+table+" DROP CONSTRAINT if exists "+key+"_constraint_"+table)


    def addColumnToTable(self, table=None, columnname=None, columntype=None, 
                         schema=None):
        """Returns postgres text to add a column if it doesn't already exist.
         If you can do a query to check the existing columns, and then conditionally do a second query, of course it would look prettier. But this is the simplest way to do it in a single pass.

        e.g.:
        cur.execute(addColumnToTable(table='osmdata.sampled_distances_osmnode',columnname='id_3',columntype='int'))

        TO DO: Replace this SQL exception catching with Python exception catching; it'll shorter and look cleaner.
         """
        if schema:
            schema+='.'
        else:
            schema=''

        cmd="""
        DO $$ 
            BEGIN
                BEGIN
                    ALTER TABLE %(table_name)s ADD COLUMN %(column_name)s %(column_type)s;
                EXCEPTION
                    WHEN duplicate_column THEN RAISE NOTICE 'column %(column_name)s already exists in %(table_name)s.';
                END;
            END;
        $$
        """%dict(column_name=columnname, column_type=columntype, 
                 table_name=schema+table)
        self.execute(cmd)

    def delete_all_tables(self,contains=None,startswith=None,schema=None,  force_do_not_ask=False):
        # Uncautiously delete all tables based on a string in (or starting) 
        # their name.
        # Optionally, specify a schema; otherwise, iterates through all schemas.
        # Case insensitive.
        if contains is None and startswith is None:
            self.report(' FAILED: Some criterion must be given.')
        contains,startswith=['' if x==None else x for x in (contains,startswith)]
        if schema:
            schemas=[schema]
        else:
            schemas=self.search_path
        for schema in schemas:
            lot=[tt for tt in self.list_tables(schema=schema) if 
                (len(startswith)>0 and tt.lower().startswith(startswith.lower())
                ) or 
                (len(contains)>0 and contains.lower() in tt.lower())
                ]
            if not lot:
                print(' No '+schema+' tables match drop criterion '+ \
                      (len(contains)>0)*('contains = "'+contains+'"')+ \
                      (len(startswith)>0)*('startswith = "'+startswith+'"'))
                continue
            print(' About to delete the following: ')
            for table in lot:
                print(("          DROP TABLE IF EXISTS "+schema+'.'+table+"  CASCADE;"))
            if not force_do_not_ask:
                assert input(' Continue??? ').lower().strip() in ['y','yes','hell yes'] #About to drop {}.{} ...'.format(schema,table))

            for table in lot:
                self.execute("DROP TABLE IF EXISTS "+schema+'.'+table+"  CASCADE;")

    # Pass a string or list of strings contains as argument. Returns a list
    # of two-element lists [ schema,table ], corresponding to all the distinct
    # combinations of:
    # # table: A table or view with contains (or, if contains is a list, all the
    # # elements of contains) in the name
    # # schema: The schema where table lives
    def find_tables(self,contains):
        assert ( isinstance(contains,str) or 
                 all([ isinstance(c,str) for c in contains ]) )
        if isinstance(contains,str):
            contains=[contains]
        searchStr=' and '.join([ 
                                'substring(table_name,\'{}\') is not null'.format(c)
                                for c in contains 
                               ])
        cmd="""select table_schema,table_name from information_schema.tables
               where {};""".format(searchStr)
        self.execute(cmd)
        res=self.fetchall()
        return res

    def list_tables(self,schema=None):
        """ List all tables in the schema. Views are not tables, so don't list them.
        TODO: Could add an "all_schemas" option?
        """
        if not schema:
            schema=self.default_schema
        cmd="SELECT table_name FROM information_schema.tables WHERE table_type != 'VIEW' and table_schema = '"+schema+"';"
        return [tt[0] for tt in self.execfetch(cmd)]

    # List all the schemas in the database.

    # Specify rasters to include the raster schema.
    # Specify all_ to include the raster schema, pg_catalog,public and 
    # information_schema.
    def list_schemas(self,rasters=False,all_=False):
        cmd='SELECT schema_name from information_schema.schemata;'
        schemas=[s[0] for s in self.execfetch(cmd)]
        if all_:
            return schemas
        schemas=[s for s in schemas if s not in
                ['pg_catalog','public','information_schema']
                ]
        if rasters:
            return schemas
        return [s for s in schemas if s!=defaults['osm']['rasterSchema']]

    def list_functions(self):
        """ List all user-defined functions in the scheme (e.g. Safe_ST_Clip)
        See http://stackoverflow.com/questions/1347282/how-can-i-get-a-list-of-all-functions-stored-in-the-database-of-a-particular-sch"""
        cmd = '''SELECT p.proname 
                FROM pg_proc p INNER JOIN pg_namespace ns ON (p.pronamespace = ns.oid)
                WHERE ns.nspname = '%s';''' % self.default_schema
        return [tt[0] for tt in self.execfetch(cmd)]

    def report_all_table_sizes(self, schema=None):
        """ Returns a lookup (DataFrame) of all tables and indices, and info on their size. Also, it prints out a list of them all, which makes useful the command line interface, e.g.:

        python postgres_tools.py sizes|grep asia
        
        to do: drop schema argument. Report totals also by schema.  Since returning a DataFrame, no need to filter by schema, etc.
        to do: Should simply loop over public databases, and over all tables/schemas within. Return a single full dataframe.

        bug: giving no WHERE clause by schema runs into a permissions error
        """

        whereclause = ' WHERE '+ ' OR '.join([" table_schema = '{}' ".format(aschema)  for aschema  in self.list_schemas()])

        command = """

        select table_schema, table_name, table_type, pg_size_pretty(pg_relation_size(table_schema||'.'||table_name)) sizetxt, pg_relation_size(table_schema||'.'||table_name) size
        from information_schema.tables
        {WHERE}
        order by size
        ;
        """.format(WHERE = whereclause) #'' if schema =='' else " where table_schema = '{}' ".format(schema))
        table1 =self.execfetch(command)
        #self.report('\n'.join(['\t'.join(['%25s'%str(cc) for cc in RR]) for RR in table]))
        dfs= pd.DataFrame([list(dr) for dr in table1], columns=['schema','tablename','type','sizetxt','sizeb',])
        dfs['type']=dfs.type.map(lambda tt: {'r':'table','i':'index','t':'idunno','BASE TABLE':'table','VIEW':'view'}.get(tt,tt))
        self.report('Total : %f GB'%(dfs.sizeb.sum()/1e9))

        
        command= """ -- The following gets at indices too. How to include those but with schema as a column?
        SELECT
           relname AS objectname,
           relkind AS objecttype,
           reltuples AS rows,
           relpages::bigint*8*1024 as sizeb, pg_size_pretty(relpages::bigint*8*1024) AS sizetxt
           FROM pg_class
           WHERE relpages >= 8
           ORDER BY relpages DESC;"""

        table2=self.execfetch(command)[::-1]
        #self.report('\n'.join(['\t'.join(['%25s'%str(cc) for cc in RR]) for RR in table]))
        df= pd.DataFrame([list(dr) for dr in table2], columns=['tablename','type2','rows','sizeb2','sizetxt2',])
        df['type2']=df.type2.map(lambda tt: {'r':'table','i':'index','t':'idunno','BASE TABLE':'table','VIEW':'view'}.get(tt,tt))
        self.report(df[~(df.tablename.str.startswith('pg_')) & (df.type2=="table")])
        self.report('Total, including indices: %f GB'%(df.sizeb2.sum()/1e9))

        dfo= df.set_index('tablename').join(dfs.set_index('tablename'))

        return dfo[['type','sizeb','sizetxt','schema','rows']]

    def delete_many_rows(self,tablename,id_col,id_drop_list,schema=None):
        """
        Dropping rows can be very slow, especially when there are any indices. Here's one way which might be faster than some.

        This requires that there is enough RAM for several (5?) times the size of the table, according to
        http://stackoverflow.com/questions/8290900/best-way-to-delete-millions-of-rows-by-id

        It also involves listing all the ids together, in one query.
        """
        if schema:
            schema+='.'
        else:
            schema=''

        cmd="""
        -- SET temp_buffers = 600GB -- or whatever you can spare temporarily
        DROP TABLE IF EXISTS tmp_%(tablename)s;
        CREATE TEMP TABLE tmp_%(tablename)s AS
        SELECT t.*
        FROM   %(schema)s%(tablename)s t
        LEFT   JOIN (VALUES %(drop_list)s) as d (%(id_col)s)  USING (%(id_col)s)
        WHERE  d.%(id_col)s IS NULL;      -- copy surviving rows into temporary table

        TRUNCATE %(schema)s%(tablename)s;             -- empty table - truncate is very fast for big tables

        INSERT INTO %(schema)s%(tablename)s
        SELECT * FROM tmp_%(tablename)s;        -- insert back surviving rows.

        """
        subs=dict(drop_list="<LIST OF %d %s's>"%(len(id_drop_list),id_col),
                 tablename=tablename,
                 schema=schema,
                 id_col=id_col)
        self.logger.write(cmd%subs)
        subs=dict(drop_list=','.join(['('+str(anid)+')' for anid in id_drop_list]),
                 tablename=tablename,
                 schema=schema,
                 id_col=id_col)
        self.silently_execute(cmd%subs)

    def alter_geom(self, tablename, to_geom, geom_type, geom_column="geom",
                   schema=None):
        if schema:
            tablename=schema+'.'+tablename
        cmdDict=dict(tablename=tablename,togeom=to_geom,geomtype=geom_type,geomcolumn=geom_column)
        self.execute("""ALTER TABLE %(tablename)s ALTER COLUMN %(geomcolumn)s
            SET DATA TYPE geometry(%(geomtype)s, %(togeom)d)
            USING ST_Transform(%(geomcolumn)s, %(togeom)d);"""%cmdDict)

    def check_whether_table_exists(self, schema, tablename, extantTables=None, drop_if_exists=False):
        """
        Returns true if the table exists when this function is finished. i.e. if it doesn't exist or if drop_if_exists is True, this function returns False.
        """
        assert schema
        if extantTables is None:
            tablesL = self.execfetch("SELECT table_name FROM information_schema.tables WHERE table_schema='"+schema+"'")
            extantTables=[table[0] for table in tablesL]
        if tablename in extantTables:
            if drop_if_exists:
                print((' Table '+tablename+' already exists: dropping it.'))
                self.execute('DROP TABLE IF EXISTS '+schema+'.'+tablename+';')
                return False
            else:
                print((' Table '+tablename+'  already exists: leaving it intact.'))
                return True
        return False

    # Ensuring the geometry column of a table contains only one distinct
    # geometry type can really speed up QGIS loading PostGIS layers
    # For more information, see the discussion at issue #182 on Barrington-Leigh/Millard-Ball's private repo version

    # If a table's geometry column contains Multi and regular geometry types,
    # e.g. Polygons and MultiPolygons, ensure_consistent_geom(table) casts
    # the Polygons to MultiPolygons

    # If batch is True, pass a list of tables and perform ensure_consistent_geom
    # on all elements of the list
    def ensure_consistent_geom(self, tables, column='geom', geom='polygon', 
                               schema=None, batch=False):
        assert ( isinstance(tables, list) if batch
                 else isinstance(tables, str) )
        schema=schema+'.' if schema else ''
        if not batch:
            tables=[tables]
        for table in tables:
            strdict=dict(table=schema+table, column=column)

            # If the geom column is empty, no need to continue
            if self.execfetch('SELECT %(column)s FROM %(table)s LIMIT 1;' \
                                  %strdict)  in [[[None]],[]]:
                continue

            self.execute('SELECT DISTINCT GeometryType(%(column)s) FROM \
                         %(table)s;'%strdict)
            geomList=[ row for row in self.fetchall() ]
            assert all(
                [geom in _type.lower() for row in geomList for _type in row]
            )
            self.execute('SELECT DISTINCT ST_SRID(%(column)s) FROM \
                         %(table)s;'%strdict)
            geomSRID=self.fetchall()
            assert len(geomSRID)==1 and len(geomSRID[0])==1
            geomSRID=geomSRID[0][0]
            
            # If the geometry type is already consistent, don't need to convert
            # to Multi type
            if len(geomList)>1:
                geomType='geometry(Multi'+geom.title()+', '+str(geomSRID)+')'
                strdict['geomType']=geomType
                self.execute('UPDATE %(table)s SET %(column)s = \
                             ST_MULTI(%(column)s);'%strdict)

            else:
                geom=geomList[0][0]
                geomType='geometry('+geom.title()+', '+str(geomSRID)+')'
                strdict['geomType']=geomType

            # But we should still make sure the geometry type is explicit so
            # QGIS doesn't have to resolve type
            self.execute('ALTER TABLE %(table)s ALTER COLUMN %(column)s \
                         TYPE %(geomType)s;'%strdict)


    # Return True if the data in Postgres tables t1 and t2 is equal. Otherwise,
    # returns False.
    #
    # Option to specify columns, in which case returns True if data in the
    # specified columns of t1 and t2 is equal. Otherwise, returns False.
    #
    # Option to specify schema1 (limits the scope of the search for t1 to
    # schema1) and schema2 (limits the scope of the search for t2 to schema2).
    # Otherwise, searches through schemas in the hierarchy defined by the
    # cursor's search path.
    def are_equal(self,t1,t2,columns=None,schema1=None,schema2=None):
        if not columns:
            columns=self.list_columns_in_table(t1,schema=schema1)
            assert set(columns)==set(self.list_columns_in_table(t2,
                                     schema=schema2))
        else:
            assert all([ column in self.list_columns_in_table(t1,schema=schema1)
                         for column in columns ])
            assert all([ column in self.list_columns_in_table(t2,schema=schema2)
                         for column in columns ])
        data1=self.execfetch('select %s from %s%s;'%(','.join(columns),
                                                     schema1+'.' if schema1 else
                                                     '',t1)
                                                    )
        data2=self.execfetch('select %s from %s%s;'%(','.join(columns),
                                                     schema2+'.' if schema2 else
                                                     '',t2)
                                                    )
        for d in data1,data2:
            for i in range(len(d)):
                for j in range(len(d[i])):
                    if isinstance(d[i][j],Decimal):
                        d[i][j]=float(d[i][j])
        data1=set([ tuple(row) for row in data1 ])
        data2=set([ tuple(row) for row in data2 ])
        effectively_eq=( lambda x,y: all([ ( isinstance(a,float) and
                         isinstance(b,float) and abs(a-b) < 1e-5 ) or (
                         not isinstance(a,float) and not isinstance(b,float)
                         and a==b ) for a,b in zip(x,y) ])
                       )
        for d1,d2 in [ (data1,data2),(data2,data1) ]:
            for r in d1:
                if not any([ effectively_eq(r,r_) for r_ in d2 ]):
                    return False
        
        return True

################################################################################################
################################################################################################
################################################################################################
if __name__ == '__main__':
################################################################################################
################################################################################################
################################################################################################
    import sys
    runmode= 'default' if len(sys.argv)<2 else sys.argv[1].lower()


    if runmode not in ['default','test','repeatmerge','sizes', 'delete-lookups', 'test_tunnel']:
        print('\n\n UNRECOGNIZED RUN MODE...\n\n')

    if runmode in ['test_tunnel']:

        #def __init__(self, schema=None, verbose=False,  host=None, db=None, curType='DictCursor', logger=None, command=None):

        db=dbConnection(db='osm10m', host='tunnel', verbose=True)
        db.list_tables()
        
    if runmode in ['sizes']: # Convenience tool for OSM work
            cur=dbConnection('osm', False)
            out= cur.report_all_table_sizes('osmdata')
            out= cur.report_all_table_sizes('osmprocessing')
            out= cur.report_all_table_sizes('osmanalysis')
    if runmode in ['delete-lots-of-things']:
            cur=dbConnection('osm',verbose=True)
            df= cur.report_all_table_sizes()
            dft=df[df.type=='table']
            for nn in dft[dft.index.str.contains('_10')].index.values:
                    cur.execute('DROP TABLE %s   CASCADE ;'%nn)

            lu=dft[dft.index.str.startswith('lookup')] 

            for nn,row in lu.iterrows():
                    if row['type']=='table' and    (nn.startswith('lookup_osmnode') or 'northamerica' in nn or 'asia' in nn or 'southamerica' in nn or 'europe' in nn or '_10' in nn):
                            cur.execute('DROP TABLE %s;'%nn)
            for nn,row in df.iterrows():
                    if row['type']=='table' and    (nn.startswith('lookup_osmnode') or 'northamerica' in nn or 'asia' in nn or 'southamerica' in nn or 'europe' in nn or '_10' in nn):
                            cur.execute('DROP TABLE %s;'%nn)

            df= cur.report_all_table_sizes()
            print(df)

    if runmode in ['test']:
        db=dbConnection('processing', True)
