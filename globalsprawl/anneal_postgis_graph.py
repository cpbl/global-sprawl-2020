#! /usr/bin/python
import sys
import time
import networkx as nx
import re
import osm_networkx as osmnx
import osmTools as osmt
from osm_config import defaults
import postgres_tools as psqlt

"""
3-step annealing process.
Step 1: Delete all small loops made up of two parallel edges.

Step 2: Create instance of Anneal that uses networkx to build graphs of
long chains of edges with degree-2 spt and ept clusters, collapse the edges into
one edge, and update the edges table with the new edges.

Step 3: anneal_with_postgis combines these chains with incident edges that have
EITHER spt OR ept cluster of degree 2.
"""

class Anneal:
    def __init__(self,continent,clusterRadius,overwriteLogger=True):
        self.continent=continent
        self.cluster=int(clusterRadius)
        self.tablename='edges_%s_%s'%(continent,clusterRadius)
        self.nodestablename=self.tablename.replace('edge','node')
        self.logger=osmt.osm_logger('annealing'+self.tablename,verbose=True,timing=True,overwrite=overwriteLogger)
        self.db=osmt.pgisConnection(verbose=True,logger=self.logger)
        self.edge_ids=[]

    def str_reps(self,dosubs,ss):
        for a,b in list(dosubs.items()):
            ss=ss.replace(a,b)
        return(ss)

    # Only for debugging/development, really, store backup versions of the table before annealing.
    def make_backup_tables(self):
        self.logger.write("Making backup version of %s before annealing"%self.tablename)

        cmd="""
        DROP TABLE IF EXISTS """+defaults['osm']['processingSchema']+""".withdeg2_TABLENAME;
        CREATE TABLE """+defaults['osm']['processingSchema']+""".withdeg2_TABLENAME AS
        TABLE TABLENAME;

        DROP TABLE IF EXISTS """+defaults['osm']['processingSchema']+""".withdeg2_NODESTABNAME;
        CREATE TABLE """+defaults['osm']['processingSchema']+""".withdeg2_NODESTABNAME AS
        TABLE NODESTABNAME;
        """
        self.db.execute(cmd.replace("TABLENAME",self.tablename).replace("NODESTABNAME",self.nodestablename))
        self.db.fix_permissions_of_new_table('withdeg2_'+self.tablename)
        self.db.fix_permissions_of_new_table('withdeg2_'+self.nodestablename)

    #Neither anneal_with_nx or anneal_postgis_table can process the special case of two parallel edges that form a loop.
    #Here, we process these separately, and yield one edge--a self-loop with one degree-2 node.
    #See issue #141.
    def anneal_two_edge_loops(self):
        self.logger.write("Begin Step 1 (anneal two-edge loops)")

        continent,cluster=self.tablename.split('_')[1:]

        #Identify all edges which have spt_degree and ept_degree=2, then reduce this sample to just parallel edges
        #(one edge for each set of parallel edges)
        self.db.execute("""
        SELECT edge_id from %s WHERE spt_degree=2 AND ept_degree=2
        ;"""%self.tablename)
        edgelist=[ i[0] for i in self.db.fetchall() ]
        if not edgelist:
            return
        luEdges=self.db.find_parallel_edges(continent=continent,cluster=cluster,unique=True,qgisFormat=False,edgelist=edgelist)
        if luEdges:
            luEdges=[ i[0] for i in luEdges ]

        self.logger.write(' Found {} luEdges (two-edge loops) to anneal'.format(len(luEdges)))
        
        #For each lookup edge, merge its geometry with its parallel neighbour, and drop the parallel neighbour.
        if len(luEdges)>0:
            # Drop the indexes on ept_cluster_id and sept_cluster_id, because that affects update performance and they are not used (only edge_id is)
            for idx in ['spt','ept']:
                self.db.execute('DROP INDEX IF EXISTS %s_idx_%s_cluster_id;' % (self.tablename, idx))
        
            cmdDict={ 'edgestn':self.tablename,'nodestn':self.nodestablename,'luedges': ','.join([str(ee) for ee in luEdges])}
            cmd = """WITH  neighbour as ( SELECT edge_id,spt_cluster_id,ept_cluster_id,nocars,geom
                         FROM %(edgestn)s WHERE (least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id)) in
                         (
                         SELECT least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id)
                         FROM %(edgestn)s WHERE edge_id IN (%(luedges)s)
                         ) and edge_id NOT IN (%(luedges)s)
                         )
                         UPDATE %(edgestn)s edges
                         SET geom=ST_MakeLine(edges.geom,CASE WHEN neighbour.ept_cluster_id=edges.spt_cluster_id THEN neighbour.geom
                         ELSE ST_Reverse(neighbour.geom) END),
                         length_m=ST_Length(ST_Union(edges.geom,neighbour.geom)::geography)::INTEGER,
                         ept_cluster_id=edges.spt_cluster_id,
                         nocars = least(edges.nocars,neighbour.nocars)
                         FROM neighbour
                         WHERE edges.edge_id IN (%(luedges)s) 
                          AND (neighbour.ept_cluster_id=edges.spt_cluster_id OR neighbour.spt_cluster_id=edges.spt_cluster_id)
                         RETURNING neighbour.edge_id,
                                   CASE WHEN neighbour.spt_cluster_id=edges.spt_cluster_id THEN neighbour.ept_cluster_id ELSE neighbour.spt_cluster_id END
                         ;""" %cmdDict
            result = self.db.execfetch(cmd)

            # Drop all edges and nodes that are now redundant with anneal
            cmdDict['dropEdges'] = ','.join([str(rr[0]) for rr in result])
            cmdDict['dropNodes'] = ','.join([str(rr[1]) for rr in result])

            self.db.execute("""DELETE FROM %(edgestn)s WHERE edge_id IN(%(dropEdges)s);"""%cmdDict)
            self.db.execute("""DELETE FROM %(nodestn)s WHERE cluster_id IN(%(dropNodes)s);"""%cmdDict)

            # Recreate the indexes on spt_ and ept_cluster_id, that we dropped earlier
            self.db.create_indices(self.tablename,non_unique_columns=['spt_cluster_id','ept_cluster_id'])

    def get_connected_components(self):
        """
        Returns a list of subgraphs of the connected components of an nx graph constructed from edges to anneal.
        These are all edges where both the starting and ending nodes have degree 2.
        This EXCLUDES parallel edges. The nx simple graph would lose "self-loops" made
        up of two edges and replace them with an edge only half the length. anneal_postgis_graph
        should take care of these edges.
        """

        self.logger.write("Get connected components of networkx graph of edges incident to ONLY deg.2 nodes")
        whereclause="""
        WHERE (least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id)) NOT IN
            (
                SELECT least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id)
                FROM %s
                GROUP BY least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id)
                HAVING COUNT(*)>1
            ) AND spt_degree = 2 AND ept_degree = 2"""%self.tablename
        G,edges,edge_ids,eid,egeom=osmnx.build_osmnx_graph_from_edges_table(continent=self.continent,cluster=self.cluster,logger=self.logger,where=whereclause)
        self.edge_ids=edge_ids
        #return list(nx.connected_component_subgraphs(G)),eid  #old networkx
        return list((G.subgraph(c) for c in nx.connected_components(G))),eid

    def make_temp_table(self):
        st=time.time()
        tn="%s.tmpanneal_%s_%d"%(defaults["osm"]["processingSchema"],self.continent,self.cluster)
        assert self.db.execfetch('SELECT ST_SRID(geom) FROM %s LIMIT 1;' % self.tablename)[0][0]==4326
        cmd = """DROP TABLE IF EXISTS %s;
    CREATE TABLE %s(
        edge_id bigint NOT NULL,
        spt_cluster_id bigint NOT NULL,
        ept_cluster_id bigint NOT NULL,
        nocars boolean NOT NULL,
        length_m integer,
        geom geometry(Linestring,4326)); """%(tn,tn)
        self.db.execute(cmd)
        
        # index should be there on first pass, but likely not on second pass. See also #451
        self.db.create_indices(self.tablename, key='edge_id', forceUpdate=False)
        
        subgraphs,eid=self.get_connected_components()        
        self.logger.write( "Took %d seconds to find %d subgraphs"%(time.time()-st,len(subgraphs)) )
        sgcounter=0
        for sg in subgraphs: 
            if nx.__version__[0]=='1':
                deg1nodes = [nn[0] for nn in sg.degree_iter() if nn[1]==1]
            else:
                deg1nodes = [nn[0] for nn in list(sg.degree) if nn[1]==1]
            startnode = deg1nodes[0] if len(deg1nodes)>0 else sg.edges()[0][1]  # second case is for a loop
            if len(deg1nodes) > 0:
                assert deg1nodes.count(startnode) == 1
            if nx.__version__[0]=='1':
                edges = [(startnode,sg.neighbors(startnode)[0])]
            else:
                edges = [(startnode, list(sg.neighbors(startnode))[0])]
            for ii in range(sg.number_of_edges()-1):
                sn = edges[-1][-1]
                if nx.__version__[0]=='1':
                    en = [nn for nn in sg.neighbors(sn) if nn!=edges[-1][0]]
                else:
                    en = [nn for nn in list(sg.neighbors(sn)) if nn!=edges[-1][0]]
                assert len(en)==1
                edges+=[(sn,en[0])]
            if len(deg1nodes)>0: assert edges[-1][1] in deg1nodes
            
            edgeIds = [sg.edge[ee[0]][ee[1]]['edge_id'] for ee in edges]
            startClusters = [ee[0] for ee in edges]

            # see http://stackoverflow.com/questions/866465/order-by-the-in-value-list
            newData = ','.join([str((int(eid), int(sc), ii+1)) for ii, (eid, sc) in enumerate(zip(edgeIds, startClusters))])
            eptCluster = edges[-1][0] if startClusters[-1]==edges[-1][1] else edges[-1][1]
            cmdDict = {'values':newData,'spt':startClusters[0],'ept':eptCluster,'new_edge_id':edgeIds[0],'tn':tn,'table':self.tablename}
            cmd = '''INSERT INTO %(tn)s 
                    SELECT %(new_edge_id)s, %(spt)s, %(ept)s, bool_or(nocars), ROUND(SUM(ST_Length(geom::geography))), ST_MakeLine(geom) FROM 
                        (SELECT nocars, CASE WHEN spt_cluster_id=startcluster then geom ELSE ST_Reverse(geom) END AS geom
                         FROM %(table)s AS edges
                         JOIN (values %(values)s) AS t1 (edge_id,startcluster,ordering)
                         ON edges.edge_id = t1.edge_id
                         ORDER BY t1.ordering) t2
                    RETURNING length_m=ROUND(ST_Length(geom::geography))::integer''' % cmdDict
            retVal = self.db.silently_execfetch(cmd)
            sgcounter+=1
            try:
                assert retVal[0][0] is True   # length of annealed edge should be the same as the sum of the original lengths
                if sgcounter%10000==0:
                    self.logger.write( "      Done {}/{} subgraphs".format(sgcounter,len(subgraphs)))
            except:
                self.logger.write( "Different lengths calculated for annealed edge %(new_edge_id)s and the union of its non-annealed components"%cmdDict )
        return tn
    
    def update_nodes_table(self):
        """
        Only keep nodes that are the spt or ept cluster for some edge. Drop indices and recreate them afterwards to speed up.
        """
        self.logger.write("Update nodes table")
        nodesTable = self.tablename.replace('edge','node')
        cmdDict = {'nodesTable':nodesTable, 'edgesTable':self.tablename, 'tmpTable':nodesTable+'_tmp_merge'}
        self.db.execute(' DROP TABLE IF EXISTS %(tmpTable)s' % cmdDict)
        cmd = '''CREATE TABLE %(tmpTable)s AS 
                    SELECT nodes.* FROM %(nodesTable)s AS nodes LEFT JOIN 
                     ( SELECT spt_cluster_id AS cid FROM %(edgesTable)s
                            UNION
                       SELECT ept_cluster_id AS cid FROM %(edgesTable)s ) AS edges
                  ON nodes.cluster_id=edges.cid
                  WHERE   edges.cid IS NOT NULL;''' % cmdDict
                  
        self.db.execute(cmd)
        oldN = self.db.execfetch('SELECT COUNT(*) FROM %(nodesTable)s' % cmdDict)[0][0]
        newN = self.db.execfetch('SELECT COUNT(*) FROM %(tmpTable)s' % cmdDict)[0][0]
        self.logger.write( 'Dropped %d of %d nodes from %s that are not present in edges table' % ((oldN-newN), oldN, nodesTable ))
        
        self.db.execute(' DROP TABLE %(nodesTable)s;' % cmdDict)
        self.db.execute(' ALTER TABLE %(tmpTable)s RENAME TO %(nodesTable)s;' % cmdDict)

        self.db.create_primary_key_constraint(nodesTable,'cluster_id')
        self.db.create_indices(nodesTable, key='cluster_id', geog=True)

    def merge_annealed_edges_into_table(self,mergefrom=None):
        """
        Delete edges from edges table with deg.2 nodes on each end. Replace these with edges in annealed nx graph.
        """  

        self.logger.write("Begin Step 2 (anneal long chains with networkx)")

        if not mergefrom:
            mergefrom=self.make_temp_table()
    
        #If we're not replacing any edges, just return.
        if not self.edge_ids:
            return

        self.logger.write("Update edges table with new data")
        #First add columns spt_degree,ept_degree and length_m to mergefrom. 
        cmd = """ALTER TABLE mergefrom ADD COLUMN spt_degree integer,
                ADD COLUMN ept_degree integer;
        UPDATE mergefrom SET spt_degree = 2;
        UPDATE mergefrom SET ept_degree = 2;""".replace("mergefrom",mergefrom)

        self.logger.write( cmd )
        self.db.execute(cmd)
        
        cmdDict = {'edgesTable':self.tablename, 'mergeTable':mergefrom, 'colNames':'edge_id, geom, spt_cluster_id, ept_cluster_id, spt_degree, ept_degree, nocars, length_m'}
        # where clause here is the opposite of that used in get_connected_components()
        self.db.execute('DROP TABLE IF EXISTS %(edgesTable)s_union_tmp' % cmdDict)
        cmd = '''CREATE TABLE %(edgesTable)s_union_tmp AS
                  SELECT %(colNames)s FROM %(mergeTable)s 
                  UNION
                  SELECT %(colNames)s FROM 
                    (SELECT * FROM %(edgesTable)s 
                       WHERE NOT ((least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id)) NOT IN
                            ( SELECT least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id)
                                FROM %(edgesTable)s
                            GROUP BY least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id)
                        HAVING COUNT(*)>1
                        ) AND spt_degree = 2 AND ept_degree = 2)) t1;''' % cmdDict
        self.db.execute(cmd)
        oldN = self.db.execfetch('SELECT COUNT(*) from %s' % self.tablename)[0][0]
        self.db.execute('DROP TABLE %s' % self.tablename)
        self.db.execute('DROP TABLE %s' % mergefrom)
        self.db.execute('ALTER TABLE %(edgesTable)s_union_tmp RENAME TO  %(edgesTable)s' % cmdDict)
        newN = self.db.execfetch('SELECT COUNT(*) from %s' % self.tablename)[0][0]

        self.logger.write( 'Annealing dropped %d edges from %s' % (oldN-newN, self.tablename) )

        #Update nodes table
        self.update_nodes_table()

    #Finish up the annealing process, e.g. anneal the ends of long chains, in Postgres
    def anneal_postgis_table(self):
        """
        Obeying the two rules:
        - never anneal two edges which each have two degree-2 nodes
        - when one edge of a pair has two degree 2 nodes, keep its edge_id and drop the other's.

        We repeatedly edit the edges table and corresponding nodes table, annealing edges which have only one degree-2 node. 
        This should properly reduce loops to simple self loop edges.

        Assume we already have degrees calculated and listed in the edges table; this is our reformed graph already. Currently, this also updates length_m.
        """

        self.logger.write("Begin Step 3 (anneal in Postgres)")

        # The following psql command string is used eight times in each iteration, with different values substituted in. For instance, the first two times it's:
        # dict(KEEPSTART='spt', KEEPEND='ept', DROPSTART='spt',DROPEND='ept', NEWGEOM='ST_REVERSE(ST_MAKELINE(ST_REVERSE(wl.keepgeom),ST_REVERSE(wl.dropgeom)))' ),
        # with PARITY taking the value '<' or '>'.
        # "KEEP" in KEEPEND and KEEPSTART refers to the *edge* whose i.d. will be kept. "DROP" refers to the other edge.
        # In this language, the KEEP edge's END is connected to (same as) the DROP edge's START.
        # This keeps the psql syntax relatively simple, but lets us look for the four cases in which the start or end point of one edge shares a degree-2 node with the start or end point of another edge.
        varcmd="""
        --   keepedge's KEEPEND is connected to dropedge's DROPSTART:
        WITH worklist as (
        SELECT edges.edge_id as keepedge, edges.KEEPEND_cluster_id as dropnode, edges.KEEPSTART_cluster_id as spt, edges.geom as keepgeom, edges.length_m as keeplength, edges.nocars as keepnocars,
            neighbours.edge_id as dropedge, neighbours.DROPEND_cluster_id as dept, neighbours.DROPSTART_cluster_id  as neighDropnode, neighbours.DROPEND_degree as edegree, neighbours.geom as dropgeom, neighbours.length_m as droplength, neighbours.nocars as dropnocars
        FROM TABLENAME as edges, TABLENAME as neighbours  
            WHERE 
                -- Second equality below is redundant, but I thought might restrict match set more efficiently
                edges.KEEPEND_degree =2 AND neighbours.DROPSTART_degree = 2 AND 
                edges.KEEPEND_cluster_id=neighbours.DROPSTART_cluster_id   AND 
                -- n.b. following line is wrong. It shoudl be the DROPEND.  But this fix requires rejigging all the line stitching. So let's fix the start/end problem first.
                 neighbours.DROPEND_degree !=2 AND   -- edges.KEEPSTART_degree !=2 AND  --
                 edges.edge_id PARITY neighbours.edge_id    -- a "!=" avoids self matching. The inequality avoids including pairs twice
        )  

        UPDATE TABLENAME
            SET geom = NEWGEOM, 
            KEEPEND_cluster_id=wl.dept ,  
            KEEPEND_degree = wl.edegree,
            nocars = LEAST(keepnocars, dropnocars),
            length_m= wl.keeplength + wl.droplength
        FROM worklist AS wl
        WHERE edge_id=wl.keepedge
        RETURNING wl.dropnode,wl.dropedge, wl.spt,wl.keepedge 
        ----  Following for debugging only
        ,ST_ASTEXT(wl.keepgeom),ST_ASTEXT(wl.dropgeom), ST_ASTEXT(NEWGEOM)
        ;"""

        tabnames=dict(TABLENAME=self.tablename, NODESTABNAME=self.nodestablename)

        allids,deletenodes,ids, lastLen, iIter =[], [],[-9999], 0, 0
        while len(ids)> 0:
            ids=[]
            lastLen=0
            iIter+=1
            # Loop over the psql command string four times, to catch the four possibilities of start/end connections
            for roundname,ssubs in sorted({'1': dict(KEEPSTART='spt', KEEPEND='ept', DROPSTART='spt',DROPEND='ept', NEWGEOM='ST_MAKELINE(wl.keepgeom,wl.dropgeom)' ),
                                       '2': dict(KEEPSTART='spt', KEEPEND='ept', DROPSTART='ept',DROPEND='spt', NEWGEOM='ST_MAKELINE(wl.keepgeom,ST_REVERSE(wl.dropgeom))' ),
                                       '3': dict(KEEPSTART='ept', KEEPEND='spt', DROPSTART='spt',DROPEND='ept', NEWGEOM='ST_MAKELINE(ST_REVERSE(wl.dropgeom),keepgeom)' ),
                                       '4': dict(KEEPSTART='ept', KEEPEND='spt', DROPSTART='ept',DROPEND='spt', NEWGEOM='ST_MAKELINE(wl.dropgeom,keepgeom)' ),
                                }.items()):
                ssubs.update(tabnames)
                # Loop over the psql command string two time each, taking cases when the edge_id to keep is smaller or larger than the one we're going to throw out.
                for parity in ['<','>']:
                    ssubs.update(dict(PARITY=parity))
                    self.logger.write('Starting iteration %d type %s parity %s'%(iIter,roundname,parity))
                    outs=self.db.execfetch(self.str_reps(ssubs,varcmd))
                    ids+= [rr[3] for rr in outs]
                    lastLen+=len(outs)
                    self.logger.write('        --> Iteration %d type %s parity %s Modified %d edges'%(iIter,roundname,parity, len(outs)))
                    if outs:
                        # Print the first and last coordinate of initial and final edges for the first row processed;
                        print(outs[0][0:4])
                        print(outs[0][4].split(',')[0], ' to ', outs[0][4].split(',')[-1])
                        print(outs[0][5].split(',')[0], ' to ', outs[0][5].split(',')[-1])
                        print(outs[0][6].split(',')[0], ' to ', outs[0][6].split(',')[-1])
                        # Drop delete edges; drop deleted nodes, before we go on to the next iteration
                        self.db.delete_many_rows(self.tablename,'edge_id',[ rr[1] for rr in outs])
                        deletenodes+=[rr[0] for rr in outs]

            allids+=[ids]
            lenallids=[len(cc) for cc in allids]
            self.logger.write('Altogether, sequence of numbers of modified edges (total %d) by round: %s'%(sum(lenallids), str(lenallids)))
            if deletenodes:
                self.db.delete_many_rows(self.nodestablename,'cluster_id',deletenodes)

    def delete_selfloops(self):
        self.db.execute("""DELETE FROM %s USING %s
        WHERE spt_degree=2 AND ept_degree=2 AND spt_cluster_id=ept_cluster_id 
        AND spt_cluster_id=cluster_id;""" % (self.nodestablename, self.tablename))
        nRows = self.db.cursor.rowcount
        self.logger.write('Dropped %d nodes from isolated selfloops' % nRows)
        
        self.db.execute("""
        DELETE FROM %s 
        WHERE spt_degree=2 AND ept_degree=2 AND spt_cluster_id=ept_cluster_id ;"""%self.tablename)
        nRows = self.db.cursor.rowcount
        self.logger.write('Dropped %d isolated selfloops' % nRows)

    def log_selfloops(self):
        self.db.execute("""
        SELECT edge_id,ept_degree,spt_cluster_id,ept_cluster_id FROM %s
        WHERE spt_degree=2;"""%self.tablename)
        for loop in self.db.fetchall():
            assert loop[1]==2
            assert loop[2]==loop[3]
            self.logger.write( 'Isolated loop %d has a deg.2 endpoint'%loop[0] )

    def anneal(self, makeBackup=False):
        if makeBackup: self.make_backup_tables()
        self.anneal_two_edge_loops()
        self.merge_annealed_edges_into_table()
        self.anneal_postgis_table()
        self.delete_selfloops()
        self.log_selfloops()  # should not be any left

if __name__=='__main__':
    testAnneal=Anneal(continent=sys.argv[1],clusterRadius=sys.argv[2])
    if "totable" in sys.argv:
        testAnneal.make_temp_table()
    else:
        testAnneal.anneal()
