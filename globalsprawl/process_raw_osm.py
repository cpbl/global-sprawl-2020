#!/usr/bin/python
# coding=utf-8


"""
If you need to insert test/mock data into this work flow, substitute only the create_tables_from_osm_download(region, sourcefile) step. That is, look at the segs_table format which that produces, and simulate/reproduce that.
"""

"""
Explanation of output
---------------
Creates tables in topology schema for each input file as follows:
 - filename_road_segs: road segments with nodal degree for each buffer distance.
    E.g. for a buffer of 10m, the fields are ept_val_10 and spt_val_10 (spt = start point, ept = end point)
 - filename_buff_xx: intersections buffered to a distance of xx.
   The field degree denotes the nodal degree of the CLUSTER of overlapping buffers (clusterid gives the unique cluster number)

Nodal degree is calculated as the number of segment endpoints in a cluster of buffers. Usually, this is just the number of segments
that intersect the buffer, but the following refinements are sometimes relevant:
- segments that simply pass through a buffer but don't start or end there are NOT counted
- segments that start AND end in the same cluster and have a length <= c*bd are NOT counted (c is a constant, normally 2; bd is the buffer distance)
- segments that start AND end in the same cluster and have a length > c*bd are counted TWICE

Note that osm2po is a dependency, and the config file needs to be modified (i.e., don't do a fresh download!)
This step should be included here

We wait to reproject the output until everything is done.
The reason is that the projection will affect the distortion of the buffers (or distance calculations). So we should do the analysis in an equal-area projection that also minimizes that distortion, or at least doesn't have a bias based on latitude.
Projections are defined in the config file

"""
from osm_config import paths,defaults
import os, shutil, sys, platform, time, gc, logging, psycopg2, psycopg2.extras, io, multiprocessing
import os, sys, datetime
import osmTools as osmt
import numpy as np
import scipy.sparse as sps
import postgres_tools as psqlt
from cpblUtilities.parallel import runFunctionsInParallel
from osmTools import  define_table_names

osm2poJarFn = paths['input'] +  'osm/osm2po/osm2po-core-4.8.8-signed.jar'

def delete_scratch_files(region):
    """ 
    Deletes the interim files created by osm2po
    Only saves the graph and the log files (copies these over to a new directory)
    """
    fn = region + '_2po.gph'
    os.rename(paths['scratch']+region+'/'+fn, paths['scratch']+'osmGraphs/'+fn)
    fn = region + '_2po.log'
    os.rename(paths['scratch']+region+'/'+fn, paths['scratch']+'osmLogs/'+fn)
    shutil.rmtree(paths['scratch']+region)  # delete everything else


def osm_raw_filename_from_continent(cc):
    """ Some continents have hyphens in their raw file name, but not in our continent names"""
    return( cc.replace('america','-america').replace('oceania','-oceania')+'-latest.osm.pbf')
    
def move_osm_data_for_reprocessing(regions=None):
    if regions is None:
        print(('mv --no-clobber %sosm/Data/Done/*pbf %sosm/Data/ '%(paths['input'], paths['input'])))
    else:
        for rr in regions:
            os.system('mv --no-clobber %sosm/Data/Done/%s %sosm/Data/ '%(paths['input'], osm_raw_filename_from_continent(rr),paths['input']))

class raw_osm_processor():
    """
        Do only the first step of importing data from OSM to make a "segs" table. Currently, this depends on osm2po, an obscure and closed code whose advantages are ...?? 
             - Ability to take on the largest files.
             - Does the chopping up of ways into edges for us (but that's easy to do ourselves)

     This step is independent of clusterRadii.

    To do: 
       Possibly a method for creating indices on segs_tables should be accessible to the outside. It would get called in the create junctions step, which updates the segs table.
    """

    def __init__(self, regions=None):
        """
        The set of regions to be processed, regions, defaults to the list of continents specified in config files.
        """
        self.db = osmt.pgisConnection(verbose=True, schema=defaults['osm']['osmSchema'])
        #self.srs = defaults['osm']['srid_equaldistance'] # equidistant is best for analysis. No! See #197
        self.logger=osmt.osm_logger('process_raw_osm', verbose=True, timing=True)
        for newDir in ['osmGraphs']:
            if not os.path.exists(paths['scratch']+newDir): os.mkdir(paths['scratch']+newDir)
        if not os.path.exists(paths['input'] + 'osm/Data/Done'): os.mkdir(paths['input'] + 'osm/Data/Done')
        
        # Check input files
        if regions is None:
                regions=defaults['osm']['continents']
        todop,donep=paths['input']+'osm/Data/',paths['input']+'osm/Data/Done/'
        #filenames=[(cc,cc.replace('america','-america').replace('oceania','-oceania')+'-latest.osm.pbf') for cc in regions]
        self.continents=[]
        for continent in regions:
            fn=osm_raw_filename_from_continent(continent)
            if os.path.exists(todop+fn):
                self.logger.write('Continent %s to be processed'%continent)
                self.continents+=[[continent,todop+fn]]
            elif os.path.exists(donep+fn):
                self.logger.write('Continent %s already completed'%continent)
            else:
                self.logger.write('Continent %s missing! Data file not found in Data/ nor Done/'%continent)
        self.buf_list = defaults['osm']['clusterRadii']     # Radius of buffers created around nodes (meters)
        # self.process_all()


    def create_tables_from_osm_download(self,region, sourcefile):
        """
        For a given region, use osm2po to segmentize and load into postgres
        Creates a table of segments (seg_table)
        """

        # DEFINE TABLE NAMES AND DROP OLD TABLES
        table_names=define_table_names(region)
        for table in [tt for tt in self.db.list_tables(schema='osmdata') if region in tt]: # This will only drop in the "osmdata" schema
            self.db.execute('DROP TABLE IF EXISTS %s' % table) 
        seg_table,junc_table=table_names['seg_table'],table_names['junc_table']

        # LOAD WAYS FROM OSM DATA
        self.logger.write( "Creating road edges for region %s" % region )
        # Note: osm2po.config specifies what types of ways to load. Right now, just car. Change through editing wtr.finalMask = car in config file
        # Note that we drop service roads, footpaths, etc. right now. This is easy to change in the config file
        # This config file should come back to the git repo.
        ######if not os.path.exists(paths['scratch']+region): os.mkdir(paths['scratch']+region)
        tileSize = '90x90,1' if 'planet' in region or 'world' in region else 'x' # see issue #175 # Works for June 2017 planet file, on server with 756G RAM (ie, avoids "Message: java.lang.ArrayIndexOutOfBoundsException: Array index out of range" error in OSM2PO)
        tileSize = '60x60,.8' if 'planet' in region or 'world' in region else 'x' # see issue #175  # 
        #tileSize = '45x45,5' if 'planet' in region or 'world' in region else 'x' # see issue #175  # I think this means should give tiles 45 degrees by 45 degrees with an overlap of 5 degrees for edges that cross the border of the tiles. I'm not sure that there's even any compromise in the quality of the result, doing this??
        assert os.path.exists('osm2po.config') # otherwise, config file will not be used if you are running osm2po from a different directory
        
        # whether to include just car ways, or bike/foot ones and/or track/service too
        assert all( [w in ['False','bike','foot', 'track', 'service'] for w in defaults['osm']['includeExtraWays']])
        ors = '|'.join([w for w in defaults['osm']['includeExtraWays'] if w in ['bike','foot', 'track', 'service']])
        if ors: ors = '|'+ors
        finalMask = "wtr.finalMask='car" + ors + "'"
        
        cmd = "nice -n 19 java -Xmx100g -jar %s prefix=%s workDir='%s' %s tileSize=%s cmd=c '%s'" % (osm2poJarFn, region, paths['scratch']+region, finalMask, tileSize, sourcefile)
      
        self.logger.write(cmd)
        os.system(cmd)

        # prepend change to search path. This forces the table to be written to preferred schema, not public (the default). So consistent with other methods
        with open(paths['scratch']+region+'/'+region+'_2po_4pgr.sql', "r+") as f:
            old = f.read() # read everything in the file
            f.seek(0) # rewind
            f.write("SET search_path = osmdata,public;\n" + old) # write the new line before

        self.logger.write('\tExecuting SQL file created by osm2po')
        self.db.execute_sql_file("%s/%s_2po_4pgr.sql" % ( paths['scratch']+region,region) , nice=True)  # calls psql to run the sql file produced by osm2po

        # change permissions of new table from amb or cpbl to osmusers
        self.db.fix_permissions_of_new_table('%s_2po_4pgr'%region)
        
        # Rename the table (delete the _2po_4pgr suffix), project and change the geom_way column to geom.
        # Note: don't project. Keep in 4326
        self.logger.write("\tRenaming things")
        self.db.execute('ALTER TABLE %s_2po_4pgr RENAME TO %s' %(region, seg_table))
        # We want different names for the constraints and indices made by osm2po. Use postgres_tools' built-in function to name it. The spatial index is already correctly named.
        self.db.execute('ALTER TABLE '+seg_table+' DROP CONSTRAINT pkey_'+region+'_2po_4pgr  ') 

        for vv in ['target','source']:
                self.db.execute('ALTER INDEX idx_'+region+'_2po_4pgr_'+vv+'  RENAME TO '+seg_table+'_idx_'+vv)
        #self.db.execute('ALTER INDEX idx_'+region+'_2po_4pgr_'+vv+'  RENAME TO '+seg_table+'_idx_'+vv)
        #self.db.execute('''ALTER TABLE %s ALTER COLUMN geom_way TYPE Geometry(LineString, %d) USING ST_Transform(geom_way, %d);''' % (seg_table, self.srs, self.srs))   #  
        self.db.execute('''ALTER TABLE %s RENAME geom_way TO geom''' % seg_table)
        self.db.execute('''ALTER TABLE %s RENAME id TO edge_id''' % seg_table)
        
        # drop edges that hit the edges of the map. Avoids projection problems later on during buffering
        assert self.db.execfetch('SELECT ST_Srid(geom) FROM %s LIMIT 1' % seg_table)[0][0]==4326
        self.db.execute('''DELETE FROM %s WHERE ABS(ST_X(ST_StartPoint(geom))) > 179.9999'''  % seg_table)
        self.db.execute('''DELETE FROM %s WHERE ABS(ST_X(ST_EndPoint(geom)))   > 179.9999'''  % seg_table)
        
        self.db.create_primary_key_constraint(seg_table,'edge_id')

        self.db.create_indices(seg_table,key='edge_id', non_unique_columns=None, gadm=False, geom=True)

        # Drop duplicate edges. Fixes #266
        dups = self.db.execfetch('''DELETE FROM %s a USING %s b 
                           WHERE ST_Equals(a.geom,b.geom)
                           AND a.edge_id<b.edge_id
                           RETURNING a.osm_id;''' % (seg_table, seg_table))
        self.logger.write('Dropped %d duplicate ways from %s' % (len(dups), seg_table))   
        with open(paths['scratch']+'duplicates_'+seg_table+'.txt','w') as f:
            f.write('\n'.join([str(dd[0]) for dd in dups]))
        print('Wrote duplicate osm ids to %sduplicates_%s.txt' % (paths['scratch'], seg_table))
        
        # drop edges that go out to (215,215) in incomplete continents. This seems to be an artefact of osmium
        # See https://gis.stackexchange.com/questions/241152/how-to-fix-referential-integrity-in-osmium
        if 'incomplete' in region:
            cmd = '''DELETE FROM '''+seg_table+''' 
                     USING ST_GeomFromText('LINESTRING(213 216,216 213)', 4326) AS line
                     WHERE st_intersects(geom, line);'''
            self.db.execute(cmd)
            self.logger.write('Dropped %d problem ways in %s' % (self.db.cursor.rowcount, seg_table))
        

    def process_all(self):
        """ Parallelize this: """
        funcs,names=[],[]
        for region,fn in self.continents:
            funcs+=[[self.process_one_region, [region,fn]]]
            names+=['process_one_region: '+region]
        runFunctionsInParallel(funcs,names=names, parallel=True)
    def process_one_region(self,region,sourcefile):
        self.create_tables_from_osm_download(region, sourcefile)
        os.system('mv '+sourcefile+' '+paths['input']+'osm/Data/Done/')

        
if __name__ == '__main__':
        pass
