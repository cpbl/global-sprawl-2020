#!/usr/bin/python
"""
Calculates connectivity metrics for edges and nodes

"""
from osm_config import paths,defaults
SP=paths['scratch']
WP=paths['working']
import os,time,random,string
import numpy as np
import networkx as nx
import pandas as pd
import math
from cpblUtilities.parallel import runFunctionsInParallel
from cpblUtilities import dgetget,dsetset,orderListByRule
import osmTools as osmt
import postgres_tools as psqlt
import gadm
from osm_networkx import Graph as osmnGraph
import create_roadgraph_tables as crt
import osm_lookups

class regionGraphGetter():
    """The interface provides a way to step through all countries/sub-national-regions that
    are (partly) in the continent, and to find the optimal spatial
    scale (GADM level) at which to analyze them in a piecewise
    fashion. It retrieves edges and nodes for each region, as well as
    edges and nodes for all the neighbouring regions (at the same
    scale) around each.

    Because of the structure of our OSM data (tables/files), this is
    instantiated for just one continent. In addition, one must choose
    analysis based on 7-m or 10-m (or etc) radius clustering at the
    outset. (At some point, we should possibly run the entire globe as
    a single continent)

    A list of all the regions served is stored in the class object.

    The implementation relies on generators, retrieving data only when
    needed.  The class stores certain information, when first
    required/retrieved, to avoid repeating lookups.

    A major tuning parameter is "max_nodes", which sets the scale of regions sought. When the number of nodes in a core region plus its neighbours exceeds max_nodes, a higher-resolution scale of analysis is pursued (if available).

    Update: This is now instantiated in parallel separately for each pGADM (used to be for each ISO). 
    """
    def __init__(self,continent=None, pGADM = None, clusterradius=None, max_nodes=1e5, logger=None, pGADM_lookups=False):
        """
        This can be instantiated (by specifying continent but not pGADM) in its normal mode in which regions are limited to one continent, or in pGADM mode, in which case continent can be specified but defaults to "planet".

        pGADM_lookups = True means that a special edge lookup table has been created just for this pGADM. Therefore, do not check for pGADMs in WHERE queries, and use the modified lookup table name (append pGADM to the normal edge lookup table).
        """
        if logger is None:
            logger=osmt.osm_logger(None,verbose=True, timing=True)
        self.logger=logger
        self.db = psqlt.dbConnection(verbose=False, logger=logger)  
        self.clusterradius=clusterradius if clusterradius is not None else '10'
        if clusterradius is None:
            logger.write(" You didn't specify a cluster radius. Let's choose 10.")
        if continent is None:
            logger.write(" You didn't specify a continent. Using planet.")
        self.continent= continent if continent is not None else 'planet'
        self.pGADM = pGADM # This is optional (ie can be None), introduced to allow pGADM-level parallelization
        logger.write(" RegionGraphGetter running in {} mode.".format('pGADM' if self.pGADM else 'continent'))
        self.gadmlu=gadm.regions_and_neighbours() # A region adjacency lookup which doesn't reload what it's already loaded
        self.roughNodeCounts={} # Store approx size of regions
        self.max_nodes=max_nodes
        self.regions_served=[]
        self.pGADM_lookups = pGADM_lookups

    def rough_count_of_nodes(self,GADM_ids):
        """
        Assess size of a region by the number of nodes (actually, osmnodes, at the moment).
        We store results, so as not to duplicate lookups.  By default (all_at_once_on_the_fly=True), the first time we are lacking a count, we do all the counts for that continent (or at that level of GADM IDs.

        This is used because, for speed optimization, we want to go to finer spatial resolution when the number of nodes is too large. Also, we want to sample nodes based on their density.
        """
        # Strip trailing zeros from the region:
        GADM_ids=gadm.strip_zeros_from_id(GADM_ids)
        idlevel=len(GADM_ids)-1
        if idlevel not in self.roughNodeCounts:
            self.roughNodeCounts[idlevel]={}
            lookupTableName=osm_lookups.GADM_roadgraph_lookups('nodes', self.clusterradius, continent=self.continent).get_lookup_tablename()
            ids=defaults['gadm']['ID_LEVELS'][:idlevel+1]
            self.logger.write('Counting nodes in ALL GADM regions at %s level (and storing results for future use)'%defaults['gadm']['ID_LEVELS'][idlevel])
            #for ISO in gadm.regions_and_neighbours().get_all_GADM_countries():
            #    if ISO not in self.roughNodeCounts:
            #        self.roughNodeCounts[idlevel][ISO]={'nNodes':0}
            #GADMLEVELNONZERO= defaults['gadm']['ID_LEVELS'][idlevel]+' != 0' if idlevel>0 else "ISO != ''"
            #whereStr= compareGADM_string(GADM_ids, tablename='lookup')
            cmd= "SELECT COUNT(lookup.cluster_id),"+','.join(ids)+"  FROM "+ lookupTableName+" AS lookup GROUP BY ("+','.join(ids)+") ;"
            allcounts=self.db.execfetch(cmd)
            for ac in allcounts:
                dsetset(self.roughNodeCounts,[idlevel]+ac[1:]+['nNodes'], ac[0])
        nNodes=dgetget(self.roughNodeCounts,[idlevel]+GADM_ids+['nNodes'],0)
        self.logger.write(' Looked up size (# nodes) of '+str(GADM_ids)+': %d'%nNodes),
        return(nNodes)


    def subregion_generator(self, pGADM):
        """ Given a region (specified in the form of a pGADM)
        decide whether it's too big. If it is, recurse over next GADM level. If it is not, return a 2-tuple of region and some other info in a dict.
        """
        gstr, region = osm_lookups.pGADMformat(pGADM)
        idlevel= 0 if isinstance(region,str) else len(region)-1  # This is the level of the core, not the neighbours
        spaces=' '*idlevel + '  '
        nNodes_core=self.rough_count_of_nodes(region)
        if nNodes_core==0: # No need to continue
            self.logger.write(spaces*0+' Skipping this empty (no nodes) region ...')
            return # Is this how I skip to the next value?
        nNodes=nNodes_core   
        info = dict(continent=self.continent, clusterradius=self.clusterradius)
        #self.logger.write(spaces+'Found roughly %d nodes in %s: '%(nNodes,str(region)))
        if nNodes==0:
            self.logger.write(spaces*0+' Skipping this one...')
        elif nNodes<self.max_nodes:
            self.logger.write(spaces*0+' %d,%d GOOD SIZE for analysis: '%(nNodes_core,nNodes) +str(region))
            self.regions_served+=[info]
            yield (region,info)
        else:
            self.logger.write(spaces*0+' TOO BIG')
            if idlevel+1>5:
                deeperlist=None
            elif region[-1] ==0:
                    self.logger.write('No gadm ids deeper than %s (based on trailing zero)' %(str(region)))
                    deeperlist=None
            else:
                if 1:#try:
                    deeperlist=self.gadmlu.get_regions_within(region)#,idlevel+1)
                    if len(deeperlist)==1:
                        self.logger.write('No gadm ids deeper than %s which has %d nodes' %(str(region), nNodes))
                        deeperlist=None
                if  0: #except:
                    raise Exception( 'Exception thrown while running get_regions_and_neighbors' )
            if deeperlist:
                for aregion in deeperlist:
                    for a,b in self.subregion_generator(tuple(aregion)):
                        yield a,b # Do not store this one in regions_served; it's in a recursion
            else:
                self.logger.write('Alas, there is no more detail for this country. So we will analyze at scale of %s'%(str(region)))
                self.regions_served+=[info]
                yield (region,info)

    def iterate_through_all_regions(self, subset=None):
        """
        This is generator which will go through all regions in the entire continent, at the right spatial scale, returning, for each, a three-element tuple:
                     regionIDs, list of neighbour IDs, dict with ancillary info
        The dict might contain node counts, but it's empty for the moment.

        subset can be passed, to restrict the iteration to only one country; otherwise all will be included.
        """
        """

        Following is algorithm notes (for subregion_generator)
        ISO 
        -> yield ISO,[]
        -> or loop over id_1 and  neighbours
            for an id_1 and neighbours:
                count N in all neighbours (for edges), and in core (for sampling)
                -> if N is small enough, yield the region and neighbours.
                -> otherwise, loop over id_2 and neighbours.

        ie def checkSize_and_yield_or_loop( region and its neighbours)
        if size okay, yield it.
        else, determine idlevel from region name, and loop over th enext level within the region
            yield checkSize_and_yield_or_loop( region and its neighbours) for each one.

        Only look for id5 neighbours, including zero-padded ones, from new adjacency system.
        """
        if self.pGADM:
            assert subset in [ None, self.pGADM] #  self.pGADM == subset is a sign that the code is no longer clean; it's redundnat
            for one_set in self.subregion_generator(self.pGADM): # Start with one country
                yield(one_set)
        else:
            for pGADM in np.unique(sorted(self.gadmlu.GADMcountries)): #sorted(self.get_pGADM_regions()):# xxxxx   #self.gadmlu.get_all_GADM_countries()):
                if subset and not pGADM==subset:
                    continue
                for one_set in self.subregion_generator(pGADM): # Start with one country
                    yield(one_set)

    
    def get_network_from_region_and_neighbors(self,region):
        """ 
        Returns network consisting of a specified GADM region and its
        neighbouring GADM regions; a list of the nodes comprising the
        core region, and a list of the edges comprising the core
        region.

        The latter two lists are not necessary perfectly correlelated, since edges
        span boundaries. However, the point of having these lists is
        that we will only update network properties of the edges and
        nodes in the core.
        
        Moreover, this function returns ONLY those nodes which are designated as accessible by car, since we only wish to calculate node metrics for them.


        region is a either list or tuple of GADM levels (ISO, id_1, id_2, ...), or a single string (an ISO).

        neighbours is a list of lists or tuples.
        
        neighbouredges and coreedges are a tuple of (rownames, list of edges)
        
        No graph objects are built; the caller can choose what to do with the raw edge list.
        
        corenodes is (now) a dict, with value "nocars", ie a flag for whether the node is designated non-car

        """
        edges,coreedges=self.get_edges_by_GADM(region, cols=None,includeGeometry=False, testlimit=False)

        # Get nodes in core region  (It's possible for there to be zero corenodes, even if there are some edges)
        lookupMaker=osm_lookups.GADM_roadgraph_lookups('nodes', self.clusterradius, continent=self.continent)
        corenodes=lookupMaker.get_nodes_by_GADM([region], extraCols = ['nocars']) 
        # I believe I should reduce duplicates, etc here, not later.

        # Return complete network, but only the car-accessible core nodes:
        return edges,coreedges, [k for k,v in list(corenodes.items()) if not v] # This format is not yet final! Need to massage.

    def get_edges_by_GADM(self,core_ids,filter_distance=True,cols=None,includeGeometry=False, testlimit=False):
        """
        Retrieve edges which intersect a given region (core_ids) or its neighbors. 
        Each region is a tuple (or list) of ISO, id_1, id_2
        So examples of valid GADM_ids are:
            'AUT'
            ('AUT',2)

        By default, we get the 7-m or 10-m cluster ids which define an edge. ['spt_cluster_id', 'ept_cluster_id',], plus the edge_id, plus the GADM ids corresponding to the depth specified by GADM_ids.
        If you want other columns, specify cols explicitly. [This might be fragile, and should have limitations imposed]

        For the moment, we use lookups by edge_id, which are currently built before the modifications based on cluster centroids. That is, these lookups are based on continentlatest_road_segs, not on continent_roadgraph tables.

        Returns the column names and the list of data.

        testlimit sets a limit to the size of the returned list, just for testing.
        
        filter_distance will limit the edges returned to those within a distance (specified in the config file) of the core ids
        """

        # Choose lookup-table GADM ids to retrieve:   Include one set of GADM ids for each edge
        if isinstance(core_ids, str): core_ids = [core_ids]
        if isinstance(core_ids, tuple): core_ids = list(core_ids)
        assert isinstance(core_ids,list)
        if "'" not in core_ids[0]: core_ids[0] = "'"+core_ids[0]+"'"
        whereStrCore=  ' AND '.join(['lookup.'+gidName+"="+str(gid) for gidName, gid in zip(defaults['gadm']['ID_LEVELS'][:len(core_ids)], core_ids)])
        whereStrCore2= whereStrCore.replace('lookup.','gadm.')
        whereStrGAn= whereStrCore.replace('lookup.','')
        whereStrNeigh= ' AND '.join(['ga.'+gidName+"="+str(gid) for gidName, gid in zip(defaults['gadm']['ID_LEVELS'][:len(core_ids)], core_ids)])
        whereStrNeigh+= ' AND '
        whereStrNeigh+= ' AND '.join(['lookup.'+gid+'=ga.neighbour_'+gid for gid in defaults['gadm']['ID_LEVELS']])
        whereStrNeighLUonly= ' AND '.join(['lookup.'+gid+'=ga.neighbour_'+gid for gid in defaults['gadm']['ID_LEVELS']])

        # Limit if necessary to those edges within a certain distance of the core GADM
        if filter_distance:
            distance_ids=gadm.strip_zeros_from_id(core_ids)
            gadm_table = defaults['gadm']['TABLE']+'_'+defaults['gadm']['ID_LEVELS'][len(core_ids)-1]
            # use gadm table rather than aggregated version, because the performance is much faster...
            #    see https://gis.stackexchange.com/questions/166010/postgis-st-intersects-performance for why
            # and the DISTINCT below removes many duplicates
            # Note that projecting the edges on the fly doesn't give a performance hit in testing, because the gadm index (not the edges index) is used
            # this is fine as long as we don't try and aggregate the gadm table or use it within  WITH xxx AS() clause
            fromClause = ', (SELECT geom_compromise FROM %s WHERE %s) AS core' %(defaults['gadm']['TABLE'], whereStrCore.replace('lookup.',''))
            distanceClause = ' AND ST_DWithin(ST_Transform(edges.geom, %s), gadm.geom_compromise, %s)' % (defaults['osm']['srid_compromise'], defaults['osm']['GADM_neighbourRadius'])
        else:            
            fromClause,distanceClause = '', ''
            
        # Choose edge-table columns to retrieve
        if cols is  None: 
            cols=['edges.spt_cluster_id','edges.ept_cluster_id','edges.edge_id']+['edges.geom']*includeGeometry+['edges.length_m']
        else:
            assert  isinstance(cols, list)
            assert all([ss.startswith('edges.') or ss.startswith('lookup')  for ss in cols])
            cols=cols+  ['edge_id']*('edge_id' not in cols) # Not checked for uniqueness

        getColString = ', '.join(cols)
        getUniqueColString=getColString.replace('edges.','').replace('lookup.','').replace('geom','ST_AsText(geom)').replace('length_m','min(length_m)') 
        lookupColString='lookup.edge_id'

        lookupMaker=osm_lookups.GADM_roadgraph_lookups('edges', self.clusterradius, continent=self.continent)
        
        #All but one of all sets of parallel edges are thrown out when turning data into simple nx graphs.
        #For accurate distance ratios, we select edges with the shortest length for each set of parallel edges.
        #This selects edges distinct on endpoints. Output is only useful if we're only interested in turning it into simple graphs
        if self.pGADM_lookups:
            gstr, spgids = osm_lookups.pGADMformat(self.pGADM)
        else:
            gstr=''
            
        cmdDict = {'continent':self.continent, 'GADM_TABLE':defaults['gadm']['TABLE'], 'clusterradius':self.clusterradius, 
             'ROADGRAPHTABLE':defaults['osm']['edge_tables'].replace('REGION',self.continent).replace('CLUSTERRADIUS',self.clusterradius),
                   'whereStrCore':whereStrCore, 'whereStrCore2':whereStrCore2, 'whereStrNeigh':whereStrNeigh, 'whereStrGAn':whereStrGAn,'whereStrNeighLUonly':whereStrNeighLUonly,
             'columns_to_get':getColString, 'columns_to_lookup':lookupColString,  
             'LIMIT':testlimit*"LIMIT 10", 'LOOKUPTABLE':lookupMaker.get_lookup_tablename()+self.pGADM_lookups * gstr,
             'FROM_CLAUSE':fromClause, 'DISTANCE_CLAUSE':distanceClause,
             'unique_edge_columns':getUniqueColString}
        
        # Following  query does a pre-select /distinct on a subset of the ga:  
        # This is advantagous for large countries? Or for high-resolution (GADM id5) countries?
        
        cmd="""WITH core_edges AS (SELECT DISTINCT {columns_to_lookup} FROM {LOOKUPTABLE} AS lookup WHERE {whereStrCore} {LIMIT}),   
                   neigh_edges AS (

         SELECT
         DISTINCT {columns_to_lookup} FROM {LOOKUPTABLE} AS lookup,
         (SELECT DISTINCT neighbour_iso, neighbour_id_1, neighbour_id_2, neighbour_id_3, neighbour_id_4, neighbour_id_5  FROM  {GADM_TABLE}_adjacencies
           WHERE {whereStrGAn} ) AS  ga
                        WHERE {whereStrNeighLUonly}  {LIMIT}),
                     all_edges AS (SELECT DISTINCT ON (edge_id) * FROM ( -- first DISTINCT is to drop edge_ids that are in both core and neigh
                            SELECT 1 AS coreedge, {columns_to_get}
                                FROM {ROADGRAPHTABLE} AS edges, core_edges WHERE edges.edge_id = core_edges.edge_id 
                            UNION ALL
                            SELECT 0 AS coreedge, {columns_to_get}
                                FROM {ROADGRAPHTABLE} AS edges, neigh_edges, {GADM_TABLE} AS gadm 
                                WHERE edges.edge_id = neigh_edges.edge_id AND {whereStrCore2} {DISTANCE_CLAUSE}) unionedges
                      ORDER BY edge_id, coreedge DESC) -- ORDER BY is to make sure we select the core edge in preference if there are duplicates
                SELECT DISTINCT ON
                (least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id))
                coreedge, {unique_edge_columns} OVER (PARTITION by (least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id)))
                FROM all_edges;""".format( **cmdDict )

        # The two nodes of an edge should be listed in numerical order. Use coreedge column to split into core and neighbours
        assert cols[:2] == ['edges.spt_cluster_id','edges.ept_cluster_id']
        result = self.db.execfetch(cmd)
        alledges  = [  cc.split('.')[1] for cc in cols],     [   sorted(RR[1:3])+RR[3:]  for RR in result  ]
        coreedges = [  cc.split('.')[1] for cc in cols],     [   sorted(RR[1:3])+RR[3:]  for RR in result if RR[0]==1 ]
        return(alledges, coreedges) 


    def iterate_through_region_graphs(self,subset=None):
        """
        This yields all the information needed for carrying out processing on (one at a time) manageably-sized subsets of a continent.
        
        This is the primary external interface to the class.
        """
        for region,info in self.iterate_through_all_regions(subset=subset):
            edges,coreedges,corenodes=self.get_network_from_region_and_neighbors(region) # corenodes is only those which are car-accessible
            if corenodes:
                yield dict(region=region, info=info, 
                            edges=edges[1], columns_edges=edges[0] ,coreedges=coreedges[1], columns_coreedges=coreedges[0],corenodes=corenodes)
            else:
                self.logger.write('Skipping region with no nodes in it:'+str(info)+ ':'+str(region))

    def report_regions_served(self):
        """ Report all regions served so far. """
        return self.regions_served 
                
def get_analysis_radii():
    """
    Using the config settings, get a list of the radii that will be used for analysing certain node properties. These define the structure of the nodal analysis database tables.
    """
    dR,Rmax=defaults['osm']['neighbourRadiusRes'],defaults['osm']['neighbourRadiusMax']
    Rmax=Rmax-Rmax%dR 

    Rvec=                np.linspace(dR,Rmax,Rmax//dR)
    innervec,outervec =  np.insert(Rvec[:-1],0,0)  , Rvec
    return innervec,outervec
def get_analysis_radii_suffixes():
    innervec,outervec=get_analysis_radii()
    return(['%d_%d'%(a,b) for a,b in zip(innervec,outervec)])

class connectivityCalculator():
    """Carry out our connectivity analysis on a subset of  the nodes and edges of a given graph.
    
    This is designed to act on the output from
    regionGraphGetters.iterate_through_region_graphs(). That is, it is
    instantiated with the edges in a core region, the edges
    surrounding that core, and information to identify the edges and
    nodes in our database.


    If this is not being used in parallel, you can pass in a dbConnection instance (db).
    This still needs to use a logger; it uses db's logger if it exists.

    
    tsv files: Classification routines write all classifications to
    one of two (for nodes; edges) text files. There is one file per
    continent (in continent mode) or one file per pGADM (in pGADM mode).  When complete, the file is renamed to have a
    "-complete" suffix.

    N.B. We assume that the header of an existing file matches the columns we will generate. To ensure this, and since we now sample 100% of nodes, it is important that in pGADM mode this class is called in utility mode every run, to  delete/reinitialize the two tsv files every time.
    """
    def __init__(self, regiondict=None, verbose=True, db=None, logger=None):
        #continent=None, iso = None, clusterradius=None):
        """

        regiondict:  a dict with all the info we might need. It should have
        elements "edges", "coreedges", and "corenodes", at least.


        Calling mode 1:

        connectivityCalculator(regiondict)  

        # Normal use, for calculating metrics for one region, and
        # appending results to continent-level output files. Continent and clusterradius are
        # taken from regiondict. pGADM, if present, is taken from
        # regiondict, invoking pGADM mode.

        Calling mode 2:

        connectivityCalculator()

        Used to instantiate for one-off utility use (e.g. for deleting/intializing the output files in continent mode.)

        """
        self.clusterradius=None
        self.snap=None
        self.networkx=None
        self.logger=logger
        if logger is None and db:
            self.logger=db.logger
        # Ensure adherence to calling mode options:
        #formoptions=continent is None , clusterradius is None , regiondict is not None
        #print formoptions
        #assert all(formoptions) or not any(formoptions)
        #self.iso = iso

        # Use column names, provided, to build a dict for each edge. This is huge, though it's also how networkx stores data
        # N.B. SNAP only allows integer node labels
        if regiondict:
            self.pGADMmode = 'pGADM' in regiondict['info'] and regiondict['info']['pGADM'] is not None
            self.reportTime('Initializing  connectivityCalculator in {} mode with regiondict[region]={} ...'.format('pGADM' if self.pGADMmode else 'continent', regiondict['region']))
            self.pGADM = None if not self.pGADMmode else regiondict['info']['pGADM'] 
            self.continent=regiondict['info']['continent']
            self.clusterradius=regiondict['info']['clusterradius']
            self.region=regiondict['region']
            self.GADM_analysis_scale=0 if isinstance(self.region,str) else  len(self.region)-1
            self.tablenames=osmt.define_table_names(self.continent,[self.clusterradius])
  
            self.edgedata= dict(   [ [(int(ed[0]), int(ed[1])),  dict(list(zip(regiondict['columns_edges'][2:],ed[2:])))] for ed in regiondict['edges']]  )
            if not len(self.edgedata)==len(regiondict['edges']):
                self.reportTime('    N.B. Edges reduced from %d to %d: non-unique endpoints??'%(len(regiondict['edges']),len(self.edgedata)))
                #raise Exception( 'Number of edges reduced while building connectivityCalculator for %s in %s. You might have parallel edges'% (self.continent,self.region))

            self.edges=list(self.edgedata.keys())
            self.coreedgedata= dict(   [ [(int(ed[0]), int(ed[1])),  dict(list(zip(regiondict['columns_edges'][2:],ed[2:])))] for ed in regiondict['coreedges']]  )
            self.corenodes=regiondict['corenodes']
            if 'length_m' in regiondict['columns_edges']:
                self.length_m=dict([ [ee,dd['length_m']] for ee,dd in list(self.edgedata.items())])
            if 'edge_id' in regiondict['columns_edges']:
                self.edge_id=dict([ [ee,dd['edge_id']] for ee,dd in list(self.edgedata.items())])
            
            self.coreedges=[ ee[2] for ee in regiondict['coreedges'] ]
            self.setup_output_file_names(self.continent,self.clusterradius,pGADM=self.pGADM)
            
        """    
        elif continent is not None:
            self.reportTime('Initializing  connectivityCalculator in {} mode withOUT regiondict ...'.format('pGADM' if self.iso is not None else 'continent'))
            self.continent=continent
            self.clusterradius=clusterradius
            self.tablenames=osmt.define_table_names(self.continent,[self.clusterradius])
        assert self.continent is not None
        assert self.clusterradius is not None
        assert self.tablenames is not None
        
        self.reportTime('Defined self edges and nodes ...') # Really? Not necessarily, right?
        """
        self.verbose=verbose
        if db is None:
            self.db=psqlt.dbConnection(verbose=verbose, logger=self.logger)
        else:
            self.db=db
        #self.startime=time.time()
        #self.latesttime=time.time()
        # The following node_properties_columns tells us the order of columns in the database table for node properties. We create this table's structure implicitly by sending a table of values.
        innervec,outervec=get_analysis_radii()
        self.max_network_distance=outervec[-1]   # When building network distance matrix, look this far (measured in m). Also, for missing network distances, assume this value.
        self.node_properties_columns = ['cluster_id','GADM_analysis_scale']
        for ii,oo in zip(innervec,outervec):
            self.node_properties_columns+=['N_destinations_%d_%d'%(ii,oo),
                                  'sumCartesian_%d_%d'%(ii,oo),
                                  'sumNetworkD_%d_%d'%(ii,oo),
                                     ]
        """
        if isinstance(self.iso,list): # Special mode: we have been given a list of ISOs. We must look for output files and concatenate them.
            assert regiondict is None
            ISOs = self.iso
            self.iso = self.look_for_and_concatenate_ISO_output_files(ISOs)
        if isinstance(self.iso,basestring): # ISO mode, either one actual ISO, or a meta-ISO to get at concatenated tsv files after all ISOs have run
            self.edges_output_file, self.nodes_output_file  =  self.output_file_name('edge', iso =self.iso, continent = continent), self.output_file_name('node', iso = self.iso, continent=continent)
        else:  # Continent mode
            self.edges_output_file, self.nodes_output_file  =  self.output_file_name('edge', continent =self.continent), self.output_file_name('node', continent = self.continent)
        """
            
        """ Some edges are classified as coreedges in multiple regions. How many coreedges have already been classified?
         However, doing this check was taking >99% (?) of the entire connectivity calculation time... so it's been disabled for now.
        TO DO:  ! Do it more efficiently then...
        """
        if 1:
            self.reportTime(' Skipping the check for already-classified edges (Because it is never worthwhile using existing code)...')
        """
        else:
            self.reportTime(' About to check for already-classified edges...')
            if regiondict and os.path.exists(self.edges_output_file):
                with open(self.edges_output_file,'r') as ecf:
                    ecf=ecf.read()
                already_been_classified=0
                for ee in self.coreedges:
                    if "%d"%ee in ecf:
                        already_been_classified+=1
                self.reportTime('%d of %d core edges have already been classified.'%(already_been_classified,len(self.coreedges)))
        """
        if regiondict: # This means that we are continuing to analyze a continent, so make sure the files exist.
            self.initialize_edges_output_file()
            self.initialize_nodes_output_file()
        #else we are probably calling reset_output_files() next; we could do that here, automatically?

    def reportTime(self,logmessage):
        """ For historical reasons only, this is a separate method which simply wraps logger.write.  
        It used to be that logger could be Null, in which case this class carried out its own timing."""
        if self.logger:
            self.logger.write(logmessage)
        else:
            print((' No logger present: '+logmessage))

    def build_networkx_graph(self, anneal=False):
        if self.networkx is None:
            G=osmnGraph(self.edges)
            if nx.__version__[0]=='1':
                nx.set_edge_attributes(G,'length_m', self.length_m)
                nx.set_edge_attributes(G,'edge_id', self.edge_id)
            else:
                nx.set_edge_attributes(G, self.length_m, name='length_m')
                nx.set_edge_attributes(G,self.edge_id, name='edge_id')
            self.reportTime('Built networkx')
            if anneal: # Database is already annealed in normal flow
                G.anneal_degree2_nodes(attributes_sum=['length_m'], accumulate_attributes=['edge_id'])
                self.reportTime('Annealed networkx')                
            self.networkx=G

    def setup_output_file_names(self,continent,clusterradius,pGADM=None):
        """ This is externally callable, when the class has been instantiated in the utility mode (no regiondict). It defines some class members
        """
        self.clusterradius = clusterradius
        if pGADM is not None:
            assert isinstance(pGADM,str) # pGADM mode, either one actual pGADM, or a meta-ISO to get at concatenated tsv files after all pGADMs have run
            self.edges_output_file, self.nodes_output_file  =  self.output_file_name('edge', pGADM=pGADM, continent = continent), self.output_file_name('node', pGADM = pGADM, continent=continent)
        else:  # Continent mode
            self.edges_output_file, self.nodes_output_file  =  self.output_file_name('edge', continent = continent), self.output_file_name('node', continent = continent)

        self.continent = continent
        self.pGADM = pGADM
        self.tablenames = osmt.define_table_names(continent,[clusterradius])      
            
    def output_file_name(self, featuretype, pGADM=None,continent=None):
        """ Specify file naming for output tsv files
        """
        assert featuretype in ['edge','node']
        if pGADM is not None:# and not os.path.exists(paths['scratch']+'pGADMmode/'):
            gstr, gadm_ids = osm_lookups.pGADMformat(pGADM)
            os.system('mkdir -p '+paths['scratch']+'pGADMmode/'+str(continent)) # Sorry, non-POSIX folks.
            edges_output_file=paths['scratch']+'pGADMmode/{}/classified_edges_{}__{}__{}.tsv'.format(str(continent),str(continent),gstr,self.clusterradius)
            nodes_output_file=paths['scratch']+'pGADMmode/{}/node_connectivity_{}__{}__{}.tsv'.format(str(continent),str(continent),gstr,self.clusterradius)
        else:
            assert  continent is not None
            edges_output_file=paths['scratch']+'classified_edges_'+continent+  '_'+self.clusterradius+'.tsv'
            nodes_output_file=paths['scratch']+'node_connectivity_'+continent+ '_'+self.clusterradius+'.tsv'
        return {'edge':edges_output_file, 'node':nodes_output_file}[featuretype]

    def look_for_and_concatenate_pGADM_output_files(self, pGADMs, continent, clusterradius):
        """  A utility used in pGADM-level parallelization. When all pGADMs have finished, we concatenate the files to do the merge.
        Return the paths to a pair of new concatenated files.
        Also, set the continent and clusterradius and pGADM members of this object, to be ready for the next step.

        The plan below is: find out how many of those pGADMs correspond to completed output files. Concatenate the results to a new file, and create a synthetic ISO name, ISOn where "n" is the number of PGADMs with output, for the concatenated file.
        """
        self.continent = continent
        self.clusterradius = clusterradius
        assert self.continent is not None
        assert self.clusterradius is not None
        self.reportTime(' ConnectivityCalculator: Looking for {} pGADM output (tsv) files for edges and nodes...'.format(len(pGADMs)))
        if 0: # This first loop just does a check that these files exist. But they should, so this can be skipped
            goodpGADMs =[]
            for pGADM in pGADMs:
                efn, nfn = self.output_file_name('edge', pGADM=pGADM,continent=continent), self.output_file_name('node', pGADM=pGADM,continent=continent)
                if not os.path.exists(efn) or not os.path.exists(nfn):
                    self.reportTime('    WARNING: Missing edge and/or node output tsv file(s) for pGADM = {}.  Will NOT continue without it. '.format(pGADM))
                    raise
                else:
                    goodpGADMs+=[pGADM]

        newISO = 'isos{}'.format(len(pGADMs))
        efn, nfn = self.output_file_name('edge', pGADM=newISO,continent=continent), self.output_file_name('node', pGADM=newISO,continent=continent)
        edgeFout = open(efn,'wt')
        nodeFout = open(nfn,'wt')
        ecounter,ncounter = 0, 0  # Allow for possibility that first pGADM region has zero edges or zero nodes, independently
        for pGADM in pGADMs:
            efn1, nfn1 = self.output_file_name('edge', pGADM=pGADM,continent=continent), self.output_file_name('node', pGADM=pGADM,continent=continent)
            ##assert (os.path.exists(efn1)+ os.path.exists(nfn1)) in [0,2] # Reduce two ifs below to 1
            if not os.path.exists(efn1):
                self.logger.write('  Failed to find {} even though {} listed. Maybe there were no EDGES from {} in {}?'.format(efn1, pGADM,defaults['osm']['continents'], pGADM))
            else:
                edgeFout.write(''.join( open(efn1,'rt').readlines()[(1 if ecounter>0 else 0):]) ) # Keep the header from the first file, only.
                ecounter = ecounter+1
            if not os.path.exists(nfn1):
                self.logger.write('  Failed to find {} even though {} listed. Maybe there were no NODES from {} in {}?'.format(nfn1, pGADM,defaults['osm']['continents'], pGADM))
            else:
                nodeFout.write(''.join( open(nfn1,'rt').readlines()[(1 if ncounter>0 else 0):]) ) # Keep the header from the first file, only.
                ncounter = ncounter+1


        edgeFout.close()
        nodeFout.close()
        self.reportTime(' Created synthetic ISO files of concatenated results:\n    {}\n    {}'.format(efn,nfn))
        # Now set up this object for the new file:
        self.pGADM = newISO
        self.setup_output_file_names(continent,clusterradius,pGADM=newISO)
        return(newISO)
            
    def reset_output_files(self):
        """ Delete the edges and nodes classification output files. This should be done at the beginning of a new continent/radius or ISO/radius analysis run.
        In fact, the files should not exist unless a previous run was interrupted.
        """
        for delfile in (self.edges_output_file,self.nodes_output_file):
            if os.path.exists(delfile):
                nlines=len(open(delfile,'r').readlines())
                self.reportTime('UNEXPECTED: output file %s already exists, with %d rows already filled. Maybe previous run was interrupted?'%(delfile,nlines))
                randomstring=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))
                os.rename(delfile,delfile.replace('.tsv','')+'_INTERRUPTED_'+randomstring+'.tsv')
                
    def initialize_edges_output_file(self):
        """ We classify all edges, so we always open a new file and add the header. (Contrast: nodes)"""
        if not os.path.exists(self.edges_output_file):
        	with open(self.edges_output_file,'w') as fout:
            		fout.write('edge_id\tclassification\tGADM_analysis_scale\n')
        return(self.edges_output_file)
    def close_edges_output_file(self):
        if self.edges_output_file:
            newfn=self.edges_output_file.replace('.tsv','-complete.tsv')
            os.rename(self.edges_output_file, newfn)
            self.edges_output_file=None
            return(newfn)

    def initialize_nodes_output_file(self):
        """ If the file already exists, we don't want to lose the analysis for already-sampled nodes, so we just append, ie simply check that the header is still consistent with what we will be supplying.
        Otherwise, open a file and add the header.

        """
        if os.path.exists(self.nodes_output_file): # Check header (still) fits our output columns:
            nodesHeaderRow = open(self.nodes_output_file,'rt').readline().strip()
            if not set(nodesHeaderRow.split('\t')) == set(self.node_properties_columns):
                raise Exception('Header of existing .tsv file for node pointmetrics does not match new columns')
        else:
            with open(self.nodes_output_file,'w') as fout:
                fout.write('\t'.join(self.node_properties_columns)+'\n')
        return(self.nodes_output_file)
    
    def close_nodes_output_file(self):
        if self.nodes_output_file:
            newfn=self.nodes_output_file.replace('.tsv','-complete.tsv')
            os.rename(self.nodes_output_file, newfn)
            self.nodes_output_file=None
            return(newfn)

    #To classify edges, pass a dict by_edge_id that stores classifications for the whole continent
    #Calling this function once per region updates it for that region
    def classify_edges_networkx(self):
        self.build_networkx_graph()
        by_edge_id=self.networkx.classify_edges_dendricity_by_ID()
        self.reportTime('Classified edges')
        return self.write_edge_classification(by_edge_id)

    def ________________minimum_cuts(self,origin,destinations):
            # For min edge cuts we need to go pair by pair:
            for destination in destinations:
                min_cuts_a = [len(self.networkx.minimum_edge_cut(origin,destination)) for destination in destinations_R if origin != destination]
            return(  len(min_cuts_a), sum(min_cuts_a)  )
  
    def write_edge_classification(self,by_edge_id):
        """
        Write edge classification data to file. For each row (edge), also record the GADM level used for analysis (if provided)
        """
        if self.edges_output_file:
            with open(self.edges_output_file,'a') as fout:
                for eid in self.coreedges:
                    try:
                        fout.write('%d\t%s\t%d\n'%(eid,by_edge_id[eid],self.GADM_analysis_scale))
                    except KeyError: # eid is not in the dictionary of classifications. See #20
                        print(('   (edge %s not classified)'%str(eid)))
            self.reportTime('Wrote to file: edges...')
        else:
            return(by_edge_id)

    def write_node_connectivity(self, by_node_id):
        
        if self.nodes_output_file:
            with open(self.nodes_output_file,'a') as fout:
                for nid,bndict in by_node_id:
                    fout.write('%d\t%d\t'%(nid,self.GADM_analysis_scale) + '\t'.join([str(bndict[kk]) for kk in self.node_properties_columns[2:]])+'\n')
            self.reportTime('Wrote to file: nodes...')
        else:
            return(by_node_id)

    # Returns a list of all regions at the finest resolution nested within the
    # connectivityCalculator's region
    def get_nested_regions(self):
        #gstr, gids = osm_lookups.pGADMformat(self.region)
        whereclause = osm_lookups.compareGADM_string2(self.region)
        #if isinstance(self.region,str):
        #    whereclause='%s=\'%s\''%(defaults['gadm']['ID_LEVELS'][0],self.region)
        #elif isinstance(self.region,tuple):
        #    whereclause='(%s)=%s'%(', '.join(defaults['gadm']['ID_LEVELS'][:len(self.region)]),
        #                           self.region)
        #else:
        #    raise Exception('region must be an ISO or tuple of GADM regions')
        cmd="""
            SELECT %s from %s WHERE %s
            ;"""%(', '.join(defaults['gadm']['ID_LEVELS']),defaults['gadm']['TABLE'],whereclause)
        self.db.execute(cmd)
        cast_=lambda x : [ int(y) if not isinstance(y,str) else y for y in x ]
        return [ tuple(cast_(r)) for r in self.db.fetchall() ]

    # Select sample of nodes for which to calculate pointmetrics.
    # Sampling is stratified across the set of finest-resolution regions
    # nested in the connectivityCalculator's region.
    #
    # Within each of these regions, a random sample is chosen. The size of the
    # sample depends on the number of nodes in the region (x) and the parameter
    # sampling_floor (can be changed in the user's config file):
    # x <= sampling_floor : sample size = x
    # .05*x <= sampling_floor < x : sample size = 10
    # .05*x > sampling_floor : sample size = .05*x 
    #
    # Relevant unit test:
    # pointmetrics_calculations_test.test_sample_core_nodes()
    #
    def sample_core_nodes(self):
        # Initialize list of sampled nodes
        sampled_nodes=[]

        # First get a list of all regions to sample from
        nested_regions=self.get_nested_regions()
        x=defaults['osm']['sampling_floor']
        for r in nested_regions:
            cmd="""
                SELECT DISTINCT l.cluster_id FROM lookup_{CONTINENT}_cluster{CR}_{GADM_TABLE} as l, {NODETABLE} as n
                    WHERE   ({GADMLEVELS})={GSTR}
                    AND l.cluster_id = n.cluster_id
                    AND n.nocars=FALSE
                ;""".format(CONTINENT=self.continent,
                            CR=self.clusterradius,
                            GADM_TABLE=defaults['gadm']['TABLE'],
                            GADMLEVELS = ', '.join(defaults['gadm']['ID_LEVELS']),
                            NODETABLE= self.tablenames['node_tables'][self.clusterradius],
                            GSTR=r)
            self.db.execute(cmd)
            nodes=[ n[0] for n in self.db.fetchall() ]
            if len(nodes)==0:
                continue
            if x > defaults['osm']['max_nodes'] : # We do not want to sample! We are taking a census
                sampled_nodes+=list(nodes)
            else:
                sampled_nodes+=list(np.random.choice(nodes, size=min([len(nodes),   
                                            max([x,int(np.ceil(len(nodes)*0.05))
                                                ])]), replace=False)
                               )

        return sampled_nodes

    def get_sumCartesian(self,ii,a_destinations_vec,innervec,outervec):
        sumOrNan=lambda ff: np.nan if len(ff)==0 else sum(ff)
        return ['sumCartesian_%d_%d'%(innervec[ii],outervec[ii]),
                sumOrNan(list(a_destinations_vec.values()))
               ]

    def get_sumNetworkD(self,ii,a_destinations_vec,innervec,outervec,ndist):
        sumOrNan=lambda ff: np.nan if len(ff)==0 else sum(ff)
        return ['sumNetworkD_%d_%d'%(innervec[ii],outervec[ii]),
                sumOrNan([ndist.get(cluster_id,self.max_network_distance)
                          for cluster_id,R in list(a_destinations_vec.items())
                         ])
               ]

    # Collect various node-level calculations   
    def analyze_nodes_sample(self):
        node_measures=[]

        if defaults['osm']['sampling_floor'] < 9999:
            sampled_nodes=self.sample_core_nodes()
        else:
            sampled_nodes  = self.corenodes
        self.reportTime('Analyzing %d sampled nodes...'%(len(sampled_nodes)))

        innervec,outervec=get_analysis_radii()
        for origin in sampled_nodes:
            if origin not in self.networkx.node:
                #self.reportTime("""Anomaly to investigate:
                #           Node %d is in core_nodes but not in graph!"""%origin)
                # Issue #19. Should be fixed
                raise Exception("""Node %d is in core_nodes but not in graph!"""%origin)
                continue

            # Get all cartesian distances:
            destinations=self.get_postgis_nodes_within_radius(origin,
                                                         radius_m=max(outervec))

            # Get all network  distances:
            ndist=nx.single_source_dijkstra_path_length(self.networkx,origin,
                                               cutoff=self.max_network_distance,
                                               weight='length_m')

            # Caution: following "nid in ndist" hides unexplained problems, ie it is a kludge to account for the fact that some nodes are missing
            # List of problems:
            missing=[dd for dd,RR in destinations if dd not in ndist]
            destinations=[(nid,R) for nid,R in destinations if nid in ndist ]

            # For each circle, annulus, get a list of all nodes (and the
            # cartesian distance to each):
            destinations_vec=[
                dict([(nid,R) for nid,R in destinations if R<= outer and R>inner
                     ])
                for inner,outer in zip(innervec,outervec)]
            assert origin not in destinations_vec[0]
            
            # And should not count the origin as a destination

            # Compile database column values for this node:
            cols=[
                ['N_destinations_%d_%d'%(innervec[ii],outervec[ii]),
                 len(a_destinations_vec)]
                for ii,a_destinations_vec  in enumerate(destinations_vec)]

            cols+=[self.get_sumCartesian(ii,a_destinations_vec,innervec,outervec)
                   for ii,a_destinations_vec in enumerate(destinations_vec)
                  ]

            cols+=[self.get_sumNetworkD(ii,a_destinations_vec,innervec,outervec,
                                        ndist)
                   for ii,a_destinations_vec in enumerate(destinations_vec)
                  ]
 
            node_measures+=[[origin,
                             dict(cols)
                             ]]
        self.reportTime('Analyzed nodes')
        self.write_node_connectivity(node_measures)

    def get_postgis_nodes_within_radius(self, cluster_id, radius_m=2000, exclude_nocars=True):
        """
        For a single node, ask the database for all nodes with a given distance, and the distance to each.
        The radius is an integer, measured in metres.
        exclude_nocars: We want to exclude nodes designated as non-car from possible destinations (or should this parameter come straight from defaults?).

        """
        cmd = """SELECT neighbor.cluster_id, ST_Distance(node.geog, neighbor.geog,False)::int as dist
                   FROM {GRAPHNODETABLE} neighbor,  {GRAPHNODETABLE} node 
                     WHERE node.cluster_id={cluster_id} AND ST_DWithin(node.geog,neighbor.geog,{radius_m},False)
                          {nocar_clause}
                         ORDER BY dist;""".format( continent=self.continent, NNCL=self.clusterradius,
                                                  cluster_id=cluster_id, radius_m=radius_m,
                   GRAPHNODETABLE=defaults['osm']['cluster_tables'].replace('REGION',self.continent).replace('CLUSTERRADIUS',self.clusterradius),
                                                   nocar_clause = exclude_nocars* """ AND neighbor.nocars = FALSE """
                                                  )
        
        self.db.silently_execute(cmd)
        neighbournodes=self.db.fetchall()
        if not neighbournodes[0][0]==cluster_id:
            self.logger.write(' GITLAB ISSUE #430 AHEAD! {}\n{}, {}, {}'.format(cmd, neighbournodes[0][0], cluster_id, neighbournodes[0],  neighbournodes))
            #assert neighbournodes[0][0]==cluster_id
        return neighbournodes[1:]


    def merge_results_file_into_postgis(self, mode,results_file=None, forceUpdate=False):
        """
        Read a file of node-based or edge-based results for one continent (and cluster type), insert it into a temporary table, and join it into our main "abstract" cluster table in postGIS.

        Our node-based connectivity metrics are written out to a file for one continent at a time.  
        When the file is finished, this method is called to submit the results to postGIS. That way, we avoid the inefficient alternative of trying to update the database row by row while it has an index.
        """
        assert mode in ['node','edge']#['cluster','roadgraph']
        if results_file is None:
            results_file = {'node':self.nodes_output_file,'edge':self.edges_output_file}[mode]
        assert results_file
        ntmp="tmp_"+os.path.split(results_file)[1].replace('-complete.tsv','').replace('.tsv','')
        ntable=self.tablenames[mode+'_tables'][self.clusterradius]
        data=results_file
        #If edges are duplicated in classification file, save the row classified as 'edge' and drop the other one
        if mode=='edge':
            data=pd.read_table(results_file)
            assert 'classification' in data.columns
            data=data.sort_values(by='classification',ascending=False)
            data=data.drop_duplicates(subset='edge_id',keep='first')

        self.db.update_table_from_array(data, ntable, temptablename=ntmp, drop_incoming_duplicates=True, forceUpdate=True,
        )
 
    def overwrite_edge_classification_for_deadends(self):
        """
        osm_networkx normally classifies deadend edges as bridges. Let's overwrite them 
        """
        self.db.execute("""
            UPDATE """+self.tablenames['edge_tables'][self.clusterradius]+"""
                    SET classification = 'D'
            where ept_degree=1 or spt_degree=1
            ;
            """)
        self.reportTime('Overwriting deadends in postgres')

    def overwrite_edge_classification_for_selfloops(self):
        """
        We can't classify selfloops using osm_network_x, because it won't recognize parallel selfloops
        (multiple selfloops from the same node).
        """
        self.db.execute("""
            UPDATE """+self.tablenames['edge_tables'][self.clusterradius]+"""
            SET classification = 'S' WHERE spt_cluster_id=ept_cluster_id;""")
        self.reportTime('Overwriting selfloop edge classifications in postgres')

    def overwrite_edge_classification_for_parallel_edges(self):
        """
        Parallel edges are ignored by the osmnxGraph class, since it inherits from an nx simple graph.
        Here we'll overwrite classification of parallel edges as cycles.
        """
        self.db.execute("""
                UPDATE """+self.tablenames['edge_tables'][self.clusterradius]+"""
                SET classification = 'C' WHERE edge_id in (
                        SELECT edge_id FROM """+self.tablenames['edge_tables'][self.clusterradius]+ """ WHERE (
                        least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id)
                        ) in (
                                SELECT least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id) FROM """+self.tablenames['edge_tables'][self.clusterradius]+
                        """ GROUP BY least(spt_cluster_id,ept_cluster_id),greatest(spt_cluster_id,ept_cluster_id)
                        HAVING COUNT(*)>1
                        )
                );""")
        self.reportTime('Overwriting parallel edge classifications in postgres')

    def add_startenddistance(self, forceUpdate=False):
        """Adds the distance from start to end (needed to calculate curviness"""
        self.logger.write('Adding startend_length_m to edges.')
        tmpTable = "tmp_startend_"+self.tablenames['edge_tables'][self.clusterradius]
        seg_table = self.tablenames['edge_tables'][self.clusterradius]
        self.db.execute("""
            CREATE TABLE """+tmpTable+""" AS
            SELECT edge_id, 
                CAST(round(ST_Distance(ST_EndPoint(geom)::geography, ST_StartPoint(geom)::geography)) as int) AS startend_length_m
            FROM """+seg_table+""";""")

        self.db.merge_table_into_table(from_table=tmpTable, targettablename=seg_table,  commoncolumns='edge_id', forceUpdate=True)
        self.db.execute('DROP TABLE %s' % tmpTable)


class pGADM_completion_log(object):
    """ Just for clarity, separate out this function which keeps a list of completed pGADMs 
    and can also look at completed calculations before the pGADMs are concatenated to a continent (planet)
    This should make it easy to transition (201807) from using text files to using the database.  
    The database version should record the latest completion date/time.
    """
    def __init__(self, clusterRadius=None, continent= None,  logger=None):
        self.continent=continent
        self.clusterRadius = clusterRadius
        #self.forceUpdate = forceUpdate
        self.logger=logger
        self.completionFile =  paths['scratch']+'pGADMmode/pGADMlist_{}_{}.txt'.format(clusterRadius,str(self.continent))
        os.system('/bin/mkdir -p {}pGADMmode/{}/'.format(paths['scratch'], self.continent))
        assert os.path.exists('{}pGADMmode/{}/'.format(paths['scratch'], self.continent))
        if not os.path.exists(self.completionFile):
            self.clear_calculation_results()
    def clear_calculation_results(self):
        os.system('/bin/rm -r {}pGADMmode/{}/'.format(paths['scratch'], self.continent))
        """ This deletes all data, and deletes all "completed" entries in the log """
        with open(self.completionFile, 'wt') as fout:
            pass # Reset this list (on disk) of pGADMs for this continent.
        if self.logger:  self.logger.write('All pGADM-mode results and completion list reset')
        
    def mark_completed(self, pGADM):
        gstr, gadm_ids = osm_lookups.pGADMformat(pGADM)
        with open(self.completionFile, 'a') as fout:
            fout.write(gstr+'\n')
        if self.logger:  self.logger.write('pGADM {} marked as completed.'.format(gstr))
    def get_completion_list(self):
        pGADMs = [LL.strip() for LL in open(self.completionFile, 'rt').readlines() if LL.strip()]
        for pGADM in pGADMs: # Assert that the results files exist, as they must, for pGADMs listed as complete
            # Following names are hardcoded elsewhere. Should that be moved here? Or otherwise consolidated?
            edges_output_file=paths['scratch']+'pGADMmode/{}/classified_edges_{}__{}__{}.tsv'.format(str(self.continent),str(self.continent),pGADM,self.clusterRadius)
            nodes_output_file=paths['scratch']+'pGADMmode/{}/node_connectivity_{}__{}__{}.tsv'.format(str(self.continent),str(self.continent),pGADM,self.clusterRadius)
            if not os.path.exists(edges_output_file):
                self.logger.write('  Failed to find {} even though {} listed as complete. Maybe there were no nodes/edges?'.format(edges_output_file, pGADM))
                #assert os.path.exists(edges_output_file)
            if not os.path.exists(nodes_output_file):
                self.logger.write('  Failed to find {} even though {} listed as complete. Maybe there were no nodes/edges?'.format(nodes_output_file, pGADM))
                #assert os.path.exists(nodes_output_file)
        return pGADMs

def main_one_pGADM(pGADM,clusterRadius, continent=None):
    """ For use in main_by_pGADM, below. """
    gstr, gadm_ids = osm_lookups.pGADMformat(pGADM)
    plogger=osmt.osm_logger('_'.join(['roadgraph','connectivity',str(continent),clusterRadius, gstr]), verbose=True, timing=True)
    plogger.write(' RAM fraction at start: {}'.format(osmt.RAM_fraction_used()))
    icl =pGADM_completion_log(clusterRadius= clusterRadius, continent= continent,  logger=plogger)
    osmrlu = osm_lookups.GADM_roadgraph_lookups('edges',clusterRadius,continent, logger=plogger)
    osmrlu.make_GADM_subset_edge_lookup(pGADM)

    rg=regionGraphGetter(pGADM=pGADM, continent=continent, clusterradius=clusterRadius, max_nodes=defaults['osm']['max_nodes'] , logger=plogger, pGADM_lookups=True)
    for regiondict in rg.iterate_through_region_graphs(subset=pGADM):
        regiondict['info']['pGADM'] = pGADM
        # Pass in the dbConnection from regionGraphGetter to be used by connectivity Calculator (safe if we aren't parallelizing):
        cCalc=connectivityCalculator(regiondict, db=rg.db, logger=plogger) #, continent=continent, pGADM=pGADM)
        cCalc.build_networkx_graph()
        cCalc.classify_edges_networkx()
        dd=cCalc.analyze_nodes_sample()

    pGADMsFound = rg.report_regions_served()
    if pGADMsFound: # This country exists in the continent
        plogger.write(' We seem to have completed region-subdividing for {} in {}.'.format(pGADM,continent))
        icl.mark_completed(pGADM)

    osmrlu.delete_GADM_subset_edge_lookup(pGADM) # Delete a temporary lookup in the database
    plogger.write(' RAM fraction at close: {}'.format(osmt.RAM_fraction_used()))
    plogger.close()


def main_by_country(logger = None, forceUpdate=False):
    """
    Implement a parallel version of node/edge calculations which run countries in parallel, so, for instance, that we can parallelize over countries for a "planet" continent.
    We can also parallelize when using smaller continents (e.g. GADM's seven continents), but the continents should not be parallelized over in that case, since some countries show up in more than one continent.

The procedure is as follows:
     - run all ISO's in parallel. Using "ISO" mode above will cause outputs to be saved to separate per-ISO files.
     - Then concatenate all the output files into a continent-level meta-output-file.  Use that for the "merge_results_file_into..." functions.
    """

    if logger is None:
        logger=osmt.osm_logger('_'.join(['roadgraph','connectivity','parallel']), verbose=True, timing=True)
    for clusterRadius in defaults['osm']['clusterRadii']:
        for continent in defaults['osm']['continents']: # Do continents in serial, ISOs within each in parallel
            ICL =pGADM_completion_log(clusterRadius= clusterRadius, continent= continent,  logger=logger)
            if forceUpdate: ICL.clear_calculation_results()
            predone = ICL.get_completion_list()
            
            # It would be best to do the following in order of decreasing country-computation-time. One proxy for that is country size, though number of GADM regions seems to matter too (FRA, DEU, PHL take a long time, and PHL is not in the top 20).
            lookups = osm_lookups.GADM_roadgraph_lookups( mode='nodes', cluster_radius=clusterRadius, continent=continent, forceUpdate=False, logger=logger, create=False)
            df_ISOsInOrder=lookups.count_nodes_by_GADM_ISO().query('iso !="SP-"')  #[::-1]
            

            # Keep this simple for now: for sufficiently large countries, split up into id_1s. And put them all at the top. No other depth at the moment.
            big = df_ISOsInOrder.query('nodes>{}'.format(defaults['osm']['max_country_serial_nodes'])).iso.tolist()
            if big:
                df1 = gadm.regions_and_neighbours().get_all_GADM_regions(level = 1).set_index('iso').loc[big]
                list_ISOsInOrder = (df1.index + '_'+df1.id_1.astype(str)).tolist() + [ii for ii in df_ISOsInOrder.iso if ii not in big]
            else:
                list_ISOsInOrder=  orderListByRule( df_ISOsInOrder[df_ISOsInOrder.nodes>5].iso.values, ['FRA', 'PHL', 'DEU'])  # nodes > 5 gets rid of 'SP-' and 'PCN'

            # Hardcoded to optimize run speed:
            # Following list is from du -sk of the tsv files for the radius=10, cars=True case. It will be different for the non-(cars=True) case.
            large_in_order = ['JPN_12', 'USA_18', 'DEU_9', 'IND_20', 'USA_19', 'USA_3', 'CAN_9', 'IND_32', 'USA_24', 'USA_41', 'BRA_16', 'USA_50', 'JPN_1', 'FRA_11', 'POL', 'ZAF', 'DEU_1', 'USA_48', 'USA_37', 'UKR', 'USA_15', 'USA_43', 'IDN_9', 'ARG_1', 'USA_1', 'FRA_1', 'FRA_10', 'USA_23', 'USA_26', 'DEU_10', 'USA_36', 'USA_33', 'DEU_2', 'BRA_13', 'USA_14', 'USA_47', 'USA_11', 'USA_39', 'USA_34', 'USA_10', 'BRA_25', 'USA_5', 'GBR_1', 'USA_44'][::-1]
            list_ISOsInOrder=  orderListByRule(list_ISOsInOrder, large_in_order)

            if forceUpdate:
                assert not predone
            else:
                list_ISOsInOrder = [g for g in list_ISOsInOrder if g not in predone]
                logger.write(' Found {} pGADMs (in {} ISOs) already completed from a previous run (forceUpdate=False): {}'.format(len(predone), len(np.unique([g.split('_')[0] for g in predone])),  predone))
                logger.write('   Remaining pGADMs to be analysed (in order): {}'.format(list_ISOsInOrder))


            logger.write(' Beginning pGADM-parallel roadgraph calculations for {} with radius {}'.format(continent,clusterRadius))
                
            funcs=[]
            names=[]
            for pGADM in list_ISOsInOrder: # This is in descending order of size 
                funcs+=[  [main_one_pGADM, [pGADM,clusterRadius,continent]]]
                names+=[  defaults['server']['postgres_db']+':pointmetrics_calculations main '+' '.join([continent, str(clusterRadius),pGADM])]
            logger.write('  The whole world will take about 10 hours in parallel on sprawl server, with a small (100) sampling floor. If you are including all nodes, it will be days due to USA, JPN, ...')
            runFunctionsInParallel(funcs,names=names,maxAtOnce=40, parallel=defaults['server']['parallel'])
            
            pGADMsfound = ICL.get_completion_list()
            cCalc = connectivityCalculator(logger=logger) # For utility object only
            newISO =  cCalc.look_for_and_concatenate_pGADM_output_files(pGADMsfound, continent,clusterRadius) # This will also define file names, set class member valuse
            logger.write(' Finished pGADM calculations, now concatenated as meta-pGADM '+newISO)
 
            cCalc.merge_results_file_into_postgis('edge', forceUpdate=forceUpdate, )
            cCalc.close_edges_output_file() # Rename the output file when we have completed the entire continent
            cCalc.merge_results_file_into_postgis('node', forceUpdate=forceUpdate)
            cCalc.close_nodes_output_file() # Rename the output file when we have completed the entire continent

            cCalc.overwrite_edge_classification_for_parallel_edges()
            cCalc.overwrite_edge_classification_for_selfloops()
            cCalc.overwrite_edge_classification_for_deadends()
            cCalc.add_startenddistance(forceUpdate=forceUpdate) 
            
        # We cannot recreate indices until the old_ table has been dropped, since its indices have the original names.
        # The above proceeds by creating a new table, so we should (vacuum? and) recreate indices for the new table

        print(('============== Finished %s in parallel ========================='%(continent)))
    
def main_serial_one_continent(continent,forceUpdate=False):
    """ Used only by main_by_continent() """
    for clusterRadius in defaults['osm']['clusterRadii']:
        logger=osmt.osm_logger('_'.join(['roadgraph','connectivity',continent,clusterRadius]), verbose=True, timing=True, prefix='')
        rg=regionGraphGetter(continent=continent, clusterradius=clusterRadius, max_nodes=defaults['osm']['max_nodes'] , logger=logger)
        cCalc=None

        # Delete old results files for this continent
        # Instantiate dummy just in order to overwrite/clear the output files.
        utilCalc =   connectivityCalculator()
        utilCalc.setup_output_file_names(continent, clusterRadius)
        utilCalc.reset_output_files()
        logger.write('  Initiated main_serial_one_continent and reset output files')

        #for rf in [ paths['scratch']+'classified_'+kind+'_'+continent+'_'+clusterRadius+'.tsv' for kind in [ 'edges','nodes' ] ]:
        #    
        #        with open(rf,'w') as fout:
        #            fout.write('edge_id\tclassification\tGADM_analysis_scale\n')
        #            fout.close()

        for regiondict in rg.iterate_through_region_graphs():
            # Pass in the dbConnection from regionGraphGetter to be used by connectivity Calculator (safe if we aren't parallelizing):
            logger.write('  Retrieved edges. ')
            cCalc=connectivityCalculator(regiondict, db=rg.db, logger=logger) 
            cCalc.build_networkx_graph()
            cCalc.classify_edges_networkx()
            dd=cCalc.analyze_nodes_sample()

        # Use most recently instantiated connectivityCalculator to deal with the output files:
        if not cCalc: continue
        cCalc.merge_results_file_into_postgis('edge', forceUpdate=forceUpdate)#,cCalc.edges_output_file)
        cCalc.close_edges_output_file() # Rename the output file when we have completed the entire continent
        cCalc.merge_results_file_into_postgis('node', forceUpdate=forceUpdate)#,cCalc.nodes_output_file)
        cCalc.close_nodes_output_file() # Rename the output file when we have completed the entire continent
        # We cannot recreate indices until the old_ table has been dropped, since its indices have the original names.
        # The above proceeds by creating a new table, so we should (vacuum? and) recreate indices for the new table
        cCalc=connectivityCalculator(db=rg.db) 
        cCalc.tablenames = osmt.define_table_names(continent,[clusterRadius])
        cCalc.clusterradius = clusterRadius
        cCalc.continent = continent

        cCalc.overwrite_edge_classification_for_parallel_edges()
        cCalc.overwrite_edge_classification_for_selfloops()
        cCalc.overwrite_edge_classification_for_deadends()
        cCalc.add_startenddistance(forceUpdate=forceUpdate)
        print(('============== Finished %s  cluster %s ========================='%(continent,clusterRadius)))
def main_by_continent(continent=None, forceUpdate=False):
    """
    Calculate edge and node metrics for the whole world, and load the results up into the cluster and roadgraph tables (at the level of edges and nodes), and recreate the indices for those tables.
    """
    if continent is None:
        regions=defaults['osm']['continents']
    else:
        regions=[continent]
    funcs=[ [main_serial_one_continent,[continent,forceUpdate]]        for continent in regions]
    runFunctionsInParallel(funcs,names=[defaults['server']['postgres_db']+':roadgraph_connectivity_main_'+rr for rr in regions])


################################################################################################
################################################################################################
################################################################################################
if __name__ == '__main__':
################################################################################################
################################################################################################
################################################################################################
    import sys
    runmode= 'default' if len(sys.argv)<2 else sys.argv[1].lower()
    if runmode not in ['default','test','countstest']:
        print('\n\n UNRECOGNIZED RUN MODE...\n\n')
        stophere
    if runmode in ['repeatmerge']:
        repeat_merge_of_complete_results_into_postgis() # Moved to exons
    if runmode in ['default']:
        main_parallel() #main()
    if runmode in ['countstest']:
        rgg= regionGraphGetter('africa','10')
        #rgg.generate_rough_counts_of_nodes_all()
        rgg.rough_count_of_nodes(['EGY',1,2])
        rgg.rough_count_of_nodes(['EGY',1])
        rgg.rough_count_of_nodes(['EGY'])
        rgg.rough_count_of_nodes(['EGY',1,2])
        rgg.rough_count_of_nodes(['EGY',1])
        rgg.rough_count_of_nodes(['EGY'])
        rgg.rough_count_of_nodes(['EGY',1,4])
        rgg.rough_count_of_nodes(['EGY',2])
        rgg.rough_count_of_nodes(['EGY'])
        rgg.rough_count_of_nodes(['DEU'])
        rgg.rough_count_of_nodes(['DEU',1])
        rgg.rough_count_of_nodes(['DEU',1,2])

    if runmode in ['test']:
        db=psqlt.dbConnection(verbose=True)
        osmt.generate_GADM_lookups()# mode=None,forceUpdate='append', separateTables=True, continent=None):
        crt.final_graph_builder(forceUpdate=True,   build_all=True)
        print((db.list_columns_in_table('antarctica_roadgraph_7')))
        main()
