#!/usr/bin/python
# -*- coding: utf-8 -*
"""
Aggregates results to different national geography (not just GADM)
Combine with automobility data and report results
"""
import os, tarfile
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from osm_config import paths,defaults
import osmTools as osmt
import postgres_tools as psqlt
from cpblUtilities.mathgraph import empiricalcdf

import aggregate_connectivity_metrics as aggmetrics
import kmeans_analysis as kmeans

def setup_new_nonGADM_database():
    deprecated_now_done_in_regular_osm_database
    import inquirer
    from shutil import copyfile
    from cpblUtilities import orderListByRule

    newdb = input('Name of new non-GADM database:').lower()
    defaults['server']['postgres_db'] = newdb
    print(('Setting default db to '+defaults['server']['postgres_db']))
    # It's important to import this after setting the default, above.
    from server_setup import copy_template_db
    print((' Creating (or confirming created) '+newdb))
    copy_template_db(newdb)
    
    new_dbc = psqlt.dbConnection(db=newdb, verbose=True)
    adbs = new_dbc.execfetch("""
    SELECT datname FROM pg_database
    WHERE datistemplate = false;
    """)
    adbs = [d[0] for d in adbs if d[0] not in ['postgres', newdb]]
    olddb = inquirer.prompt([  inquirer.List('A', message='Copy pointmetrics from which database?', choices = orderListByRule(adbs, ['osm10m','osm7m','osm10','osm7']))])['A']
    fromdb = psqlt.dbConnection(db=olddb, verbose=True)
    oldTables = [[[a,b] for a,b in fromdb.find_tables(findstring) if b.startswith(findstring)] for findstring in ['nodes_planet', 'edges_planet','urban_thresholds','kmeans_k10_10_urban']]
    for ot in oldTables:
        ss,tt = ot[0]
        if new_dbc.check_whether_table_exists(ss, tt):
            print((' Found {} already existing in {}. NOT copying (nor updating)...'.format(tt,newdb)))
        else:
            sysstr =' pg_dump  -t {table} {fromdb} | psql -d {newdb}'.format(table= tt, fromdb=olddb, newdb=newdb)
            print (sysstr)
            os.system(sysstr)

    # copy over PCA coefficients
    pcaFn = 'master_id5_PCA_coefficients_planet_10_urban.pyshelf'
    copyfile(paths['baseworking']+'osm10m/'+pcaFn, paths['baseworking']+newdb+'/'+pcaFn)

class nonGADMgeometries():
    """Loads non-GADM geometries into a new schema, and creates lookups and aggregations for them
    Right now, geometries are hardcoded in loadGeometries"""
    def  __init__(self, logger=None, forceUpdate=False):
        self.forceUpdate=forceUpdate
        self.logger = osmt.osm_logger('beyond_gadm', verbose=True, timing=True) if logger is None else logger
        self.db = osmt.pgisConnection(verbose=True, schema = defaults['osm']['processingSchema'], logger=self.logger)   

    def loadGeometries(self):
        # loads subnational geographies
    
        # tuple is path, srid
        # note: japan_mun is in ITRF94, but docs (http://www.gsi.go.jp/kankyochiri/gm_japan_e.html) say that WGS94 is taken as equivalent
        shpFns = {'us_bgs': (paths['otherinput']+'bgs_allstates_WGS_cartographic.shp', 4326),
                  'eu_nuts2': (paths['otherinput']+'cars_country/EU/boundaries/NUTS_RG_60M_2013_4326_LEVL_2.shp', 4326),
                  #'japan_pref': (paths['otherinput']+'cars_country/Japan/boundaries/gadm36_JPN_1.shp', 4326),
                  #'japan_mun': (paths['otherinput']+'cars_country/Japan/boundaries/gm-jpn-bnd_u_2_1/polbnda_jpn.shp', 4326),
                  'japan': (paths['otherinput']+'cars_country/Japan/boundaries/N03-180101_GML/N03-18_180101.shp', 4326),
                  'nz': (paths['otherinput']+'cars_country/NZ/boundaries/AU2013_GV_Clipped.shp', 2193),
                  'france': (paths['otherinput']+'cars_country/France/boundaries/CONTOURS-IRIS.shp', 2154),
                  'ca_cts': (paths['otherinput']+'Canada-maps/2011/gct_000b11a_e.shp', 4269), # EPSG:4269, NAD83, srid 94269
                  'ca_das': (paths['otherinput']+'Canada-maps/2011/gda_000b11a_e.shp', 4269), # EPSG:4269, NAD83, srid 94269
                  'ca_plos': (paths['otherinput']+'Canada-maps/HSBL2018/Community_Happiness.shp', 3347), # EPSG:3347, NAD83 / Statistics Canada Lambert, srid 93347
                  'japan_felix': (paths['otherinput']+'cars_country/Japan/boundaries/trainstations_2116.shp', 4326)
                  }
        self.db.execute('CREATE SCHEMA IF NOT EXISTS non_gadm;')
        for region in shpFns:
            if region in self.db.list_tables('non_gadm'):
                if not self.forceUpdate:
                    if self.logger: self.logger.write('Already loaded %s. Skipping.' % region)
                    continue
                else:
                    if self.logger: self.logger.write('Dropping and recreating %s.' % region)
                    self.db.execute('DROP TABLE non_gadm.%s;' % region) 
            if self.logger: self.logger.write('Loading %s' % region)
            encoding = '"Shift_JIS" ' if region=='japan' else '"latin1" '
            assert os.system("""shp2pgsql -s """+str(shpFns[region][1])+""":4326 -g geom -I -W """+encoding+shpFns[region][0]+' non_gadm.'+region+"""  | psql -q %s """%(self.db.psql_command_line_flags()))==0
            self.db.fix_permissions_of_new_table('non_gadm.'+region)

        # create fake gadm with all of them
        self.db.execute('''DROP TABLE IF EXISTS non_gadm.all_boundaries;
                    CREATE TABLE non_gadm.all_boundaries AS
                      -- SELECT 'japan_pref' AS ng_0, gid_1 AS ng_1, geom AS the_geom FROM (SELECT gid_1, ST_Union(geom) AS geom FROM non_gadm.japan_pref GROUP BY gid_1) t_japan
                      -- UNION ALL
                      -- SELECT 'japan_mun' AS ng_0, laa AS ng_1, geom AS the_geom FROM (SELECT laa, ST_Union(geom) AS geom FROM non_gadm.japan_mun GROUP BY laa) t_japanmun
                      -- UNION ALL
                      SELECT 'japan' AS ng_0, N03_007 AS ng_1, geom AS the_geom FROM (SELECT N03_007, ST_Union(geom) AS geom FROM non_gadm.japan WHERE N03_007 is not Null GROUP BY N03_007 ) t_japan
                      UNION ALL
                      SELECT 'nz' AS ng_0, au2013 AS ng_1, geom AS the_geom FROM (SELECT au2013, ST_Union(geom) AS geom FROM non_gadm.nz GROUP BY au2013) t_nz
                      UNION ALL
                      SELECT 'france' AS ng_0, code_iris AS ng_1, geom AS the_geom FROM (SELECT code_iris, ST_Union(geom) AS geom FROM non_gadm.france GROUP BY code_iris) t_france
                      UNION ALL
                      SELECT 'eu' AS ng_0, nuts_id AS ng_1, geom AS the_geom FROM (SELECT nuts_id, ST_Union(geom) AS geom FROM non_gadm.eu_nuts2 GROUP BY nuts_id) t_eu
                      UNION ALL
                      SELECT 'us' AS ng_0, geoid AS ng_1, geom AS the_geom FROM (SELECT geoid, ST_Union(geom) AS geom FROM non_gadm.us_bgs GROUP BY geoid) t_us
                      UNION ALL
                      SELECT 'ca' AS ng_0, CAST(ctuid AS VARCHAR) AS ng_1, geom AS the_geom FROM non_gadm.ca_CTs;''')
        self.db.fix_permissions_of_new_table('all_boundaries',schema='non_gadm')
        self.db.create_indices('all_boundaries',geom='the_geom', schema='non_gadm') 

        # this is for disconnectivity - geographies needed at each id level
        # in practice, we just create fake tables here to simplify matteres. 
        self.db.execute('DROP TABLE IF EXISTS non_gadm.all_boundaries_ng_0;')
        self.db.execute('CREATE TABLE non_gadm.all_boundaries_ng_0 AS SELECT DISTINCT ng_0, Null AS geom, Null as geog, Null as ls_pop, Null as ls_density FROM non_gadm.all_boundaries;')
        self.db.execute('DROP TABLE IF EXISTS non_gadm.all_boundaries_ng_1;')
        self.db.execute('CREATE TABLE non_gadm.all_boundaries_ng_1 AS SELECT ng_0, ng_1, the_geom AS geom, Null as geog, Null as ls_pop, Null as ls_density FROM non_gadm.all_boundaries;')

    def createLookups(self, cr, mode='nodes'):
        """Adapted from osm_lookups, which is hardcoded to gadm ids, and also catches nodes/edges that don't intersect any id"""
        
        lookup_table = 'lookup_planet_'+{'nodes':'cluster','edges':'edge'}[mode]+cr+'_nongadm'
        strlu = {'fromtable':mode+'_planet_'+cr,
                 'newtable': lookup_table,
                 'tmptable' : lookup_table+'_tmpmerge',
                 'schema' : 'non_gadm',
                 'mode': mode,
                 'geoCol': {'nodes':'geog','edges':'geom'}[mode],
                 'restrictByDegree': (' AND nodes.degree != 2 ') * (mode=='node'),
                 'key': {'nodes':'cluster_id','edges':'edge_id'}[mode],}
             
        if mode=='nodes': # create 'fake' index on cast geometry, which should already exist
            self.db.execute('CREATE INDEX IF NOT EXISTS %(fromtable)s_geogcast_idx  ON %(fromtable)s  USING gist  (CAST(geog AS geometry));'%strlu)

        if not self.forceUpdate and strlu['newtable'] in self.db.list_tables(schema=defaults['osm']['processingSchema']):
            if self.logger: self.logger.write('Table '+strlu['newtable']+' already exists. Skipping lookup...')
            return
        if self.forceUpdate:
            self.db.execute('DROP TABLE IF EXISTS  %(schema)s.%(newtable)s '%strlu)
        if self.logger: self.logger.write( 'Creating lookup for %(mode)s for planet with fake gadm regions' % strlu)
        cmd = '''CREATE TABLE %(schema)s.%(newtable)s   AS
                         SELECT %(mode)s.%(key)s  as %(key)s, all_boundaries.ng_0, all_boundaries.ng_1
                         FROM %(fromtable)s AS %(mode)s, non_gadm.all_boundaries
                         WHERE ST_Intersects(%(mode)s.%(geoCol)s::geometry, all_boundaries.the_geom)   %(restrictByDegree)s  ;'''%strlu
        self.db.execute(cmd)
        self.db.fix_permissions_of_new_table('%(schema)s.%(newtable)s' %strlu)
        self.db.create_indices(strlu['newtable'], non_unique_columns=['ng_0', 'ng_1'], forceUpdate=True, schema='non_gadm') # No unique key, key = strlu['key'])

    def createbg2CellLookup(self):
        """Add a block group to grid cell lookup for the US, to allow weighted calculations
        Basic idea: rasterize the cell along our landscan grid, and calculate the area of each pixel"""
    
        if 'usbg_cell_lookup' in self.db.list_tables(schema='non_gadm') and not self.forceUpdate:
            if self.logger: self.logger.write('Already created bg2cell lookup. Skipping.')
            return
            
        # do this in two stages in order to allow use of indices
        cmd = '''DROP TABLE IF EXISTS non_gadm.bg_raster_tmp;
                 CREATE TABLE non_gadm.bg_raster_tmp AS
                    SELECT (ST_PixelAsPolygons(ST_Asraster(geom, 1./120, 1./120,-180,0,'1BB'::text),1,False)).geom AS pgeom, geoid
                    FROM non_gadm.us_bgs;'''
        self.db.execute(cmd)
        self.db.create_indices('bg_raster_tmp', geom='pgeom', non_unique_columns=['geoid'], schema='non_gadm')
    
        cmd = '''DROP TABLE IF EXISTS non_gadm.usbg_cell_lookup;
                 CREATE TABLE non_gadm.usbg_cell_lookup AS
                 SELECT b.geoid,
                        TRUNC((ST_Y(ST_Centroid(pgeom))-90)/-180*21600)::int AS row,
                        TRUNC((ST_X(ST_Centroid(pgeom))+180)/360*43200)::int AS col,
                        CASE WHEN ST_Within(pgeom, geom) THEN ST_Area(pgeom::geography)
                             ELSE ST_Area(ST_Intersection(pgeom, geom)::geography) END AS area,
                        Null::float AS gridcell_frc, Null::float AS bg_frc
                 FROM non_gadm.us_bgs b, non_gadm.bg_raster_tmp r
                 WHERE b.geoid=r.geoid AND ST_Intersects(pgeom, geom);'''
        self.db.execute(cmd)
    
        # calculate fractions (columns are already there - need to populate)
        cmd = '''WITH g AS (SELECT row, col, SUM(area) AS cellarea FROM non_gadm.usbg_cell_lookup GROUP BY row, col),
                      b AS (SELECT geoid,    SUM(area) AS bgarea   FROM non_gadm.usbg_cell_lookup GROUP BY geoid)
                 UPDATE non_gadm.usbg_cell_lookup l 
                        SET gridcell_frc = area / cellarea, bg_frc = area / bgarea
                        FROM g, b
                        WHERE l.geoid = b.geoid AND l.row = g.row AND l.col = g.col;'''
        self.db.execute(cmd)
        self.db.execute('DROP TABLE non_gadm.bg_raster_tmp;')

    def aggregate(self):
        aggmetrics.GADM_TABLE = 'all_boundaries'
        aggmetrics.GADM_IDs = ['ng_0', 'ng_1']
        agg=aggmetrics.aggregate_node_and_edge_results(resolution=[0,1],density=True, ghsl=True, urban=True,
                                                   aggregation_IDs=['ng_0','ng_1'], edge_lookup_table = 'non_gadm.lookup_planet_edge{}_nongadm'.format(defaults['osm']['clusterRadius']),
                                                   node_lookup_table = 'non_gadm.lookup_planet_cluster{}_nongadm'.format(defaults['osm']['clusterRadius']),
                                                   aggregation_geometry_table = 'non_gadm.all_boundaries',
                                                   forceUpdate=self.forceUpdate)
        agg.run_all(skipCellsandRegions=True)
        disconnector=aggmetrics.disconnectivity_analysis(density=True, ghsl=True, urban=True, 
                                        aggregation_IDs=['ng_0','ng_1'],
                                        aggregation_geometry_table = 'non_gadm.all_boundaries')
        disconnector.create_final_connectivity_tables(resolution=[0,1],include_geom=False, forceUpdate=True)

    def run_felix_aggregation(self):
        """Special Japan aggregation for Felix. 
        This could be integrated into nongadm, but safer to keep separate"""
        cr = '10'
        # this is for disconnectivity - geographies needed at each id level
        # in practice, we just create fake tables here. 
        self.db.execute('DROP TABLE IF EXISTS non_gadm.japan_felix_osm_id;')
        self.db.execute('CREATE TABLE non_gadm.japan_felix_osm_id AS SELECT DISTINCT osm_id, Null AS geom, Null as geog, Null as ls_pop, Null as ls_density FROM non_gadm.japan_felix;')

        # create lookups
        self.db.execute('CREATE INDEX IF NOT EXISTS nodes_planet_%(cr)s_geogcast_idx  ON nodes_planet_%(cr)s  USING gist  (CAST(geog AS geometry));'%{'cr':cr})
        for mode in ['nodes','edges']:
            lookup_table = 'lookup_planet_'+{'nodes':'cluster','edges':'edge'}[mode]+cr+'_japan_felix'
            strlu = {'fromtable':mode+'_planet_'+cr,
                 'newtable': lookup_table,
                 'tmptable' : lookup_table+'_tmpmerge',
                 'schema' : 'non_gadm',
                 'mode': mode,
                 'geoCol': {'nodes':'geog','edges':'geom'}[mode],
                 'restrictByDegree': (' AND nodes.degree != 2 ') * (mode=='node'),
                 'key': {'nodes':'cluster_id','edges':'edge_id'}[mode],}                

            if not self.forceUpdate and strlu['newtable'] in self.db.list_tables(schema=defaults['osm']['processingSchema']):
                if self.logger: self.logger.write('Table '+strlu['newtable']+' already exists. Skipping lookup...')
                continue
            if self.forceUpdate:
                self.db.execute('DROP TABLE IF EXISTS  %(schema)s.%(newtable)s '%strlu)
            if self.logger: self.logger.write( 'Creating lookup for %(mode)s for planet with fake gadm regions' % strlu)
            cmd = '''CREATE TABLE %(schema)s.%(newtable)s   AS
                            SELECT %(mode)s.%(key)s  as %(key)s, japan_felix.osm_id
                            FROM %(fromtable)s AS %(mode)s, non_gadm.japan_felix
                            WHERE ST_Intersects(%(mode)s.%(geoCol)s::geometry, japan_felix.geom)   %(restrictByDegree)s  ;'''%strlu
            self.db.execute(cmd)
            self.db.fix_permissions_of_new_table('%(schema)s.%(newtable)s' %strlu)
            self.db.create_indices(strlu['newtable'], non_unique_columns=['osm_id'], forceUpdate=True, schema='non_gadm') # No unique key, key = strlu['key'])

        # now aggregate
        aggmetrics.GADM_TABLE = 'japan_felix'
        aggmetrics.GADM_IDs = ['osm_id']
        agg=aggmetrics.aggregate_node_and_edge_results(resolution=[0],density=True, ghsl=True, urban=True,
                                                   aggregation_IDs=['osm_id'], edge_lookup_table = 'non_gadm.lookup_planet_edge{}_japan_felix'.format(defaults['osm']['clusterRadius']),
                                                   node_lookup_table = 'non_gadm.lookup_planet_cluster{}_japan_felix'.format(defaults['osm']['clusterRadius']),
                                                   aggregation_geometry_table = 'non_gadm.japan_felix',
                                                   forceUpdate=self.forceUpdate)
        agg.run_all(skipCellsandRegions=True)
        disconnector=aggmetrics.disconnectivity_analysis(density=True, ghsl=True, urban=True, 
                                        aggregation_IDs=['osm_id'],
                                        aggregation_geometry_table = 'non_gadm.japan_felix')
        disconnector.create_final_connectivity_tables(resolution=[0],include_geom=False, forceUpdate=True)

        df = self.db.db2df('disconnectivity_osm_id_planet_10_urban')
        df.to_csv(paths['working']+'Japan_stations.csv', index=False)
        print(('Saved output for Felix as {}'.format(paths['working']+'Japan_stations.csv')))
        
    def runall(self):
        if defaults['osm']['continents']!=['planet']:
            print('Skipping nongadm because continent is not [planet]')
            return
        self.loadGeometries()
        for cr in defaults['osm']['clusterRadii']:
            self.createLookups(cr, 'nodes')
            self.createLookups(cr, 'edges')
        self.createbg2CellLookup()
        self.aggregate()
        
class automobility_analysis():
    """Uses the non-GADM aggregates to analyze car ownership and frc commute by walk against disconnectivity"""
    def  __init__(self, logger=None):
        self.USdf  = None
        self.japanDf = None
        self.franceDf = None
        self.allDf = None
        self.logger = osmt.osm_logger('beyond_gadm', verbose=True, timing=True) if logger is None else logger

    def getCarData(self, urban=True, forceUpdate=False):
        """merge car ownership with disconnectivity data
        urban uses only disconnectivity data from urban areas (as defined by us), but car ownership data may not match"""
    
        if self.allDf is not None and not forceUpdate:
            return self.allDf
    
        if os.path.exists(paths['scratch']+'beyond_gadm.pandas') and os.path.exists(paths['scratch']+'beyond_gadm.tsv') and not forceUpdate:
            self.allDf = pd.read_pickle(paths['scratch']+'beyond_gadm.pandas')
            return self.allDf
    
        db = osmt.pgisConnection(logger=self.logger)
    
        df = db.db2df('disconnectivity_ng_1_planet_10'+'_urban'*urban)
        from metric_settings import add_standard_derived_metrics
        df = add_standard_derived_metrics(df)
        assert df.ng_1.is_unique
    
        areas = db.execfetch('SELECT ng_0, ng_1, ST_Area(the_geom::geography)/1000/1000 FROM non_gadm.all_boundaries;')
        areas = pd.DataFrame([list(rr) for rr in areas], columns=['ng_0','ng_1','area_sqkm'])
    
        # load car ownership data for EU NUTS 2
        EUdf = pd.read_csv(paths['otherinput']+'cars_country/EU/tran_r_vehst.tsv', sep='\t')
        #EUpop = pd.read_csv(paths['otherinput']+'cars_country/EU/demo_r_d2jan.tsv', sep='\t')
    
        EUdf = EUdf[EUdf['vehicle,unit,geo\\time'].str.startswith('CAR,P_THAB')]
        EUdf = EUdf[EUdf['2016 ']!=': '] # get rid of missing     
        EUdf['ng_0'] = 'eu'
        EUdf['ng_1'] = EUdf['vehicle,unit,geo\\time'].apply(lambda x: x.split(',')[-1])
        EUdf['cars_pers']= EUdf['2016 '].astype(float)/1000
    
        # P_THAB is per thousand inhabitants, so need to convert to per HH for consistency
        # For consistency, compute HH size using same census 2011 dataset (no more recent data on HH size exist, it seems)
        EUhhs = pd.read_csv(paths['otherinput']+'cars_country/EU/cens_11htts_r2.tsv', sep='\t')
        EUhhs = EUhhs[EUhhs['hhcomp,tenure,unit,time\geo'].str.startswith('TOTAL,TOTAL')].drop('hhcomp,tenure,unit,time\geo',axis=1).T
        EUhhs.columns = ['hhs']
        EUhhs = EUhhs[EUhhs.hhs!=': '].astype(float)  # get rid of missing 
        EUhhs[EUhhs['hhs']==0] = np.nan 
        EUhhs.index = EUhhs.index.str.strip()
        EUhhs.index.name = 'ng_1'

        EUpop = pd.read_csv(paths['otherinput']+'cars_country/EU/cens_11hou_r2.tsv', sep='\t') 
        EUpop = EUpop[EUpop['sex,age,housing,unit,time\geo'].str.startswith('T,TOTAL,TOTAL')].drop('sex,age,housing,unit,time\geo',axis=1).T
        EUpop.columns = ['pop']
        EUpop = EUpop[EUpop['pop']!=': '].astype(float) # get rid of missing 
        EUpop[EUpop['pop']==0] = np.nan
        EUpop.index = EUpop.index.str.strip()
        EUpop.index.name = 'ng_1'    
    
        EUdf = EUdf.set_index('ng_1').join(EUhhs).join(EUpop).reset_index()
        EUdf['hh_size'] = EUdf['pop'] / EUdf.hhs
        EUdf['cars_HH']= EUdf.cars_pers / EUdf.hh_size
        EUdf['country'] = EUdf.ng_1.str.slice(0,2)
        
        # Load car ownership data for France IRIS
        franceDf = pd.read_excel(paths['otherinput']+'cars_country/France/base-ic-logement-2015.xls', sheet_name='IRIS', skiprows=5)
        franceDf = franceDf[['IRIS','P15_PMEN','P15_MEN','P15_RP_VOIT1', 'P15_RP_VOIT2P']]
        franceDf.rename(columns={'IRIS':'ng_1', 'P15_PMEN':'pop', 'P15_MEN':'hhs', 'P15_RP_VOIT1':'hhs_1car', 'P15_RP_VOIT2P':'hhs_2pluscars'}, inplace=True)
        franceDf['ng_0'] = 'france'
        franceDf['cars_HH'] = (franceDf.hhs_1car + franceDf.hhs_2pluscars*2).astype(float) / franceDf.hhs
    
        # load car ownership data for US census block groups
        USdf = self.loadUSdf()
    
        # prefectures and Tokyo - superceded
        if 0:
            JPNprefdf = pd.read_csv(paths['otherinput']+'cars_country/Japan/Japan_cars_2017.tsv', encoding='utf-16le', sep='\t')
            JPNprefdf.rename(columns={'GADM_id':'ng_1','Per_HH':'cars_HH'}, inplace=True)
            JPNprefdf['ng_0'] = 'japan_pref'
    
            JPNmundf = pd.read_csv(paths['otherinput']+'cars_country/Japan/tokyo_2016.csv')
            JPNmundf['cars_HH'] = JPNmundf.cars.astype(float)/JPNmundf.hhs
            #JPNmundf.rename(columns={'municipality':'ng_1'}, inplace=True)
            JPNmundf['ng_1'] = JPNmundf.municipality.str.replace('-',' ').str.title()
            JPNmundf['ng_0'] = 'japan_mun'
            JPNmundf['pop'] = JPNmundf.hhs * JPNmundf.hh_size
       
            # waiting for full country data before merging
            JPNpop = pd.read_csv(paths['otherinput']+'cars_country/Japan/FEI_CITY_180606071352.csv', skiprows=8)
            JPNpop = JPNpop[JPNpop.YEAR==2015]
            JPNpop['pop'] = JPNpop['A1101_Total population[person]'].str.replace(',','').apply(lambda x: np.nan if x=='***' else float(x))
            JPNpop['hhs'] = JPNpop['A7101_Total households'].str.replace(',','').apply(lambda x: np.nan if x=='***' else float(x))
            JPNpop['ng_1'] = JPNpop.AREA.apply(lambda x: x.split()[-1])
      
        japanDf = self.loadJapanDf()
        
        # load household data file
        NZcols = {'Area_Code_and_Description':'Code_and_Desc', 'Code':'ng_1',
                  '2013_Census_number_of_motor_vehicles_for_households_in_occupied_private_dwellings_No_Motor_Vehicle':'nocar',
                  '2013_Census_number_of_motor_vehicles_for_households_in_occupied_private_dwellings_One_Motor_Vehicle':'onecar',
                  '2013_Census_number_of_motor_vehicles_for_households_in_occupied_private_dwellings_Two_Motor_Vehicles':'twocars',
                  '2013_Census_number_of_motor_vehicles_for_households_in_occupied_private_dwellings_Three_or_More_Motor_Vehicles':'threecars',
                  '2013_Census_number_of_motor_vehicles_for_households_in_occupied_private_dwellings_Total_households_stated':'carsdenom',
                  '2013_Census_number_of_motor_vehicles_for_households_in_occupied_private_dwellings_Not_Elsewhere_Included(19)':'carsna',
                  '2013_Census_number_of_motor_vehicles_for_households_in_occupied_private_dwellings_Total_households_in_occupied_private_dwellings':'hhs'}
        NZdf = pd.read_csv(paths['otherinput'] +'cars_country/NZ/2013-mb-dataset-Total-New-Zealand-Household.csv', usecols=NZcols,skipfooter=32,engine='python') # 32 rows of footnotes
        NZdf.rename(columns=NZcols, inplace=True)
        if 0: # for meshblock, but we want area unit level
            NZdf = NZdf[NZdf['Code_and_Desc'].str.startswith('MB')].set_index('Code_and_Desc')
        else:
            NZdf = NZdf[np.logical_not(NZdf['Code_and_Desc'].str.startswith('MB'))]
            NZdf = NZdf[NZdf.ng_1.str.len()==6].set_index('ng_1')
        
        for col in ['nocar','onecar','twocars','threecars']:
            NZdf[col] = pd.to_numeric(NZdf[col],'coerce')
        NZdf['n_HH'] = NZdf[['nocar','onecar','twocars','threecars']].sum(axis=1)
        NZdf['cars_HH'] = (NZdf.onecar+NZdf.twocars*2+NZdf.threecars*3).astype(float)/NZdf.n_HH
    
        # load individual data file
        NZcols = {'Area_Code_and_Description':'Code_and_Desc','Code':'ng_1',
                  '2013_Census_main_means_of_travel_to_work_for_the_employed_census_usually_resident_population_count_aged_15_years_and_over(1)_Worked_at_Home':'home',
                  '2013_Census_main_means_of_travel_to_work_for_the_employed_census_usually_resident_population_count_aged_15_years_and_over(1)_Did_Not_Go_to_Work_Today':'nowork',
                  '2013_Census_main_means_of_travel_to_work_for_the_employed_census_usually_resident_population_count_aged_15_years_and_over(1)_Drove_a_Private_Car_Truck_or_Van':'drovealone',
                  '2013_Census_main_means_of_travel_to_work_for_the_employed_census_usually_resident_population_count_aged_15_years_and_over(1)_Drove_a_Company_Car_Truck_or_Van':'drovealone_company',
                  '2013_Census_main_means_of_travel_to_work_for_the_employed_census_usually_resident_population_count_aged_15_years_and_over(1)_Passenger_in_a_Car_Truck_Van_or_Company_Bus':'passenger',
                  '2013_Census_main_means_of_travel_to_work_for_the_employed_census_usually_resident_population_count_aged_15_years_and_over(1)_Public_Bus':'bus',
                  '2013_Census_main_means_of_travel_to_work_for_the_employed_census_usually_resident_population_count_aged_15_years_and_over(1)_Train':'train',
                  '2013_Census_main_means_of_travel_to_work_for_the_employed_census_usually_resident_population_count_aged_15_years_and_over(1)_Motor_Cycle_or_Power_Cycle':'mc',
                  '2013_Census_main_means_of_travel_to_work_for_the_employed_census_usually_resident_population_count_aged_15_years_and_over(1)_Bicycle':'bike',
                  '2013_Census_main_means_of_travel_to_work_for_the_employed_census_usually_resident_population_count_aged_15_years_and_over(1)_Walked_or_Jogged':'walk',
                  '2013_Census_main_means_of_travel_to_work_for_the_employed_census_usually_resident_population_count_aged_15_years_and_over(1)_Other':'other',
                  '2013_Census_main_means_of_travel_to_work_for_the_employed_census_usually_resident_population_count_aged_15_years_and_over(1)_Total_people_stated':'totalstated',
                  '2013_Census_main_means_of_travel_to_work_for_the_employed_census_usually_resident_population_count_aged_15_years_and_over(1)_Not_Elsewhere_Included(5)':'notinc',
                  '2013_Census_main_means_of_travel_to_work_for_the_employed_census_usually_resident_population_count_aged_15_years_and_over(1)_Total_people':'pop'}
        NZdf2 = pd.read_csv(paths['otherinput'] +'cars_country/NZ/2013-mb-dataset-Total-New-Zealand-Individual-Part-3b.csv', usecols=NZcols)
        NZdf2.rename(columns=NZcols, inplace=True)
        NZdf2 = NZdf2[np.logical_not(NZdf2['Code_and_Desc'].str.startswith('MB'))]
        NZdf2 = NZdf2[NZdf2.ng_1.str.len()==6].set_index('ng_1')

        for col in NZdf2.columns:
            NZdf2[col] = pd.to_numeric(NZdf2[col],'coerce')
        NZdf2['n_people'] = NZdf2[['home','nowork','drovealone','drovealone_company','passenger','bus','train','mc','bike','walk','other']].sum(axis=1,skipna=False)
        NZdf2['frcwalk'] = NZdf2.walk.astype(float)/NZdf2.n_people
        NZdf2['frcdrivealone'] = NZdf2[['drovealone','drovealone_company']].sum(axis=1).astype(float)/NZdf2.n_people

        NZdf = NZdf.join(NZdf2.drop('Code_and_Desc',axis=1)).reset_index() # join on geoID
        NZdf['ng_0'] = 'nz'
    
        allDf = pd.concat([EUdf[['ng_0','ng_1','cars_HH','cars_pers','pop','hhs','country']],
                           franceDf[['ng_0','ng_1','cars_HH','pop','hhs']],
                           USdf[['ng_0','ng_1','cars_HH','frcwalk','frcdrivealone','popDensity','pop','hhs']], 
                           NZdf[['ng_0','ng_1','cars_HH','frcwalk','frcdrivealone','pop','hhs']],
                           japanDf[['ng_0','ng_1','ng_1_name','cars_HH','pop','hhs']],
                           ], sort=True).set_index(['ng_0','ng_1']).join(areas.set_index(['ng_0','ng_1']))
        allDf['popDensity'] = allDf['popDensity'].combine_first(allDf['pop'] / allDf.area_sqkm)
        allDf.loc[pd.isnull(allDf.country),'country'] = 'na'  # for non-EU
    
        df = df.set_index(['ng_0','ng_1']).join(allDf)
        assert df.index.is_unique  # we should not have any repeated ids
        df.to_pickle(paths['scratch']+'beyond_gadm.pandas')
        df.to_csv(paths['scratch']+'beyond_gadm.tsv', sep='\t') 
        print(('Saved %sbeyond_gadm.tsv and .pandas' % paths['scratch']))
        self.allDf = df
        
        return df


    def loadUSdf(self):
        """Load car ownership data for US census block groups"""
        # Used by both addCarData and gridCellPlot, so put this in a separate function
        # Note that original should be on OKAI - paths['scratch'] + 'census_data_all_bglevel.pandas' (see county_tools.py)
    
        if self.USdf is None:         
            # Extract census data
            dfc = paths['otherinput']+'cars_country/USA/census/ACS2016_bgs.pandas'
            if not(os.path.exists(dfc)):
                USdf, _ = unpackUSCensus()  # returns bgs, tracts

                # get land area
                db = osmt.pgisConnection(curType='default',logger=self.logger)
                aland = pd.DataFrame(db.execfetch('SELECT geoid, aland/10^6 from non_gadm.us_bgs;'), columns=['GEOID','aland']).set_index('GEOID')
                USdf = USdf.join(aland)
                
                USdf.reset_index(inplace=True)
                USdf['ng_0'] = 'us'
                USdf.rename(columns={'GEOID':'ng_1', 'B01003_Total':'pop', 'B25044_Total:':'hhs'}, inplace=True)
                USdf['cars_HH'] = USdf['B25046_Aggregate number of vehicles available:'] / USdf['hhs']
                USdf['frcdrivealone'] = USdf['B08301_Drove alone'].astype(float) / USdf['B08301_Total:']
                USdf['frcwalk'] = USdf['B08301_Walked'].astype(float) / USdf['B08301_Total:']
                USdf['popdensity'] =  USdf['pop'].astype(float)/USdf.aland
                USdf = USdf[['ng_0','ng_1','pop','popDensity','hhs','cars_HH','frcdrivealone','frcwalk']]
                
                USdf.to_pickle(dfc)
            else:
                USdf = pd.read_pickle(dfc)
            self.USdf = USdf
            
        return self.USdf    

    def loadJapanDf(self):
        """Loads car ownership data for Japanese municipalities
        This is complicated in terms of the joins, cleaning, etc. - hence the separate function
        Returns and stores dataframe
        Note columns with Japanese names can be read using str.decode('Shift_JIS')
        """
        if self.japanDf is not None: return self.japanDf
        
        # load car registration data
        dfc = pd.read_excel(paths['otherinput']+'cars_country/Japan/purchased/市区町村別_平成29年3月末.xlsx',sheet_name=1)

        # Important: the car data are by place of registration
        # There was a restructuring of municipalities. They go by what the owner used (old or new)
        # So just using the "new" will miss the people who didn't change their registration. 
        # This number seems small (we drop ~2% of cars, and this includes registration unknown
 
        # clean up municipality names
        # Hokkaido - delete the last 3 characters (運輸局) which are "transport bureau"
        dfc['id'] = dfc['地方運輸局'].str.slice(0,-3)+'_'+dfc['市区町村']
        # Other prefectures - add the last character which is "prefecture" (or "fu" for Osaka or "metro area" for Tokyo
        dfc.loc[dfc['地方運輸局']!='北海道運輸局','id'] = dfc['支局']+'県_'+dfc['市区町村']
        dfc.loc[dfc['支局']=='大阪','id'] = dfc['支局']+'府_'+dfc['市区町村']  # this is Osaka
        dfc.loc[dfc['支局']=='京都','id'] = dfc['支局']+'府_'+dfc['市区町村']  # this is Kyoto
        dfc.loc[dfc['支局']=='東京','id'] = dfc['支局']+'都_'+dfc['市区町村']  # this is Tokyo
        
        # Manual matches for municipalities that have slighly different spellings
        for oldId, newId in [('岩手県_胆沢郡金ヶ崎町', '岩手県_胆沢郡金ケ崎町'), ('茨城県_龍ヶ崎市', '茨城県_龍ケ崎市'),
                             ('埼玉県_鶴ケ島市','埼玉県_鶴ヶ島市'), ('千葉県_鎌ヶ谷市', '千葉県_鎌ケ谷市'), ('千葉県_袖ヶ浦市','千葉県_袖ケ浦市'),
                             ('東京都_三宅支庁三宅島三宅村', '東京都_三宅支庁三宅村'), ('東京都_八丈支庁八丈島八丈町', '東京都_八丈支庁八丈町'),
                             ('岐阜県_不破郡関ヶ原町', '岐阜県_不破郡関ケ原町'), ('熊本県_芦北郡芦北町', '熊本県_葦北郡芦北町'),
                             ('熊本県_芦北郡津奈木町', '熊本県_葦北郡津奈木町'), ('福岡県_糟屋郡須惠町', '福岡県_糟屋郡須恵町')]:
            dfc.loc[dfc.id==oldId,'id'] = newId

        dfc.rename(columns={'乗用計':'n_cars'},inplace=True)

        dfc = dfc[dfc['業態']=='自家用'] # get household cars only (exclude business)
        dfc.set_index('id', inplace=True)
        dfc = dfc[['地方運輸局','支局','市区町村','n_cars']]
        dfc.loc[dfc['n_cars']=='-','n_cars']=np.nan
        
        # drop the subtotals (to avoid double counting)
        dfc = dfc[np.logical_not(dfc['市区町村'].str.endswith('計'))]
        # drop "unknown" municipality and 'before govt decree' boundaries
        dfc = dfc[np.logical_not(dfc['市区町村'].str.endswith('不明'))]
        dfc = dfc[np.logical_not(dfc['市区町村'].str.endswith('（政令移行前）'))]

        assert dfc.index.is_unique

        # geographical boundaries - put the geoid (i.e. n03_007 = ng_1) into the car dataframe   
        db = osmt.pgisConnection(logger=self.logger)
        db.connection.set_client_encoding('Shift_JIS')
        dfg = db.db2df('japan',schema='non_gadm').set_index('gid')
        dfg.drop_duplicates(inplace=True)  # because of islands, etc.
        dfg=dfg[pd.notnull(dfg.n03_007)]  # tiny islands not assigned to a prefecture
        dfg.loc[pd.isnull(dfg.n03_003),'n03_003'] = ''
        dfg['id'] = dfg['n03_001'].str.decode('Shift_JIS')+'_'+dfg['n03_003'].str.decode('Shift_JIS')+dfg['n03_004'].str.decode('Shift_JIS')
        dfg.set_index('id', inplace=True)
        assert dfg.index.is_unique

        # Missing ids in geography data frame are 1695-1700: Kuril islands (disputed, controlled by Russia)
        # so let's drop them
        dfg = dfg[np.logical_not(dfg.n03_007.isin(['0'+str(xx) for xx in range(1695,1701)]))]

        # now let's add the population estimates
        dfp = pd.read_csv(paths['otherinput']+'cars_country/Japan/FEH_00200521_180718024903.csv',encoding='Shift_JIS',skiprows=1)
        #dfp.rename(columns={u'人口【人】':'n_people',},inplace=True) # u'地域（2015） コード':'N03_007'
        dfp['pop'] = dfp['人口【人】'].apply(lambda x: np.nan if x=='***' else float(x.replace(',','')))
        dfp['hhs'] = dfp['世帯数【世帯】'].apply(lambda x: np.nan if x=='***' else float(x.replace(',','')))

        dfp.rename(columns={'地域（2015） コード':'n03_007'},inplace=True) # for consistent names with dfc and dfg
        dfp = dfp[dfp['全域・人口集中地区（2015） コード']==710]  # This is "all areas", not subdistricts
        dfp = dfp[pd.notnull(dfp.n03_007)]
        dfp.set_index('n03_007',inplace=True)
        assert dfp.index.is_unique

        # join all 3
        df = dfg.join(dfc['n_cars'], how='inner')  
        assert len(df)==len(dfg) # all geographical units should be joined, but ~2% of cars in car dataframe left out
        df = df.reset_index().set_index('n03_007').join(dfp[['pop','hhs']])
        df['cars_HH'] = df.n_cars.astype(float)/df.hhs
        df.loc[df.cars_HH>50,'cars_HH'] = np.nan # error in aggregation

        # add iso codes
        df.reset_index(inplace=True)
        df.id=df.id.str.encode('UTF-8')
        df.rename(columns={'n03_007':'ng_1', 'id':'ng_1_name'}, inplace=True)
        df['ng_0'] = 'japan'
        
        self.japanDf = df
        return df
            
    def gridCellPlot(self, reverseAxis=False):
        """For the US (which has higher-resolution data), how does car ownership and frc walk vary with PCA1?
        if reverseAxis is True, then the frcWalk axis runs from 1 to 0 (March 2019 behavior, but the reviewer didn't like it)"""

        db = osmt.pgisConnection(logger=self.logger)
    
        USdf = self.loadUSdf().rename(columns={'ng_1':'geoid'})
    
        # get cluster ids
        colsToUse = kmeans.kmDefaultCols
        km = kmeans.kmeans_analyzer(nClusters=kmeans.defaultK, columns=[cc for cc in colsToUse], byCell=True,clusterradius=defaults['osm']['clusterRadii'][0],dropClusters=kmeans.kmDefaultDrop,sortCol='pca1')
        clustDf = db.db2df('kmeans_k'+str(kmeans.defaultK)+'_10_urban').set_index(['row','col'])

        # get lookups to grid cells from server
        lookup = db.db2df('usbg_cell_lookup', schema='non_gadm').set_index('geoid')

        # add weighted car ownership and frcwalk
        lookup = lookup.join(USdf.set_index('geoid'))
        lookup.set_index(['row','col'], inplace=True)
        for metric in ['frcwalk','cars_HH','frcdrivealone']:
            mask = pd.notnull(lookup[metric])  # recalculate gridcell_frc excluding bgs with NA
            lookup['tmparea'] = lookup[mask].groupby(level=[0,1]).area.sum()
            lookup['tmpfrc'] = lookup.area.astype(float) / lookup.tmparea  # note frc will be >1 for cells with no bg data on that metric
            clustDf[metric] = (lookup.loc[mask,metric]*lookup.loc[mask,'tmpfrc']).groupby(level=[0,1]).sum()
    
        clustDf=clustDf[clustDf.cluster.isin(km.clusterLabelMap)]
        clustDf['clusterLabel'] = clustDf.cluster.apply(lambda x: km.clusterLabelMap[x])
    
        meanDf = clustDf.groupby('clusterLabel')[['frcwalk','cars_HH','frcdrivealone']].mean().reset_index()
        meanDf['clusterName'] = meanDf.clusterLabel.apply(lambda x: kmeans.clusterNames[x])
        if self.logger: self.logger.write(str(meanDf))
    
        # kmeans plot
        labelNames = {'cars_HH':'Cars per household','frcwalk':'Fraction commute by walking','frcdrivealone':'fraction commute by drive alone'}
        kdeColors = {km.clusterLabelMap[ii]:km.clustColors[ii] for ii in list(km.clusterLabelMap.keys())} # this is robust to dropping some clusters

        fig, axes = plt.subplots(2,2,figsize = (osmt.figSizes['2.5col'][0],osmt.figSizes['2.5col'][1]*2))
        for cl in list(km.clusterLabelMap.values()):
            mask = clustDf.clusterLabel==cl
            for ax, metric in zip(axes[0],['cars_HH','frcwalk',]): # 'frcdrivealone']):
                clustDf.loc[mask,metric].plot(kind='kde',ax=ax,label=cl+': '+kmeans.clusterNames[cl], color=kdeColors[cl])
                if cl==list(km.clusterLabelMap.values())[-1]:  # last iteration through axes
                    if metric=='cars_HH': ax.set_ylabel('Kernel density') # only on left plot
                    #ax.set_xlabel(metric) # this is on lower plot
                    xlims = {'cars_HH':(0.5,3),'frcwalk':(0,0.1),'frcdrivealone':(0.5,1)}[metric]
                    ax.set_xlim(xlims[0],xlims[1])
                    if metric=='frcwalk': 
                        ax.set_ylabel('')
                        ax.legend(fontsize=8)
            # now plot empirical cdf
            for ax, metric in zip(axes[1],['cars_HH','frcwalk','frcdrivealone']):
                xvals = clustDf.loc[mask,metric].dropna().sort_values()
                cdf = empiricalcdf(xvals)
                ax.plot(xvals,cdf, label=cl+': '+kmeans.clusterNames[cl], color=kdeColors[cl])
                if cl==list(km.clusterLabelMap.values())[-1]:
                    if metric=='cars_HH': ax.set_ylabel('Cumulative density')
                    ax.set_xlabel(labelNames[metric])
                    xlims = {'cars_HH':(0.5,3),'frcwalk':(0,0.1),'frcdrivealone':(0.5,1)}[metric]
                    ax.set_xlim(xlims[0],xlims[1])
                    ax.set_yticks(np.arange(0,1.1,0.25))

        plt.tight_layout()
        figFn = paths['graphics']+'figSI-xs-USbgs-cars-walk-bycluster.pdf'
        fig.savefig(figFn)
        if self.logger: self.logger.write('Saved '+figFn)
        
        # do it the other way around - distribution of clusters by decile
        barColors = [km.clustColors[ii] for ii in sorted(clustDf.cluster.unique())]
        fig,axes = plt.subplots(2,1,figsize = (osmt.figSizes['2col'][0], osmt.figSizes['1col'][1]*2))
        fig.subplots_adjust(hspace=0.5, bottom=0.15)
        for ax,metric in zip(axes, ['cars_HH','frcwalk']):
            if 'quantile' in clustDf.columns: clustDf.drop('quantile',axis=1, inplace=True)
            labelName = labelNames[metric]
            qtiles = clustDf[metric].quantile(np.arange(0,1.1,0.1))
            if metric=='frcwalk': # lower quantiles not unique, so qcut doesn't work
                # what is the first quantile > 0?
                nquantiles = 10-(qtiles==0).sum()+1
                mask = clustDf[metric]>0
                clustDf.loc[mask,'quantile']=pd.qcut(clustDf.loc[mask, metric], nquantiles, labels=list(range(10-nquantiles+1,11)))
                clustDf.loc[clustDf[metric]==0,'quantile'] = 10-nquantiles
            else:
                clustDf['quantile']=pd.qcut(clustDf[metric], 10, labels=list(range(1,11)))
            mask = pd.notnull(clustDf['quantile'])
            plotDf = clustDf[mask].groupby(['quantile','cluster']).size()/clustDf[mask].groupby(['quantile']).size()
        
            plotDf = plotDf.reset_index().pivot(index='quantile',columns='cluster')
            plotDf.columns= plotDf.columns.droplevel()
            plotDf.rename(columns=km.clusterLabelMap, inplace=True)  # letter ID
            plotDf.rename(columns={cc:cc+': '+kmeans.clusterNames[cc] for cc in plotDf.columns},inplace=True) # add name
            plotDf.fillna(0,inplace=True)

            if metric=='frcwalk' and reverseAxis: # reverse sort to go from most walking to least
                plotDf.sort_index(ascending=False, inplace=True)
                
            plotDf.plot(kind='bar', stacked=True, color=barColors, ax=ax, legend=None)
            ax.set_xlim(-0.5,9.5) # needed for frc_walk which doesn't have quantiles 0-3
            ax.set_ylim(0,1)
            ax.set_ylabel('Fraction grid cells')
            ax.set_yticks([0,0.5,1])
            ax.set_yticks(np.arange(0,1.1,0.25),minor=True)
            ax.yaxis.set_tick_params(labelsize=7)
       
            ax.set_xlabel(labelName+' (by decile)')
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
        
            if metric=='frcwalk':
                ax.set_xticks(np.arange(-0.5,8,1))
                if reverseAxis:
                    ax.set_xticklabels(np.flip(np.round(qtiles.values,3),0)[:-2]) 
                else:
                    ax.set_xticklabels(np.round(qtiles.values,3)[2:]) 
            else:
                ax.set_xticks(np.arange(-0.5,10,1))
                ax.set_xticklabels(np.round(qtiles.values,2)) 
            ax.xaxis.set_tick_params(labelsize=7)
    
        handles, labels = axes[1].get_legend_handles_labels() # reverse order of legend
        axes[1].legend(handles[::-1], labels[::-1], fontsize=6, bbox_to_anchor=(0.8,0.95), frameon=False)
        figFn=paths['graphics']+'fig4-xs-US-cars-walk-bycluster-decile.pdf'
        fig.savefig(figFn)
        if self.logger: self.logger.write('Saved '+figFn)
    
    def runall(self):
        if defaults['osm']['continents']!=['planet']:
            print('Skipping nongadm because continent is not [planet]')
            return
        _ = self.getCarData() # create file for pystata
        pystata_regressions()
        self.gridCellPlot()

def unpackUSCensus(tablesToDo=None):
    """Gets the car ownership and commute tables from the giant ACS file
    Adapted from version in county_tools, but updated for 2016 ACS.
    tables can be a list of tables, or if None default B08301, B25044, B25046
    
    Returns tuple of dataframes for bgs, tracts"""
    
    if tablesToDo is None: tablesToDo = ['B01003','B08301', 'B25044', 'B25046'] 
    if isinstance(tablesToDo, str): tablesToDo = [tablesToDo]
    
    big_bg_file = tarfile.open(paths['otherinput']+'cars_country/USA/census/Tracts_Block_Groups_Only.tar.gz')
    big_bg_file.errorlevel=2  # forces exceptions if files cannot be extracted
    fnames=big_bg_file.getnames()
    scratchpath = paths['otherinput']+'cars_country/USA/census/scratch/'
    if not(os.path.exists(scratchpath)): os.mkdir(scratchpath)
    
    # read table lookup file, which gives info on where to find each table
    tlookup_all = pd.read_csv(paths['otherinput']+'cars_country/USA/census/ACS_5yr_Seq_Table_Number_Lookup.txt')
    all_tables = pd.DataFrame()
    for t in tablesToDo:
        print(('Extracting files for table '+t))

        tlookup = tlookup_all[tlookup_all['Table ID'] == t]
        seq = str(tlookup['Sequence Number'].values[0]).zfill(4) # identifies file to extract
        startCol = int(tlookup['Start Position'].values[0]-1)
        endCol = startCol + int(tlookup['Total Cells in Table'].values[0].split()[0])
        colNames = ['STATE','LOGRECNO']+[t+'_'+cc for cc in tlookup[pd.notnull(tlookup['Line Number'])]['Table Title'].values]
        
        fns = [ff for ff in fnames if ff.endswith(seq+'000.txt') 
                  and ff.startswith('data/tab4/sumfile/prod/2012thru2016/group2/e20165')
                  and '/e20165us' not in ff and '/e20165pr' not in ff] # exclude PR and USVI    
        for fn in fns: big_bg_file.extract(fn, path = scratchpath)
        table_df=pd.concat([pd.read_csv(scratchpath + fn, header=None,na_values='.', 
              usecols=[2,5]+list(range(startCol,endCol)), names=colNames, index_col=['STATE','LOGRECNO']) for fn in fns])
        for fn in fns: os.remove(scratchpath+fn)

        all_tables = pd.concat([all_tables, table_df], axis=1)
    
    big_bg_file.close()

    # add GEOID
    print('Joining to geoids')
    geoidDict = pd.read_excel(paths['otherinput']+'cars_country/USA/census/5_year_Mini_Geo.xlsx', sheet_name=None) # returns dict of states
    geoids = pd.concat([geoidDict[gg] for gg in geoidDict if gg not in ['US','PR']])
    geoids.STATE = geoids.STATE.str.lower()
    geoids.set_index(['STATE','LOGRECNO'], inplace=True)
    assert all_tables.index.is_unique and geoids.index.is_unique
    
    geoids.GEOID = geoids.GEOID.str.slice(7)  # remove header '15000US'

    all_tables = all_tables.join(geoids['GEOID'])
    
    bgs = all_tables[all_tables.GEOID.str.len()==12]
    tracts = all_tables[all_tables.GEOID.str.len()==11]    
    
    return bgs.set_index('GEOID'), tracts.set_index('GEOID')


def pystata_regressions(forceUpdate=False):
    """forceUpdate simply recreates the dta.gz file"""
    import pystata as pst
    #It's a shame that the following is needed. Is that because I'm importing inside the function?
    pst.defaults.update(defaults)
    pst.paths.update(paths)
    from pystata import stataSystem, tsv2dta, stataLoad,  df2dta, WPdta
    from cpblUtilities import str2pathname
    from pystata.latexRegressions import latexRegressionFile

    mVersion, rVersion = '2018', '1'
    latex = latexRegressionFile(
        'osm-cardata-regressions', modelVersion=mVersion, regressionVersion=rVersion, substitutions=[
            ['logdensity','log(density)'],
        ['negdegree', r'Nodal degree $\times(-1)$'],
            ['_cons','constant'],
            ['pca1', 'SNDi'],
            ['r2_a', r'R$^2_\textrm{adjusted}$'],
            ['cars_HH', 'Cars per household'],
            ['frcwalk','Fraction walking'],
            ])
    variableOrder =['pca1','negdegree','logdensity', 'country fixed effects','metro Japan'] #  'nation fixed effects', 
    latex.skipStataForCompletedTables = False
    tsv2dta(paths['scratch']+'beyond_gadm', extraStata=""" 
        drop if ng_0=="ca" | ng_0=="eu"
        encode ng_0, generate(ng_0_int)
        encode country, generate(country_int)
        generate logdensity = log(popDensity)
        generate japan_pref = substr(ng_1,1,2) if ng_0=="japan"
    tabulate ng_0_int, gen(ing0)
    tabulate country_int, gen(icountry)
    """, forceUpdate=forceUpdate)
    ings = ['ing0{}'.format(n) for n in range(5)]
    icountrys = ['icountry{}'.format(n) for n in range(35)]
    sout = pst.stataLoad(paths['scratch']+'beyond_gadm')
    oldstyle = latex.str2models( """
    *flag:country fixed effects
    regress cars_HH pca1 {ings}, robust beta
    *flag:country fixed effects
    regress cars_HH pca1 logdensity {ings}, robust beta
    *flag:country fixed effects
    regress cars_HH pca1 logdensity negdegree {ings}, robust beta
        *|
    *flag:country fixed effects
    regress frcwalk pca1 {ings}, robust beta
    *flag:country fixed effects
    regress frcwalk pca1 logdensity {ings}, robust beta
    *flag:country fixed effects
    regress frcwalk pca1 logdensity negdegree {ings}, robust beta 
    *|
    * For the SI (maybe don't show - just state in the text?) - with intra-European dummies too
    * no - not necessary now we are doing France only
    *flag:country fixed effects
    *flag:SIonly
    *regress cars_HH pca1 logdensity {ings} {icountrys}, robust beta
    * country interactions
    *regress cars_HH c.pca1##i.ng_0_int c.logdensity##i.ng_0_int , robust beta
    * only include metro prefectures in Japan - Tokyo, Kyoto, Osaka
    *flag:metro Japan
    *flag:SIonly
    regress cars_HH pca1 logdensity  if japan_pref=="13" | japan_pref=="26" | japan_pref=="27" , robust beta
        """.format(ings=' '.join(ings), icountrys=' '.join(icountrys))  )
    sout+=latex.regTable('cars-oldstyle', oldstyle,
                         variableOrder=variableOrder,
                         hidevars = icountrys+ings+[r'$\beta$ coefs', 'SIonly', '_cons', 'll', 'F'],
                         returnModels = True)
    from cpblUtilities import dgetget
    oldstyle[-3]['format'] ='c'
    latex.regTable('cars-oldstyle', oldstyle,
                         variableOrder=variableOrder,
                         hidevars = icountrys+ings+[r'$\beta$ coefs', 'SIonly', '_cons', 'll', 'F', 'beta'],
                   showModels = [m for m in oldstyle if not dgetget(m, ['flags','SIonly'], False)], skipStata = True,
                   extraTexFileSuffix = '-maintext')
    
    latex.closeAndCompile()
    stataSystem(sout,filename= paths['working']+'beyond_gadm_cars_for_stata')
    
    return()

if __name__ == '__main__':
    import inquirer
    runmode = inquirer.prompt([inquirer.List('A', message='Run which mode?', choices=[
        'run all',
        'automobility',
        'dev',
            ])])['A']
    if runmode in ['automobility']:
        df= automobility_analysis().getCarData(forceUpdate=False)
        pystata_regressions()
    if runmode in ['dev']:
        pystata_regressions()
    if runmode in ['dev2']:
        dfj=df[df.index.get_level_values(0)=='japan'].query('popDensity>0').dropna(subset=['pca1','cars_HH'])
        dfj['lpd'] = np.log10(dfj.popDensity)
        dfj[['pca1','cars_HH','popDensity','lpd']].to_stata('jtmp.dta')
        fig,ax = plt.subplots(1)
        ax.scatter(dfj.pca1, dfj.cars_HH, c = dfj.popDensity, alpha=.5, s=3, cmap="viridis_r")
        lpdRs= [-100, 1,2,3,4, 100]
        fig,ax = plt.subplots(1, figsize=(8,12))
        from cpblUtilities.mathgraph import overplotLinFit
        for ir, rr in enumerate(lpdRs[:-1]):
            dfjp = dfj.query('lpd>{} and lpd<={}'.format(rr, lpdRs[ir+1]))
            ax.plot(dfjp.pca1, dfjp.cars_HH, '.')
            overplotLinFit(dfjp.pca1, dfjp.cars_HH, ax=ax)
        plt.savefig('japan_multidensities.pdf')
        plt.show()
        
