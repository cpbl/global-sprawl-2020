# Barrington-Leigh and Millard-Ball 2019, 2020 street-network sprawl computation and analysis code

## For the latest version of this code, see [https://gitlab.com/cpbl/global-sprawl-2020](https://gitlab.com/cpbl/global-sprawl-2020)
**For the associated papers, see  [http://alum.mit.edu/www/cpbl/publications/2020-PNAS-sprawl](http://alum.mit.edu/www/cpbl/publications/2020-PNAS-sprawl) or:**

 - [https://doi.org/10.1073/pnas.1905232116](https://doi.org/10.1073/pnas.1905232116)
 - [https://doi.org/10.1371/journal.pone.0223078](https://doi.org/10.1371/journal.pone.0223078)

Using this code requires substantial hardware resources, or modification of the code. If you are using a server running the GNU/Linux operating system (e.g. Ubuntu 2017 LTS), feel free to ask us questions about the set-up.


## Download raw data

1. download GADM data version 3.6
1. download GHSL data
1. (optional) download Atlas of Urban Expansion data (200 cities version)
1. download planet-latest.osm.bz2 from OSM, or any smaller region you like. We use Monaco as our smallest test case

## Create and set up server

1. install a postgres server
1. Install other needed software. This includes osm2po, a number of Python 3 packages, and the cpblUtilities repository available at https://gitlab.com/cpbl/cpblUtilities .


## Configure settings

1. Do this:
`cp config-template.cfg config.cfg`
and edit the latter file in order to set up the folders. You may need to examine the code to see which subfolders (in inputData) the downloads, above, should go.
1. Read through the server_setup.py module and run the relevant procedures to set up your server, modifying usernames and paths as needed.

## Begin processing

You may now be ready to look through `osm_master` to get a sense of the steps needed to set up initial "template" database of GADM, raster data, adjacency tables, and so on; to do the main computations; and to produce the analysis and figures from our papers.
Follow the instructions at the top of `osm_master` to proceed.

If you run into any troubles in this setup procedure, please submit improvements to the documentation



